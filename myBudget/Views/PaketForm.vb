﻿Public Class PaketForm
#Region "Events"
    Private Sub PaketForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub PaketForm_Load")

        Try
            If intArt_der_Editierung = 1 Then 'Änderung
                ViewData_Form_Paket(0)
            ElseIf intArt_der_Editierung = 2 Then 'View
                ViewData_Form_Paket(Array.FindIndex(pbl_Paket, Function(f) f.txtPaket = pbl_txtPaket))
            End If
        Catch
            Exit Sub 'Keine Daten zum laden
        End Try

        'Abhängig von der Art der Bearbeitung werden Felder befüllt oder nicht
        If intArt_der_Editierung = 0 Then 'Neuer Eintrag -> keine Daten zum AP vorhanden
            Form_Paket.Text = "Erstellung Paket"
            lblPaket.Visible = False
            ComboPaket.Visible = False
            ComboPaket.Enabled = False
            txtPaket.Enabled = True
            txtBeschreibung.Enabled = True
            cmdSaveData.Visible = True
        ElseIf intArt_der_Editierung = 1 Then 'Bearbeitung
            Form_Paket.Text = "Änderung Projekt"
            lblPaket.Visible = False
            ComboPaket.Visible = True
            ComboPaket.Enabled = True
            txtPaket.Enabled = True
            txtBeschreibung.Enabled = True
            cmdSaveData.Visible = True
        Else 'View
            Form_Paket.Text = "Anzeige Projekt"
            lblPaket.Visible = False
            ComboPaket.Visible = False
            ComboPaket.Enabled = False
            txtPaket.Enabled = False
            txtBeschreibung.Enabled = False
            cmdSaveData.Visible = False
        End If
    End Sub

    Public Sub ComboPaket_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboPaket.SelectedIndexChanged
        Console.WriteLine("EVENT: Public Sub ComboPaket_SelectedIndexChanged")

        Try
            ViewData_Form_Paket(ViewCombo.View_ComboboxPaket(ComboPaket, ComboPaket.SelectedIndex, pbl_Paket))
        Catch
        End Try
    End Sub
#End Region

#Region "Aktionen"
    Private Sub cmdSaveData_Click(sender As Object, e As EventArgs) Handles cmdSaveData.Click
        Console.WriteLine("AKTION: Private Sub cmdSaveData_Click")

        'Prüfen ob eine aktive Verbindung zum Server existiert
        If con.State = ConnectionState.Closed Or boolConnected = False Then
            MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If

        If txtPaket.Text = "" Then
            MsgBox("Bitte geben Sie einen Paketnamen an", MsgBoxStyle.Critical, "Fehler")
            Exit Sub
        End If

        boolError = False

#Region "Paket anlegen"
        If intArt_der_Editierung = 0 Then 'NEUES PAKET ANLEGEN
            '1. Paket anlegen
            '5. Pakete neu laden
            Try
                Add_Paket(txtPaket.Text, txtBeschreibung.Text, pbl_IdAp)
                Get_Paket(True, pbl_IdAp)
                'Daten neu laden
                If pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_IdAp, 1, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
            Catch ex As Exception
                MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Sub 'Paket Anlage fehlgeschlagen
            End Try
#End Region

#Region "Paket ändern"
        Else
            '1. Paket aktualisieren
            '2. DATA aktualisieren
            '3. DATA neu laden und anzeigen
            Try
                Update_Paket(ComboPaket.Text, txtPaket.Text, txtBeschreibung.Text, pbl_IdAp)
                Get_Paket(True, pbl_IdAp)
            Catch ex As Exception
                MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Sub 'Paket Update fehlgeschlagen
            End Try
            Try
                Update_Data_Paket(pbl_IdAp, ComboPaket.Text, txtPaket.Text)
                If pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_IdAp, 1, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
            Catch ex As Exception
                MsgBox("cmdSaveData_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                Exit Sub 'Paket Update fehlgeschlagen
            End Try
#End Region
        End If

        Me.Close()
    End Sub
#End Region

#Region "Methoden"
    Function ViewData_Form_Paket(Index_Paket As Integer)
        Console.WriteLine("FUNC: Function ViewData_Form_Paket")

        ComboPaket.Text = ""
        ComboPaket.Items.Clear()
        txtPaket.Text = ""
        txtBeschreibung.Text = ""

        If Index_Paket < 0 Then Exit Function 'Kein Paket --> keine View oder Eingabe möglich

        'Projekte müssen nicht geladen werden, da diese bei der Useranmeldung gezogen werden, basierend auf Berechtigungen
        Try
            Index_Paket = ViewCombo.View_ComboboxPaket(ComboPaket, Index_Paket, pbl_Paket) 'Projekte anzeigen
            txtPaket.Text = pbl_Paket(Index_Paket).txtPaket
            txtBeschreibung.Text = pbl_Paket(Index_Paket).txtBeschreibung
        Catch
            Exit Function 'Keine Projekte verfügbar
        End Try
    End Function
#End Region
End Class