﻿Public Class UserPasswordForm
#Region "Methoden"
    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Try
            If String.IsNullOrEmpty(txtCurPw.Text) Or String.IsNullOrEmpty(txtNewPw1.Text) Or String.IsNullOrEmpty(txtNewPw1.Text) Then
                MsgBox("Bitte geben Sie das aktuelle und ein neues Password ein!", vbCritical, "Fehler")
                Exit Sub
            End If

            If Not txtCurPw.Text = pbl_dtUser(intCurrentUser).Item("txtPassword") Then
                MsgBox("Das aktuelle Password ist falsch!", vbCritical, "Fehler")
                Exit Sub
            End If

            If Not txtNewPw1.Text = txtNewPw2.Text Then
                MsgBox("Das neue Password und die Bestätigung passen nicht zusammen!", vbCritical, "Fehler")
                Exit Sub
            End If

            If Update_UserPassword(pbl_dtUser(intCurrentUser).Item("txtPNummer"), txtNewPw1.Text) = True Then
                MsgBox("Das Password wurde erfolgreich geändert!", vbInformation, "Fehler")
            Else
                MsgBox("Das Password konnte nicht geändert werden!", vbCritical, "Fehler")
            End If

        Catch ex As Exception
            MsgBox("cmdSave_Click: " & ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region "Events"
    Private Sub UserPasswordForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If ApplicationExit = False Then
            e.Cancel = True
            Me.Hide() 'Form wird nicht geschlossen sondern nur unsichtbar. DAmit kann sie ein zweites mal geöffnet werden
        End If
    End Sub
#End Region
End Class