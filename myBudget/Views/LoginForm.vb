﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Deployment
Imports System.Reflection
Imports myBudget.UI
Imports ADODB
Imports myBudget.Public_Variablen
Imports System.Diagnostics

Public Class LoginForm

#Region "Events"
    Private Sub LoginForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub LoginForm_Load")
        Dim i As Integer

        '-- VERBINDUNGEN
        boolLoggedIn = False
        ApplicationExit = False

        ReDim listConnections(2)
        listConnections(0).Path = "Provider=SQLNCLI11;Server=dewelmspdb001.msp.emea.porsche.biz;Database=mybudget;Uid=mybudget;Pwd=75vm@NzmLt2wzS@g;"
        listConnections(0).System = "Live - EM" 'P01
        listConnections(0).ServerType = "SQL"

        listConnections(1).Path = "Provider=SQLNCLI11;Server=dewelmspdb001.msp.emea.porsche.biz;Database=mybudget_test;Uid=mybudget;Pwd=75vm@NzmLt2wzS@g;"
        listConnections(1).System = "Spielwiese (Testsystem)" 'K01
        listConnections(1).ServerType = "SQL"

        listConnections(2).Path = "Provider=SQLNCLI11;Server=" & System.Environment.MachineName & "\SQLEXPRESS01;Database=mybudget;Integrated Security=SSPI;"
        listConnections(2).System = "Local (Admin only)" 'K01
        listConnections(2).ServerType = "SQL"
        '--

        If IsNothing(listConnections) Then
            pbl_SelectedConnection = -1
        ElseIf UBound(listConnections) = 0 Then 'Nur Verbindung zu einem Server möglich
            pbl_SelectedConnection = 0
        Else 'User kann Server auswählen
            Form_SelectConnection.ShowDialog()
        End If

        '--

        pbl_dtSAPCon = Set_SAP_Connection("P01") 'Default: SAP P01 

        If pbl_SelectedConnection = -1 Then
            MsgBox("Es konnte keine Verbindung zum Server hergestellt werden..." & vbCrLf & "Bitte kontaktieren Sie Ihren Systemadministrator.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Me.Close()
        End If 'YYY

        Try
            Connect_DB(pbl_SelectedConnection) 'Verbindung zur Access Datenbank herstellen
            If boolConnected = True Then

#Region "check for myBudget version"
                If GotRightmyBudgetVersion() = False Then Exit Sub
#End Region

                pbl_dtUser = Get_tblUser(True) 'X Liste aller User vom Server laden
                Me.comboUser.Items.Clear()
                If pbl_dtUser.Rows.Count = 0 Then
                    MsgBox("Keine Daten auf der Datenbank vorhanden", vbCritical, "Fehler")
                    Exit Sub
                ElseIf boolLoggedIn = False Then
                    For i = 0 To pbl_dtUser.Rows.Count - 1
                        If Not IsNothing(pbl_dtUser.Rows(i).Item("txtPNummer")) OrElse Not String.IsNullOrEmpty(pbl_dtUser.Rows(i).Item("txtPNummer")) Then 'X
                            Me.comboUser.Items.Add(pbl_dtUser.Rows(i).Item("txtPNummer"))
                        End If
                    Next
                End If
            Else
                MsgBox("Es konnte keine Verbindung zum Server hergestellt werden..." & vbCrLf & "Bitte kontaktieren Sie Ihren Systemadministrator.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Leider ist ein schwerwiegender Softwarefehler aufgetreten. Das System wurde unerwartet beendet!" & vbCrLf & "Bitte wenden Sie sich an den Administrator" & vbCrLf & "Fehler:" & vbCrLf & String.Format("Error: {0}", ex.Message), Title:="Softwarefehler", Buttons:=MsgBoxStyle.Critical)
            Me.Close()
        End Try
    End Sub

    Private Sub LoginForm_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Console.WriteLine("EVENT: Private Sub LoginForm_FormClosed")

        Close_DB() 'Verbindung zum Server schließen
    End Sub
#End Region

#Region "Aktionen"
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click
        Console.WriteLine("AKTION: Private Sub OK_Click")

        Dim i As Integer
        Dim boolUserFound As Boolean
        boolUserFound = False

        If pbl_dtUser.Rows.Count > 0 Then
            For i = 0 To pbl_dtUser.Rows.Count - 1
                If pbl_dtUser.Rows(i).Item("txtPNummer") = comboUser.Text Then
                    boolUserFound = True
                    Exit For
                End If
            Next
        End If
        If boolUserFound = False Then
            MsgBox("Der eingegebene User existiert nicht", Title:="Fehler beim User", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If

        If txtPassword.Text = pbl_dtUser.Rows(i).Item("txtPassword") Then
            intCurrentUser = i
            boolLoggedIn = True

            '>
            Try
                If Not String.IsNullOrEmpty(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")) And Not String.IsNullOrEmpty(pbl_dtUser.Rows(intCurrentUser).Item("txtName")) And Not String.IsNullOrEmpty(pbl_dtUser.Rows(intCurrentUser).Item("txtVorname")) Then
                    Add_Log(Now, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtUser.Rows(intCurrentUser).Item("txtName"), pbl_dtUser.Rows(intCurrentUser).Item("txtVorname"), "Login")
                End If
            Catch
            End Try
            '<

            Read_all_Data() 'Alle wichtigen Daten fürs Hauptmenü laden

            Me.Hide() 'Verbirgt das Login Fenster

            'Hauptmenü/Form_Edit laden
            Try
                Form_Edit.ShowDialog()
                Me.Close() 'Sobald Edit Form geschlossen wird, wird die Anwendung beendet
            Catch
                Try 'Usersettings speichern
                    Dim TextGj As String = ConvertIntegerArrayToString(pbl_LstSelGj)
                    Update_User(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_IdProjekt, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, TextGj)
                Catch ex As Exception
                End Try
                '>
                Try
                    Add_Log(Now, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtUser.Rows(intCurrentUser).Item("txtName"), pbl_dtUser.Rows(intCurrentUser).Item("txtVorname"), "Logout")
                Catch
                End Try
                '<
                Close_DB()
                boolLoggedIn = False

                MsgBox("Es ist ein schwerwiegender Fehler aufgetreten. Die Software wird beendet.", Title:="ERROR", Buttons:=MsgBoxStyle.Critical)

                'Dim Form_Edit As New EditForm
                'Form_Edit.ShowDialog()

                Me.Close() 'Sobald Edit Form geschlossen wird, wird die Anwendung beendet
            End Try
        Else
            MsgBox("Das eingegebene Password ist falsch", Title:="Fehler beim Password", Buttons:=MsgBoxStyle.Critical)
        End If

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Console.WriteLine("AKTION: Private Sub Cancel_Click")

        Close_DB() 'Verbindung zum Server schließen

        Me.Close()
    End Sub
#End Region

End Class
