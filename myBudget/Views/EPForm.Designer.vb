﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EPForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EPForm))
        Me.ComboProjekt = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBeschreibung = New System.Windows.Forms.TextBox()
        Me.ComboEntwicklungspaket = New System.Windows.Forms.ComboBox()
        Me.lblEntwicklungspaket = New System.Windows.Forms.Label()
        Me.txtEntwicklungspaket = New System.Windows.Forms.TextBox()
        Me.lblep = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ComboProjekt
        '
        Me.ComboProjekt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboProjekt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboProjekt.FormattingEnabled = True
        Me.ComboProjekt.Location = New System.Drawing.Point(13, 39)
        Me.ComboProjekt.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboProjekt.Name = "ComboProjekt"
        Me.ComboProjekt.Size = New System.Drawing.Size(379, 24)
        Me.ComboProjekt.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkRed
        Me.Label6.Location = New System.Drawing.Point(9, 19)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 16)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Projekte"
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.DarkRed
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(292, 341)
        Me.cmdSaveData.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 29
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(9, 199)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 16)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Beschreibung"
        '
        'txtBeschreibung
        '
        Me.txtBeschreibung.Location = New System.Drawing.Point(13, 218)
        Me.txtBeschreibung.Multiline = True
        Me.txtBeschreibung.Name = "txtBeschreibung"
        Me.txtBeschreibung.Size = New System.Drawing.Size(379, 116)
        Me.txtBeschreibung.TabIndex = 3
        '
        'ComboEntwicklungspaket
        '
        Me.ComboEntwicklungspaket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboEntwicklungspaket.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboEntwicklungspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboEntwicklungspaket.FormattingEnabled = True
        Me.ComboEntwicklungspaket.Location = New System.Drawing.Point(13, 97)
        Me.ComboEntwicklungspaket.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboEntwicklungspaket.Name = "ComboEntwicklungspaket"
        Me.ComboEntwicklungspaket.Size = New System.Drawing.Size(379, 24)
        Me.ComboEntwicklungspaket.TabIndex = 1
        '
        'lblEntwicklungspaket
        '
        Me.lblEntwicklungspaket.AutoSize = True
        Me.lblEntwicklungspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntwicklungspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblEntwicklungspaket.Location = New System.Drawing.Point(9, 77)
        Me.lblEntwicklungspaket.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEntwicklungspaket.Name = "lblEntwicklungspaket"
        Me.lblEntwicklungspaket.Size = New System.Drawing.Size(136, 16)
        Me.lblEntwicklungspaket.TabIndex = 33
        Me.lblEntwicklungspaket.Text = "Entwicklungspaket"
        '
        'txtEntwicklungspaket
        '
        Me.txtEntwicklungspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntwicklungspaket.Location = New System.Drawing.Point(13, 156)
        Me.txtEntwicklungspaket.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEntwicklungspaket.Name = "txtEntwicklungspaket"
        Me.txtEntwicklungspaket.Size = New System.Drawing.Size(379, 22)
        Me.txtEntwicklungspaket.TabIndex = 2
        '
        'lblep
        '
        Me.lblep.AutoSize = True
        Me.lblep.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblep.ForeColor = System.Drawing.Color.DarkRed
        Me.lblep.Location = New System.Drawing.Point(9, 135)
        Me.lblep.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblep.Name = "lblep"
        Me.lblep.Size = New System.Drawing.Size(136, 16)
        Me.lblep.TabIndex = 34
        Me.lblep.Text = "Entwicklungspaket"
        '
        'EPForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(402, 386)
        Me.Controls.Add(Me.txtEntwicklungspaket)
        Me.Controls.Add(Me.lblep)
        Me.Controls.Add(Me.ComboEntwicklungspaket)
        Me.Controls.Add(Me.lblEntwicklungspaket)
        Me.Controls.Add(Me.txtBeschreibung)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.ComboProjekt)
        Me.Controls.Add(Me.Label6)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EPForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entwicklungspaket"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ComboProjekt As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtBeschreibung As System.Windows.Forms.TextBox
    Friend WithEvents ComboEntwicklungspaket As System.Windows.Forms.ComboBox
    Friend WithEvents lblEntwicklungspaket As System.Windows.Forms.Label
    Friend WithEvents txtEntwicklungspaket As System.Windows.Forms.TextBox
    Friend WithEvents lblep As System.Windows.Forms.Label
End Class
