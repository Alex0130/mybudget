﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EditForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditForm))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusDatabase = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusOperation = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.MenuReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuDatenMitVorgängerversionVergleichen = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuN_Compare = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuGetDataViaSqlStatement = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuNewReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.DruckenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuAktionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuMehrereArbeitspaketeBearbeiten = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.KostenLinearVerteilenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuProzentualeAnpassung = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuIVP = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuEKantErstellen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.VersionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuRelease = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuNewVersion = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuChangeVersion = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PaketToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuNewPaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuChangePaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntwicklungspaketToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuNewEntwicklungspaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuChangeEntwicklungspaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ArbeitspaketToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuNewArbeitspaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuChangeArbeitspaket = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuDeleteAP = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProjektToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuNewProjekt = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuServer = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerbindungHerstellenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerbindungTrennenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.DatenAktualisierenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.SAPImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuSapSystem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LiveSAPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TerminverfolgungToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HilfeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuSachkonten = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuEkant = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.MenuHandbuch = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuVerwaltung = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuPundAp = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuPasswordÄndern = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuBenachrichtigungsEmail = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComboProjekt = New System.Windows.Forms.ComboBox()
        Me.ComboArbeitspaket = New System.Windows.Forms.ComboBox()
        Me.ComboVersion = New System.Windows.Forms.ComboBox()
        Me.lblProjekt = New System.Windows.Forms.Label()
        Me.lblArbeitspaket = New System.Windows.Forms.Label()
        Me.lblVers = New System.Windows.Forms.Label()
        Me.lblGJ = New System.Windows.Forms.Label()
        Me.ComboEntwurf = New System.Windows.Forms.ComboBox()
        Me.lblEntwurfstyp = New System.Windows.Forms.Label()
        Me.Line2 = New System.Windows.Forms.PictureBox()
        Me.Line1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.TimerEditForm = New System.Windows.Forms.Timer(Me.components)
        Me.lblBearbeitung = New System.Windows.Forms.Label()
        Me.cmdArbeitspaket = New System.Windows.Forms.Button()
        Me.cmdVersion = New System.Windows.Forms.Button()
        Me.ContextMouse = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ContextKopieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextEinfügen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContextLöschen = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.MarkierenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContextMenuCopyToCache = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddRow = New System.Windows.Forms.PictureBox()
        Me.ToolTipAddRow = New System.Windows.Forms.ToolTip(Me.components)
        Me.SaveData = New System.Windows.Forms.PictureBox()
        Me.ToolTipSaveData = New System.Windows.Forms.ToolTip(Me.components)
        Me.SplitAddRow = New System.Windows.Forms.PictureBox()
        Me.VersionDescription = New System.Windows.Forms.TextBox()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.BorderData = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.OvalFalse = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.OvalTrue = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.RectangleCompare = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.lblCompare = New System.Windows.Forms.Label()
        Me.cmdHideFilter = New System.Windows.Forms.PictureBox()
        Me.cmdShowFilter = New System.Windows.Forms.PictureBox()
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.lblCurrentArbeitspaket = New System.Windows.Forms.Label()
        Me.iGrid = New TenTec.Windows.iGridLib.iGrid()
        Me.iGridAutoFilter = New TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager()
        Me.iGridDropDown_GJ = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.iGridDropDown_Entwicklungspaket = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.iGridDropDown_Paket = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.iGridDropDown_Kostenart = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.ILKostenart = New System.Windows.Forms.ImageList(Me.components)
        Me.iGridDropDown_Klasse = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.IGPrintManager1 = New TenTec.Windows.iGridLib.Printing.iGPrintManager()
        Me.LineHideShow = New System.Windows.Forms.PictureBox()
        Me.ExcelExport = New System.Windows.Forms.PictureBox()
        Me.FooterCalcMethod = New System.Windows.Forms.PictureBox()
        Me.ToolTipFooterCalcMethod = New System.Windows.Forms.ToolTip(Me.components)
        Me.ILFooter = New System.Windows.Forms.ImageList(Me.components)
        Me.SplitExcel = New System.Windows.Forms.PictureBox()
        Me.SplitGetSap = New System.Windows.Forms.PictureBox()
        Me.cmdGetSap = New System.Windows.Forms.PictureBox()
        Me.ToolTipGetSapData = New System.Windows.Forms.ToolTip(Me.components)
        Me.ShowMultiData = New System.Windows.Forms.PictureBox()
        Me.SplitMultiData = New System.Windows.Forms.PictureBox()
        Me.ToolTipMultiData = New System.Windows.Forms.ToolTip(Me.components)
        Me.CheckedListGJ = New System.Windows.Forms.CheckedListBox()
        Me.SplitEmail = New System.Windows.Forms.PictureBox()
        Me.EmailNotificationState = New System.Windows.Forms.PictureBox()
        Me.lblVersion = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyrightMHoffmannToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip.SuspendLayout()
        Me.MenuStrip.SuspendLayout()
        CType(Me.Line2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMouse.SuspendLayout()
        CType(Me.AddRow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitAddRow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdHideFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdShowFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.iGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LineHideShow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FooterCalcMethod, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitExcel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitGetSap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdGetSap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ShowMultiData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitMultiData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmailNotificationState, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusDatabase, Me.ToolStripStatusOperation})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 501)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1283, 22)
        Me.StatusStrip.TabIndex = 0
        Me.StatusStrip.Text = "Status"
        '
        'ToolStripStatusDatabase
        '
        Me.ToolStripStatusDatabase.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusDatabase.Name = "ToolStripStatusDatabase"
        Me.ToolStripStatusDatabase.Size = New System.Drawing.Size(43, 17)
        Me.ToolStripStatusDatabase.Text = "Offline"
        '
        'ToolStripStatusOperation
        '
        Me.ToolStripStatusOperation.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusOperation.Name = "ToolStripStatusOperation"
        Me.ToolStripStatusOperation.Size = New System.Drawing.Size(17, 17)
        Me.ToolStripStatusOperation.Text = "--"
        '
        'MenuStrip
        '
        Me.MenuStrip.BackColor = System.Drawing.Color.WhiteSmoke
        Me.MenuStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip.ImageScalingSize = New System.Drawing.Size(25, 25)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuReport, Me.MenuAktionen, Me.MenuServer, Me.TerminverfolgungToolStripMenuItem, Me.HilfeToolStripMenuItem, Me.MenuVerwaltung, Me.lblVersion, Me.CopyrightMHoffmannToolStripMenuItem})
        Me.MenuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(1283, 33)
        Me.MenuStrip.TabIndex = 8
        Me.MenuStrip.Text = "MenuStrip1"
        '
        'MenuReport
        '
        Me.MenuReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuDatenMitVorgängerversionVergleichen, Me.MenuN_Compare, Me.MenuGetDataViaSqlStatement, Me.ToolStripSeparator6, Me.MenuNewReport, Me.ToolStripSeparator7, Me.DruckenToolStripMenuItem})
        Me.MenuReport.Image = CType(resources.GetObject("MenuReport.Image"), System.Drawing.Image)
        Me.MenuReport.Name = "MenuReport"
        Me.MenuReport.Size = New System.Drawing.Size(85, 29)
        Me.MenuReport.Text = "Report"
        '
        'MenuDatenMitVorgängerversionVergleichen
        '
        Me.MenuDatenMitVorgängerversionVergleichen.Image = CType(resources.GetObject("MenuDatenMitVorgängerversionVergleichen.Image"), System.Drawing.Image)
        Me.MenuDatenMitVorgängerversionVergleichen.Name = "MenuDatenMitVorgängerversionVergleichen"
        Me.MenuDatenMitVorgängerversionVergleichen.Size = New System.Drawing.Size(297, 32)
        Me.MenuDatenMitVorgängerversionVergleichen.Text = "Versionen vergleichen"
        '
        'MenuN_Compare
        '
        Me.MenuN_Compare.Image = CType(resources.GetObject("MenuN_Compare.Image"), System.Drawing.Image)
        Me.MenuN_Compare.Name = "MenuN_Compare"
        Me.MenuN_Compare.Size = New System.Drawing.Size(297, 32)
        Me.MenuN_Compare.Text = "n Arbeitspakete im Vergleichsmodus"
        '
        'MenuGetDataViaSqlStatement
        '
        Me.MenuGetDataViaSqlStatement.Image = CType(resources.GetObject("MenuGetDataViaSqlStatement.Image"), System.Drawing.Image)
        Me.MenuGetDataViaSqlStatement.Name = "MenuGetDataViaSqlStatement"
        Me.MenuGetDataViaSqlStatement.Size = New System.Drawing.Size(297, 32)
        Me.MenuGetDataViaSqlStatement.Text = "Datenbank Abfrage"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(294, 6)
        '
        'MenuNewReport
        '
        Me.MenuNewReport.Image = CType(resources.GetObject("MenuNewReport.Image"), System.Drawing.Image)
        Me.MenuNewReport.Name = "MenuNewReport"
        Me.MenuNewReport.Size = New System.Drawing.Size(297, 32)
        Me.MenuNewReport.Text = "Excel Export"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(294, 6)
        '
        'DruckenToolStripMenuItem
        '
        Me.DruckenToolStripMenuItem.Image = CType(resources.GetObject("DruckenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DruckenToolStripMenuItem.Name = "DruckenToolStripMenuItem"
        Me.DruckenToolStripMenuItem.Size = New System.Drawing.Size(297, 32)
        Me.DruckenToolStripMenuItem.Text = "Drucken"
        '
        'MenuAktionen
        '
        Me.MenuAktionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuMehrereArbeitspaketeBearbeiten, Me.ToolStripSeparator14, Me.MenuCopy, Me.KostenLinearVerteilenToolStripMenuItem, Me.MenuProzentualeAnpassung, Me.MenuIVP, Me.ToolStripSeparator13, Me.MenuEKantErstellen, Me.ToolStripSeparator3, Me.VersionToolStripMenuItem, Me.ToolStripSeparator4, Me.PaketToolStripMenuItem1, Me.EntwicklungspaketToolStripMenuItem1, Me.ToolStripSeparator5, Me.ArbeitspaketToolStripMenuItem1, Me.ProjektToolStripMenuItem1})
        Me.MenuAktionen.Image = CType(resources.GetObject("MenuAktionen.Image"), System.Drawing.Image)
        Me.MenuAktionen.Name = "MenuAktionen"
        Me.MenuAktionen.Size = New System.Drawing.Size(107, 29)
        Me.MenuAktionen.Text = "Bearbeiten"
        '
        'MenuMehrereArbeitspaketeBearbeiten
        '
        Me.MenuMehrereArbeitspaketeBearbeiten.Image = CType(resources.GetObject("MenuMehrereArbeitspaketeBearbeiten.Image"), System.Drawing.Image)
        Me.MenuMehrereArbeitspaketeBearbeiten.Name = "MenuMehrereArbeitspaketeBearbeiten"
        Me.MenuMehrereArbeitspaketeBearbeiten.Size = New System.Drawing.Size(232, 32)
        Me.MenuMehrereArbeitspaketeBearbeiten.Text = "n Arbeitspakete anzeigen"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(229, 6)
        '
        'MenuCopy
        '
        Me.MenuCopy.Image = CType(resources.GetObject("MenuCopy.Image"), System.Drawing.Image)
        Me.MenuCopy.Name = "MenuCopy"
        Me.MenuCopy.Size = New System.Drawing.Size(232, 32)
        Me.MenuCopy.Text = "Daten Kopieren"
        '
        'KostenLinearVerteilenToolStripMenuItem
        '
        Me.KostenLinearVerteilenToolStripMenuItem.Image = CType(resources.GetObject("KostenLinearVerteilenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.KostenLinearVerteilenToolStripMenuItem.Name = "KostenLinearVerteilenToolStripMenuItem"
        Me.KostenLinearVerteilenToolStripMenuItem.Size = New System.Drawing.Size(232, 32)
        Me.KostenLinearVerteilenToolStripMenuItem.Text = "Kosten linear verteilen"
        '
        'MenuProzentualeAnpassung
        '
        Me.MenuProzentualeAnpassung.Image = CType(resources.GetObject("MenuProzentualeAnpassung.Image"), System.Drawing.Image)
        Me.MenuProzentualeAnpassung.Name = "MenuProzentualeAnpassung"
        Me.MenuProzentualeAnpassung.Size = New System.Drawing.Size(232, 32)
        Me.MenuProzentualeAnpassung.Text = "Prozentuale Anpassung"
        '
        'MenuIVP
        '
        Me.MenuIVP.Image = CType(resources.GetObject("MenuIVP.Image"), System.Drawing.Image)
        Me.MenuIVP.Name = "MenuIVP"
        Me.MenuIVP.Size = New System.Drawing.Size(232, 32)
        Me.MenuIVP.Text = "Automatischer IVP"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(229, 6)
        '
        'MenuEKantErstellen
        '
        Me.MenuEKantErstellen.Image = CType(resources.GetObject("MenuEKantErstellen.Image"), System.Drawing.Image)
        Me.MenuEKantErstellen.Name = "MenuEKantErstellen"
        Me.MenuEKantErstellen.Size = New System.Drawing.Size(232, 32)
        Me.MenuEKantErstellen.Text = "EKant erstellen"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(229, 6)
        '
        'VersionToolStripMenuItem
        '
        Me.VersionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuRelease, Me.MenuNewVersion, Me.MenuChangeVersion})
        Me.VersionToolStripMenuItem.Image = CType(resources.GetObject("VersionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VersionToolStripMenuItem.Name = "VersionToolStripMenuItem"
        Me.VersionToolStripMenuItem.Size = New System.Drawing.Size(232, 32)
        Me.VersionToolStripMenuItem.Text = "Version"
        '
        'MenuRelease
        '
        Me.MenuRelease.Image = CType(resources.GetObject("MenuRelease.Image"), System.Drawing.Image)
        Me.MenuRelease.Name = "MenuRelease"
        Me.MenuRelease.Size = New System.Drawing.Size(142, 32)
        Me.MenuRelease.Text = "Freigabe"
        '
        'MenuNewVersion
        '
        Me.MenuNewVersion.Image = CType(resources.GetObject("MenuNewVersion.Image"), System.Drawing.Image)
        Me.MenuNewVersion.Name = "MenuNewVersion"
        Me.MenuNewVersion.Size = New System.Drawing.Size(142, 32)
        Me.MenuNewVersion.Text = "Neu"
        '
        'MenuChangeVersion
        '
        Me.MenuChangeVersion.Image = CType(resources.GetObject("MenuChangeVersion.Image"), System.Drawing.Image)
        Me.MenuChangeVersion.Name = "MenuChangeVersion"
        Me.MenuChangeVersion.Size = New System.Drawing.Size(142, 32)
        Me.MenuChangeVersion.Text = "Änderung"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(229, 6)
        '
        'PaketToolStripMenuItem1
        '
        Me.PaketToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuNewPaket, Me.MenuChangePaket})
        Me.PaketToolStripMenuItem1.Image = CType(resources.GetObject("PaketToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.PaketToolStripMenuItem1.Name = "PaketToolStripMenuItem1"
        Me.PaketToolStripMenuItem1.Size = New System.Drawing.Size(232, 32)
        Me.PaketToolStripMenuItem1.Text = "Paket"
        '
        'MenuNewPaket
        '
        Me.MenuNewPaket.Image = CType(resources.GetObject("MenuNewPaket.Image"), System.Drawing.Image)
        Me.MenuNewPaket.Name = "MenuNewPaket"
        Me.MenuNewPaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuNewPaket.Text = "Neu"
        '
        'MenuChangePaket
        '
        Me.MenuChangePaket.Image = CType(resources.GetObject("MenuChangePaket.Image"), System.Drawing.Image)
        Me.MenuChangePaket.Name = "MenuChangePaket"
        Me.MenuChangePaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuChangePaket.Text = "Änderung"
        '
        'EntwicklungspaketToolStripMenuItem1
        '
        Me.EntwicklungspaketToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuNewEntwicklungspaket, Me.MenuChangeEntwicklungspaket})
        Me.EntwicklungspaketToolStripMenuItem1.Image = CType(resources.GetObject("EntwicklungspaketToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.EntwicklungspaketToolStripMenuItem1.Name = "EntwicklungspaketToolStripMenuItem1"
        Me.EntwicklungspaketToolStripMenuItem1.Size = New System.Drawing.Size(232, 32)
        Me.EntwicklungspaketToolStripMenuItem1.Text = "Entwicklungspaket"
        '
        'MenuNewEntwicklungspaket
        '
        Me.MenuNewEntwicklungspaket.Image = CType(resources.GetObject("MenuNewEntwicklungspaket.Image"), System.Drawing.Image)
        Me.MenuNewEntwicklungspaket.Name = "MenuNewEntwicklungspaket"
        Me.MenuNewEntwicklungspaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuNewEntwicklungspaket.Text = "Neu"
        '
        'MenuChangeEntwicklungspaket
        '
        Me.MenuChangeEntwicklungspaket.Image = CType(resources.GetObject("MenuChangeEntwicklungspaket.Image"), System.Drawing.Image)
        Me.MenuChangeEntwicklungspaket.Name = "MenuChangeEntwicklungspaket"
        Me.MenuChangeEntwicklungspaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuChangeEntwicklungspaket.Text = "Änderung"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(229, 6)
        '
        'ArbeitspaketToolStripMenuItem1
        '
        Me.ArbeitspaketToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuNewArbeitspaket, Me.MenuChangeArbeitspaket, Me.MenuDeleteAP})
        Me.ArbeitspaketToolStripMenuItem1.Image = CType(resources.GetObject("ArbeitspaketToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ArbeitspaketToolStripMenuItem1.Name = "ArbeitspaketToolStripMenuItem1"
        Me.ArbeitspaketToolStripMenuItem1.Size = New System.Drawing.Size(232, 32)
        Me.ArbeitspaketToolStripMenuItem1.Text = "Arbeitspaket"
        '
        'MenuNewArbeitspaket
        '
        Me.MenuNewArbeitspaket.Image = CType(resources.GetObject("MenuNewArbeitspaket.Image"), System.Drawing.Image)
        Me.MenuNewArbeitspaket.Name = "MenuNewArbeitspaket"
        Me.MenuNewArbeitspaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuNewArbeitspaket.Text = "Neu"
        '
        'MenuChangeArbeitspaket
        '
        Me.MenuChangeArbeitspaket.Image = CType(resources.GetObject("MenuChangeArbeitspaket.Image"), System.Drawing.Image)
        Me.MenuChangeArbeitspaket.Name = "MenuChangeArbeitspaket"
        Me.MenuChangeArbeitspaket.Size = New System.Drawing.Size(142, 32)
        Me.MenuChangeArbeitspaket.Text = "Änderung"
        '
        'MenuDeleteAP
        '
        Me.MenuDeleteAP.Image = CType(resources.GetObject("MenuDeleteAP.Image"), System.Drawing.Image)
        Me.MenuDeleteAP.Name = "MenuDeleteAP"
        Me.MenuDeleteAP.Size = New System.Drawing.Size(142, 32)
        Me.MenuDeleteAP.Text = "Löschen"
        '
        'ProjektToolStripMenuItem1
        '
        Me.ProjektToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuNewProjekt})
        Me.ProjektToolStripMenuItem1.Image = CType(resources.GetObject("ProjektToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ProjektToolStripMenuItem1.Name = "ProjektToolStripMenuItem1"
        Me.ProjektToolStripMenuItem1.Size = New System.Drawing.Size(232, 32)
        Me.ProjektToolStripMenuItem1.Text = "Projekt"
        '
        'MenuNewProjekt
        '
        Me.MenuNewProjekt.Image = CType(resources.GetObject("MenuNewProjekt.Image"), System.Drawing.Image)
        Me.MenuNewProjekt.Name = "MenuNewProjekt"
        Me.MenuNewProjekt.Size = New System.Drawing.Size(109, 32)
        Me.MenuNewProjekt.Text = "Neu"
        '
        'MenuServer
        '
        Me.MenuServer.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerbindungHerstellenToolStripMenuItem, Me.VerbindungTrennenToolStripMenuItem, Me.ToolStripSeparator8, Me.DatenAktualisierenToolStripMenuItem, Me.ToolStripSeparator9, Me.SAPImportToolStripMenuItem, Me.MenuSapSystem, Me.LiveSAPToolStripMenuItem})
        Me.MenuServer.Image = CType(resources.GetObject("MenuServer.Image"), System.Drawing.Image)
        Me.MenuServer.Name = "MenuServer"
        Me.MenuServer.Size = New System.Drawing.Size(82, 29)
        Me.MenuServer.Text = "Server"
        '
        'VerbindungHerstellenToolStripMenuItem
        '
        Me.VerbindungHerstellenToolStripMenuItem.Image = CType(resources.GetObject("VerbindungHerstellenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VerbindungHerstellenToolStripMenuItem.Name = "VerbindungHerstellenToolStripMenuItem"
        Me.VerbindungHerstellenToolStripMenuItem.Size = New System.Drawing.Size(236, 32)
        Me.VerbindungHerstellenToolStripMenuItem.Text = "Verbindung herstellen"
        '
        'VerbindungTrennenToolStripMenuItem
        '
        Me.VerbindungTrennenToolStripMenuItem.Image = CType(resources.GetObject("VerbindungTrennenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VerbindungTrennenToolStripMenuItem.Name = "VerbindungTrennenToolStripMenuItem"
        Me.VerbindungTrennenToolStripMenuItem.Size = New System.Drawing.Size(236, 32)
        Me.VerbindungTrennenToolStripMenuItem.Text = "Verbindung trennen"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(233, 6)
        '
        'DatenAktualisierenToolStripMenuItem
        '
        Me.DatenAktualisierenToolStripMenuItem.Image = CType(resources.GetObject("DatenAktualisierenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatenAktualisierenToolStripMenuItem.Name = "DatenAktualisierenToolStripMenuItem"
        Me.DatenAktualisierenToolStripMenuItem.Size = New System.Drawing.Size(236, 32)
        Me.DatenAktualisierenToolStripMenuItem.Text = "Datentabelle aktualisieren"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(233, 6)
        '
        'SAPImportToolStripMenuItem
        '
        Me.SAPImportToolStripMenuItem.Image = CType(resources.GetObject("SAPImportToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SAPImportToolStripMenuItem.Name = "SAPImportToolStripMenuItem"
        Me.SAPImportToolStripMenuItem.Size = New System.Drawing.Size(236, 32)
        Me.SAPImportToolStripMenuItem.Text = "SAP Import"
        '
        'MenuSapSystem
        '
        Me.MenuSapSystem.Image = CType(resources.GetObject("MenuSapSystem.Image"), System.Drawing.Image)
        Me.MenuSapSystem.Name = "MenuSapSystem"
        Me.MenuSapSystem.Size = New System.Drawing.Size(236, 32)
        Me.MenuSapSystem.Text = "SAP System: P01"
        '
        'LiveSAPToolStripMenuItem
        '
        Me.LiveSAPToolStripMenuItem.Image = CType(resources.GetObject("LiveSAPToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LiveSAPToolStripMenuItem.Name = "LiveSAPToolStripMenuItem"
        Me.LiveSAPToolStripMenuItem.Size = New System.Drawing.Size(236, 32)
        Me.LiveSAPToolStripMenuItem.Text = "Download SAP Ist+Obligo"
        '
        'TerminverfolgungToolStripMenuItem
        '
        Me.TerminverfolgungToolStripMenuItem.Image = CType(resources.GetObject("TerminverfolgungToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TerminverfolgungToolStripMenuItem.Name = "TerminverfolgungToolStripMenuItem"
        Me.TerminverfolgungToolStripMenuItem.Size = New System.Drawing.Size(147, 29)
        Me.TerminverfolgungToolStripMenuItem.Text = "Terminverfolgung"
        '
        'HilfeToolStripMenuItem
        '
        Me.HilfeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuSachkonten, Me.MenuEkant, Me.ToolStripSeparator10, Me.MenuHandbuch})
        Me.HilfeToolStripMenuItem.Image = CType(resources.GetObject("HilfeToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HilfeToolStripMenuItem.Name = "HilfeToolStripMenuItem"
        Me.HilfeToolStripMenuItem.Size = New System.Drawing.Size(71, 29)
        Me.HilfeToolStripMenuItem.Text = "Hilfe"
        '
        'MenuSachkonten
        '
        Me.MenuSachkonten.Image = CType(resources.GetObject("MenuSachkonten.Image"), System.Drawing.Image)
        Me.MenuSachkonten.Name = "MenuSachkonten"
        Me.MenuSachkonten.Size = New System.Drawing.Size(153, 32)
        Me.MenuSachkonten.Text = "Sachkonten"
        '
        'MenuEkant
        '
        Me.MenuEkant.Image = CType(resources.GetObject("MenuEkant.Image"), System.Drawing.Image)
        Me.MenuEkant.Name = "MenuEkant"
        Me.MenuEkant.Size = New System.Drawing.Size(153, 32)
        Me.MenuEkant.Text = "Porsche IVP"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(150, 6)
        '
        'MenuHandbuch
        '
        Me.MenuHandbuch.Image = CType(resources.GetObject("MenuHandbuch.Image"), System.Drawing.Image)
        Me.MenuHandbuch.Name = "MenuHandbuch"
        Me.MenuHandbuch.Size = New System.Drawing.Size(153, 32)
        Me.MenuHandbuch.Text = "Handbuch"
        '
        'MenuVerwaltung
        '
        Me.MenuVerwaltung.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuUser})
        Me.MenuVerwaltung.Image = CType(resources.GetObject("MenuVerwaltung.Image"), System.Drawing.Image)
        Me.MenuVerwaltung.Name = "MenuVerwaltung"
        Me.MenuVerwaltung.Size = New System.Drawing.Size(109, 29)
        Me.MenuVerwaltung.Text = "Verwaltung"
        '
        'MenuUser
        '
        Me.MenuUser.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuPundAp, Me.MenuPasswordÄndern, Me.MenuBenachrichtigungsEmail})
        Me.MenuUser.Image = CType(resources.GetObject("MenuUser.Image"), System.Drawing.Image)
        Me.MenuUser.Name = "MenuUser"
        Me.MenuUser.Size = New System.Drawing.Size(112, 32)
        Me.MenuUser.Text = "User"
        '
        'MenuPundAp
        '
        Me.MenuPundAp.Image = CType(resources.GetObject("MenuPundAp.Image"), System.Drawing.Image)
        Me.MenuPundAp.Name = "MenuPundAp"
        Me.MenuPundAp.Size = New System.Drawing.Size(322, 32)
        Me.MenuPundAp.Text = "Projekt und Arbeitspaket Berechtigungen"
        '
        'MenuPasswordÄndern
        '
        Me.MenuPasswordÄndern.Image = CType(resources.GetObject("MenuPasswordÄndern.Image"), System.Drawing.Image)
        Me.MenuPasswordÄndern.Name = "MenuPasswordÄndern"
        Me.MenuPasswordÄndern.Size = New System.Drawing.Size(322, 32)
        Me.MenuPasswordÄndern.Text = "Password ändern"
        '
        'MenuBenachrichtigungsEmail
        '
        Me.MenuBenachrichtigungsEmail.Image = CType(resources.GetObject("MenuBenachrichtigungsEmail.Image"), System.Drawing.Image)
        Me.MenuBenachrichtigungsEmail.Name = "MenuBenachrichtigungsEmail"
        Me.MenuBenachrichtigungsEmail.Size = New System.Drawing.Size(322, 32)
        Me.MenuBenachrichtigungsEmail.Text = "Benachrichtigungs-Email"
        '
        'ComboProjekt
        '
        Me.ComboProjekt.BackColor = System.Drawing.Color.White
        Me.ComboProjekt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboProjekt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboProjekt.ForeColor = System.Drawing.Color.Black
        Me.ComboProjekt.FormattingEnabled = True
        Me.ComboProjekt.Location = New System.Drawing.Point(122, 39)
        Me.ComboProjekt.Name = "ComboProjekt"
        Me.ComboProjekt.Size = New System.Drawing.Size(392, 24)
        Me.ComboProjekt.TabIndex = 1
        '
        'ComboArbeitspaket
        '
        Me.ComboArbeitspaket.BackColor = System.Drawing.Color.White
        Me.ComboArbeitspaket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboArbeitspaket.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboArbeitspaket.ForeColor = System.Drawing.Color.Black
        Me.ComboArbeitspaket.FormattingEnabled = True
        Me.ComboArbeitspaket.Location = New System.Drawing.Point(122, 88)
        Me.ComboArbeitspaket.Name = "ComboArbeitspaket"
        Me.ComboArbeitspaket.Size = New System.Drawing.Size(392, 24)
        Me.ComboArbeitspaket.TabIndex = 3
        '
        'ComboVersion
        '
        Me.ComboVersion.BackColor = System.Drawing.Color.White
        Me.ComboVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboVersion.ForeColor = System.Drawing.Color.Black
        Me.ComboVersion.FormattingEnabled = True
        Me.ComboVersion.Location = New System.Drawing.Point(362, 118)
        Me.ComboVersion.Name = "ComboVersion"
        Me.ComboVersion.Size = New System.Drawing.Size(152, 24)
        Me.ComboVersion.TabIndex = 4
        '
        'lblProjekt
        '
        Me.lblProjekt.AutoSize = True
        Me.lblProjekt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjekt.ForeColor = System.Drawing.Color.DarkRed
        Me.lblProjekt.Location = New System.Drawing.Point(59, 42)
        Me.lblProjekt.Name = "lblProjekt"
        Me.lblProjekt.Size = New System.Drawing.Size(57, 16)
        Me.lblProjekt.TabIndex = 7
        Me.lblProjekt.Text = "Projekt"
        '
        'lblArbeitspaket
        '
        Me.lblArbeitspaket.AutoSize = True
        Me.lblArbeitspaket.BackColor = System.Drawing.Color.Transparent
        Me.lblArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArbeitspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblArbeitspaket.Location = New System.Drawing.Point(20, 91)
        Me.lblArbeitspaket.Name = "lblArbeitspaket"
        Me.lblArbeitspaket.Size = New System.Drawing.Size(96, 16)
        Me.lblArbeitspaket.TabIndex = 8
        Me.lblArbeitspaket.Text = "Arbeitspaket"
        '
        'lblVers
        '
        Me.lblVers.AutoSize = True
        Me.lblVers.BackColor = System.Drawing.Color.Transparent
        Me.lblVers.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVers.ForeColor = System.Drawing.Color.DarkRed
        Me.lblVers.Location = New System.Drawing.Point(295, 121)
        Me.lblVers.Name = "lblVers"
        Me.lblVers.Size = New System.Drawing.Size(61, 16)
        Me.lblVers.TabIndex = 9
        Me.lblVers.Text = "Version"
        '
        'lblGJ
        '
        Me.lblGJ.AutoSize = True
        Me.lblGJ.BackColor = System.Drawing.Color.Transparent
        Me.lblGJ.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGJ.ForeColor = System.Drawing.Color.DarkRed
        Me.lblGJ.Location = New System.Drawing.Point(13, 121)
        Me.lblGJ.Name = "lblGJ"
        Me.lblGJ.Size = New System.Drawing.Size(103, 16)
        Me.lblGJ.TabIndex = 10
        Me.lblGJ.Text = "Geschäftsjahr"
        '
        'ComboEntwurf
        '
        Me.ComboEntwurf.BackColor = System.Drawing.Color.White
        Me.ComboEntwurf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboEntwurf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboEntwurf.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboEntwurf.ForeColor = System.Drawing.Color.Black
        Me.ComboEntwurf.FormattingEnabled = True
        Me.ComboEntwurf.Location = New System.Drawing.Point(637, 39)
        Me.ComboEntwurf.Name = "ComboEntwurf"
        Me.ComboEntwurf.Size = New System.Drawing.Size(152, 24)
        Me.ComboEntwurf.TabIndex = 2
        '
        'lblEntwurfstyp
        '
        Me.lblEntwurfstyp.AutoSize = True
        Me.lblEntwurfstyp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntwurfstyp.ForeColor = System.Drawing.Color.DarkRed
        Me.lblEntwurfstyp.Location = New System.Drawing.Point(545, 42)
        Me.lblEntwurfstyp.Name = "lblEntwurfstyp"
        Me.lblEntwurfstyp.Size = New System.Drawing.Size(86, 16)
        Me.lblEntwurfstyp.TabIndex = 13
        Me.lblEntwurfstyp.Text = "Entwurfstyp"
        '
        'Line2
        '
        Me.Line2.BackColor = System.Drawing.Color.DarkRed
        Me.Line2.Location = New System.Drawing.Point(5, 154)
        Me.Line2.Name = "Line2"
        Me.Line2.Size = New System.Drawing.Size(850, 2)
        Me.Line2.TabIndex = 21
        Me.Line2.TabStop = False
        '
        'Line1
        '
        Me.Line1.BackColor = System.Drawing.Color.DarkRed
        Me.Line1.Location = New System.Drawing.Point(5, 72)
        Me.Line1.Name = "Line1"
        Me.Line1.Size = New System.Drawing.Size(850, 2)
        Me.Line1.TabIndex = 22
        Me.Line1.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(1113, 36)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 38)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 23
        Me.PictureBox3.TabStop = False
        '
        'TimerEditForm
        '
        '
        'lblBearbeitung
        '
        Me.lblBearbeitung.AutoSize = True
        Me.lblBearbeitung.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBearbeitung.ForeColor = System.Drawing.Color.DarkRed
        Me.lblBearbeitung.Location = New System.Drawing.Point(450, 220)
        Me.lblBearbeitung.Name = "lblBearbeitung"
        Me.lblBearbeitung.Size = New System.Drawing.Size(51, 16)
        Me.lblBearbeitung.TabIndex = 29
        Me.lblBearbeitung.Text = "Status"
        Me.lblBearbeitung.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdArbeitspaket
        '
        Me.cmdArbeitspaket.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdArbeitspaket.ForeColor = System.Drawing.Color.Transparent
        Me.cmdArbeitspaket.Image = CType(resources.GetObject("cmdArbeitspaket.Image"), System.Drawing.Image)
        Me.cmdArbeitspaket.Location = New System.Drawing.Point(518, 88)
        Me.cmdArbeitspaket.Name = "cmdArbeitspaket"
        Me.cmdArbeitspaket.Size = New System.Drawing.Size(24, 24)
        Me.cmdArbeitspaket.TabIndex = 30
        Me.cmdArbeitspaket.UseVisualStyleBackColor = True
        '
        'cmdVersion
        '
        Me.cmdVersion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdVersion.ForeColor = System.Drawing.Color.Transparent
        Me.cmdVersion.Image = CType(resources.GetObject("cmdVersion.Image"), System.Drawing.Image)
        Me.cmdVersion.Location = New System.Drawing.Point(518, 117)
        Me.cmdVersion.Name = "cmdVersion"
        Me.cmdVersion.Size = New System.Drawing.Size(24, 24)
        Me.cmdVersion.TabIndex = 32
        Me.cmdVersion.UseVisualStyleBackColor = True
        '
        'ContextMouse
        '
        Me.ContextMouse.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.ContextMouse.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContextKopieren, Me.ContextEinfügen, Me.ToolStripSeparator1, Me.ContextLöschen, Me.ToolStripSeparator11, Me.MarkierenToolStripMenuItem, Me.ToolStripSeparator12, Me.ContextMenuCopyToCache})
        Me.ContextMouse.Name = "ContextMouse"
        Me.ContextMouse.Size = New System.Drawing.Size(230, 132)
        Me.ContextMouse.Text = "Context"
        '
        'ContextKopieren
        '
        Me.ContextKopieren.Name = "ContextKopieren"
        Me.ContextKopieren.Size = New System.Drawing.Size(229, 22)
        Me.ContextKopieren.Text = "Kopieren"
        '
        'ContextEinfügen
        '
        Me.ContextEinfügen.Name = "ContextEinfügen"
        Me.ContextEinfügen.Size = New System.Drawing.Size(229, 22)
        Me.ContextEinfügen.Text = "Einfügen"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(226, 6)
        '
        'ContextLöschen
        '
        Me.ContextLöschen.Name = "ContextLöschen"
        Me.ContextLöschen.Size = New System.Drawing.Size(229, 22)
        Me.ContextLöschen.Text = "Löschen"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(226, 6)
        '
        'MarkierenToolStripMenuItem
        '
        Me.MarkierenToolStripMenuItem.Name = "MarkierenToolStripMenuItem"
        Me.MarkierenToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.MarkierenToolStripMenuItem.Text = "Markieren"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(226, 6)
        '
        'ContextMenuCopyToCache
        '
        Me.ContextMenuCopyToCache.Name = "ContextMenuCopyToCache"
        Me.ContextMenuCopyToCache.Size = New System.Drawing.Size(229, 22)
        Me.ContextMenuCopyToCache.Text = "In Cache kopieren (STRG + C)"
        '
        'AddRow
        '
        Me.AddRow.Image = CType(resources.GetObject("AddRow.Image"), System.Drawing.Image)
        Me.AddRow.Location = New System.Drawing.Point(55, 209)
        Me.AddRow.Name = "AddRow"
        Me.AddRow.Size = New System.Drawing.Size(35, 35)
        Me.AddRow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.AddRow.TabIndex = 36
        Me.AddRow.TabStop = False
        '
        'SaveData
        '
        Me.SaveData.Image = CType(resources.GetObject("SaveData.Image"), System.Drawing.Image)
        Me.SaveData.Location = New System.Drawing.Point(6, 208)
        Me.SaveData.Name = "SaveData"
        Me.SaveData.Size = New System.Drawing.Size(35, 35)
        Me.SaveData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.SaveData.TabIndex = 37
        Me.SaveData.TabStop = False
        '
        'SplitAddRow
        '
        Me.SplitAddRow.BackColor = System.Drawing.Color.Black
        Me.SplitAddRow.Location = New System.Drawing.Point(47, 209)
        Me.SplitAddRow.Name = "SplitAddRow"
        Me.SplitAddRow.Size = New System.Drawing.Size(2, 35)
        Me.SplitAddRow.TabIndex = 39
        Me.SplitAddRow.TabStop = False
        '
        'VersionDescription
        '
        Me.VersionDescription.BackColor = System.Drawing.Color.White
        Me.VersionDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.VersionDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VersionDescription.ForeColor = System.Drawing.Color.DarkRed
        Me.VersionDescription.Location = New System.Drawing.Point(548, 121)
        Me.VersionDescription.Multiline = True
        Me.VersionDescription.Name = "VersionDescription"
        Me.VersionDescription.Size = New System.Drawing.Size(152, 24)
        Me.VersionDescription.TabIndex = 42
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.BorderData, Me.OvalFalse, Me.OvalTrue, Me.RectangleCompare})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1283, 523)
        Me.ShapeContainer1.TabIndex = 49
        Me.ShapeContainer1.TabStop = False
        '
        'BorderData
        '
        Me.BorderData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BorderData.BorderWidth = 4
        Me.BorderData.Location = New System.Drawing.Point(6, 253)
        Me.BorderData.Name = "BorderData"
        Me.BorderData.Size = New System.Drawing.Size(1270, 243)
        '
        'OvalFalse
        '
        Me.OvalFalse.BackColor = System.Drawing.Color.White
        Me.OvalFalse.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.OvalFalse.Location = New System.Drawing.Point(287, 214)
        Me.OvalFalse.Name = "OvalFalse"
        Me.OvalFalse.Size = New System.Drawing.Size(25, 25)
        '
        'OvalTrue
        '
        Me.OvalTrue.BackColor = System.Drawing.Color.SteelBlue
        Me.OvalTrue.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.OvalTrue.Location = New System.Drawing.Point(324, 214)
        Me.OvalTrue.Name = "OvalTrue"
        Me.OvalTrue.Size = New System.Drawing.Size(25, 25)
        '
        'RectangleCompare
        '
        Me.RectangleCompare.BackColor = System.Drawing.Color.DarkRed
        Me.RectangleCompare.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleCompare.CornerRadius = 17
        Me.RectangleCompare.Location = New System.Drawing.Point(280, 209)
        Me.RectangleCompare.Name = "RectangleCompare"
        Me.RectangleCompare.Size = New System.Drawing.Size(75, 35)
        '
        'lblCompare
        '
        Me.lblCompare.AutoSize = True
        Me.lblCompare.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompare.ForeColor = System.Drawing.Color.DarkRed
        Me.lblCompare.Location = New System.Drawing.Point(140, 220)
        Me.lblCompare.Name = "lblCompare"
        Me.lblCompare.Size = New System.Drawing.Size(124, 16)
        Me.lblCompare.TabIndex = 50
        Me.lblCompare.Text = "Vergleich Modus"
        '
        'cmdHideFilter
        '
        Me.cmdHideFilter.Image = CType(resources.GetObject("cmdHideFilter.Image"), System.Drawing.Image)
        Me.cmdHideFilter.Location = New System.Drawing.Point(752, 167)
        Me.cmdHideFilter.Name = "cmdHideFilter"
        Me.cmdHideFilter.Size = New System.Drawing.Size(35, 35)
        Me.cmdHideFilter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmdHideFilter.TabIndex = 54
        Me.cmdHideFilter.TabStop = False
        '
        'cmdShowFilter
        '
        Me.cmdShowFilter.Image = CType(resources.GetObject("cmdShowFilter.Image"), System.Drawing.Image)
        Me.cmdShowFilter.Location = New System.Drawing.Point(753, 72)
        Me.cmdShowFilter.Name = "cmdShowFilter"
        Me.cmdShowFilter.Size = New System.Drawing.Size(35, 35)
        Me.cmdShowFilter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmdShowFilter.TabIndex = 55
        Me.cmdShowFilter.TabStop = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "Prototypenmaterial.png")
        Me.ImageList.Images.SetKeyName(1, "Sonstiges_Material.jpg")
        Me.ImageList.Images.SetKeyName(2, "Arbeitskosten.png")
        Me.ImageList.Images.SetKeyName(3, "Fremdpersonal.jpg")
        Me.ImageList.Images.SetKeyName(4, "WindTunnel.png")
        Me.ImageList.Images.SetKeyName(5, "Sonstige Kosten.png")
        Me.ImageList.Images.SetKeyName(6, "Question.JPG")
        Me.ImageList.Images.SetKeyName(7, "Summe.png")
        '
        'lblCurrentArbeitspaket
        '
        Me.lblCurrentArbeitspaket.AutoSize = True
        Me.lblCurrentArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentArbeitspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblCurrentArbeitspaket.Location = New System.Drawing.Point(763, 42)
        Me.lblCurrentArbeitspaket.Name = "lblCurrentArbeitspaket"
        Me.lblCurrentArbeitspaket.Size = New System.Drawing.Size(96, 16)
        Me.lblCurrentArbeitspaket.TabIndex = 57
        Me.lblCurrentArbeitspaket.Text = "Arbeitspaket"
        '
        'iGrid
        '
        Me.iGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.iGrid.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat
        Me.iGrid.GroupBox.Visible = True
        Me.iGrid.Location = New System.Drawing.Point(12, 262)
        Me.iGrid.Name = "iGrid"
        Me.iGrid.RowHeader.Visible = True
        Me.iGrid.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.MultipleRows
        Me.iGrid.SelectionMode = TenTec.Windows.iGridLib.iGSelectionMode.MultiExtended
        Me.iGrid.Size = New System.Drawing.Size(1262, 255)
        Me.iGrid.TabIndex = 60
        Me.iGrid.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always
        '
        'iGridAutoFilter
        '
        Me.iGridAutoFilter.Grid = Me.iGrid
        '
        'iGridDropDown_Kostenart
        '
        Me.iGridDropDown_Kostenart.ImageList = Me.ILKostenart
        '
        'ILKostenart
        '
        Me.ILKostenart.ImageStream = CType(resources.GetObject("ILKostenart.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ILKostenart.TransparentColor = System.Drawing.Color.Transparent
        Me.ILKostenart.Images.SetKeyName(0, "Prototypenmaterial2.png")
        Me.ILKostenart.Images.SetKeyName(1, "Sonstiges Material.png")
        Me.ILKostenart.Images.SetKeyName(2, "Arbeitskosten-neu.png")
        Me.ILKostenart.Images.SetKeyName(3, "Fremdpersonal.png")
        Me.ILKostenart.Images.SetKeyName(4, "Leistungsarten-neu.png")
        Me.ILKostenart.Images.SetKeyName(5, "Plane3.png")
        Me.ILKostenart.Images.SetKeyName(6, "Fremd-E2.png")
        Me.ILKostenart.Images.SetKeyName(7, "Question.JPG")
        '
        'IGPrintManager1
        '
        Me.IGPrintManager1.Grid = Me.iGrid
        '
        'LineHideShow
        '
        Me.LineHideShow.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineHideShow.BackColor = System.Drawing.Color.Black
        Me.LineHideShow.Location = New System.Drawing.Point(0, 200)
        Me.LineHideShow.Name = "LineHideShow"
        Me.LineHideShow.Size = New System.Drawing.Size(1343, 2)
        Me.LineHideShow.TabIndex = 61
        Me.LineHideShow.TabStop = False
        '
        'ExcelExport
        '
        Me.ExcelExport.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ExcelExport.Image = CType(resources.GetObject("ExcelExport.Image"), System.Drawing.Image)
        Me.ExcelExport.Location = New System.Drawing.Point(1239, 209)
        Me.ExcelExport.Name = "ExcelExport"
        Me.ExcelExport.Size = New System.Drawing.Size(35, 35)
        Me.ExcelExport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ExcelExport.TabIndex = 62
        Me.ExcelExport.TabStop = False
        '
        'FooterCalcMethod
        '
        Me.FooterCalcMethod.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.FooterCalcMethod.Image = CType(resources.GetObject("FooterCalcMethod.Image"), System.Drawing.Image)
        Me.FooterCalcMethod.Location = New System.Drawing.Point(1064, 209)
        Me.FooterCalcMethod.Name = "FooterCalcMethod"
        Me.FooterCalcMethod.Size = New System.Drawing.Size(35, 35)
        Me.FooterCalcMethod.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.FooterCalcMethod.TabIndex = 63
        Me.FooterCalcMethod.TabStop = False
        '
        'ILFooter
        '
        Me.ILFooter.ImageStream = CType(resources.GetObject("ILFooter.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ILFooter.TransparentColor = System.Drawing.Color.Transparent
        Me.ILFooter.Images.SetKeyName(0, "sum.png")
        Me.ILFooter.Images.SetKeyName(1, "amount.png")
        Me.ILFooter.Images.SetKeyName(2, "fte.png")
        '
        'SplitExcel
        '
        Me.SplitExcel.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.SplitExcel.BackColor = System.Drawing.Color.Black
        Me.SplitExcel.Location = New System.Drawing.Point(1105, 209)
        Me.SplitExcel.Name = "SplitExcel"
        Me.SplitExcel.Size = New System.Drawing.Size(2, 35)
        Me.SplitExcel.TabIndex = 64
        Me.SplitExcel.TabStop = False
        '
        'SplitGetSap
        '
        Me.SplitGetSap.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.SplitGetSap.BackColor = System.Drawing.Color.Black
        Me.SplitGetSap.Location = New System.Drawing.Point(1231, 210)
        Me.SplitGetSap.Name = "SplitGetSap"
        Me.SplitGetSap.Size = New System.Drawing.Size(2, 35)
        Me.SplitGetSap.TabIndex = 65
        Me.SplitGetSap.TabStop = False
        '
        'cmdGetSap
        '
        Me.cmdGetSap.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdGetSap.Image = CType(resources.GetObject("cmdGetSap.Image"), System.Drawing.Image)
        Me.cmdGetSap.Location = New System.Drawing.Point(1162, 209)
        Me.cmdGetSap.Name = "cmdGetSap"
        Me.cmdGetSap.Size = New System.Drawing.Size(63, 35)
        Me.cmdGetSap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmdGetSap.TabIndex = 66
        Me.cmdGetSap.TabStop = False
        '
        'ShowMultiData
        '
        Me.ShowMultiData.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ShowMultiData.Image = CType(resources.GetObject("ShowMultiData.Image"), System.Drawing.Image)
        Me.ShowMultiData.Location = New System.Drawing.Point(1113, 209)
        Me.ShowMultiData.Name = "ShowMultiData"
        Me.ShowMultiData.Size = New System.Drawing.Size(35, 35)
        Me.ShowMultiData.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ShowMultiData.TabIndex = 67
        Me.ShowMultiData.TabStop = False
        '
        'SplitMultiData
        '
        Me.SplitMultiData.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.SplitMultiData.BackColor = System.Drawing.Color.Black
        Me.SplitMultiData.Location = New System.Drawing.Point(1154, 209)
        Me.SplitMultiData.Name = "SplitMultiData"
        Me.SplitMultiData.Size = New System.Drawing.Size(2, 35)
        Me.SplitMultiData.TabIndex = 68
        Me.SplitMultiData.TabStop = False
        '
        'CheckedListGJ
        '
        Me.CheckedListGJ.FormattingEnabled = True
        Me.CheckedListGJ.Location = New System.Drawing.Point(115, 121)
        Me.CheckedListGJ.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CheckedListGJ.Name = "CheckedListGJ"
        Me.CheckedListGJ.Size = New System.Drawing.Size(109, 19)
        Me.CheckedListGJ.TabIndex = 69
        '
        'SplitEmail
        '
        Me.SplitEmail.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.SplitEmail.BackColor = System.Drawing.Color.Black
        Me.SplitEmail.Location = New System.Drawing.Point(1057, 210)
        Me.SplitEmail.Name = "SplitEmail"
        Me.SplitEmail.Size = New System.Drawing.Size(2, 35)
        Me.SplitEmail.TabIndex = 70
        Me.SplitEmail.TabStop = False
        '
        'EmailNotificationState
        '
        Me.EmailNotificationState.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.EmailNotificationState.Image = Global.myBudget.My.Resources.Resources.EmailDisabled100
        Me.EmailNotificationState.ImageLocation = ""
        Me.EmailNotificationState.Location = New System.Drawing.Point(985, 209)
        Me.EmailNotificationState.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.EmailNotificationState.Name = "EmailNotificationState"
        Me.EmailNotificationState.Size = New System.Drawing.Size(67, 35)
        Me.EmailNotificationState.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.EmailNotificationState.TabIndex = 71
        Me.EmailNotificationState.TabStop = False
        '
        'lblVersion
        '
        Me.lblVersion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(63, 29)
        Me.lblVersion.Text = "Version"
        '
        'CopyrightMHoffmannToolStripMenuItem
        '
        Me.CopyrightMHoffmannToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CopyrightMHoffmannToolStripMenuItem.Name = "CopyrightMHoffmannToolStripMenuItem"
        Me.CopyrightMHoffmannToolStripMenuItem.Size = New System.Drawing.Size(155, 29)
        Me.CopyrightMHoffmannToolStripMenuItem.Text = "copyright M. Hoffmann"
        '
        'EditForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1283, 523)
        Me.Controls.Add(Me.EmailNotificationState)
        Me.Controls.Add(Me.SplitEmail)
        Me.Controls.Add(Me.CheckedListGJ)
        Me.Controls.Add(Me.SplitExcel)
        Me.Controls.Add(Me.SplitMultiData)
        Me.Controls.Add(Me.ShowMultiData)
        Me.Controls.Add(Me.FooterCalcMethod)
        Me.Controls.Add(Me.cmdGetSap)
        Me.Controls.Add(Me.SplitGetSap)
        Me.Controls.Add(Me.lblEntwurfstyp)
        Me.Controls.Add(Me.ComboEntwurf)
        Me.Controls.Add(Me.ExcelExport)
        Me.Controls.Add(Me.VersionDescription)
        Me.Controls.Add(Me.LineHideShow)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.lblCompare)
        Me.Controls.Add(Me.lblBearbeitung)
        Me.Controls.Add(Me.lblProjekt)
        Me.Controls.Add(Me.ComboProjekt)
        Me.Controls.Add(Me.lblCurrentArbeitspaket)
        Me.Controls.Add(Me.SaveData)
        Me.Controls.Add(Me.SplitAddRow)
        Me.Controls.Add(Me.AddRow)
        Me.Controls.Add(Me.iGrid)
        Me.Controls.Add(Me.cmdShowFilter)
        Me.Controls.Add(Me.cmdVersion)
        Me.Controls.Add(Me.cmdArbeitspaket)
        Me.Controls.Add(Me.Line2)
        Me.Controls.Add(Me.lblGJ)
        Me.Controls.Add(Me.lblVers)
        Me.Controls.Add(Me.lblArbeitspaket)
        Me.Controls.Add(Me.ComboVersion)
        Me.Controls.Add(Me.ComboArbeitspaket)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.cmdHideFilter)
        Me.Controls.Add(Me.Line1)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EditForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "myBudget"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        CType(Me.Line2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMouse.ResumeLayout(False)
        CType(Me.AddRow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SaveData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitAddRow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdHideFilter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdShowFilter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.iGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LineHideShow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExcelExport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FooterCalcMethod, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitExcel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitGetSap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdGetSap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ShowMultiData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitMultiData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmailNotificationState, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents ComboProjekt As System.Windows.Forms.ComboBox
    Friend WithEvents ComboArbeitspaket As System.Windows.Forms.ComboBox
    Friend WithEvents ComboVersion As System.Windows.Forms.ComboBox
    Friend WithEvents lblProjekt As System.Windows.Forms.Label
    Friend WithEvents lblArbeitspaket As System.Windows.Forms.Label
    Friend WithEvents lblVers As System.Windows.Forms.Label
    Friend WithEvents lblGJ As System.Windows.Forms.Label
    Friend WithEvents ComboEntwurf As System.Windows.Forms.ComboBox
    Friend WithEvents lblEntwurfstyp As System.Windows.Forms.Label
    Friend WithEvents Line2 As System.Windows.Forms.PictureBox
    Friend WithEvents Line1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStripStatusOperation As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MenuReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TimerEditForm As System.Windows.Forms.Timer
    Friend WithEvents lblBearbeitung As System.Windows.Forms.Label
    Friend WithEvents cmdArbeitspaket As System.Windows.Forms.Button
    Friend WithEvents cmdVersion As System.Windows.Forms.Button
    Friend WithEvents ContextMouse As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ContextKopieren As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextEinfügen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuAktionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VersionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuRelease As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewVersion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuChangeVersion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaketToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewPaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuChangePaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntwicklungspaketToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewEntwicklungspaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuChangeEntwicklungspaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArbeitspaketToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewArbeitspaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuChangeArbeitspaket As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProjektToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewProjekt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuNewReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextLöschen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuN_Compare As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AddRow As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTipAddRow As System.Windows.Forms.ToolTip
    Friend WithEvents SaveData As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTipSaveData As System.Windows.Forms.ToolTip
    Friend WithEvents SplitAddRow As System.Windows.Forms.PictureBox
    Friend WithEvents VersionDescription As System.Windows.Forms.TextBox
    Friend WithEvents MenuServer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerbindungHerstellenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VerbindungTrennenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HilfeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuSachkonten As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuEkant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblCompare As System.Windows.Forms.Label
    Friend WithEvents cmdHideFilter As PictureBox
    Friend WithEvents cmdShowFilter As PictureBox
    Friend WithEvents MenuHandbuch As ToolStripMenuItem
    Friend WithEvents ToolStripStatusDatabase As ToolStripStatusLabel
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As ToolStripSeparator
    Friend WithEvents ImageList As ImageList
    Friend WithEvents lblCurrentArbeitspaket As System.Windows.Forms.Label
    Friend WithEvents iGridAutoFilter As TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager
    Friend WithEvents iGridDropDown_Entwicklungspaket As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents iGridDropDown_Paket As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents iGridDropDown_Klasse As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents iGridDropDown_GJ As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents ILKostenart As ImageList
    Private WithEvents iGridDropDown_Kostenart As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents DatenAktualisierenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IGPrintManager1 As TenTec.Windows.iGridLib.Printing.iGPrintManager
    Friend WithEvents DruckenToolStripMenuItem As ToolStripMenuItem
    Public WithEvents iGrid As TenTec.Windows.iGridLib.iGrid
    Friend WithEvents SAPImportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents LineHideShow As PictureBox
    Friend WithEvents MenuMehrereArbeitspaketeBearbeiten As ToolStripMenuItem
    Friend WithEvents MenuDatenMitVorgängerversionVergleichen As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator7 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator9 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator10 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator11 As ToolStripSeparator
    Friend WithEvents MarkierenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TerminverfolgungToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExcelExport As PictureBox
    Friend WithEvents ToolStripSeparator12 As ToolStripSeparator
    Friend WithEvents ContextMenuCopyToCache As ToolStripMenuItem
    Friend WithEvents LiveSAPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FooterCalcMethod As PictureBox
    Friend WithEvents ToolTipFooterCalcMethod As ToolTip
    Friend WithEvents ILFooter As ImageList
    Friend WithEvents SplitExcel As PictureBox
    Friend WithEvents MenuSapSystem As ToolStripMenuItem
    Friend WithEvents MenuVerwaltung As ToolStripMenuItem
    Friend WithEvents MenuUser As ToolStripMenuItem
    Friend WithEvents MenuPundAp As ToolStripMenuItem
    Friend WithEvents MenuIVP As ToolStripMenuItem
    Friend WithEvents MenuDeleteAP As ToolStripMenuItem
    Friend WithEvents MenuGetDataViaSqlStatement As ToolStripMenuItem
    Friend WithEvents SplitGetSap As PictureBox
    Friend WithEvents cmdGetSap As PictureBox
    Friend WithEvents ToolTipGetSapData As ToolTip
    Friend WithEvents MenuPasswordÄndern As ToolStripMenuItem
    Friend WithEvents ShowMultiData As PictureBox
    Friend WithEvents SplitMultiData As PictureBox
    Friend WithEvents ToolTipMultiData As ToolTip
    Friend WithEvents KostenLinearVerteilenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MenuEKantErstellen As ToolStripMenuItem
    Friend WithEvents MenuProzentualeAnpassung As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator13 As ToolStripSeparator
    Friend WithEvents MenuBenachrichtigungsEmail As ToolStripMenuItem
    Private WithEvents ShapeContainer1 As ShapeContainer
    Private WithEvents OvalFalse As OvalShape
    Private WithEvents OvalTrue As OvalShape
    Private WithEvents RectangleCompare As RectangleShape
    Private WithEvents BorderData As RectangleShape
    Friend WithEvents CheckedListGJ As CheckedListBox
    Friend WithEvents SplitEmail As PictureBox
    Friend WithEvents EmailNotificationState As PictureBox
    Friend WithEvents lblVersion As ToolStripMenuItem
    Friend WithEvents CopyrightMHoffmannToolStripMenuItem As ToolStripMenuItem
End Class
