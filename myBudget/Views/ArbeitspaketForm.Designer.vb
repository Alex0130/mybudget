﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ArbeitspaketForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ArbeitspaketForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtArbeitspaket = New System.Windows.Forms.TextBox()
        Me.lblArbeitspaket = New System.Windows.Forms.Label()
        Me.ComboArbeitspaket = New System.Windows.Forms.ComboBox()
        Me.txtPrämissen = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdSaveData = New System.Windows.Forms.Button()
        Me.ComboProjekt = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboTeam = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtVerantwortlich = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPSP = New System.Windows.Forms.TextBox()
        Me.cbxInactive = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DarkRed
        Me.Label1.Location = New System.Drawing.Point(14, 145)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Beschreibung"
        '
        'txtArbeitspaket
        '
        Me.txtArbeitspaket.Location = New System.Drawing.Point(18, 166)
        Me.txtArbeitspaket.Margin = New System.Windows.Forms.Padding(4)
        Me.txtArbeitspaket.Name = "txtArbeitspaket"
        Me.txtArbeitspaket.Size = New System.Drawing.Size(379, 22)
        Me.txtArbeitspaket.TabIndex = 3
        '
        'lblArbeitspaket
        '
        Me.lblArbeitspaket.AutoSize = True
        Me.lblArbeitspaket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArbeitspaket.ForeColor = System.Drawing.Color.DarkRed
        Me.lblArbeitspaket.Location = New System.Drawing.Point(14, 81)
        Me.lblArbeitspaket.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblArbeitspaket.Name = "lblArbeitspaket"
        Me.lblArbeitspaket.Size = New System.Drawing.Size(96, 16)
        Me.lblArbeitspaket.TabIndex = 2
        Me.lblArbeitspaket.Text = "Arbeitspaket"
        '
        'ComboArbeitspaket
        '
        Me.ComboArbeitspaket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboArbeitspaket.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboArbeitspaket.FormattingEnabled = True
        Me.ComboArbeitspaket.Location = New System.Drawing.Point(18, 101)
        Me.ComboArbeitspaket.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboArbeitspaket.Name = "ComboArbeitspaket"
        Me.ComboArbeitspaket.Size = New System.Drawing.Size(379, 24)
        Me.ComboArbeitspaket.TabIndex = 2
        '
        'txtPrämissen
        '
        Me.txtPrämissen.Location = New System.Drawing.Point(18, 427)
        Me.txtPrämissen.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPrämissen.Multiline = True
        Me.txtPrämissen.Name = "txtPrämissen"
        Me.txtPrämissen.Size = New System.Drawing.Size(379, 69)
        Me.txtPrämissen.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(15, 407)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Beschreibung"
        '
        'cmdSaveData
        '
        Me.cmdSaveData.BackColor = System.Drawing.Color.Maroon
        Me.cmdSaveData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdSaveData.ForeColor = System.Drawing.Color.White
        Me.cmdSaveData.Location = New System.Drawing.Point(297, 504)
        Me.cmdSaveData.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSaveData.Name = "cmdSaveData"
        Me.cmdSaveData.Size = New System.Drawing.Size(100, 35)
        Me.cmdSaveData.TabIndex = 8
        Me.cmdSaveData.Text = "Speichern"
        Me.cmdSaveData.UseVisualStyleBackColor = False
        '
        'ComboProjekt
        '
        Me.ComboProjekt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboProjekt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboProjekt.FormattingEnabled = True
        Me.ComboProjekt.Location = New System.Drawing.Point(18, 36)
        Me.ComboProjekt.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboProjekt.Name = "ComboProjekt"
        Me.ComboProjekt.Size = New System.Drawing.Size(379, 24)
        Me.ComboProjekt.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.DarkRed
        Me.Label6.Location = New System.Drawing.Point(18, 15)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Projekte"
        '
        'ComboTeam
        '
        Me.ComboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboTeam.FormattingEnabled = True
        Me.ComboTeam.Location = New System.Drawing.Point(21, 340)
        Me.ComboTeam.Margin = New System.Windows.Forms.Padding(4)
        Me.ComboTeam.Name = "ComboTeam"
        Me.ComboTeam.Size = New System.Drawing.Size(376, 24)
        Me.ComboTeam.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DarkRed
        Me.Label3.Location = New System.Drawing.Point(18, 320)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Team"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DarkRed
        Me.Label4.Location = New System.Drawing.Point(18, 260)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 16)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "PSP Element"
        '
        'txtVerantwortlich
        '
        Me.txtVerantwortlich.Location = New System.Drawing.Point(18, 223)
        Me.txtVerantwortlich.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVerantwortlich.Name = "txtVerantwortlich"
        Me.txtVerantwortlich.Size = New System.Drawing.Size(379, 22)
        Me.txtVerantwortlich.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.DarkRed
        Me.Label5.Location = New System.Drawing.Point(18, 203)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 16)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Verantwortlich"
        '
        'txtPSP
        '
        Me.txtPSP.Location = New System.Drawing.Point(18, 280)
        Me.txtPSP.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPSP.Name = "txtPSP"
        Me.txtPSP.Size = New System.Drawing.Size(379, 22)
        Me.txtPSP.TabIndex = 5
        '
        'cbxInactive
        '
        Me.cbxInactive.AutoSize = True
        Me.cbxInactive.Location = New System.Drawing.Point(18, 371)
        Me.cbxInactive.Name = "cbxInactive"
        Me.cbxInactive.Size = New System.Drawing.Size(115, 20)
        Me.cbxInactive.TabIndex = 33
        Me.cbxInactive.Text = "Deaktivieren"
        Me.cbxInactive.UseVisualStyleBackColor = True
        '
        'ArbeitspaketForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(409, 552)
        Me.Controls.Add(Me.cbxInactive)
        Me.Controls.Add(Me.txtPSP)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtVerantwortlich)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboTeam)
        Me.Controls.Add(Me.ComboProjekt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cmdSaveData)
        Me.Controls.Add(Me.txtPrämissen)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboArbeitspaket)
        Me.Controls.Add(Me.lblArbeitspaket)
        Me.Controls.Add(Me.txtArbeitspaket)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DarkRed
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "ArbeitspaketForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arbeitspaket"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtArbeitspaket As System.Windows.Forms.TextBox
    Friend WithEvents lblArbeitspaket As System.Windows.Forms.Label
    Friend WithEvents ComboArbeitspaket As System.Windows.Forms.ComboBox
    Friend WithEvents txtPrämissen As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdSaveData As System.Windows.Forms.Button
    Friend WithEvents ComboProjekt As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboTeam As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtVerantwortlich As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPSP As System.Windows.Forms.TextBox
    Friend WithEvents cbxInactive As System.Windows.Forms.CheckBox
End Class
