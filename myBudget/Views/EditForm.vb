﻿Imports System
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Deployment
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Diagnostics

Imports Microsoft.Office.Interop

Imports TenTec.Windows.iGridLib

Imports myBudget.UI
Imports myBudget.iGCopyPasteManager

<Assembly: AssemblyVersion("5.0.3.3")>
<Assembly: AssemblyFileVersion("5.0.3.3")>

'Einklappen: STRG+O+M

Public Class EditForm
    Inherits System.Windows.Forms.Form

#Region "Edit Form"

#Region "Main_Form"
    Public Sub New()
        Console.WriteLine(">> Public Sub New")

        Try
            Me.KeyPreview = True 'Ermöglicht Shortcuts
            InitializeComponent()
        Catch ex As Exception
            MsgBox("Public Sub New: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub EditForm_Initialize(ByVal sender As Object,
    ByVal e As System.EventArgs) Handles MyBase.Load 'Fängt das Laden der EditForm ab
        Console.WriteLine(">> Private Sub EditForm_Initialize")

        Try
            If con.State = ConnectionState.Closed Then Exit Sub
            Load_EditForm()
        Catch ex As Exception
            MsgBox("EditForm_Initialize - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub EditForm_Closed(ByVal sender As Object,
    ByVal e As System.EventArgs) Handles MyBase.Closed
        Console.WriteLine(">> Private Sub EditForm_Closed")

        Try
#Region "User Settings and Connect Log"
            Try 'Usersettings speichern
                Dim TextGj As String = ConvertIntegerArrayToString(pbl_LstSelGj)
                Update_User(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_IdProjekt, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, TextGj)
            Catch ex As Exception
            End Try
            '>
            Try
                Add_Log(Now, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtUser.Rows(intCurrentUser).Item("txtName"), pbl_dtUser.Rows(intCurrentUser).Item("txtVorname"), "Logout")
            Catch
            End Try
            '<
#End Region

            Close_DB()
            boolLoggedIn = False

            ApplicationExit = True
            For i = System.Windows.Forms.Application.OpenForms.Count - 1 To 1 Step -1
                Dim form As Form = System.Windows.Forms.Application.OpenForms(i)
                form.Close()
            Next i

            'Emilhandling beenden
            TerminateEmailHandling()    '!ab added
        Catch ex As Exception
            MsgBox("Private Sub EditForm_Closed: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub TimerEditForm_Tick(sender As Object, e As EventArgs) Handles TimerEditForm.Tick
        Console.WriteLine("< Private Sub TimerEditForm_Tick")

        ToolStripStatusOperation.Text = ""
        TimerEditForm.Stop()
    End Sub
#End Region

#Region "Edit Form - ComboBox(en)"
    Public Sub ComboProjekt_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboProjekt.SelectedIndexChanged 'Handhabt die Änderung von Projekten
        Console.WriteLine("< Private Sub ComboProjekt_SelectedIndexChanged")

        Try
            'Wird ein Projekt geändert, müssen alle davon abhängigen Daten neu geladen werden
            '1. Arbeitspakete + Versionen: Daten laden + Combobox neu befüllen
            '2. Geschäftsjahre: Daten laden + Combobox neu befüllen
            '3. Entwicklungspakete: Daten laden + Combobox neu befüllen
            '4. Subskiptionstatusanzeige aktualisieren !ab

            If con.State = ConnectionState.Closed Then Exit Sub

            ToolStripStatusOperation.Text = "Projektdaten werden geladen..."
            StatusStrip.Refresh()

#Region "Entry and Projekt ID"
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_IdProjekt & "")
                    If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                        Dim IndexProjekt As Integer = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                        ComboProjekt.SelectedIndex = IndexProjekt
                    End If
                    Exit Sub
                Else
                    CopyRow = -1 'Kopierfunktion zurückgesetzt
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            pbl_IdProjekt = pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT") 'Es muss einen Index geben, da er gewählt wurde

            'Abhängige Felder leeren:
            Try
                ComboArbeitspaket.Items.Clear()
                ComboArbeitspaket.Text = ""
                ComboVersion.Items.Clear()
                ComboVersion.Text = ""
            Catch
            End Try
#End Region

#Region "Arbeitspaket und Version"
            Try
                Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False)
                pbl_IdAp = pbl_dtArbeitspaket.Rows(ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, 0, pbl_dtArbeitspaket)).Item("ID_AP")
            Catch
                pbl_IdAp = -1
            End Try

            Try
                Get_VersionArbeitspaket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)
                Dim IndexVersion = ViewCombo.View_ComboboxVersion(ComboVersion, 0, pbl_VersionAp)
                pbl_lngVersion = pbl_VersionAp(IndexVersion).lngVersion
                VersionDescription.Text = pbl_VersionAp(IndexVersion).txtVersionErklärung
                Get_Paket(True, pbl_IdAp)
            Catch
                pbl_lngVersion = -1
            End Try
#End Region

#Region "GJ und Entwicklunspaket"
            Try
                Get_GJ(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"))
                pbl_LstSelGj = ConvertStringToIntegerArray(pbl_dtUser.Rows(intCurrentUser).Item("qryGj"))
                CheckListGJ_Load() 'GJ

                Get_Entwicklungspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"))
            Catch
            End Try
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
            Catch
            End Try

            Try
                RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
                iGrid_ClearFilterAndGroups()
                AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

                ReadOnlyMode = False
                Initialize_iGrid(pbl_dt)

                'Projektanwahl geändert -> Subscriptionstatusanzeige aktualisieren
                RefreshSubscriptionState(EmailNotificationState)    '!ab added
            Catch
            End Try
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ComboProjekt_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboArbeitspaket_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboArbeitspaket.SelectedIndexChanged
        Console.WriteLine("< Private Sub ComboArbeitspaket_SelectedIndexChanged")

        Try
            'Wird ein Arbeitspaket geändert, müssen alle davon abhängigen Daten neu geladen werden
            '1. Versionen

            If con.State = ConnectionState.Closed Then Exit Sub

            ToolStripStatusOperation.Text = "Arbeitspaket Daten werden geladen..."
            StatusStrip.Refresh()

#Region "Entry and AP ID"
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
                    If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                        ComboArbeitspaket.SelectedIndex = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
                    Else
                        ComboArbeitspaket.SelectedIndex = 0
                    End If
                    Exit Sub
                Else
                    CopyRow = -1 'Kopierfunktion zurückgesetzt
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            pbl_IdAp = pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP") 'gewählte ID

            'Abhängige Felder leeren:
            Try
                ComboVersion.Items.Clear()
                ComboVersion.Text = ""
            Catch
            End Try
#End Region

#Region "Version und Paket"
            Try
                Get_VersionArbeitspaket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)
                Dim IndexVersion As Integer = ViewCombo.View_ComboboxVersion(ComboVersion, 0, pbl_VersionAp)
                pbl_lngVersion = pbl_VersionAp(IndexVersion).lngVersion 'Höchste Verison vorbelegen
                VersionDescription.Text = pbl_VersionAp(IndexVersion).txtVersionErklärung

                Get_Paket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"))
            Catch
                pbl_lngVersion = -1
            End Try
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
                RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
                iGrid_ClearFilterAndGroups()
                AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

                ReadOnlyMode = False
                Initialize_iGrid(pbl_dt)

                'Arbeitspaketanwahl geändert -> Subscriptionstatusanzeige aktualisieren
                RefreshSubscriptionState(EmailNotificationState)    '!ab added
            Catch
            End Try
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ComboArbeitspaket_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboVersion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboVersion.SelectedIndexChanged
        Console.WriteLine("< Private Sub ComboVersion_SelectedIndexChanged")

        If con.State = ConnectionState.Closed Then Exit Sub

        Try

            ToolStripStatusOperation.Text = "Daten werden geladen..."
            StatusStrip.Refresh()

#Region "Entry and Version"
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    ComboVersion.SelectedIndex = Array.FindIndex(pbl_VersionAp, Function(f) f.lngVersion = pbl_lngVersion)
                    Exit Sub
                Else
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            pbl_lngVersion = pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion 'gewählten Index öffentlich machen
            VersionDescription.Text = pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung

#Region "Version Berechtigungen"
#End Region
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
                RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
                iGrid_ClearFilterAndGroups()
                AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

                ReadOnlyMode = False
                Reload_iGrid(pbl_dt)
            Catch
            End Try
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ComboVersion_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub ComboEntwurf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboEntwurf.SelectedIndexChanged
        Console.WriteLine("< Private Sub ComboEntwurf_SelectedIndexChanged")

        Try
            'Wird eine Entwurfsart geändert müssen folgende Daten neu geladen werden
            '1. Versionen: Daten laden + Combobox neu befüllen

            If con.State = ConnectionState.Closed Then Exit Sub

            ToolStripStatusOperation.Text = "Daten werden geladen..."
            StatusStrip.Refresh()

#Region "Entry and Entwurf"
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    ComboEntwurf.SelectedIndex = Array.FindIndex(pbl_Entwurf, Function(f) f.txtEntwurfstyp = pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp)
                    Exit Sub
                Else
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            pbl_IndexEntwurf = ComboEntwurf.SelectedIndex 'gewählten Index öffentlich machen
#End Region

#Region "Berechtigungen Version"
            'Abhängige Felder leeren:
            Try
                ComboVersion.Items.Clear()
                ComboVersion.Text = ""
            Catch
            End Try

            'Versionen
            Try
                Get_VersionArbeitspaket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)
                Dim IndexVersion As Integer = ViewCombo.View_ComboboxVersion(ComboVersion, 0, pbl_VersionAp)
                pbl_lngVersion = pbl_VersionAp(IndexVersion).lngVersion
                VersionDescription.Text = pbl_VersionAp(IndexVersion).txtVersionErklärung
            Catch
                pbl_lngVersion = -1
            End Try
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
                RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
                iGrid_ClearFilterAndGroups()
                AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

                ReadOnlyMode = False
                Reload_iGrid(pbl_dt)

                'Entwurfanwahl geändert -> Subscriptionstatusanzeige aktualisieren
                RefreshSubscriptionState(EmailNotificationState)    '!ab added
            Catch
            End Try
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ComboEntwurf_SelectedIndexChanged - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Edit Form - Checkbox(en)"
    Private Sub CheckedListGJ_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles CheckedListGJ.ItemCheck
        Console.WriteLine(">> Private Sub CheckedListGJ_ItemCheck")

        Try
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    Exit Sub
                Else
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            ToolStripStatusOperation.Text = "Daten werden geladen..."
            StatusStrip.Refresh()

#Region "Selected GJ"
            Erase pbl_LstSelGj

            If e.NewValue = CheckState.Checked Then 'Check/Uncheck wird erst nach dem Ereignis ausgelöst. Daher muss die Check Eigenschaft vorher umgesetzt werden
                If IsNothing(pbl_LstSelGj) Then
                    ReDim pbl_LstSelGj(0)
                Else
                    ReDim Preserve pbl_LstSelGj(UBound(pbl_LstSelGj) + 1)
                End If
                pbl_LstSelGj(UBound(pbl_LstSelGj)) = CheckedListGJ.Items(e.Index)
            Else

            End If

            For Each indexChecked As Object In CheckedListGJ.CheckedIndices
                If Not indexChecked = e.Index Then
                    If IsNothing(pbl_LstSelGj) Then
                        ReDim pbl_LstSelGj(0)
                    Else
                        ReDim Preserve pbl_LstSelGj(UBound(pbl_LstSelGj) + 1)
                    End If
                    pbl_LstSelGj(UBound(pbl_LstSelGj)) = CheckedListGJ.Items(indexChecked)
                End If
            Next
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If

                ReadOnlyMode = False
                Reload_iGrid(pbl_dt)
            Catch
            End Try
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()

        Catch ex As Exception
            MsgBox("Private Sub CheckedListGJ_ItemCheck: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub CheckListGJ_Load()
        Console.WriteLine("< CheckListGJ_Load")

        Dim i As Long, j As Long, TopIndex As Integer
        Try
#Region "Get GJ"
            CheckedListGJ.Items.Clear()

            For i = LBound(pbl_Gj) To UBound(pbl_Gj)
                CheckedListGJ.Items.Add(pbl_Gj(i).intGJ)
                If pbl_Gj(i).intGJ = Year(Now) Then
                    TopIndex = i
                End If
                If IsNothing(pbl_LstSelGj) Then 'Es gibt keine gespeicherten Jahre
                    If pbl_Gj(i).intGJ = Year(Now) Then 'Aktuelles GJ wird vorbelegt
                        RemoveHandler CheckedListGJ.ItemCheck, AddressOf CheckedListGJ_ItemCheck
                        CheckedListGJ.SetItemChecked(i, True) 'Setzt das zugehörige Kreuz
                        AddHandler CheckedListGJ.ItemCheck, AddressOf CheckedListGJ_ItemCheck
                        Erase pbl_LstSelGj
                        ReDim pbl_LstSelGj(0)
                        pbl_LstSelGj(0) = Year(Now)
                    End If
                Else
                    For j = LBound(pbl_LstSelGj) To UBound(pbl_LstSelGj)
                        If pbl_Gj(i).intGJ = pbl_LstSelGj(j) Then 'Aktuelles GJ wird vorbelegt
                            RemoveHandler CheckedListGJ.ItemCheck, AddressOf CheckedListGJ_ItemCheck
                            CheckedListGJ.SetItemChecked(i, True) 'Setzt das zugehörige Kreuz
                            AddHandler CheckedListGJ.ItemCheck, AddressOf CheckedListGJ_ItemCheck
                        End If
                    Next
                End If
            Next
            CheckedListGJ.TopIndex = TopIndex
#End Region
        Catch ex As Exception
            MsgBox("CheckListGJ_Load: " & ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region "Editform Events"
    Private Sub EditForm_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If (e.Control AndAlso (e.KeyCode = Keys.C)) Then
            ' When STRG + P is pressed, the Click event for the print
            ' button is raised.
            ContextMenuCopyToCache.PerformClick()
        End If
    End Sub
#End Region


#Region "Methoden"
    Function Load_EditForm()
        Console.WriteLine("< Function Load_EditForm")

        Try
#Region "Head and Version"
            ToolStripStatusOperation.Text = "Daten werden geladen..."
            Dim i As Long

            ShowFilter()

            TimerEditForm.Enabled = True
            Dim boolGotUserSettings As Boolean = False

            lblVersion.Text = My.Application.Info.Version.ToString()
#End Region

            Me.SuspendLayout()

#Region "Datenbank Verbindung"
            If con.State = ConnectionState.Open Then
                ToolStripStatusDatabase.Text = "[" & listConnections(pbl_SelectedConnection).System & "]"
            Else
                ToolStripStatusDatabase.Text = "[Offline]"
            End If
#End Region

#Region "Berechtigungen prüfen"
            CheckAuthorization()
#End Region

#Region "Tool Tips"
            ToolTipAddRow = New ToolTip()
            ToolTipAddRow.SetToolTip(AddRow, "Eine neue Zeile am Ende der Datensätze hinzufügen")

            ToolTipSaveData = New ToolTip()
            ToolTipSaveData.SetToolTip(SaveData, "Daten auf Datenbank speichern")

            ToolTipSaveData = New ToolTip()
            ToolTipSaveData.SetToolTip(ExcelExport, "Aktuelle Tabelle in Excel exportieren")

            ToolTipFooterCalcMethod = New ToolTip()
            ToolTipFooterCalcMethod.SetToolTip(FooterCalcMethod, "Berechnungsmethode der Summenzeile ändern")

            ToolTipMultiData = New ToolTip()
            ToolTipMultiData.SetToolTip(ShowMultiData, "Multi Datenauswahl Cockpit öffnen")

            ToolTipGetSapData = New ToolTip()
            ToolTipGetSapData.SetToolTip(cmdGetSap, "SAP Ist & Obligo Daten herunterladen")
#End Region

#Region "Compare Mode"
            lblCompare.Text = "Edit Mode"
            lblCompare.ForeColor = Color.DarkRed
            OvalFalse.Visible = True
            OvalTrue.Visible = False
            boolCompareMode = False
            If IsNothing(pbl_Sql) Then
                BorderData.BorderColor = Color.DarkRed
            Else
                BorderData.BorderColor = Color.Orange
            End If
#End Region

#Region "User Data"
            Try
                If pbl_dtUser.Rows(intCurrentUser).Item("qryProjekt") < 0 _
            Or pbl_dtUser.Rows(intCurrentUser).Item("qryArbeitspaket") < 0 _
            Or pbl_dtUser.Rows(intCurrentUser).Item("qryEntwurfstyp") = "" _
            Or pbl_dtUser.Rows(intCurrentUser).Item("qryVersion") < 0 Then  'Keine Usersettings vorhanden
                    boolGotUserSettings = False
                Else
                    boolGotUserSettings = True
                End If
            Catch
                boolGotUserSettings = False
            End Try

#End Region

#Region "Projekt Data"
            Try
                If boolGotUserSettings = False Then
                    pbl_IndexEntwurf = ViewCombo.View_ComboboxEntwurf(ComboEntwurf, 0, pbl_Entwurf) 'Entwurfstypen
                    pbl_IdProjekt = pbl_dtProjekt.Rows(ViewCombo.View_ComboboxProjekt(ComboProjekt, 0, pbl_dtProjekt)).Item("ID_PROJEKT") 'Projekte laden und ID speichern
                Else
                    pbl_IndexEntwurf = ViewCombo.View_ComboboxEntwurf(ComboEntwurf, Array.FindIndex(pbl_Entwurf, Function(f) f.txtEntwurfstyp = pbl_dtUser.Rows(intCurrentUser).Item("qryEntwurfstyp")), pbl_Entwurf) 'Entwurfstypen

                    Dim IndexProjekt As Integer
                    Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_dtUser.Rows(intCurrentUser).Item("qryProjekt") & "")
                    If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                        IndexProjekt = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                    Else
                        IndexProjekt = 0
                    End If
                    pbl_IdProjekt = pbl_dtProjekt.Rows(ViewCombo.View_ComboboxProjekt(ComboProjekt, IndexProjekt, pbl_dtProjekt)).Item("ID_PROJEKT") 'Projekte laden und ID speichern
                End If
            Catch
                MsgBox("Keine Berechtigung für Projekte vorhanden." & vbCrLf & "Die Nutzung jeglicher Funktionen kann zu Fehlern führen." & vbCrLf & "Bitte an den Administartor wenden.", vbCritical, "Fehlende Berechtigung(en)")
                Exit Function 'Ohne Projekte keine Arbeit möglich
            End Try
#End Region

#Region "Arbeitspaket Data"
            Try
                Get_tblArbeitspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False) 'Arbeitspakete abhängig vom Projekt laden
                If boolGotUserSettings = False Then
                    pbl_IdAp = pbl_dtArbeitspaket.Rows(ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, 0, pbl_dtArbeitspaket)).Item("ID_AP") 'Arbeitspakete
                Else
                    Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_dtUser.Rows(intCurrentUser).Item("qryArbeitspaket") & "")
                    Dim RowIndex As Integer = -1
                    If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                        RowIndex = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
                        pbl_IdAp = pbl_dtArbeitspaket.Rows(ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, RowIndex, pbl_dtArbeitspaket)).Item("ID_AP")
                    Else 'Erstes Arbeitspaket auswählen
                        pbl_IdAp = pbl_dtArbeitspaket.Rows(ViewCombo.View_ComboboxArbeitspaket(ComboArbeitspaket, 0, pbl_dtArbeitspaket)).Item("ID_AP")
                    End If
                End If
            Catch
                pbl_IdAp = 0
            End Try
#End Region

#Region "Entwicklungspaket"
            Try
                Get_Entwicklungspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT")) 'Entwicklungspakete
                Get_GJ(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT")) 'GJ abhängig vom Projekt laden
                If boolGotUserSettings = True Then
                    pbl_LstSelGj = ConvertStringToIntegerArray(pbl_dtUser.Rows(intCurrentUser).Item("qryGj"))
                End If
                CheckListGJ_Load()
            Catch
            End Try
#End Region

#Region "Paket & Version Arbeitspaket"
            Try
                Get_Paket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"))
                Get_VersionArbeitspaket(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp) 'Versionen abhängig vom Arbeitspaket laden
                If boolGotUserSettings = False Then
                    Dim IndexVersion = ViewCombo.View_ComboboxVersion(ComboVersion, 0, pbl_VersionAp)
                    pbl_lngVersion = pbl_VersionAp(IndexVersion).lngVersion 'Versionen
                    VersionDescription.Text = pbl_VersionAp(IndexVersion).txtVersionErklärung
                Else
                    Dim IndexVersion = ViewCombo.View_ComboboxVersion(ComboVersion, Array.FindIndex(pbl_VersionAp, Function(f) f.lngVersion = pbl_dtUser.Rows(intCurrentUser).Item("qryVersion")), pbl_VersionAp)
                    pbl_lngVersion = pbl_VersionAp(IndexVersion).lngVersion 'Versionen
                    VersionDescription.Text = pbl_VersionAp(IndexVersion).txtVersionErklärung
                End If
            Catch
                pbl_lngVersion = -1
            End Try
#End Region

#Region "Data"
            Try
                If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                Else
                    Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                End If
            Catch
            End Try

            If pbl_dt.Columns.Count = 0 Then 'Projekt, Arbeitspaket, ... existiert nicht: Trotzdem Dummy Daten laden um den Grid aufbauen zu können
                Dim DummyGj(0) As Integer
                DummyGj(0) = 9999
                Get_tblData(True, 0, 0, "Ist", True, DummyGj, pbl_Sql)
            End If

            Try
                ReadOnlyMode = False
                Initialize_iGrid(pbl_dt)
            Catch
            End Try
#End Region

#Region "Design and Finish"

            Me.ResumeLayout()
            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()

        Catch ex As Exception
            MsgBox("Function Load_EditForm: " & ex.Message.ToString)
        End Try
#End Region

#Region "Outlook Initialization"    '!ab added

        If InitializeEmailHandling() Then
            Me.ResumeLayout()
            ToolStripStatusOperation.Text = "Outlook erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        End If

        'Subscriptionstatusanzeige aktualisieren
        RefreshSubscriptionState(EmailNotificationState)    '!ab added

#End Region
    End Function

    Sub CheckAuthorization()
        Try
#Region "Berechtigungen"
            If Not LCase(GotRights("VERSION", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuNewVersion.Visible = False
            Else
                MenuNewVersion.Visible = True
            End If

            If Not LCase(GotRights("EP", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                EntwicklungspaketToolStripMenuItem1.Visible = False
            Else
                EntwicklungspaketToolStripMenuItem1.Visible = True
            End If

            If Not LCase(GotRights("AP", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                ArbeitspaketToolStripMenuItem1.Visible = False
            Else
                ArbeitspaketToolStripMenuItem1.Visible = True
            End If

            If Not LCase(GotRights("PROJEKT", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                ProjektToolStripMenuItem1.Visible = False
            Else
                ProjektToolStripMenuItem1.Visible = True
            End If

            If Not LCase(GotRights("IMPORT", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                SAPImportToolStripMenuItem.Visible = False
            Else
                SAPImportToolStripMenuItem.Visible = True
            End If

            If Not LCase(GotRights("TERMINE", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                TerminverfolgungToolStripMenuItem.Visible = False
            Else
                TerminverfolgungToolStripMenuItem.Visible = True
            End If

            If Not LCase(GotRights("KOPIEREN", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuCopy.Visible = False
            Else
                MenuCopy.Visible = True
            End If

            If Not LCase(GotRights("SAP_SYSTEM", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuSapSystem.Visible = False
            Else
                MenuSapSystem.Visible = True
            End If

            If Not LCase(GotRights("USERAUTH", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuPundAp.Visible = False
            Else
                MenuPundAp.Visible = True
            End If

            If Not LCase(GotRights("AP_LOESCHEN", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuDeleteAP.Visible = False
            Else
                MenuDeleteAP.Visible = True
            End If

            If Not LCase(GotRights("DATAVIASQL", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then
                MenuGetDataViaSqlStatement.Visible = False
            Else
                MenuGetDataViaSqlStatement.Visible = True
            End If

            If Not InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 AndAlso Not LCase(GotRights("PROJEKTMANAGER", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then 'Admin und Projektmanager dürfen Daten verändern
                MenuMehrereArbeitspaketeBearbeiten.Text = "n Arbeitspakete anzeigen"
            Else
                MenuMehrereArbeitspaketeBearbeiten.Text = "n Arbeitspakete bearbeiten"
            End If
        Catch
        End Try
#End Region
    End Sub
#End Region
#End Region

#Region "iGrid"

#Region "iGrid Behaviour"
    Sub Initialize_iGrid(dt As System.Data.DataTable)
        Console.WriteLine("< Sub Initialize_iGrid(dt As System.Data.DataTable)")

        iGrid.BeginUpdate()
        Try
            iGrid.Cols.Clear()
            iGrid.Rows.Clear()

            iGrid_Columns()
            '
            iGrid_ColNaming()

            iGrid_Dropdowns()

            iGrid_Visibility(False)

            iGrid_FillData(dt, True)

            iGrid_Design()

            iGrid_InitializeNumbers() 'Zahlen neu berechnen

            iGrid_ColAndRowWidth()

            iGrid_EnableEditFunctions(True, True, "Bearbeitung möglich")
        Catch ex As Exception
            MsgBox("Initialize_iGrid: " & ex.Message.ToString)
        End Try
        iGrid.EndUpdate()
    End Sub

    Sub Reload_iGrid(dt As System.Data.DataTable)
        Console.WriteLine("< Sub Reload_iGrid")

        iGrid.BeginUpdate()
        Try
            iGrid_FillData(dt, True)

            iGrid_InitializeNumbers() 'Zahlen neu berechnen

            iGrid_ColAndRowWidth()

            iGrid_Visibility(False)

            iGrid_EnableEditFunctions(True, True, "Bearbeitung möglich")

            boolDataChanged = False
        Catch ex As Exception
            MsgBox("Initialize_iGrid: " & ex.Message.ToString)
        End Try
        iGrid.EndUpdate()
    End Sub

    Sub iGrid_Columns() 'Defines the iGrid columns
        Dim i As Integer, curCol As Integer

        Try
            If pbl_dt.Columns.Count > 0 Then
                For i = 0 To pbl_dt.Columns.Count - 1
                    iGrid.Cols.Add()
                    curCol = iGrid.Cols.Count - 1
                    'Define column
                    iGrid.Cols(curCol).Key = pbl_dt.Columns(i).ColumnName
                    iGrid.Cols(curCol).CellStyle.ValueType = pbl_dt.Columns(i).DataType
                Next
            End If
        Catch ex As Exception
            MsgBox("iGrid_Columns: " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_Dropdowns() 'Define Dropdowns
        Dim i As Long
        Try

            iGridDropDown_GJ.Items.Clear()
            For i = LBound(pbl_Gj) To UBound(pbl_Gj)
                iGridDropDown_GJ.Items.Add(pbl_Gj(i).intGJ)
            Next

            iGridDropDown_Entwicklungspaket.Items.Clear()
            For i = LBound(pbl_Ep) To UBound(pbl_Ep)
                iGridDropDown_Entwicklungspaket.Items.Add(pbl_Ep(i).txtEntwicklungspaket)
            Next

            iGridDropDown_Paket.Items.Clear()
            For i = LBound(pbl_Paket) To UBound(pbl_Paket)
                iGridDropDown_Paket.Items.Add(pbl_Paket(i).txtPaket)
            Next

            iGridDropDown_Kostenart.Items.Clear()
            iGridDropDown_Kostenart.ImageList = ILKostenart

            For i = LBound(pbl_KstArt) To UBound(pbl_KstArt)
                If pbl_KstArt(i).txtKostenart = "Prototypenmaterial" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 0)
                ElseIf pbl_KstArt(i).txtKostenart = "Sonstiges Material" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 1)
                ElseIf pbl_KstArt(i).txtKostenart = "Arbeitskosten" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 2)
                ElseIf pbl_KstArt(i).txtKostenart = "Fremdpersonal" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 3)
                ElseIf pbl_KstArt(i).txtKostenart = "Leistungsarten" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 4)
                ElseIf pbl_KstArt(i).txtKostenart = "Sonstige Kosten" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 5)
                ElseIf pbl_KstArt(i).txtKostenart = "Fremd-E" Then
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 6)
                Else
                    iGridDropDown_Kostenart.Items.Add(pbl_KstArt(i).txtKostenart, 7)
                End If
            Next

            iGridDropDown_Klasse.Items.Clear()

            For i = LBound(pbl_Klasse) To UBound(pbl_Klasse)
                iGridDropDown_Klasse.Items.Add(pbl_Klasse(i).txtKlasse)
            Next
        Catch ex As Exception
            MsgBox("iGrid_Dropdowns: " & ex.Message.ToString)
        End Try

    End Sub

    Sub iGrid_ColNaming() 'Naming and Order and DropDown assignment and Read/Write
        Try
            iGrid.Cols("lngSort").Text = "Nr."
            iGrid.Cols("lngSort").Order = 0

            iGrid.Cols("Projekt").Text = "Projekt"
            iGrid.Cols("Projekt").Order = 1

            iGrid.Cols("txtArbeitspaket").Text = "Arbeitspaket"
            iGrid.Cols("txtArbeitspaket").Order = 2

            iGrid.Cols("qryGeschäftsjahr").Text = "GJ"
            iGrid.Cols("qryGeschäftsjahr").Order = 3
            iGrid.Cols("qryGeschäftsjahr").CellStyle.DropDownControl = iGridDropDown_GJ

            iGrid.Cols("txtBeschreibung").Text = "Beschreibung"
            iGrid.Cols("txtBeschreibung").Order = 4

            iGrid.Cols("qryPaket").Text = "Paket"
            iGrid.Cols("qryPaket").Order = 5
            iGrid.Cols("qryPaket").CellStyle.DropDownControl = iGridDropDown_Paket

            iGrid.Cols("qryEntwicklungspaket").Text = "Entwicklungspaket"
            iGrid.Cols("qryEntwicklungspaket").Order = 6
            iGrid.Cols("qryEntwicklungspaket").CellStyle.DropDownControl = iGridDropDown_Entwicklungspaket

            iGrid.Cols("qryKostenart").Text = "Kostenart"
            iGrid.Cols("qryKostenart").Order = 7
            iGrid.Cols("qryKostenart").CellStyle.DropDownControl = iGridDropDown_Kostenart
            iGrid.Cols("qryKostenart").CellStyle.ImageList = iGridDropDown_Kostenart.ImageList

            iGrid.Cols("txtKst_Lst").Text = "Sachkonto"
            iGrid.Cols("txtKst_Lst").Order = 8

            iGrid.Cols("txtBedarfsnummer").Text = "KST / BEDNR / AUF"
            iGrid.Cols("txtBedarfsnummer").Order = 9

            iGrid.Cols("CostsPerYear").Text = "Kosten pro Jahr"
            iGrid.Cols("CostsPerYear").Order = 10
            iGrid.Cols("CostsPerYear").CellStyle.ReadOnly = False

            iGrid.Cols("AmountPerYear").Text = "Anzahl pro Jahr"
            iGrid.Cols("AmountPerYear").Order = 11
            iGrid.Cols("AmountPerYear").CellStyle.ReadOnly = False

            iGrid.Cols("curKosten_pro_Einheit").Text = "Kosten pro Einheit"
            iGrid.Cols("curKosten_pro_Einheit").Order = 12

            iGrid.Cols("dblJanuar").Text = "Januar | Q1"
            iGrid.Cols("dblJanuar").Order = 13

            iGrid.Cols("dblFebruar").Text = "Februar"
            iGrid.Cols("dblFebruar").Order = 14

            iGrid.Cols("dblMärz").Text = "März"
            iGrid.Cols("dblMärz").Order = 15

            iGrid.Cols("dblApril").Text = "April | Q2"
            iGrid.Cols("dblApril").Order = 16

            iGrid.Cols("dblMai").Text = "Mai"
            iGrid.Cols("dblMai").Order = 17

            iGrid.Cols("dblJuni").Text = "Juni"
            iGrid.Cols("dblJuni").Order = 18

            iGrid.Cols("dblJuli").Text = "Juli | Q3"
            iGrid.Cols("dblJuli").Order = 19

            iGrid.Cols("dblAugust").Text = "August"
            iGrid.Cols("dblAugust").Order = 20

            iGrid.Cols("dblSeptember").Text = "September"
            iGrid.Cols("dblSeptember").Order = 21

            iGrid.Cols("dblOktober").Text = "Oktober | Q4"
            iGrid.Cols("dblOktober").Order = 22

            iGrid.Cols("dblNovember").Text = "November"
            iGrid.Cols("dblNovember").Order = 23

            iGrid.Cols("dblDezember").Text = "Dezember"
            iGrid.Cols("dblDezember").Order = 24

            iGrid.Cols("qryKlassifizierung").Text = "Klassifizierung"
            iGrid.Cols("qryKlassifizierung").Order = 25
            iGrid.Cols("qryKlassifizierung").CellStyle.DropDownControl = iGridDropDown_Klasse

            iGrid.Cols("boolObligo").Text = "Obligo"
            iGrid.Cols("boolObligo").Order = 26
            iGrid.Cols("boolObligo").CellStyle.Type = TenTec.Windows.iGridLib.iGCellType.Check
            iGrid.Cols("boolObligo").CellStyle.ImageAlign = iGContentAlignment.TopCenter

            iGrid.Cols("boolRenneinsatz").Text = "Renneinsatz"
            iGrid.Cols("boolRenneinsatz").Order = 27
            iGrid.Cols("boolRenneinsatz").CellStyle.Type = TenTec.Windows.iGridLib.iGCellType.Check
            iGrid.Cols("boolRenneinsatz").CellStyle.ImageAlign = iGContentAlignment.TopCenter

            iGrid.Cols("txtBemerkung").Text = "Bemerkung (Kommentar1)"
            iGrid.Cols("txtBemerkung").Order = 28

            iGrid.Cols("txtZusatzfeld").Text = "Zusatzbemerkung (Kommentar2)"
            iGrid.Cols("txtZusatzfeld").Order = 29

            iGrid.Cols("ID_DATA").Text = "ID Data"
            iGrid.Cols("ID_DATA").Order = 30

            iGrid.Cols("qryArbeitspaket").Text = "ID Arbeitspaket"
            iGrid.Cols("qryArbeitspaket").Order = 31

            iGrid.Cols("qryVersion").Text = "Version"
            iGrid.Cols("qryVersion").Order = 32

            iGrid.Cols("qryEntwurfstyp").Text = "Entwurfstyp"
            iGrid.Cols("qryEntwurfstyp").Order = 33

            iGrid.Cols("PROJEKT_ID").Text = "PROJEKT_ID"
            iGrid.Cols("PROJEKT_ID").Order = 34

            iGrid.Cols("datErstellungsdatum").Text = "Zeitstempel"
            iGrid.Cols("datErstellungsdatum").Order = 35

            iGrid.Cols("Benutzer").Text = "Benutzer"
            iGrid.Cols("Benutzer").Order = 36

            iGrid.Cols("IdSap").Text = "IdSap"
            iGrid.Cols("IdSap").Order = 37

            iGrid.Cols("PSP").Text = "PSP"
            iGrid.Cols("PSP").Order = 38

            iGrid.Cols("PSP_Verantw").Text = "Verantwortlich"
            iGrid.Cols("PSP_Verantw").Order = 39

            iGrid.Cols.Add("Action") 'Letzte Zeile
            iGrid.Cols(iGrid.Cols.Count - 1).Key = "Action"
            iGrid.Cols(iGrid.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)
        Catch ex As Exception
            MsgBox("iGrid_ColNaming : " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_Visibility(ShowMore As Boolean) 'Visibility of iGrid Columns
        Console.WriteLine("< Sub iGrid_Visibility")

        Try
            If InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then 'Admin sieht den Action Log immer
                iGrid.Cols("ID_DATA").Visible = True
                iGrid.Cols("qryArbeitspaket").Visible = True
                iGrid.Cols("txtArbeitspaket").Visible = True
                iGrid.Cols("qryVersion").Visible = True
                iGrid.Cols("qryEntwurfstyp").Visible = True
                iGrid.Cols("datErstellungsdatum").Visible = True
                iGrid.Cols("PROJEKT_ID").Visible = True
                iGrid.Cols("Projekt").Visible = True
                iGrid.Cols("Action").Visible = True
                iGrid.Cols("IdSap").Visible = True
                iGrid.Cols("PSP").Visible = True
                iGrid.Cols("PSP_Verantw").Visible = True
            ElseIf ShowMore = False Then
                iGrid.Cols("ID_DATA").Visible = False
                iGrid.Cols("qryArbeitspaket").Visible = False
                iGrid.Cols("txtArbeitspaket").Visible = False
                iGrid.Cols("qryVersion").Visible = False
                iGrid.Cols("qryEntwurfstyp").Visible = False
                iGrid.Cols("datErstellungsdatum").Visible = True
                iGrid.Cols("PROJEKT_ID").Visible = False
                iGrid.Cols("Projekt").Visible = False
                iGrid.Cols("Action").Visible = False
                iGrid.Cols("IdSap").Visible = False
                iGrid.Cols("PSP").Visible = True
                iGrid.Cols("PSP_Verantw").Visible = False
            Else 'Projekt und Arbeitspaket anzeigen
                iGrid.Cols("ID_DATA").Visible = False
                iGrid.Cols("qryArbeitspaket").Visible = False
                iGrid.Cols("txtArbeitspaket").Visible = True
                iGrid.Cols("qryVersion").Visible = False
                iGrid.Cols("qryEntwurfstyp").Visible = True
                iGrid.Cols("datErstellungsdatum").Visible = True
                iGrid.Cols("PROJEKT_ID").Visible = False
                iGrid.Cols("Projekt").Visible = True
                iGrid.Cols("Action").Visible = False
                iGrid.Cols("IdSap").Visible = False
                iGrid.Cols("PSP").Visible = True
                iGrid.Cols("PSP_Verantw").Visible = True
            End If
        Catch ex As Exception
            MsgBox("iGrid_Visibility : " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_Design() 'Design and Look&Feel
        Try
            'Styles zuordnen incl. DropDown
            iGrid.Cols("qryGeschäftsjahr").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("qryGeschäftsjahr").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("txtBeschreibung").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("txtBeschreibung").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("txtKst_Lst").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("txtKst_Lst").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("txtBedarfsnummer").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("txtBedarfsnummer").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("qryKostenart").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("qryKostenart").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("CostsPerYear").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("CostsPerYear").CellStyle.BackColor = Color.LightGray
            iGrid.Cols("CostsPerYear").CellStyle.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Cols("CostsPerYear").CellStyle.FormatString = "{0:N2} €"
            iGrid.Cols("AmountPerYear").CellStyle.ForeColor = Color.DarkRed
            iGrid.Cols("AmountPerYear").CellStyle.BackColor = Color.LightGray
            iGrid.Cols("AmountPerYear").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("curKosten_pro_Einheit").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblJanuar").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblFebruar").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblMärz").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblApril").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblMai").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblJuni").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblJuli").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblAugust").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblSeptember").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblOktober").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblNovember").CellStyle.FormatString = "{0:N2}"
            iGrid.Cols("dblDezember").CellStyle.FormatString = "{0:N2}"

            'Header Row
            iGrid.Header.Font = New Font(iGrid.Font, FontStyle.Bold)
            iGrid.Header.ForeColor = Color.Black

            ' Make all the group rows semi-transparent. To see the effect check the Background Picture option.
            fCellStyleSubTotals.BackColor = Color.FromArgb(110, fCellStyleSubTotals.BackColor)
            iGrid.GroupRowLevelStyles(0).BackColor = fCellStyleSubTotals.BackColor

            'Spalten Einfrieren
            iGrid.FrozenArea.ColCount = 5 'Nach Beschreibung
        Catch ex As Exception
            MsgBox("iGrid_Design : " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_ColAndRowWidth()
        Console.WriteLine("< Sub iGrid_ColAndRowWidth()")

        Try
            iGrid.Cols.AutoWidth() 'Autom Spaltenbreite

            'Definierte Spaltenbreiten
            iGrid.Cols("txtBeschreibung").Width = 350
            iGrid.Cols("txtBeschreibung").CellStyle.TextFormatFlags = TenTec.Windows.iGridLib.iGStringFormatFlags.WordWrap

            iGrid.Cols("qryPaket").Width = 200
            iGrid.Cols("qryPaket").CellStyle.TextFormatFlags = TenTec.Windows.iGridLib.iGStringFormatFlags.WordWrap

            iGrid.Cols("qryEntwicklungspaket").Width = 200
            iGrid.Cols("qryEntwicklungspaket").CellStyle.TextFormatFlags = TenTec.Windows.iGridLib.iGStringFormatFlags.WordWrap

            iGrid.Cols("txtBemerkung").Width = 300
            iGrid.Cols("txtBemerkung").CellStyle.TextFormatFlags = TenTec.Windows.iGridLib.iGStringFormatFlags.WordWrap

            iGrid.Cols("txtZusatzfeld").Width = 300
            iGrid.Cols("txtZusatzfeld").CellStyle.TextFormatFlags = TenTec.Windows.iGridLib.iGStringFormatFlags.WordWrap

            If Not iGrid.Rows.Count = 0 Then
                iGrid.Rows.AutoHeight() 'Autom. Zeilenhöhe
            End If
        Catch ex As Exception
            MsgBox("iGrid_ColAndRowWidth: " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_Footer()
        Console.WriteLine("< Sub iGrid_Footer()")

        Dim i As Integer
        Try
            If boolCompareMode = False Then
                iGrid.Footer.Visible = True
                FooterCalcMethod.Visible = True
                SplitExcel.Visible = True
                iGrid.Footer.BackColor = Color.LightGray
                iGrid.Footer.ForeColor = Color.Black
                iGrid.Footer.Font = New Font(iGrid.Font, FontStyle.Bold)
                iGrid.Footer.Rows(0).Cells("CostsPerYear").AggregateFunction = iGAggregateFunction.Sum
                iGrid.Footer.Rows(0).Cells("AmountPerYear").AggregateFunction = iGAggregateFunction.Sum

                Dim Sum() As Double
                Erase Sum
                ReDim Sum(12)
                If FooterCalculationMethod = 0 Then 'Kosten pro Monat
                    iGrid.Footer.Rows(0).Cells("txtBeschreibung").Value = "Kosten pro Monat (Kosten pro Einheit * Menge)"
                    FooterCalcMethod.Image = ILFooter.Images(0)
                    For i = 0 To iGrid.Rows.Count - 1 'Geht alle Zeilen durch
                        If iGrid.Rows(i).Visible = True Then
                            For j = 1 To 12 'Monate
                                Sum(j) = Sum(j) + (iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value * iGrid.Rows(i).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value)
                            Next
                        End If
                    Next
                    For j = 1 To 12
                        iGrid.Footer.Rows(0).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value = Sum(j)
                    Next
                ElseIf FooterCalculationMethod = 1 Then 'Anzahl pro Monat
                    iGrid.Footer.Rows(0).Cells("txtBeschreibung").Value = "Menge pro Monat"
                    FooterCalcMethod.Image = ILFooter.Images(1)
                    For i = 0 To iGrid.Rows.Count - 1 'Geht alle Zeilen durch
                        If iGrid.Rows(i).Visible = True Then
                            For j = 1 To 12 'Monate
                                Sum(j) = Sum(j) + iGrid.Rows(i).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value
                            Next
                        End If
                    Next
                    For j = 1 To 12
                        iGrid.Footer.Rows(0).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value = Sum(j)
                    Next
                ElseIf FooterCalculationMethod = 2 Then 'FTE pro Monat
                    iGrid.Footer.Rows(0).Cells("txtBeschreibung").Value = "FTE pro Monat (Bei 1800h pro FTE pro Jahr)"
                    FooterCalcMethod.Image = ILFooter.Images(2)
                    For i = 0 To iGrid.Rows.Count - 1 'Geht alle Zeilen durch
                        If iGrid.Rows(i).Visible = True Then
                            For j = 1 To 12 'Monate
                                Sum(j) = Sum(j) + (iGrid.Rows(i).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value / 1800 * 12) '1800h/Jahr als Fixwert
                            Next
                        End If
                    Next
                    For j = 1 To 12
                        iGrid.Footer.Rows(0).Cells(ConvertIntegerToDoubleMonth(j).dblMonat).Value = Sum(j)
                    Next
                End If
            Else
                iGrid.Footer.Visible = False
                FooterCalcMethod.Visible = False
                SplitExcel.Visible = False
            End If
        Catch ex As Exception
            MsgBox("iGrid_Footer : " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_EnableEditFunctions(EnableEdit As Boolean, EnableSave As Boolean, text As String)
        Console.WriteLine("< Sub iGrid_EnableEditFunctions")

        Try
#Region "Abhängigkeit von Version oder Entwurfstyp"
            'Die Eigenschaften der Version oder des Entwurfstyps übersteuern die Input Parameter
            If EnableSave = True Then 'Prüfen, ob Berechtigungen die Editierrechte überschreiben
                If GotRights("ID_AP", pbl_IdAp, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth) = "read" Then
                    EnableEdit = False
                    EnableSave = False
                    text = "Nur Leserechte"
                    VersionToolStripMenuItem.Enabled = False
                Else
                    If pbl_VersionAp(ComboVersion.SelectedIndex).cbxFreigabe = True And pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp <> "Ist" Then 'Daten sind freigegeben = Keine Änderung möglich
                        EnableEdit = False
                        EnableSave = False
                        text = "Daten freigegeben"
                        VersionToolStripMenuItem.Enabled = True
                    ElseIf pbl_VersionAp(ComboVersion.SelectedIndex).cbxFreigabe = True And pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                        EnableEdit = False
                        EnableSave = True
                        VersionToolStripMenuItem.Enabled = False
                    ElseIf Not pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                        EnableEdit = True
                        EnableSave = True
                        VersionToolStripMenuItem.Enabled = True
                    ElseIf pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                        EnableEdit = False
                        EnableSave = True
                        VersionToolStripMenuItem.Enabled = False
                    End If
                End If
            End If
#End Region

#Region "Abhängigkeit vom Gruppier- und Compare Modus"
            If EnableEdit = True Then
                If Not iGrid.GroupObject.Count = 0 Then 'Im Gruppierungsmodus dürfen nur bestehende Datensätze geändert werden
                    EnableEdit = False
                End If
                If boolCompareMode = True Then
                    EnableEdit = False
                End If
            End If
#End Region

            If ReadOnlyMode = True Then
                EnableEdit = False
                EnableSave = False
                text = "Nur Lesen möglich"
            End If

#Region "Aktionen erlauben / verbieten"
            If EnableEdit = False And EnableSave = False Then 'Grid Daten wurden gruppiert: Bestimmte Funktionen abschalten!
                'iGrid.ReadOnly = True
                iGrid_ColsReadOnly(EnableEdit, EnableSave)
                SaveData.Visible = False
                AddRow.Visible = False
                SplitAddRow.Visible = False
                ContextKopieren.Enabled = True 'Kopieren geht immer
                ContextEinfügen.Enabled = False
                ContextLöschen.Enabled = False
                lblBearbeitung.Text = text
                lblBearbeitung.ForeColor = Color.DimGray
                ComboVersion.ForeColor = Color.DimGray
            ElseIf EnableEdit = False And EnableSave = True Then
                'iGrid.ReadOnly = True 'Bearbeitbare Spalten werden extra frei geschaltet.
                iGrid_ColsReadOnly(EnableEdit, EnableSave)
                SaveData.Visible = True
                AddRow.Visible = False
                SplitAddRow.Visible = False
                ContextKopieren.Enabled = True 'Kopieren geht immer
                ContextEinfügen.Enabled = False
                ContextLöschen.Enabled = False
                lblBearbeitung.Text = "Nur Änderungen möglich"
                lblBearbeitung.ForeColor = Color.SteelBlue
                ComboVersion.ForeColor = Color.DimGray
            ElseIf EnableEdit = True Then
                'iGrid.ReadOnly = False
                iGrid_ColsReadOnly(EnableEdit, EnableSave)
                SaveData.Visible = True
                AddRow.Visible = True
                SplitAddRow.Visible = True
                ContextKopieren.Enabled = True 'Kopieren geht immer
                ContextEinfügen.Enabled = True
                ContextLöschen.Enabled = True
                lblBearbeitung.Text = "Bearbeitung möglich"
                lblBearbeitung.ForeColor = Color.DarkGreen
                ComboVersion.ForeColor = Color.Black
            End If
#End Region
        Catch ex As Exception
            MsgBox("Rechte: " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_ColsReadOnly(EnableEdit As Boolean, EnableSave As Boolean)
        Console.WriteLine("< Sub iGrid_ColsReadOnly")

        Try
            'ACHTUNG: "false" bedeutet dass die Spalte ReadOnly ist, "true" ist ReadWrite

            If EnableEdit = True AndAlso EnableSave = True Then 'Alles auf ReadWrite stellen
                iGrid.ReadOnly = False
                If iGrid.Cols.Count > 0 Then
                    For z = 0 To iGrid.Cols.Count - 1 'Alle Spalten ReadWrite
                        iGrid.Cols(z).CellStyle.ReadOnly = True
                        iGrid.Cols(z).CellStyle.BackColor = Color.White
                    Next
                End If

            Else
                If iGrid.Cols.Count > 0 Then
                    For z = 0 To iGrid.Cols.Count - 1 'Alle Spalten ReadOnly
                        iGrid.Cols(z).CellStyle.ReadOnly = False
                        iGrid.Cols(z).CellStyle.BackColor = Color.WhiteSmoke
                    Next
                End If

                If EnableSave = True Then
#Region "Bestimmte Spalten zur Bearbeitung freigeben"
                    If boolCompareMode = True Then
                        iGrid.Cols("curKosten_pro_Einheit").CellStyle.ReadOnly = True
                        iGrid.Cols("curKosten_pro_Einheit").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblJanuar").CellStyle.ReadOnly = True
                        iGrid.Cols("dblJanuar").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblFebruar").CellStyle.ReadOnly = True
                        iGrid.Cols("dblFebruar").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblMärz").CellStyle.ReadOnly = True
                        iGrid.Cols("dblMärz").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblApril").CellStyle.ReadOnly = True
                        iGrid.Cols("dblApril").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblMai").CellStyle.ReadOnly = True
                        iGrid.Cols("dblMai").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblJuni").CellStyle.ReadOnly = True
                        iGrid.Cols("dblJuni").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblJuli").CellStyle.ReadOnly = True
                        iGrid.Cols("dblJuli").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblAugust").CellStyle.ReadOnly = True
                        iGrid.Cols("dblAugust").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblSeptember").CellStyle.ReadOnly = True
                        iGrid.Cols("dblSeptember").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblOktober").CellStyle.ReadOnly = True
                        iGrid.Cols("dblOktober").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblNovember").CellStyle.ReadOnly = True
                        iGrid.Cols("dblNovember").CellStyle.BackColor = Color.White

                        iGrid.Cols("dblDezember").CellStyle.ReadOnly = True
                        iGrid.Cols("dblDezember").CellStyle.BackColor = Color.White
                    ElseIf pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then
                        iGrid.Cols("txtBemerkung").CellStyle.ReadOnly = True
                        iGrid.Cols("txtBemerkung").CellStyle.BackColor = Color.White

                        iGrid.Cols("txtZusatzfeld").CellStyle.ReadOnly = True
                        iGrid.Cols("txtZusatzfeld").CellStyle.BackColor = Color.White
                    Else 'Gruppierung im Edit Mode
                        If iGrid.Cols.Count > 0 Then
                            For z = 0 To iGrid.Cols.Count - 1 'Alle Spalten ReadWrite
                                iGrid.Cols(z).CellStyle.ReadOnly = True
                                iGrid.Cols(z).CellStyle.BackColor = Color.White
                            Next
                        End If
#End Region
                    End If
                Else 'Alles ReadOnly
                    iGrid.ReadOnly = True
                End If
            End If

#Region "Spalten, die immer schreibgeschützt sind"
            iGrid.Cols("CostsPerYear").CellStyle.ReadOnly = False
            iGrid.Cols("CostsPerYear").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("AmountPerYear").CellStyle.ReadOnly = False
            iGrid.Cols("AmountPerYear").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("Projekt").CellStyle.ReadOnly = False
            iGrid.Cols("Projekt").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("PSP_Verantw").CellStyle.ReadOnly = False
            iGrid.Cols("PSP_Verantw").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("txtArbeitspaket").CellStyle.ReadOnly = False
            iGrid.Cols("txtArbeitspaket").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("AmountPerYear").CellStyle.ReadOnly = False
            iGrid.Cols("AmountPerYear").CellStyle.BackColor = Color.WhiteSmoke

            iGrid.Cols("ID_DATA").CellStyle.ReadOnly = False
            iGrid.Cols("ID_DATA").CellStyle.BackColor = Color.WhiteSmoke
#End Region
        Catch ex As Exception
            MsgBox("iGrid_ColsReadOnly: " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_DefaultSort()
        Console.WriteLine("< Sub iGrid_DefaultSort()")

        Try
            If iGrid.SortObject.Count > 0 Then 'Alte Filter löschen
                iGrid.SortObject.Clear()
            End If
            If boolCompareMode = False Then 'Projekt, Arbeitspaket, Nr.
                iGrid.SortObject.Add("PROJEKT_ID", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("qryArbeitspaket", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("lngSort", iGSortOrder.Ascending, iGSortType.ByValue)
            Else 'Projekt, Arbeitspaket, Jahr, Monat, Entwurfstpy, Kosten
                iGrid.SortObject.Add("PROJEKT_ID", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("qryArbeitspaket", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("qryGeschäftsjahr", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("lngSort", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("qryEntwurfstyp", iGSortOrder.Ascending, iGSortType.ByValue)
                iGrid.SortObject.Add("CostsPerYear", iGSortOrder.Descending, iGSortType.ByValue)
            End If

            'If iGrid.GroupObject.Count > 0 Then
            '    MsgBox(iGrid.GroupObject.Count & " Gruppierungen")
            'End If
            ''Try
            '    If iGrid.SortObject.Count > 0 Then
            '        iGrid.Sort() 'Gesetzte Sortierung beibehalten
            '    End If
            'Catch
            'End Try

            'If iGridAutoFilter.ProcessedRowCount > 0 Then
            '    iGridAutoFilter.ReapplyFilter() 'Gesetzte Filter beibehalten
            'End If
        Catch ex As Exception
            MsgBox("iGrid_SortData: " & ex.Message.ToString)
        End Try
    End Sub


#Region "iGrid Events"
    Private Sub iGrid_AfterCommitEdit(sender As Object, e As TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs) Handles iGrid.AfterCommitEdit
        Console.WriteLine("< Private Sub iGrid_AfterCommitEdit")

        Dim i As Integer
        Try
            If Not IsNothing(iGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value) Then
                If Not CheckCellValueChanged = iGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value.ToString Then 'Datensatz wurde geändert
                    boolDataChanged = True 'Es haben sich Daten geändert

#Region "Define Action"
                    If String.IsNullOrEmpty(iGrid.Rows(e.RowIndex).Cells("ID_DATA").Value) Then
                        iGrid.Rows(e.RowIndex).Cells("Action").Value = "N" 'New

                        If String.IsNullOrEmpty(iGrid.Rows(e.RowIndex).Cells("lngSort").Value) Then
                            If e.RowIndex = 0 Then 'Nummerierung anpassen
                                iGrid.Rows(e.RowIndex).Cells("lngSort").Value = 100
                            Else
                                iGrid.Rows(e.RowIndex).Cells("lngSort").Value = iGrid.Rows(e.RowIndex - 1).Cells("lngSort").Value + 100
                            End If
                        End If

                    ElseIf iGrid.Rows(e.RowIndex).Cells("Action").Value = "D" Then
                        iGrid.Rows(e.RowIndex).Cells("Action").Value = "D" 'Delete
                    Else
                        iGrid.Rows(e.RowIndex).Cells("Action").Value = "C" 'Change
                    End If
#End Region

#Region "Update Calculated Numbers"
                    If iGrid.Cols(e.ColIndex).Key = "curKosten_pro_Einheit" Or iGrid.Cols(e.ColIndex).Key = "dblJanuar" Or iGrid.Cols(e.ColIndex).Key = "dblFebruar" Or iGrid.Cols(e.ColIndex).Key = "dblMärz" Or iGrid.Cols(e.ColIndex).Key = "dblApril" Or iGrid.Cols(e.ColIndex).Key = "dblMai" Or iGrid.Cols(e.ColIndex).Key = "dblJuni" Or iGrid.Cols(e.ColIndex).Key = "dblJuli" Or iGrid.Cols(e.ColIndex).Key = "dblAugust" Or iGrid.Cols(e.ColIndex).Key = "dblSeptember" Or iGrid.Cols(e.ColIndex).Key = "dblOktober" Or iGrid.Cols(e.ColIndex).Key = "dblNovember" Or iGrid.Cols(e.ColIndex).Key = "dblDezember" Then
                        iGrid.Rows(e.RowIndex).Cells("AmountPerYear").Value = iGrid.Rows(e.RowIndex).Cells("dblJanuar").Value + iGrid.Rows(e.RowIndex).Cells("dblFebruar").Value + iGrid.Rows(e.RowIndex).Cells("dblMärz").Value + iGrid.Rows(e.RowIndex).Cells("dblApril").Value + iGrid.Rows(e.RowIndex).Cells("dblMai").Value + iGrid.Rows(e.RowIndex).Cells("dblJuni").Value + iGrid.Rows(e.RowIndex).Cells("dblJuli").Value + iGrid.Rows(e.RowIndex).Cells("dblAugust").Value + iGrid.Rows(e.RowIndex).Cells("dblSeptember").Value + iGrid.Rows(e.RowIndex).Cells("dblOktober").Value + iGrid.Rows(e.RowIndex).Cells("dblNovember").Value + iGrid.Rows(e.RowIndex).Cells("dblDezember").Value
                        iGrid.Rows(e.RowIndex).Cells("CostsPerYear").Value = iGrid.Rows(e.RowIndex).Cells("AmountPerYear").Value * iGrid.Rows(e.RowIndex).Cells("curKosten_pro_Einheit").Value
                        If boolCompareMode = True Then 'In allen Zeilen mit der gleichen ID_DATA ein Update durchführen, wenn sich der Preis pro Einheit geändert hat
                            If iGrid.Cols(e.ColIndex).Key = "curKosten_pro_Einheit" Then
                                For i = 0 To iGrid.Rows.Count - 1
                                    If iGrid.Rows(i).Cells("ID_DATA").Value = iGrid.Rows(e.RowIndex).Cells("ID_DATA").Value Then
                                        If Not i = e.RowIndex Then
                                            iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value = iGrid.Rows(e.RowIndex).Cells("curKosten_pro_Einheit").Value
                                            iGrid.Rows(i).Cells("AmountPerYear").Value = iGrid.Rows(i).Cells("dblJanuar").Value + iGrid.Rows(i).Cells("dblFebruar").Value + iGrid.Rows(i).Cells("dblMärz").Value + iGrid.Rows(i).Cells("dblApril").Value + iGrid.Rows(i).Cells("dblMai").Value + iGrid.Rows(i).Cells("dblJuni").Value + iGrid.Rows(i).Cells("dblJuli").Value + iGrid.Rows(i).Cells("dblAugust").Value + iGrid.Rows(i).Cells("dblSeptember").Value + iGrid.Rows(i).Cells("dblOktober").Value + iGrid.Rows(i).Cells("dblNovember").Value + iGrid.Rows(i).Cells("dblDezember").Value
                                            iGrid.Rows(i).Cells("CostsPerYear").Value = iGrid.Rows(i).Cells("AmountPerYear").Value * iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    End If

                    iGrid_Footer() 'Zahlen neu berechnen
                    iGrid_CreateSumRows()

                    iGrid.CurRow = iGrid.Rows(e.RowIndex) 'Set Fokus

#End Region
                End If
            End If
        Catch ex As Exception
            MsgBox("iGrid_AfterCommitEdit : " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub iGrid_BeforeCommitEdit(sender As Object, e As TenTec.Windows.iGridLib.iGBeforeCommitEditEventArgs) Handles iGrid.BeforeCommitEdit
        Console.WriteLine("< Private Sub iGrid_BeforeCommitEdit")

        Try
            If Not String.IsNullOrEmpty(iGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value) Then
                CheckCellValueChanged = iGrid.Rows(e.RowIndex).Cells(e.ColIndex).Value.ToString
            Else
                CheckCellValueChanged = ""
            End If
        Catch ex As Exception
            MsgBox("iGrid_BeforeCommitEdit : " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub iGrid_MouseDown(sender As Object, e As MouseEventArgs) Handles iGrid.MouseDown
        Console.WriteLine(">> Private Sub iGrid_MouseDown")

        Try
            If e.Button = MouseButtons.Right Then
                If SelectedRow < 0 Then Exit Sub

                Me.ContextMouse.Show()
                Me.ContextMouse.Top = Control.MousePosition.Y
                Me.ContextMouse.Left = Control.MousePosition.X
            End If
        Catch ex As Exception
            MsgBox("iGrid_MouseDown: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub iGrid_CellClick(sender As Object, e As TenTec.Windows.iGridLib.iGCellClickEventArgs) Handles iGrid.CellClick
        Console.WriteLine(">> Private Sub iGrid_CellClick")

        Try
            SelectedRow = e.RowIndex
            SelectedColumn = e.ColIndex
        Catch ex As Exception
            MsgBox("DataGridViewData_CellClick: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub fGrid_BeforeContentsGrouped(ByVal sender As Object, ByVal e As System.EventArgs) Handles iGrid.BeforeContentsGrouped
        Console.WriteLine("< Private Sub fGrid_BeforeContentsGrouped")

        iGrid.BeginUpdate()
        Try
            iGrid_RemoveSumRows() 'Bestehenden Summenzeile(n) löschen

            iGrid_DefaultSort() 'Standard Sortierung
        Catch ex As Exception
            MsgBox("fGrid_BeforeContentsGrouped: " & ex.Message.ToString)
        End Try
        iGrid.EndUpdate()
    End Sub

    Public Sub fGrid_AfterContentsGrouped(ByVal sender As Object, ByVal e As System.EventArgs) Handles iGrid.AfterContentsGrouped
        Console.WriteLine("< fGrid_AfterContentsGrouped")

        iGrid.BeginUpdate()

        Try
            'Summenzeile(n) bilden
            iGrid_CreateSumRows()

            If Not iGrid.GroupObject.Count = 0 Then
                iGrid_EnableEditFunctions(False, True, "")
            ElseIf boolCompareMode = False Then
                iGrid_EnableEditFunctions(True, True, "") 'Im Editiermodus ist alles möglich
            Else
                iGrid_EnableEditFunctions(False, True, "")
            End If

            iGrid_ColAndRowWidth()

            If Not iGrid.GroupObject.Count = 0 Then
                iGrid.Rows.CollapseAll() 'Alles einklappen
            End If
        Catch ex As Exception
            MsgBox("fGrid_AfterContentsGrouped : " & ex.Message.ToString)
        End Try

        iGrid.EndUpdate()
    End Sub

    Private Sub iGrid_AfterAutoGroupRowCreated(sender As Object, e As iGAfterAutoGroupRowCreatedEventArgs) Handles iGrid.AfterAutoGroupRowCreated
        'Slave from CreateSumRows

        Dim myGroupRowCell As iGCell

        Try
            Select Case iGrid.Cols(e.GroupedColIndex).Key
                Case "qryKostenart"

                    ' Add text to the country group rows
                    ' (as the Country column is grouped by image index
                    ' it's group rows will contain only images).
                    myGroupRowCell = iGrid.RowTextCol.Cells(e.AutoGroupRowIndex)
                    Dim myFirstCellInGroup As iGCell = iGrid.Cells(e.GroupedRowIndex, e.GroupedColIndex)
                    myGroupRowCell.ImageIndex = myFirstCellInGroup.ImageIndex
                    myGroupRowCell.Style = iGrid.GroupRowLevelStyles(0).Clone
                    myGroupRowCell.ImageList = iGrid.Cols(e.GroupedColIndex).CellStyle.ImageList
                Case "Type"


            End Select
        Catch ex As Exception
            MsgBox("iGrid_AfterAutoGroupRowCreated: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub iGridAutoFilter_FilterApplied(sender As Object, e As Filtering.iGFilterAppliedEventArgs) Handles iGridAutoFilter.FilterApplied
        Console.WriteLine("< Private Sub iGridAutoFilter_FilterApplied")

        iGrid.BeginUpdate()
        If iGrid.GroupObject.Count = 0 Then 'Nur wenn keine Gruppierung vorhanden ist: ansonsten werden die neuen Zahlen über die "AfterContentsGrouped" Methode berechnet
            iGrid_InitializeNumbers() 'Zahlen neu berechnen
        End If
        iGrid.EndUpdate()
    End Sub
#End Region
#End Region

#Region "SummenZeile"
    Public Sub iGrid_CreateSumRows()
        Console.WriteLine("< Public Sub iGrid_CreateSumRows()")

        Dim mySum() As tableSumRows 'Speichert alle Nicht Ist Zahlen
        Dim GroupLevel() As Boolean
        Dim curLevel As Integer, prevLevel As Integer
        Dim i As Integer, j As Integer, z As Integer
        Dim mySubTotalRow As iGRow
        Dim myRow As iGRow
        Dim boolCreateLastSumRows As Boolean = False

#Region "Compare Mode: Datentabellen erzeugen"
        Dim LastMonth As Integer, CurMonth As Integer
        Dim curMonthData As tableMonat
        Dim LastMonthData As tableMonat
        Dim dt_Delta As System.Data.DataTable

        dt_Delta = pbl_dt.Clone 'Strukturen übernehmen
        For j = 0 To dt_Delta.Columns.Count - 1
            dt_Delta.Columns(j).AllowDBNull = True
        Next
        Dim newRowIndex As Integer
        Dim boolFirstDataRow As Boolean = True

        dt_Delta = CompMode_LevelHandling(dt_Delta, 0)
#End Region

        'Bei GroupRows ist das Level = Level der Row + 1
        'Bei NormalRows ist das Level = Level der Row

        Try
            ' Create subtotal row pattern.
            Dim myRowPat As iGRowPattern = iGrid.DefaultRow.Clone()
            myRowPat.Type = iGRowType.ManualGroupRow

#Region "Cell Styles"
            fCellStyleSubTotals.ForeColor = Color.Black
            fCellStyleSubTotals.BackColor = Color.LightGray
            fCellStyleSubTotals.Font = New Font(iGrid.Font, FontStyle.Bold)

#Region "Compare Mode: Layout der Zeile"
            cellStylePositive.BackColor = Color.LightBlue
            cellStylePositive.Font = New Font(iGrid.Font, FontStyle.Bold)
            cellStyleNegative.BackColor = Color.LightPink
            cellStyleNegative.Font = New Font(iGrid.Font, FontStyle.Bold)
#End Region

#End Region

            ReDim mySum(0) 'Initialisieren: Überall Ebene 0 einführen
            ReDim GroupLevel(0)
            GroupLevel(0) = True 'Ebene 0 ist immer sichtbar
            i = 0
            prevLevel = 0

            pbl_dblPlanBudget = 0 'Speichert das Gesamtbudget für einen Vergleich mit dem SAP Budget

            If iGrid.Rows.Count = 0 Then Exit Sub

            '>>>
            Do Until i > iGrid.Rows.Count - 1 'Letzte Zeile wird extra behandelt
                myRow = iGrid.Rows(i)

                If i = iGrid.Rows.Count - 1 Then 'Letzte Zeile erreicht: Prüfen ob die letzte Zeile eine Value Zeile ist, dann muss ein Summenabschluss noch erfolgen
                    If Not myRow.Type = iGRowType.ManualGroupRow And Not IsSubTotal(myRow, True) = True Then
                        boolCreateLastSumRows = True
                    End If
                End If


                If myRow.Visible = True Then

#Region "Save current and previos Level"
                    'curLevel
                    If iGrid.Rows(myRow.Index).Type = iGRowType.AutoGroupRow Then 'Level der vorherigen Zeile speichern
                        curLevel = iGrid.Rows(myRow.Index).Level + 1
                    ElseIf Not iGrid.Rows(myRow.Index).Type = iGRowType.AutoGroupRow Then 'Normale Zeile: Level = Level -1
                        curLevel = iGrid.Rows(myRow.Index).Level
                    End If
#End Region

#Region "Compare Mode: Ist/Plan farblich markieren"
                    If boolCompareMode = True Then
                        If Not myRow.Type = iGRowType.AutoGroupRow Then
                            If iGrid.Rows(myRow.Index).Cells("qryEntwurfstyp").Value = "Ist" Then
                                If iGrid.Rows(myRow.Index).Cells("boolObligo").Value = True Then
                                    myRow.CellStyle.ForeColor = Color.DimGray
                                Else
                                    myRow.CellStyle.ForeColor = Color.Black
                                End If
                            Else 'Plan Zahlen
                                myRow.CellStyle.ForeColor = Color.SteelBlue
                            End If
                        End If
                    End If
#End Region

#Region ">> Prüfen um was für eine Zeile es sich handelt"

#Region "UPDATE EXISTING SUM OR PER/BIS ROW"
                    If myRow.Type = iGRowType.ManualGroupRow AndAlso IsSubTotal(myRow, False) = True Then
                        i = CompMode_EndOfMonth(myRow.Index, myRowPat, myRow.Index, curLevel, dt_Delta, curMonthData, False)
                        boolFirstDataRow = True 'Die nächste Value Zeile ist die erste eines neuen Monats
                    ElseIf myRow.Type = iGRowType.ManualGroupRow AndAlso IsSubTotal(myRow, True) = True Then 'Update Sum Row
                        CreateSumRow(i, myRowPat, curLevel, False, mySum, GroupLevel)
#Region "Compare Mode: Neues Level erstellen bzw. bereits existierendes Level leeren"
                        If boolCompareMode = True Then
                            dt_Delta = CompMode_LevelHandling(dt_Delta, curLevel)
                            boolFirstDataRow = True
                        End If
#End Region
#End Region

#Region "LEVEL UP"
                    ElseIf myRow.Type = iGRowType.AutoGroupRow AndAlso curLevel > prevLevel Then 'Neue Gruppierung: Level nach oben

#Region "Neues Up Level anlegen bzw. initialisieren"
                        ReDim Preserve mySum(curLevel)
                        mySum(curLevel).Sum = 0 'Zurück setzen
                        mySum(curLevel).IstUndObligo = 0
                        mySum(curLevel).Ist = 0
                        mySum(curLevel).Obligo = 0
                        ReDim Preserve GroupLevel(curLevel)
                        GroupLevel(curLevel) = myRow.Expanded 'Aufklappstatus des aktuellen Levels speichern
#End Region

#Region "Compare Mode: Neues Level erstellen bzw. bereits existierendes Level leeren"
                        If boolCompareMode = True Then
                            dt_Delta = CompMode_LevelHandling(dt_Delta, curLevel)
                            boolFirstDataRow = True
                        End If
#End Region

                        prevLevel = curLevel
#End Region

#Region "SAME LEVEL: END OF GROUP"
                    ElseIf myRow.Type = iGRowType.AutoGroupRow AndAlso curLevel = prevLevel Then 'Neue Gruppierung: gleiches Level
                        myRowPat.Level = curLevel 'Gruppe auf gleichem Level abschließen
                        GroupLevel(curLevel) = myRow.Expanded 'Aufklappstatus des aktuellen Levels speichern

                        If Not iGrid.Rows(myRow.Index - 1).RowTextCell.Style Is fCellStyleSubTotals Then 'Gruppierung ist bereits abgeschlossen
#Region "Compare Mode: Vorherigen Monat Per/Bis abschließen"
                            If boolCompareMode = True Then
                                i = CompMode_EndOfMonth(i, myRowPat, i, curLevel, dt_Delta, curMonthData, True)
                            End If
#End Region
#Region "Summenzeile am Ende einer Gruppierung"
                            mySum = CreateSumRow(i, myRowPat, curLevel, True, mySum, GroupLevel)
                            i = i + 1
#End Region
#Region "Compare Mode: Aktuelles Level zurücksetzen"
                            If boolCompareMode = True Then
                                dt_Delta = CompMode_LevelHandling(dt_Delta, curLevel)
                                boolFirstDataRow = True 'in der nächsten Gruppierung ist die erste Zeile wieder Neu
                            End If
#End Region
                        End If

#End Region

#Region "LEVEL DOWN"
                    ElseIf myRow.Type = iGRowType.AutoGroupRow AndAlso curLevel < prevLevel Then 'Gruppierung Ende: Level runter
                        GroupLevel(curLevel) = myRow.Expanded 'Aufklappstatus des aktuellen Levels speichern

                        If Not iGrid.Rows(myRow.Index - 1).RowTextCell.Style Is fCellStyleSubTotals Then 'Wenn vorherige Zeile eine Summenzeile ist geht es nur um die aktualisierung von bestehnden Zeilen
#Region "Compare Mode: Per/Bis für vorherige Zeile abschließen"
                            If boolCompareMode = True Then
                                i = CompMode_EndOfMonth(i, myRowPat, i, prevLevel, dt_Delta, curMonthData, True)
                            End If
#End Region

                            For j = prevLevel To curLevel Step -1
                                myRowPat.Level = j

#Region "Compare Mode: Alle Level down initialisieren"
                                If boolCompareMode = True Then
                                    dt_Delta = CompMode_LevelHandling(dt_Delta, j)
                                    boolFirstDataRow = True
                                End If
#End Region
#Region "Summenzeile am Ende einer Gruppe"
                                mySum = CreateSumRow(i, myRowPat, j, True, mySum, GroupLevel)
                                i = i + 1
#End Region

                            Next j
                        End If

                        prevLevel = curLevel
#End Region

#Region "SAME LEVEL: VALUE"
                    ElseIf Not myRow.Type = iGRowType.AutoGroupRow AndAlso Not myRow.Type = iGRowType.ManualGroupRow AndAlso myRow.Index <= iGrid.Rows.Count - 1 Then  'Value Zeile innerhalb des aktuellen Levels
                        'Wert(e) ergänzen
                        For j = curLevel To LBound(mySum) Step -1 'Werte in im aktuellen und allen niedrigeren Levels eintragen
                            myRowPat.Level = j

                            If boolCompareMode = False Then
                                mySum(j).Sum = mySum(j).Sum + iGrid.Rows(myRow.Index).Cells("CostsPerYear").Value
                            Else
                                If iGrid.Rows(myRow.Index).Cells("qryEntwurfstyp").Value = "Ist" Then
                                    mySum(j).IstUndObligo = mySum(j).IstUndObligo + iGrid.Rows(myRow.Index).Cells("CostsPerYear").Value 'Ist + Obligo
                                    If iGrid.Rows(myRow.Index).Cells("boolObligo").Value = False Then
                                        mySum(j).Ist = mySum(j).Ist + iGrid.Rows(myRow.Index).Cells("CostsPerYear").Value 'Ist
                                    Else
                                        mySum(j).Obligo = mySum(j).Obligo + iGrid.Rows(myRow.Index).Cells("CostsPerYear").Value 'Obligo
                                    End If
                                Else
                                    mySum(j).Sum = mySum(j).Sum + iGrid.Rows(myRow.Index).Cells("CostsPerYear").Value
                                End If

#Region "Compare Mode"
                                CurMonth = iGrid.Rows(myRow.Index).Cells("lngSort").Value 'Aktueller Monat
                                If Not CurMonth = LastMonth Then
                                    curMonthData = ConvertIntegerToDoubleMonth(CurMonth)
                                End If

                                If Not CurMonth = LastMonth AndAlso boolFirstDataRow = False Then 'Zwischensummenzeile einfügen
                                    i = CompMode_EndOfMonth(i, myRowPat, i, j, dt_Delta, LastMonthData, True)

                                    LastMonth = CurMonth
                                    LastMonthData = curMonthData
                                    myRow = iGrid.Rows(i)
                                End If

                                'Wert in Delta Summe addieren/summieren
                                If iGrid.Rows(myRow.Index).Cells("qryEntwurfstyp").Value = "Ist" Then '= Kosten = abziehen von Summe 
                                    dt_Delta.Rows(j)(curMonthData.dblMonat) = dt_Delta.Rows(j)(curMonthData.dblMonat) - iGrid.Rows(myRow.Index).Cells(curMonthData.dblMonat).Value * iGrid.Rows(myRow.Index).Cells("curKosten_pro_Einheit").Value
                                    dt_Delta.Rows(j)("CostsPerYear") = dt_Delta.Rows(j)("CostsPerYear") - iGrid.Rows(myRow.Index).Cells(curMonthData.dblMonat).Value * iGrid.Rows(myRow.Index).Cells("curKosten_pro_Einheit").Value
                                Else '= Planung = addieren zu Summe
                                    dt_Delta.Rows(j)(curMonthData.dblMonat) = dt_Delta.Rows(j)(curMonthData.dblMonat) + iGrid.Rows(myRow.Index).Cells(curMonthData.dblMonat).Value * iGrid.Rows(myRow.Index).Cells("curKosten_pro_Einheit").Value
                                    dt_Delta.Rows(j)("CostsPerYear") = dt_Delta.Rows(j)("CostsPerYear") + iGrid.Rows(myRow.Index).Cells(curMonthData.dblMonat).Value * iGrid.Rows(myRow.Index).Cells("curKosten_pro_Einheit").Value
                                End If

                                If boolFirstDataRow = True Then
                                    boolFirstDataRow = False 'Nach einem Durchlauf ist die erste Zeile mit Daten bearbeitet.
                                    LastMonth = CurMonth
                                    LastMonthData = curMonthData
                                End If
#End Region
                            End If
                        Next

                    End If
#End Region

#End Region
                    i = i + 1
                Else 'Zeile überspringen
                    i = i + 1
                End If
            Loop

#Region "LAST ROW: alle Gruppierungen abschließen"
            If boolCreateLastSumRows = True Then
                myRow = iGrid.Rows(iGrid.Rows.Count - 1)

#Region "Compare Mode: Letzte Zeile des Grid abschließen"
                If boolCompareMode = True Then
                    myRowPat.Level = curLevel
                    i = CompMode_EndOfMonth(myRow.Index, myRowPat, iGrid.Rows.Count, curLevel, dt_Delta, curMonthData, True)
                End If
#End Region

                'vorherige Gruppierung(en) abschließen
                For j = curLevel To LBound(mySum) Step -1 'Werte in im aktuellen und allen niedrigeren Levels eintragen
                    mySum = CreateSumRow(iGrid.Rows.Count, myRowPat, j, True, mySum, GroupLevel)
                Next
            End If
#End Region

        Catch ex As Exception
            MsgBox("fGrid_CreateSumRows : " & ex.Message.ToString)
        End Try

    End Sub

    Public Sub iGrid_RemoveSumRows()
        Console.WriteLine("< Public Sub iGrid_RemoveSumRows()")

        Try
            For myRowIndex As Integer = iGrid.Rows.Count - 1 To 0 Step -1
                If IsSubTotal(iGrid.Rows(myRowIndex), True) Then 'Sum row
                    iGrid.Rows.RemoveAt(myRowIndex)
                ElseIf IsSubTotal(iGrid.Rows(myRowIndex), False) Then 'Per/Bis row
                    iGrid.Rows.RemoveAt(myRowIndex)
                End If
            Next
        Catch ex As Exception
            MsgBox("iGried_RemoveSumRows : " & ex.Message.ToString)
        End Try
    End Sub

    Private Function IsSubTotal(ByVal row As iGRow, IsSumRow As Boolean) As Boolean
        Try
            If IsSumRow = True Then
                If row.Type = Global.TenTec.Windows.iGridLib.iGRowType.ManualGroupRow And row.RowTextCell.Style Is fCellStyleSubTotals Then
                    Return True
                Else
                    Return False
                End If
            Else
                If row.Type = Global.TenTec.Windows.iGridLib.iGRowType.ManualGroupRow And row.RowTextCell.Style Is cellStylePositive Then
                    Return True
                ElseIf row.Type = Global.TenTec.Windows.iGridLib.iGRowType.ManualGroupRow And row.RowTextCell.Style Is cellStyleNegative Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            MsgBox("Private Function IsSubTotal: " & ex.Message.ToString)
        End Try
    End Function

    Private Function CompMode_EndOfMonth(i As Integer, RowPattern As iGRowPattern, insertAt As Integer, Level As Integer, dt As System.Data.DataTable, Month As tableMonat, CreateNewRow As Boolean) As Integer
        Dim mySubTotalRow As iGRow
        cellStylePositive.BackColor = Color.LightBlue
        cellStylePositive.ForeColor = Color.Black
        cellStyleNegative.BackColor = Color.LightPink
        cellStyleNegative.ForeColor = Color.Black

        Try
            RowPattern.Level = Level

            If CreateNewRow = True Then
                'If dt.Rows(Level)(Month.dblMonat) < 0 Or dt.Rows(Level)("CostsPerYear") < 0 Then 'Per oder Bis negativ = Zeile highlighten
                '    mySubTotalRow = iGrid.Rows.Insert(insertAt, RowPattern)
                '    iGrid.Rows(insertAt).Pattern = RowPattern
                'Else
                '    mySubTotalRow = iGrid.Rows.Insert(insertAt, RowPattern)
                '    iGrid.Rows(insertAt).Pattern = RowPattern
                'End If
                mySubTotalRow = iGrid.Rows.Insert(insertAt, RowPattern)
                iGrid.Rows(insertAt).Pattern = RowPattern
            Else
                mySubTotalRow = iGrid.Rows(insertAt)
            End If

            mySubTotalRow.RowTextCell.Value = String.Format("Im " & Month.Monat & ": €{0:#,0}, Bis " & Month.Monat & ": €{1:#,0}", dt.Rows(Level)(Month.dblMonat), dt.Rows(Level)("CostsPerYear"))
            'If dt.Rows(Level)(Month.dblMonat) < 0 And dt.Rows(Level)("CostsPerYear") < 0 Then 'Per oder Bis negativ = Zeile highlighten
            If dt.Rows(Level)("CostsPerYear") < 0 Then 'Per oder Bis negativ = Zeile highlighten
                mySubTotalRow.RowTextCell.Style = cellStyleNegative 'Objekt initialisieren
            Else
                mySubTotalRow.RowTextCell.Style = cellStylePositive
            End If

            If CreateNewRow = True Then
                Return i + 1 'Index um eins erhöhen
            Else
                Return i
            End If
        Catch ex As Exception
            MsgBox("CompMode_EndOfMonth: " & ex.Message.ToString)
        End Try
    End Function

    Private Function CompMode_LevelHandling(dt As System.Data.DataTable, Level As Integer) As System.Data.DataTable
        Dim z As Integer
        Dim RowIndex As Integer
        Try

            'Compare Mode Zeile(n) löschen und neu einfügen, sprich Zeile leeren
            If Level > dt.Rows.Count - 1 Then 'Diese Ebene existiert noch nicht
                dt.Rows.Add()
                RowIndex = dt.Rows.Count - 1
            Else 'Ebene existiert: Zeilen "leeren"
                Dim myNewRow As DataRow = dt.NewRow()
                RowIndex = Level
                dt.Rows.InsertAt(myNewRow, Level) 'Neue Zeile einfügen
                dt.Rows(Level + 1).Delete() 'Alte Zeile löschen
            End If

            For z = 0 To dt.Columns.Count - 1 'Double Werte mit 0 vorbelegen
                If dt.Columns(z).DataType = GetType(Double) Then
                    dt.Rows(RowIndex)(z) = 0
                End If
            Next

            Return dt
        Catch ex As Exception
            MsgBox("CompMode_LevelHandling: " & ex.Message.ToString)
        End Try
    End Function

    Private Function CreateSumRow(InsertAt As Integer, RowPat As iGRowPattern, Level As Integer, CreateNewRow As Boolean, Sum() As tableSumRows, GroupLevel() As Boolean) As tableSumRows()
        Dim mySubTotalRow As iGRow
        RowPat.Level = Level

        If CreateNewRow = True Then
            mySubTotalRow = iGrid.Rows.Insert(InsertAt)
        End If

        Try
#Region "Create a Sum Descpription"
            Dim SumString As String
            If boolCompareMode = False Then
                SumString = String.Format("Summe: €{0:#,0}", Sum(Level).Sum)
                'iGrid.Rows(InsertAt).RowTextCell.Value = String.Format("Summe: €{0:#,0}", Sum(Level).Sum)
            Else
                SumString = String.Format("Plan: €{0:#,0}  |  Ist+Obligo: €{1:#,0}  (Ist: €{2:#,0}, Obligo: €{3:#,0})", Sum(Level).Sum, Sum(Level).IstUndObligo, Sum(Level).Ist, Sum(Level).Obligo)
            End If
            iGrid.Rows(InsertAt).Pattern = RowPat
            iGrid.Rows(InsertAt).RowTextCell.Style = fCellStyleSubTotals
            iGrid.Rows(InsertAt).RowTextCell.Value = SumString
#End Region

            pbl_dblPlanBudget = pbl_dblPlanBudget + Sum(Level).Sum 'Gesamt geplantes Budget

#Region "Extend the AutoGroupRow Description by the sum"
            If Level > 0 Then
                For i As Integer = InsertAt To 0 Step -1
                    If iGrid.Rows(i).Type = iGRowType.AutoGroupRow And iGrid.Rows(i).Level = Level - 1 Then
                        Dim GroupRowCell As iGCell = iGrid.RowTextCol.Cells(i)
                        GroupRowCell.Value = GroupRowCell.Value & "   -   " & SumString
                        Exit For
                    End If
                Next
            End If
#End Region

            If CreateNewRow = False Then 'Gruppierungslevel beibehalten
                iGrid.Rows(InsertAt).VisibleParentExpanded = GroupLevel(Level)
                iGrid.Rows(InsertAt).Expanded = GroupLevel(Level)
            End If

            Sum(Level).Sum = 0 'Wert zurücksetzen
            Sum(Level).IstUndObligo = 0
            Sum(Level).Ist = 0
            Sum(Level).Obligo = 0
            Return Sum
        Catch ex As Exception
            MsgBox("Private Function CreateSumRow: " & ex.Message.ToString)
        End Try
    End Function
#End Region

#Region "Steuerelemente - Buttons"
    Private Sub AddRow_Click(sender As Object, e As EventArgs) Handles AddRow.Click
        Console.WriteLine(">> Private Sub AddRow_Click")

        iGrid.BeginUpdate()
        Try
            Dim i As Integer, newRow As Integer = 0
            Dim boolSel As Boolean = False

            If iGrid.Rows.Count > 0 Then
                For i = 0 To iGrid.Rows.Count - 1
                    If iGrid.Rows(i).Selected = True Then
                        newRow = i
                        boolSel = True
                        Exit For
                    End If
                Next
            End If

            iGrid_RemoveSumRows()

            iGrid_NewRow(newRow, boolSel)

            iGrid_CreateSumRows()

            iGrid_ColAndRowWidth()

            boolDataChanged = True

            iGrid.EndUpdate()

#Region "Set Fokus"
            Try
                If boolSel = True Then 'Set Fokus
                    iGrid.CurRow = iGrid.Rows(newRow)
                Else
                    iGrid.CurRow = iGrid.Rows(iGrid.Rows.Count - 1)
                End If
            Catch
            End Try
#End Region
        Catch ex As Exception
            MsgBox("AddRow_Click: " & ex.Message.ToString)
            iGrid.EndUpdate()
        End Try
    End Sub

    Private Sub SaveData_Click(sender As Object, e As EventArgs) Handles SaveData.Click
        Console.WriteLine(">> Private Sub SaveData_Click")

        Dim ReloadiGrid As Boolean
        If con.State = ConnectionState.Closed Then Exit Sub

        iGrid.BeginUpdate()
        Try
            ReloadiGrid = iGrid_SaveData() 'Daten speichern

            boolDataChanged = False

            'Zahlen aktualisieren (bspw. für gelöschte Zeilen)
            If ReloadiGrid = False Then
                iGrid_Footer()
                iGrid_CreateSumRows()
            Else 'Nur im Compare Mode möglich
                Dim dtPlan As System.Data.DataTable = Get_tblData(True, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                Dim dtIst As System.Data.DataTable = Get_tblData(False, pbl_IdAp, 1, "Ist", False, pbl_LstSelGj, pbl_Sql)
                DoCompare(dtPlan, dtIst)
            End If

            ToolStripStatusOperation.Text = "Änderungen erfolgreich gespeichert"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("SaveData_Click : " & ex.Message.ToString)
        End Try
        iGrid.EndUpdate()
    End Sub

    Public Sub ExcelExport_Click(sender As Object, e As EventArgs) Handles ExcelExport.Click
        Try
            ToolStripStatusOperation.Text = "Daten werden in Excel exportiert..."
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()

            iGridToExcel(Form_Edit.iGrid)
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub FooterCalcMethod_Click(sender As Object, e As EventArgs) Handles FooterCalcMethod.Click
        If FooterCalculationMethod = 0 Then 'XX Bilder wechseln
            FooterCalculationMethod = 1 'Anzahl pro Monat
        ElseIf FooterCalculationMethod = 1 Then
            FooterCalculationMethod = 2 'FTE pro Monat
        ElseIf FooterCalculationMethod = 2 Then
            FooterCalculationMethod = 0 'Kosten pro Monat
        End If
        iGrid_Footer()
    End Sub

    Private Sub cmdGetSap_Click(sender As Object, e As EventArgs) Handles cmdGetSap.Click
        Console.WriteLine("CLICK: Private Sub cmdGetSap_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 4 'Live SAP Daten

            Form_Psb.Show()
        Catch ex As Exception
            MsgBox("Private Sub cmdGetSap_Click: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ShowMultiData_Click(sender As Object, e As EventArgs) Handles ShowMultiData.Click
        MenuMehrereArbeitspaketeBearbeiten.PerformClick()
    End Sub
#End Region

#Region "Methoden"
    Sub iGrid_NewRow(NewRow As Integer, BoolNewRow As Boolean)
        Console.WriteLine(">> Sub iGrid_NewRow")

        Try
#Region "Check if new Row can be added"
            If Not iGrid.Rows.Count > 0 Then 'Es gibt keine Kopierzeilen: Prüfen ob alle Muss Felder vorhanden sind
                If IsNothing(pbl_Gj) Then Exit Sub
                If IsNothing(pbl_Paket) Then Exit Sub
                If IsNothing(pbl_Ep) Then Exit Sub
                If IsNothing(pbl_KstArt) Then Exit Sub
                If IsNothing(pbl_Klasse) Then Exit Sub
                If pbl_IdAp < 0 Then Exit Sub
                If pbl_lngVersion < 0 Then Exit Sub
                If IsNothing(pbl_Entwurf) Then Exit Sub
                If pbl_IndexEntwurf < 0 Then Exit Sub
            End If
#End Region

#Region "Index of new Row"
            If BoolNewRow = False Then 'Neue Zeile am Ende einfügen
                iGrid.Rows.Add()
                NewRow = iGrid.Rows.Count - 1
            ElseIf iGrid.Rows.Count = 0 Then
                iGrid.Rows.Add()
                NewRow = iGrid.Rows.Count - 1
            Else
                iGrid.Rows.Insert(NewRow)
            End If
#End Region

#Region "Comboboxen vorbelegen"
            If iGrid.Rows.Count > 0 And NewRow > 0 Then 'Es gibt bereits Daten
                iGrid.Rows(NewRow).Cells("qryGeschäftsjahr").Value = iGrid.Rows(NewRow - 1).Cells("qryGeschäftsjahr").Value
                iGrid.Rows(NewRow).Cells("qryPaket").Value = iGrid.Rows(NewRow - 1).Cells("qryPaket").Value
                iGrid.Rows(NewRow).Cells("qryEntwicklungspaket").Value = iGrid.Rows(NewRow - 1).Cells("qryEntwicklungspaket").Value
                iGrid.Rows(NewRow).Cells("qryKostenart").Value = iGrid.Rows(NewRow - 1).Cells("qryKostenart").Value
                iGrid.Rows(NewRow).Cells("qryKlassifizierung").Value = iGrid.Rows(NewRow - 1).Cells("qryKlassifizierung").Value
                iGrid.Rows(NewRow).Cells("boolObligo").Value = iGrid.Rows(NewRow - 1).Cells("boolObligo").Value
                iGrid.Rows(NewRow).Cells("boolRenneinsatz").Value = iGrid.Rows(NewRow - 1).Cells("boolRenneinsatz").Value
                iGrid.Rows(NewRow).Cells("lngSort").Value = iGrid.Rows(NewRow - 1).Cells("lngSort").Value + 10
            Else
                iGrid.Rows(NewRow).Cells("qryGeschäftsjahr").Value = pbl_Gj(0).intGJ
                iGrid.Rows(NewRow).Cells("qryPaket").Value = pbl_Paket(0).txtPaket
                iGrid.Rows(NewRow).Cells("qryEntwicklungspaket").Value = pbl_Ep(0).txtEntwicklungspaket
                iGrid.Rows(NewRow).Cells("qryKostenart").Value = pbl_KstArt(0).txtKostenart
                iGrid.Rows(NewRow).Cells("qryKlassifizierung").Value = pbl_Klasse(0).txtKlasse
                iGrid.Rows(NewRow).Cells("boolObligo").Value = False
                iGrid.Rows(NewRow).Cells("boolRenneinsatz").Value = False
                iGrid.Rows(NewRow).Cells("lngSort").Value = 100
            End If
            iGrid.Rows(NewRow).Cells("qryArbeitspaket").Value = pbl_IdAp
            iGrid.Rows(NewRow).Cells("PROJEKT_ID").Value = pbl_IdProjekt

            Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
            Dim IndexAp As Integer = -1
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                IndexAp = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
                iGrid.Rows(NewRow).Cells("txtArbeitspaket").Value = pbl_dtArbeitspaket.Rows(IndexAp).Item("txtArbeitspaket")
                iGrid.Rows(NewRow).Cells("PSP").Value = pbl_dtArbeitspaket.Rows(IndexAp).Item("txtPSP_Element")
                iGrid.Rows(NewRow).Cells("PSP_Verantw").Value = pbl_dtArbeitspaket.Rows(IndexAp).Item("txtVerantwortlich")
            End If

            Dim IndexProject As Integer
            Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & pbl_IdProjekt & "")
            If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                IndexProject = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
            Else
                IndexProject = 0
            End If

            iGrid.Rows(NewRow).Cells("Projekt").Value = pbl_dtProjekt.Rows(IndexProject).Item("txtProjekt")
            iGrid.Rows(NewRow).Cells("qryVersion").Value = pbl_lngVersion
            iGrid.Rows(NewRow).Cells("qryEntwurfstyp").Value = pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp
            iGrid.Rows(NewRow).Cells("datErstellungsdatum").Value = Now
            iGrid.Rows(NewRow).Cells("Action").Value = "N"
            'Felder initialisieren
            iGrid.Rows(NewRow).Cells("txtBeschreibung").Value = ""
            iGrid.Rows(NewRow).Cells("txtKst_Lst").Value = ""
            iGrid.Rows(NewRow).Cells("txtBedarfsnummer").Value = ""
            iGrid.Rows(NewRow).Cells("curKosten_pro_Einheit").Value = 0
            iGrid.Rows(NewRow).Cells("dblJanuar").Value = 0
            iGrid.Rows(NewRow).Cells("dblFebruar").Value = 0
            iGrid.Rows(NewRow).Cells("dblMärz").Value = 0
            iGrid.Rows(NewRow).Cells("dblApril").Value = 0
            iGrid.Rows(NewRow).Cells("dblMai").Value = 0
            iGrid.Rows(NewRow).Cells("dblJuni").Value = 0
            iGrid.Rows(NewRow).Cells("dblJuli").Value = 0
            iGrid.Rows(NewRow).Cells("dblAugust").Value = 0
            iGrid.Rows(NewRow).Cells("dblSeptember").Value = 0
            iGrid.Rows(NewRow).Cells("dblOktober").Value = 0
            iGrid.Rows(NewRow).Cells("dblNovember").Value = 0
            iGrid.Rows(NewRow).Cells("dblDezember").Value = 0
            iGrid.Rows(NewRow).Cells("txtBemerkung").Value = ""
            iGrid.Rows(NewRow).Cells("txtZusatzfeld").Value = ""
            iGrid.Rows(NewRow).Cells("ID_DATA").Value = ""
#End Region

            'iGrid.CurRow = iGrid.Rows(NewRow) 'Set Fokus
        Catch ex As Exception
            MsgBox("iGrid_NewRow: " & ex.Message.ToString)
        End Try
    End Sub

    Sub iGrid_FillData(dt_data As System.Data.DataTable, UseExistingColumns As Boolean) 'Fill Data Table with Data from SQL Server
        iGrid.FillWithData(dt_data, UseExistingColumns)
    End Sub

    Function iGrid_SaveData() As Boolean 'Save, Update or Delete Data | BOOLEAN gibt an ob Daten komplett neu geladen werden müssen oder nur aktualisiert
        Console.WriteLine("< Function iGrid_SaveData()")

        Dim i As Integer = 0
        Dim z As Integer
        Dim CompModeReloadData As Boolean = False

        If iGrid.Rows.Count <= 0 Then 'Nothing to save
            Exit Function
        End If

        If iGrid_CheckForErrorBeforeSave() = True Then 'Check for missing data
            Exit Function
        End If

        RemoveHandler iGrid.AfterCommitEdit, AddressOf iGrid_AfterCommitEdit
        RemoveHandler iGrid.BeforeCommitEdit, AddressOf iGrid_BeforeCommitEdit

        Try
            Do While i <= iGrid.Rows.Count - 1
                If Not String.IsNullOrEmpty(iGrid.Rows(i).Cells("Action").Value) Then
                    If iGrid.Rows(i).Cells("qryEntwurfstyp").Value = "Ist" Then
                        iGrid.Rows(i).Cells("Action").Value = "" 'Clear Bearbeitung
                        MsgBox("Zeile " & i & " (" & iGrid.Rows(i).Cells("txtBeschreibung").Value & ") kann nicht gespeichert werden." & vbCrLf & "Es ist nicht möglich 'Ist oder Obligo' Daten zu verändern!", vbInformation, "Error")
                    End If
                End If

#Region "New Data"
                If iGrid.Rows(i).Cells("Action").Value = "N" Then 'Neuer Datensatz
                    Dim ID_DATA As Integer
                    Dim StrUser As String = pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer") & " - " & pbl_dtUser.Rows(intCurrentUser).Item("txtName")
                    ID_DATA = Add_Data(iGrid.Rows(i).Cells("qryEntwurfstyp").Value,
                        iGrid.Rows(i).Cells("qryArbeitspaket").Value,
                        iGrid.Rows(i).Cells("qryVersion").Value,
                        iGrid.Rows(i).Cells("qryGeschäftsjahr").Value,
                        iGrid.Rows(i).Cells("qryKostenart").Value,
                        iGrid.Rows(i).Cells("txtBeschreibung").Value,
                        iGrid.Rows(i).Cells("txtBemerkung").Value,
                        iGrid.Rows(i).Cells("txtZusatzfeld").Value,
                        iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value,
                        iGrid.Rows(i).Cells("boolRenneinsatz").Value,
                        Now,
                        iGrid.Rows(i).Cells("txtBedarfsnummer").Value,
                        iGrid.Rows(i).Cells("dblJanuar").Value,
                        iGrid.Rows(i).Cells("dblFebruar").Value,
                        iGrid.Rows(i).Cells("dblMärz").Value,
                        iGrid.Rows(i).Cells("dblApril").Value,
                        iGrid.Rows(i).Cells("dblMai").Value,
                        iGrid.Rows(i).Cells("dblJuni").Value,
                        iGrid.Rows(i).Cells("dblJuli").Value,
                        iGrid.Rows(i).Cells("dblAugust").Value,
                        iGrid.Rows(i).Cells("dblSeptember").Value,
                        iGrid.Rows(i).Cells("dblOktober").Value,
                        iGrid.Rows(i).Cells("dblNovember").Value,
                        iGrid.Rows(i).Cells("dblDezember").Value,
                        iGrid.Rows(i).Cells("qryKlassifizierung").Value,
                        iGrid.Rows(i).Cells("qryEntwicklungspaket").Value,
                        iGrid.Rows(i).Cells("qryPaket").Value,
                        iGrid.Rows(i).Cells("boolObligo").Value,
                        iGrid.Rows(i).Cells("txtKst_Lst").Value,
                        iGrid.Rows(i).Cells("lngSort").Value,
                        StrUser)
                    If Not ID_DATA < 0 Then 'Datensatz auf DB erfolgreich hinzugefügt
                        iGrid.Rows(i).Cells("ID_DATA").Value = ID_DATA
                        iGrid.Rows(i).Cells("Action").Value = ""
                    Else
                        MsgBox("Zeile " & i & " (" & iGrid.Rows(i).Cells("txtBeschreibung").Value & ") konnte nicht gespeichert werden, bitte prüfen Sie die Server Verbindung und speichern dann erneut.")
                    End If
#End Region

#Region "Change Data"
                ElseIf iGrid.Rows(i).Cells("Action").Value = "C" Then 'Geänderter Datensatz
                    If boolCompareMode = False Then 'Es wird die ganze Zeile aktualisiert
                        If Not iGrid.Rows(i).Cells("qryEntwurfstyp").Value = "Ist" Then
                            Dim StrUser As String = pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer") & " - " & pbl_dtUser.Rows(intCurrentUser).Item("txtName")
                            If Update_Data_IdData(iGrid.Rows(i).Cells("ID_DATA").Value,
                                 iGrid.Rows(i).Cells("txtBeschreibung").Value,
                                 iGrid.Rows(i).Cells("txtBemerkung").Value,
                                 iGrid.Rows(i).Cells("txtZusatzfeld").Value,
                                 iGrid.Rows(i).Cells("txtBedarfsnummer").Value,
                                 iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value,
                                 iGrid.Rows(i).Cells("boolRenneinsatz").Value,
                                 iGrid.Rows(i).Cells("dblJanuar").Value,
                                 iGrid.Rows(i).Cells("dblFebruar").Value,
                                 iGrid.Rows(i).Cells("dblMärz").Value,
                                 iGrid.Rows(i).Cells("dblApril").Value,
                                 iGrid.Rows(i).Cells("dblMai").Value,
                                 iGrid.Rows(i).Cells("dblJuni").Value,
                                 iGrid.Rows(i).Cells("dblJuli").Value,
                                 iGrid.Rows(i).Cells("dblAugust").Value,
                                 iGrid.Rows(i).Cells("dblSeptember").Value,
                                 iGrid.Rows(i).Cells("dblOktober").Value,
                                 iGrid.Rows(i).Cells("dblNovember").Value,
                                 iGrid.Rows(i).Cells("dblDezember").Value,
                                 iGrid.Rows(i).Cells("boolObligo").Value,
                                 iGrid.Rows(i).Cells("qryKostenart").Value,
                                 iGrid.Rows(i).Cells("qryKlassifizierung").Value,
                                 iGrid.Rows(i).Cells("qryEntwicklungspaket").Value,
                                 iGrid.Rows(i).Cells("qryPaket").Value,
                                 iGrid.Rows(i).Cells("txtKst_Lst").Value,
                                 iGrid.Rows(i).Cells("lngSort").Value,
                                 iGrid.Rows(i).Cells("qryGeschäftsjahr").Value,
                                 StrUser) = True Then 'Datensatz auf DB erfolgreich aktualisiert
                                iGrid.Rows(i).Cells("Action").Value = ""
                            Else
                                MsgBox("Zeile " & i & " (" & iGrid.Rows(i).Cells("txtBeschreibung").Value & ") konnte nicht gespeichert werden, bitte prüfen Sie die Server Verbindung und speichern dann erneut.")
                            End If
                        Else 'Kommentare in tblSapComments
                            Dim ComId As Integer = Get_ID(iGrid.Rows(i).Cells("IdSap").Value)
                            If ComId = 0 Then
                                Add_CommentRow(iGrid.Rows(i).Cells("txtBemerkung").Value, iGrid.Rows(i).Cells("txtZusatzfeld").Value, "", iGrid.Rows(i).Cells("IdSap").Value, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), iGrid.Rows(i).Cells("PSP").Value)
                            Else 'Werte aktualisieren
                                Update_CommentRow(iGrid.Rows(i).Cells("txtBemerkung").Value, iGrid.Rows(i).Cells("txtZusatzfeld").Value, "", pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), iGrid.Rows(i).Cells("PSP").Value, ComId)
                            End If
                        End If
                    Else 'Es wird nur der entsprechende Monatswert aktualisiert
                        Dim Month As String = ConvertIntegerToDoubleMonth(iGrid.Rows(i).Cells("lngsort").Value).dblMonat
                        Dim StrUser As String = pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer") & " - " & pbl_dtUser.Rows(intCurrentUser).Item("txtName")
                        If Update_Data_IdData_Double(iGrid.Rows(i).Cells("ID_DATA").Value, Month, iGrid.Rows(i).Cells(Month).Value, StrUser) = True Then
                            Update_Data_IdData_Double(iGrid.Rows(i).Cells("ID_DATA").Value, "curKosten_pro_Einheit", iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value, StrUser)
                            iGrid.Rows(i).Cells("Action").Value = ""
                            For z = 1 To 12 'Prüft ob weitere Monate verändert wurden, wenn ja: alle Daten komplett neu laden
                                Dim CheckMonth As String = ConvertIntegerToDoubleMonth(z).dblMonat
                                If Not CheckMonth = Month Then
                                    If Not iGrid.Rows(i).Cells(CheckMonth).Value = 0 Then
                                        Update_Data_IdData_Double(iGrid.Rows(i).Cells("ID_DATA").Value, CheckMonth, iGrid.Rows(i).Cells(CheckMonth).Value, StrUser)
                                        CompModeReloadData = True
                                    End If
                                End If
                            Next
                        Else
                            MsgBox("Zeile " & i & " (" & iGrid.Rows(i).Cells("txtBeschreibung").Value & ") konnte nicht gespeichert werden, bitte prüfen Sie die Server Verbindung und speichern dann erneut.")
                        End If
                    End If
#End Region

#Region "Delete Data"
                ElseIf iGrid.Rows(i).Cells("Action").Value = "D" Then 'Datensatz löschen
                    If Not String.IsNullOrEmpty(iGrid.Rows(i).Cells("ID_DATA").Value) Then
                        If Delete_Defined_ID_DATA(iGrid.Rows(i).Cells("ID_DATA").Value) = True Then 'Datensatz auf DB erfolgreich gelöscht
                            If boolCompareMode = True Then 'Alle Einträge mit der ID_DATA löschen
                                Dim ID_DATA As Integer = iGrid.Rows(i).Cells("ID_DATA").Value
                                Do While z <= iGrid.Rows.Count - 1
                                    If iGrid.Rows(i).Cells("ID_DATA").Value = ID_DATA Then 'Zeile löschen
                                        iGrid.Rows.RemoveAt(i)
                                        i = i - 1
                                    End If
                                Loop
                            Else 'Es gibt nur einen Eintrag der gelöscht werden muss
                                iGrid.Rows.RemoveAt(i)
                                i = i - 1
                            End If
                        Else
                            MsgBox("Zeile " & i & " (" & iGrid.Rows(i).Cells("txtBeschreibung").Value & ") konnte nicht gespeichert werden, bitte prüfen Sie die Server Verbindung und speichern dann erneut.")
                        End If
                    End If
                End If
#End Region
                i = i + 1
            Loop

            'delete deleted rows
            If iGrid.Rows.Count > 0 Then
                i = 0
                Do Until i = iGrid.Rows.Count
                    If iGrid.Rows(i).Cells("Action").Value = "D" Then
                        iGrid.Rows.RemoveAt(i)
                        i = i - 1
                    End If
                    i = i + 1
                Loop
            End If

        Catch ex As Exception
            MsgBox("iGrid_SaveData: " & ex.Message.ToString)
        End Try

        AddHandler iGrid.AfterCommitEdit, AddressOf iGrid_AfterCommitEdit
        AddHandler iGrid.BeforeCommitEdit, AddressOf iGrid_BeforeCommitEdit

        Return CompModeReloadData
    End Function

    Sub iGrid_CopyCompleteData(i As Integer, txtEntwurfstyp As String, ID_AP As Integer, lngVersion As Long, intGJ As Integer)
        Console.WriteLine("< Sub iGrid_CopyCompleteData")

        Try
            Dim StrUser As String = pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer") & " - " & pbl_dtUser.Rows(intCurrentUser).Item("txtName")
            If Not String.IsNullOrEmpty(iGrid.Rows(i).Cells("txtBeschreibung").Value) Then 'Nur Zeilen hinzufügen die Werte enthalten
                Add_Data(txtEntwurfstyp,
                 ID_AP,
                 lngVersion,
                 intGJ,
                 iGrid.Rows(i).Cells("qryKostenart").Value,
                 iGrid.Rows(i).Cells("txtBeschreibung").Value,
                 iGrid.Rows(i).Cells("txtBemerkung").Value,
                 iGrid.Rows(i).Cells("txtZusatzfeld").Value,
                 iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value,
                 iGrid.Rows(i).Cells("boolRenneinsatz").Value,
                 Now,
                 iGrid.Rows(i).Cells("txtBedarfsnummer").Value,
                 iGrid.Rows(i).Cells("dblJanuar").Value,
                 iGrid.Rows(i).Cells("dblFebruar").Value,
                 iGrid.Rows(i).Cells("dblMärz").Value,
                 iGrid.Rows(i).Cells("dblApril").Value,
                 iGrid.Rows(i).Cells("dblMai").Value,
                 iGrid.Rows(i).Cells("dblJuni").Value,
                 iGrid.Rows(i).Cells("dblJuli").Value,
                 iGrid.Rows(i).Cells("dblAugust").Value,
                 iGrid.Rows(i).Cells("dblSeptember").Value,
                 iGrid.Rows(i).Cells("dblOktober").Value,
                 iGrid.Rows(i).Cells("dblNovember").Value,
                 iGrid.Rows(i).Cells("dblDezember").Value,
                 iGrid.Rows(i).Cells("qryKlassifizierung").Value,
                 iGrid.Rows(i).Cells("qryEntwicklungspaket").Value,
                 iGrid.Rows(i).Cells("qryPaket").Value,
                 iGrid.Rows(i).Cells("boolObligo").Value,
                 iGrid.Rows(i).Cells("txtKst_Lst").Value,
                iGrid.Rows(i).Cells("lngSort").Value,
                StrUser)
            End If
        Catch ex As Exception
            MsgBox("iGrid_CopyCompleteData: " & ex.Message.ToString)
        End Try
    End Sub

    Function iGrid_CheckForErrorBeforeSave() As Boolean ' Check before Save Data
        Console.WriteLine("< Function iGrid_CheckForErrorBeforeSave()")

        Try
            boolError = False
            For i = 0 To iGrid.Rows.Count - 1
                If Not iGrid.Rows(i).Cells("Action").Value = "D" Then 'Zu löschende Zeilen müssen nicht geprüft werden

                    If Not IsNothing(iGrid.Rows(i).Cells("qryKostenart").Value) _
                    And Not IsNothing(iGrid.Rows(i).Cells("txtBeschreibung").Value) Then 'Keine neue leere Zeile

                        If IsNothing(iGrid.Rows(i).Cells("qryGeschäftsjahr").Value) Then
                            MsgBox("Bitte wählen Sie ein Geschäftsjahr.", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                        If IsNothing(iGrid.Rows(i).Cells("CostsPerYear").Value) Then
                            MsgBox("Bitte geben Sie bei allen Positionen Kosten ein (Kosten pro Einheit und Summen für Monat(e))", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                        If IsNothing(iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value) Then
                            MsgBox("Bitte geben Sie bei allen Positionen Kosten pro Einheit ein", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                        If IsNothing(iGrid.Rows(i).Cells("qryKlassifizierung").Value) Then
                            MsgBox("Bitte geben Sie bei allen Positionen eine Klassifizierung ein", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                        If IsNothing(iGrid.Rows(i).Cells("qryEntwicklungspaket").Value) Then
                            MsgBox("Bitte geben Sie bei allen Positionen ein Entwicklungspaket ein", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                        If IsNothing(iGrid.Rows(i).Cells("qryPaket").Value) Then
                            MsgBox("Bitte geben Sie bei allen Positionen ein Paket ein", MsgBoxStyle.Critical, "Fehler bei der Eingabe von Daten")
                            boolError = True
                            Return True
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox("iGrid_CheckForErrorBeforeSave: " & ex.Message.ToString)
        End Try
    End Function

    Sub iGrid_InitializeNumbers()
        Console.WriteLine("< Sub iGrid_InitializeNumbers()")

        iGrid_RemoveSumRows()
        iGrid_CreateSumRows()
        iGrid_Footer()
    End Sub

    Public Sub iGrid_ClearFilterAndGroups()
        Console.WriteLine("< Sub iGrid_ClearFilterAndGroups()")

        Try
            'Events deaktivieren
            RemoveHandler iGridAutoFilter.FilterApplied, AddressOf iGridAutoFilter_FilterApplied
            iGridAutoFilter.ClearFilter()
            AddHandler iGridAutoFilter.FilterApplied, AddressOf iGridAutoFilter_FilterApplied

            iGrid.GroupObject.Clear()

            RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
            RemoveHandler iGrid.AfterContentsGrouped, AddressOf fGrid_AfterContentsGrouped
            iGrid.Group()
            AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
            AddHandler iGrid.AfterContentsGrouped, AddressOf fGrid_AfterContentsGrouped
        Catch ex As Exception
            MsgBox("iGrid_ClearFilterAndGroups: " & ex.Message.ToString)
        End Try
    End Sub
#End Region

#End Region


    'Private Sub DataGridViewData_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewData.CellDoubleClick
    '    Try
    '        If e.ColumnIndex >= 0 Then
    '            Try
    '                If DataGridViewData.Columns(e.ColumnIndex).Name = "txtEntwicklungspaket" Then
    '                    pbl_txtEp = DataGridViewData.Rows(e.RowIndex).Cells("txtEntwicklungspaket").EditedFormattedValue
    '                    intArt_der_Editierung = 2 'VIEW
    '                    Form_Entwicklungspaket.ShowDialog() 'DATEN ANZEIGEN
    '                End If
    '                If DataGridViewData.Columns(e.ColumnIndex).Name = "txtPaket" Then
    '                    pbl_txtPaket = DataGridViewData.Rows(e.RowIndex).Cells("txtPaket").EditedFormattedValue
    '                    intArt_der_Editierung = 2 'VIEW
    '                    Form_Paket.ShowDialog() 'DATEN ANZEIGEN
    '                End If
    '            Catch
    '            End Try
    '        End If
    '    Catch ex As Exception
    '        MsgBox("DataGridViewData_CellDoubleClick - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
    '    End Try
    'End Sub

    'Functions

    'TREE BUDGET


#Region "Mouse Control"
    Private Sub ContextMouse_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMouse.Opening
        Console.WriteLine(">> Private Sub ContextMouse_Opening")

        Try
            'If GotRights("ID_AP", pbl_IdAp, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_Auth) = "read" Or pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp = "Ist" Then 'Nur Leserechte
            '    ContextEinfügen.Enabled = False
            'End If
        Catch ex As Exception
            MsgBox("ContextMouse_Opening - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ContextKopieren_Click(sender As Object, e As EventArgs) Handles ContextKopieren.Click
        Console.WriteLine(">> Private Sub ContextKopieren_Click")

        Try
            Dim i As Integer, j As Integer
            pbl_dtCopyData.Clear()

            If iGrid.Cols.Count <= 0 Then Exit Sub

            If pbl_dtCopyData.Columns.Count <= 0 Then 'Kopier Array muss das erste Mal definiert werden
                For i = 0 To iGrid.Cols.Count - 1
                    pbl_dtCopyData.Columns.Add(iGrid.Cols(i).Key)
                    pbl_dtCopyData.Columns(i).DataType = iGrid.Cols(i).CellStyle.ValueType
                Next
            End If

            If iGrid.Rows.Count <= 0 Then Exit Sub

            For i = 0 To iGrid.Rows.Count - 1
                If iGrid.Rows(i).Selected = True Then 'zeile soll kopiert werden
                    pbl_dtCopyData.Rows.Add()
                    Dim row As Integer = pbl_dtCopyData.Rows.Count - 1
                    'Zeile initialisieren
                    For j = 0 To pbl_dtCopyData.Columns.Count - 1
                        If pbl_dtCopyData.Columns(j).DataType = GetType(Double) OrElse pbl_dtCopyData.Columns(j).DataType = GetType(Integer) OrElse pbl_dtCopyData.Columns(j).DataType = GetType(Int64) Then
                            pbl_dtCopyData.Rows(row).Item(j) = 0
                        ElseIf pbl_dtCopyData.Columns(j).DataType = GetType(Boolean) Then
                            pbl_dtCopyData.Rows(row).Item(j) = False
                        ElseIf pbl_dtCopyData.Columns(j).DataType = GetType(Date) Then
                            pbl_dtCopyData.Rows(row).Item(j) = Now
                        Else
                            pbl_dtCopyData.Rows(row).Item(j) = ""
                        End If
                    Next

                    For j = 0 To iGrid.Cols.Count - 1
                        If Not IsDBNull(iGrid.Rows(i).Cells(j).Value) AndAlso Not String.IsNullOrEmpty(iGrid.Rows(i).Cells(j).Value) Then
                            pbl_dtCopyData.Rows(row)(iGrid.Cols(j).Key) = iGrid.Rows(i).Cells(j).Value
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            MsgBox("ContextKopieren_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ContextEinfügen_Click(sender As Object, e As EventArgs) Handles ContextEinfügen.Click
        Console.WriteLine(">> Private Sub ContextEinfügen_Click")

        Dim i As Integer, j As Integer
        Dim selectedRow As Integer, insertRow As Integer, boolGotSelectedRow As Boolean = False

        If con.State = ConnectionState.Closed Then Exit Sub

        Try
            If pbl_dtCopyData.Rows.Count <= 0 Then Exit Sub
            If pbl_dtCopyData.Columns.Count <= 0 Then Exit Sub

            If iGrid.Rows.Count <= 0 Then 'Neue erste Zeile
                selectedRow = 0
            Else
                For i = 0 To iGrid.Rows.Count - 1
                    If iGrid.Rows(i).Selected = True Then 'Zeile gefunden, ab der eingefügt werden soll
                        insertRow = i
                        boolGotSelectedRow = True
                        Exit For
                    End If
                Next
                If boolGotSelectedRow = False Then 'Keine Zeile zum Einfügen ausgewählt
                    selectedRow = 0
                End If
            End If

            For i = 0 To pbl_dtCopyData.Rows.Count - 1
                If boolGotSelectedRow = False Then
                    iGrid.Rows.Add()
                    insertRow = iGrid.Rows.Count - 1
                Else
                    iGrid.Rows.Insert(insertRow)
                End If

                iGrid.CurRow = iGrid.Rows(insertRow) 'Set Fokus

                For j = 0 To pbl_dtCopyData.Columns.Count - 1
                    iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = pbl_dtCopyData.Rows(i)(j)
                    'Bestimmte Werte sollen nicht kopiert werden bzw. angepasst werden
                    If pbl_dtCopyData.Columns(j).ColumnName = "ID_DATA" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = ""
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "lngSort" Then
                        If insertRow > 0 Then
                            iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = iGrid.Rows(insertRow - 1).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value + 10
                        End If
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "Action" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = "N"
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "qryArbeitspaket" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = pbl_IdAp
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "qryVersion" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = pbl_lngVersion
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "qryEntwurfstyp" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "PROJEKT_ID" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = pbl_IdProjekt
                    ElseIf pbl_dtCopyData.Columns(j).ColumnName = "datErstellungsdatum" Then
                        iGrid.Rows(insertRow).Cells(pbl_dtCopyData.Columns(j).ColumnName).Value = Now
                    End If
                Next
                If boolGotSelectedRow = True Then
                    insertRow = insertRow + 1
                End If
            Next

            'Zahlen aktualisieren
            iGrid_Footer()
            iGrid_RemoveSumRows()
            iGrid_CreateSumRows()

            iGrid_ColAndRowWidth()

            boolDataChanged = True
            ToolStripStatusOperation.Text = "Daten erfolgreich eingefügt!"
            StatusStrip.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub LöschenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ContextLöschen.Click 'Zeile zur Löschung markieren
        Console.WriteLine(">> Private Sub LöschenToolStripMenuItem_Click")

        Dim i As Integer, j As Integer
        Try
            If iGrid.Rows.Count <= 0 Then
                Exit Sub
            End If

            For i = 0 To iGrid.Rows.Count - 1
                If iGrid.Rows(i).Selected = True Then
                    iGrid.Rows(i).CellStyle.ForeColor = Color.LightGray
                    iGrid.Rows(i).Cells("Action").Value = "D" 'Delete
                    iGrid.Rows(i).CellStyle.Font = New Font(iGrid.Font, FontStyle.Strikeout)
                    boolDataChanged = True
                End If
                If boolCompareMode = True Then 'Alle Zeilen der gleichen ID markieren
                    For j = 0 To iGrid.Rows.Count - 1
                        If Not j = i Then
                            If iGrid.Rows(j).Cells("ID_DATA").Value = iGrid.Rows(i).Cells("ID_DATA").Value Then
                                iGrid.Rows(j).CellStyle.ForeColor = Color.LightGray
                                iGrid.Rows(j).Cells("Action").Value = "D" 'Delete
                                iGrid.Rows(j).CellStyle.Font = New Font(iGrid.Font, FontStyle.Strikeout)
                            End If
                        End If
                    Next
                End If
            Next

            If boolDataChanged = True Then
                ToolStripStatusOperation.Text = "Daten zur Löschung vorgemerkt!"
                StatusStrip.Refresh()
            End If
        Catch ex As Exception
            MsgBox("LöschenToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MarkierenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MarkierenToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub MarkierenToolStripMenuItem_Click")

        Dim i As Integer
        Try
            If Not iGrid.Rows.Count > 0 Then Exit Sub

            For i = 0 To iGrid.Rows.Count - 1
                If iGrid.Rows(i).Selected = True Then
                    iGrid.Rows(i).CellStyle.BackColor = Color.PowderBlue
                End If
            Next

        Catch ex As Exception
            MsgBox("MarkierenToolStripMenuItem_Click: " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub ContextMenuCopyToCache_Click(sender As Object, e As EventArgs) Handles ContextMenuCopyToCache.Click
        Try
            Clipboard.Clear() 'Bestehende Daten in der Zwischenablage löschen
            Clipboard.SetText("-")

            Dim fCopyPasteManager As iGCopyPasteManager
            fCopyPasteManager = New iGCopyPasteManager(iGrid)

            If Not fCopyPasteManager.CanPasteFromClipboard() Then
                MessageBox.Show(Me, "The clipboard does not contain a compatible object.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            fCopyPasteManager.CopyToClipboard()
        Catch ex As Exception
            MsgBox("ContextMenuCopyToCache_Click: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub iGrid_RowHdrMouseUp(sender As Object, e As iGRowHdrMouseUpEventArgs) Handles iGrid.RowHdrMouseUp
        If e.Button = MouseButtons.Right Then 'Rechtsklick auf den RowHeader führt nicht dazu dass bereits selektiere Zeilen wieder un-selektiert werden
            e.DoDefault = False
        End If
    End Sub
#End Region

#Region "--- MenuBar ---"

#End Region

#Region "-- Report"
    Private Sub DruckenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DruckenToolStripMenuItem.Click
        Console.WriteLine(">> DruckenToolStripMenuItem_Click")
        IGPrintManager1.PrintPreview()
    End Sub

    Private Sub CompareN_AP_Click(sender As Object, e As EventArgs) Handles MenuN_Compare.Click
        Console.WriteLine(">> CompareN_AP_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 2

            Form_Psb.Show()
        Catch ex As Exception
            MsgBox("PsbToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuDatenMitVorgängerversionVergleichen_Click(sender As Object, e As EventArgs) Handles MenuDatenMitVorgängerversionVergleichen.Click
        Console.WriteLine(">> MenuDatenMitVorgängerversionVergleichen_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            Form_DatenVergleich.ShowDialog()

            iGrid_EnableEditFunctions(False, False, "Nur Leserechte")
        Catch ex As Exception
            MsgBox("MenuDatenMitVorgängerversionVergleichen_Click: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub Excel_Export(sender As Object, e As EventArgs) Handles MenuNewReport.Click 'Neu
        Console.WriteLine(">> Private Sub Excel_Export")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 0 'NEUER REPORT

            Form_Psb.Show()

        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuGetDataViaSqlStatement_Click(sender As Object, e As EventArgs) Handles MenuGetDataViaSqlStatement.Click
        Form_Datenabfrage.ShowDialog()
    End Sub
#End Region

#Region "-- Aktionen"

#Region "Mehrere Arbeitspakete bearbeiten"
    Private Sub MehrereArbeitspaketeBearbeitenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuMehrereArbeitspaketeBearbeiten.Click
        Console.WriteLine(">> Private Sub Excel_Export")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 3 'anzeigen/bearbeiten

            Form_Psb.Show()
        Catch ex As Exception
            MsgBox("MenuManager_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Daten kopieren - OK"
    Private Sub KopierenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuCopy.Click
        Console.WriteLine(">> KopierenToolStripMenuItem_Click")
        Try
            Dim h As Integer, i As Integer, j As Integer, z As Integer
            '1. Zu kopierede Daten auswählen
            '2. Kopieraktionen durchführen
#Region "Daten prüfen"
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Then Exit Sub

            '1.
            If ComboVersion.SelectedIndex < 0 Or IsNothing(pbl_LstSelGj) Or ComboEntwurf.SelectedIndex < 0 Then
                Exit Sub 'Nicht alle Eingangsparameter gewählt
            End If
            If iGrid.Rows.Count > 0 Then 'Nur wenn Daten vorhandne sind
                Form_Kopieren.ShowDialog()
            End If
#End Region

#Region "Copy Data"
            '2
            If boolError = True Then
                Exit Sub
            End If

            progress = 5
            For i = 0 To UBound(pbl_Entwurf) 'I = Entwurf
                If pbl_Entwurf(i).boolChecked = True Then

                    For h = 0 To UBound(pbl_ListVersionAp) 'H = VERSION
                        If pbl_ListVersionAp(h).boolChecked = True Then

                            For j = 0 To UBound(pbl_Gj) 'J = GJ
                                If pbl_Gj(j).boolChecked = True Then
                                    For z = 0 To iGrid.Rows.Count - 1
                                        'Daten kopieren
                                        If progress = z Then
                                            ToolStripStatusOperation.Text = "Daten werden kopiert: " & z & "/" & iGrid.Rows.Count - 1
                                            StatusStrip.Refresh()
                                            progress = progress + 5
                                        End If

                                        iGrid_CopyCompleteData(z, pbl_Entwurf(i).txtEntwurfstyp, IDCopyArbeitspaket, pbl_ListVersionAp(h).lngVersion, pbl_Gj(j).intGJ)
                                    Next z
                                End If
                            Next j

                        End If
                    Next h

                End If
            Next i
#End Region

#Region "Data"
            If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
            Else
                Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
            End If

            Get_VersionArbeitspaket(True, pbl_IdAp, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp)
#End Region

            ToolStripStatusOperation.Text = "Daten erfolgreich kopiert"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()

        Catch ex As Exception
            MsgBox("KopierenToolStripMenuItem_Click " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Kosten linear verteilen"
    Private Sub KostenLinearVerteilenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KostenLinearVerteilenToolStripMenuItem.Click
        Form_DatenLinearVerteilen.ShowDialog()
    End Sub
#End Region

#Region "Automatischer IVP für alle sichtbaren Grid Zeilen"
    Private Sub MenuIVP_Click(sender As Object, e As EventArgs) Handles MenuIVP.Click
        Dim i As Integer, j As Integer
        Dim boolCorrectIvp As Boolean

        Try
            If iGrid.Rows.Count = 0 Then Exit Sub
            If boolCompareMode = True Then Exit Sub

            Dim dt As New System.Data.DataTable
            dt = Get_tblEkant(True)

            For i = 0 To iGrid.Rows.Count - 1
                ToolStripStatusOperation.Text = "IVP wird angepasst: " & i & "/" & iGrid.Rows.Count - 1
                StatusStrip.Refresh()

                boolCorrectIvp = False
                If iGrid.Rows(i).Visible = True Then
                    If Not IsNothing(iGrid.Rows(i).Cells("txtBedarfsnummer").Value) Then
                        If Not String.IsNullOrEmpty(iGrid.Rows(i).Cells("txtBedarfsnummer").Value) Then
                            dtIVP = dt.Select("Kostenstelle = '" & iGrid.Rows(i).Cells("txtBedarfsnummer").Value & "'")
                            If Not IsNothing(dtIVP) AndAlso dtIVP.Count > 0 Then 'Es gibt mindestens einen Treffer

                                For j = 0 To dtIVP.Count - 1
                                    If dtIVP(j).Item("Std_Satz") = iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value Then
                                        boolCorrectIvp = True
                                        Exit For
                                    End If
                                Next
                                If boolCorrectIvp = False Then 'IVP muss angepasst werden
                                    If dtIVP.Count = 1 Then 'Eindeutiger IVP
                                        IVP = dtIVP(0).Item("Std_Satz")
                                        iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value = IVP
                                    Else 'Mehrere IVP möglich
                                        Form_IVPAuswahl.ShowDialog()
                                        iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value = IVP
                                    End If
                                    If String.IsNullOrEmpty(iGrid.Rows(i).Cells("Action").Value) Then
                                        iGrid.Rows(i).Cells("Action").Value = "C"
                                    End If
                                    iGrid.Rows(i).Cells("AmountPerYear").Value = iGrid.Rows(i).Cells("dblJanuar").Value + iGrid.Rows(i).Cells("dblFebruar").Value + iGrid.Rows(i).Cells("dblMärz").Value + iGrid.Rows(i).Cells("dblApril").Value + iGrid.Rows(i).Cells("dblMai").Value + iGrid.Rows(i).Cells("dblJuni").Value + iGrid.Rows(i).Cells("dblJuli").Value + iGrid.Rows(i).Cells("dblAugust").Value + iGrid.Rows(i).Cells("dblSeptember").Value + iGrid.Rows(i).Cells("dblOktober").Value + iGrid.Rows(i).Cells("dblNovember").Value + iGrid.Rows(i).Cells("dblDezember").Value
                                    iGrid.Rows(i).Cells("CostsPerYear").Value = iGrid.Rows(i).Cells("AmountPerYear").Value * iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value
                                End If

                            End If
                        End If
                    End If
                End If
            Next

            iGrid_InitializeNumbers() 'Zahlen neu berechnen

            boolDataChanged = True 'Es haben sich Daten geändert

            ToolStripStatusOperation.Text = "IVP erfolgreich angepasst"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("MenuIVP_Click: " & ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region "Prozentuale Anpassung der Daten"
    Private Sub MenuProzentualeAnpassung_Click(sender As Object, e As EventArgs) Handles MenuProzentualeAnpassung.Click
        Dim boolEingabefehler As Boolean = True
        Dim i As Integer, j As Integer
        Dim intPercent As Double
        Dim strInput As String

        Try
            Do Until boolEingabefehler = False
                strInput = InputBox("Um wieviel Prozent sollen die angezeigten Daten angepasst werden (+/-)." & vbCrLf & vbCrLf & "Beispiel: 10% = Eingabe von 10", "Prozentuale Anpassung")
                If Not IsNumeric(strInput) Then
                    MsgBox("Bitte nur Zahlen eingeben!", vbCritical, "Fehler")
                ElseIf String.IsNullOrEmpty(strInput) Or strInput = 0 Then
                    Exit Sub
                Else
                    boolEingabefehler = False
                End If
            Loop

            intPercent = 1 + (strInput / 100)


            If iGrid.Rows.Count = 0 Then Exit Sub
            If boolCompareMode = True Then Exit Sub

            For i = 0 To iGrid.Rows.Count - 1
                ToolStripStatusOperation.Text = "Daten werden angepasst: " & i & "/" & iGrid.Rows.Count - 1
                StatusStrip.Refresh()

                If iGrid.Rows(i).Visible = True Then

                    For j = 1 To 12
                        Dim Monat As String = ConvertIntegerToDoubleMonth(j).dblMonat
                        iGrid.Rows(i).Cells(Monat).Value = iGrid.Rows(i).Cells(Monat).Value * intPercent
                    Next
                    If String.IsNullOrEmpty(iGrid.Rows(i).Cells("Action").Value) Then
                        iGrid.Rows(i).Cells("Action").Value = "C"
                    End If
                    iGrid.Rows(i).Cells("AmountPerYear").Value = iGrid.Rows(i).Cells("dblJanuar").Value + iGrid.Rows(i).Cells("dblFebruar").Value + iGrid.Rows(i).Cells("dblMärz").Value + iGrid.Rows(i).Cells("dblApril").Value + iGrid.Rows(i).Cells("dblMai").Value + iGrid.Rows(i).Cells("dblJuni").Value + iGrid.Rows(i).Cells("dblJuli").Value + iGrid.Rows(i).Cells("dblAugust").Value + iGrid.Rows(i).Cells("dblSeptember").Value + iGrid.Rows(i).Cells("dblOktober").Value + iGrid.Rows(i).Cells("dblNovember").Value + iGrid.Rows(i).Cells("dblDezember").Value
                    iGrid.Rows(i).Cells("CostsPerYear").Value = iGrid.Rows(i).Cells("AmountPerYear").Value * iGrid.Rows(i).Cells("curKosten_pro_Einheit").Value

                End If
            Next

            iGrid_InitializeNumbers() 'Zahlen neu berechnen

            boolDataChanged = True 'Es haben sich Daten geändert

            ToolStripStatusOperation.Text = "Daten erfolgreich prozentual angepasst"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("MenuProzentualeAnpassung_Click: " & ex.Message.ToString)
        End Try
    End Sub
#End Region

#Region "EKant erstellen"
    Private Sub MenuEKantErstellen_Click(sender As Object, e As EventArgs) Handles MenuEKantErstellen.Click
        Try
            CreateEkant()
        Catch ex As Exception
            MsgBox("MenuEKantErstellen_Click: " & ex.Message.ToString)
        End Try
    End Sub
#End Region


#Region "Version - OK"
    Public Sub FreigabeToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles MenuRelease.Click 'Freigabe
        Console.WriteLine(">> FreigabeToolStripMenuItem2_Click")
        If con.State = ConnectionState.Closed Then Exit Sub

        Try
            If ComboProjekt.SelectedIndex < 0 Or ComboArbeitspaket.SelectedIndex < 0 Or IsNothing(pbl_LstSelGj) _
Or ComboEntwurf.SelectedIndex < 0 Or ComboVersion.SelectedIndex < 0 Then
                Exit Sub
            End If
            Dim Index_version As Integer

            FreezeVersion(pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung)

            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("FreigabeToolStripMenuItem2_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub NeuToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles MenuNewVersion.Click 'Neu
        Console.WriteLine(">> NeuToolStripMenuItem5_Click")

        If con.State = ConnectionState.Closed Then Exit Sub

        Try
            If ComboVersion.SelectedIndex < 0 Then
                Exit Sub
            End If

            intArt_der_Editierung = 0 'NEU
            Form_Version.ShowDialog()

            If boolError = False Then
                Try
                    Dim SelectedIndex = Array.FindIndex(pbl_VersionAp, Function(f) f.lngVersion = pbl_lngVersion)
                    pbl_lngVersion = pbl_VersionAp(ViewCombo.View_ComboboxVersion(ComboVersion, SelectedIndex, pbl_VersionAp)).lngVersion
                Catch ex As Exception
                    MsgBox("NeuToolStripMenuItem5_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
                ToolStripStatusOperation.Text = "Version erfolgreich hinzugefügt"
                StatusStrip.Refresh()

            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem5_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ÄnderungToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuChangeVersion.Click 'Änderung
        Console.WriteLine(">> ÄnderungToolStripMenuItem_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Then Exit Sub

            If ComboVersion.SelectedIndex < 0 Then
                Exit Sub
            End If

            intArt_der_Editierung = 1 'ÄNDERUNG

            Form_Version.ShowDialog()

            If boolError = False Then
                VersionDescription.Text = pbl_VersionAp(ComboVersion.SelectedIndex).txtVersionErklärung
                ToolStripStatusOperation.Text = "Version erfolgreich geändert"
                StatusStrip.Refresh()
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ÄnderungToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub cmdVersion_Click(sender As Object, e As EventArgs) Handles cmdVersion.Click 'Info Button Version
        Console.WriteLine(">> cmdVersion_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Then Exit Sub

            If ComboVersion.SelectedIndex < 0 Then
                Exit Sub
            End If

            intArt_der_Editierung = 2
            Form_Version.ShowDialog()

        Catch ex As Exception
            MsgBox("cmdVersion_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Paket - OK"
    Private Sub NeuToolStripMenuItem6_Click(sender As Object, e As EventArgs) Handles MenuNewPaket.Click 'Neu
        Console.WriteLine(">> NeuToolStripMenuItem6_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 0
            Form_Paket.ShowDialog()

            If boolError = False Then 'ComboBox Projekte neu laden
                Try
                    Initialize_iGrid(pbl_dt)
                Catch ex As Exception
                End Try
                ToolStripStatusOperation.Text = "Paket erfolgreich angelegt"
                StatusStrip.Refresh()
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem6_Click- Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ÄnderungToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles MenuChangePaket.Click 'Änderung
        Console.WriteLine(">> ÄnderungToolStripMenuItem1_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            Dim IsEmtpy As Integer
            Try
                IsEmtpy = UBound(pbl_Paket)
            Catch ex As Exception
                Exit Sub
            End Try

            intArt_der_Editierung = 1
            Form_Paket.ShowDialog()

            If boolError = False Then 'ComboBox Projekte neu laden
                Try
                    Initialize_iGrid(pbl_dt)
                Catch ex As Exception
                End Try
                ToolStripStatusOperation.Text = "Paket erfolgreich geändert"
                StatusStrip.Refresh()
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ÄnderungToolStripMenuItem1_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Entwicklungspaket - OK"
    Private Sub NeuToolStripMenuItem7_Click(sender As Object, e As EventArgs) Handles MenuNewEntwicklungspaket.Click 'Neu
        Console.WriteLine(">> NeuToolStripMenuItem7_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            '0. Auf nicht gespeicherte Daten prüfen
            '1. Daten hinzufügen
            '2. EP Liste neu laden und alten Wert vorbelegen. Rest bleibt unverändert

            '1.
            intArt_der_Editierung = 0 'NEU
            Form_Entwicklungspaket.ShowDialog() 'HINZUFÜGEN

            '2.
            'Neu laden: Setup_DataGridView und ViewDataGrid_Daten
            If boolError = False Then
                Get_Entwicklungspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT")) 'Entwicklungspakete

                '-- SETUP DATAGRIDVIEW UND pbl_Data IN DATAGRIDVIEW ANZEIGEN
                Initialize_iGrid(pbl_dt)

                ToolStripStatusOperation.Text = "Entwicklungspaket erfolgreich hinzufgefügt"
                StatusStrip.Refresh()
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem7_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ÄnderungToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles MenuChangeEntwicklungspaket.Click 'Änderung
        Console.WriteLine(">> ÄnderungToolStripMenuItem2_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 1 'ÄNDERUNG
            Form_Entwicklungspaket.ShowDialog() 'DATEN ÄNDERN

            'Neu laden: Setup_DataGridView und ViewDataGrid_Daten
            If boolError = False Then
                Try
                    Get_Entwicklungspaket(True, pbl_dtProjekt.Rows(ComboProjekt.SelectedIndex).Item("ID_PROJEKT")) 'Entwicklungspakete
                    If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                        Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
                    Else
                        Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
                    End If
                Catch
                End Try

                Try
                    Initialize_iGrid(pbl_dt)
                Catch
                End Try
                ToolStripStatusOperation.Text = "Entwicklungspaket erfolgreich aktualisiert"
                StatusStrip.Refresh()
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ÄnderungToolStripMenuItem2_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Arbeitspaket"
    Private Sub NeuToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles MenuNewArbeitspaket.Click
        Console.WriteLine(">> NeuToolStripMenuItem8_Click")

        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            '0. Auf nicht gespeicherte Daten prüfen
            '1. Daten hinzufügen
            '2. AP Liste neu laden und alten Wert vorbelegen. Rest bleibt unverändert
            Dim Index_AP As Integer

            '1.
            intArt_der_Editierung = 0 'NEU
            Form_Arbeitspaket.ShowDialog()



            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem8_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub ÄnderungToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles MenuChangeArbeitspaket.Click
        Console.WriteLine(">> ÄnderungToolStripMenuItem3_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 1 'ÄNDERUNG
            Form_Arbeitspaket.ShowDialog()

            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("ÄnderungToolStripMenuItem3_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuDeleteAP_Click(sender As Object, e As EventArgs) Handles MenuDeleteAP.Click
        Console.WriteLine("FUNC Private Sub MenuDeleteAP_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 3 'Löschen
            Form_Arbeitspaket.ShowDialog()

            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("Private Sub MenuDeleteAP_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub cmdArbeitspaket_Click(sender As Object, e As EventArgs) Handles cmdArbeitspaket.Click 'Info Button AP anzeigen
        Console.WriteLine(">> cmdArbeitspaket_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 2 'VIEW

            If ComboArbeitspaket.SelectedIndex < 0 Then 'Gibt nichts zum anzeigen
                Exit Sub
            End If

            pbl_IdAp = pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP") 'Übergabe in andere Form der ID_AP
            Form_Arbeitspaket.ShowDialog()

        Catch ex As Exception
            MsgBox("cmdArbeitspaket_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Projekt"
    Private Sub NeuToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles MenuNewProjekt.Click
        Console.WriteLine(">> Private Sub NeuToolStripMenuItem9_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 0 'NEU
            Form_Projekt.ShowDialog()

            Dim i As Integer
            If boolError = False Then 'ComboBox Projekte neu laden
                Try
                    pbl_IdProjekt = pbl_dtProjekt.Rows(ViewCombo.View_ComboboxProjekt(ComboProjekt, pbl_IdProjekt, pbl_dtProjekt)).Item("ID_PROJEKT")
                    ToolStripStatusOperation.Text = "Projekt erfolgreich angelegt"
                    StatusStrip.Refresh()
                Catch ex As Exception
                    MsgBox("NeuToolStripMenuItem9_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try
            End If
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("NeuToolStripMenuItem9_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#End Region

#Region "-- Server - OK"
    Private Sub VerbindungHerstellenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerbindungHerstellenToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub VerbindungHerstellenToolStripMenuItem_Click")
        Try
            Close_DB()

            Connect_DB(pbl_SelectedConnection)
            If boolConnected = True Then
                MsgBox("Serververbindung erfolgreich hergestellt!", Title:="Verbindung zum Server", Buttons:=MsgBoxStyle.Information)
            Else
                MsgBox("Es konnte keine Verbindung zum Server hergestellt werden!" & vbCrLf & "Bitte wenden Sie sich an Ihren Systemadministrator.", Title:="Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("VerbindungHerstellenToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub VerbindungTrennenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerbindungTrennenToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub VerbindungTrennenToolStripMenuItem_Click")
        Try
            Close_DB()
            MsgBox("Verbindung zum Server getrennt!", Title:="Verbindung zum Server", Buttons:=MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox("VerbindungTrennenToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub DatenAktualisierenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DatenAktualisierenToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub DatenAktualisierenToolStripMenuItem_Click")
        Try
            If pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, False, pbl_LstSelGj, pbl_Sql)
            Else
                Get_tblData(True, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), pbl_VersionAp(ComboVersion.SelectedIndex).lngVersion, pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
            End If

            If boolCompareMode = True And Not pbl_Entwurf(ComboEntwurf.SelectedIndex).txtEntwurfstyp = "Ist" Then
                Dim Ist As System.Data.DataTable = Get_tblData(False, pbl_dtArbeitspaket.Rows(ComboArbeitspaket.SelectedIndex).Item("ID_AP"), 1, "Ist", False, pbl_LstSelGj, pbl_Sql)

                pbl_dt = VergleichPlanZuIst(pbl_dt, Ist)
            End If

            Reload_iGrid(pbl_dt)
        Catch ex As Exception
            MsgBox("DatenAktualisierenToolStripMenuItem_Click: " & ex.Message.ToString)
        End Try
    End Sub

    Public Sub SapImportTomyBudget(sender As Object, e As EventArgs) Handles SAPImportToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub SapImportTomyBudget")
        Try
#Region "Methodenbeschreibung"
            '1. Manueller Datenimport: a) Einzelkosten Ist und b) Einzelkosten Obligo
            '2. a) und b) in tableSapData Format überführen
            '3. Join a) und b) in eine Liste vom Format tableSapData
            '4. Bedarfsnummern zu BEST und BANF laden
            '5. Liste um Bedarfsnummern erweitern (For über BEST & BANF aus Performance Gründen)
            '6. Liste in Excel ausgeben
            '7. Daten in tableData Format umwandeln
            '8. Betroffene PSP-Elemente aus Access Datenbank löschen
            '9. Neue Einträge in Access Datenbank schreiben

            '7. tableSapData in tabelData Format konvertieren und gruppieren nach 
            '   PSP, GJ, Sachkosten, Obligo & Kostenstelle/Bedarfsnummer (im Zweifel führt die Bedarfsnummer)
#End Region

#Region "Methodenprüfungen"
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If
#End Region

            Me.ToolStripStatusOperation.Text = "Einzel Ist- und Obligokosten werden verarbeitet..."
            StatusStrip.Refresh()

            Dim I As Integer, J As Integer

#Region "1 + 2 Ist und Obligo Daten importieren"
            Dim dt_Ist As System.Data.DataTable
            Dim dt_Obligo As System.Data.DataTable
            'a)
            Me.ToolStripStatusOperation.Text = "Ist Einzelkosten werden importiert. Dieser Vorgang kann einige Minuten dauern..."
            Me.StatusStrip.Refresh()

            dt_Ist = ConvertIstToTableSap(Import_EinzelkostenIst)

            'b)
            Me.ToolStripStatusOperation.Text = "Obligo Einzelkosten werden importiert. Dieser Vorgang kann einige Minuten dauern..."
            Me.StatusStrip.Refresh()

            dt_Obligo = ConvertObligoToTableSap(Import_EinzelkostenObligo)
#End Region

#Region "3. Ist + Obligo in einer Tabelle vereinen"
            ToolStripStatusOperation.Text = "Daten werden zusammengefügt. Dieser Vorgang kann einige Minuten dauern..."
            StatusStrip.Refresh()

            Dim dt As System.Data.DataTable
            If Not IsNothing(dt_Ist) AndAlso dt_Ist.Rows.Count > 0 Then
                dt = dt_Ist.Clone
                dt.Merge(dt_Ist, False)
                If Not IsNothing(dt_Obligo) AndAlso dt_Obligo.Rows.Count > 0 Then
                    dt.Merge(dt_Obligo, False)
                End If
            ElseIf Not IsNothing(dt_Obligo) AndAlso dt_Obligo.Rows.Count > 0 Then
                dt = dt_Obligo.Clone
                dt.Merge(dt_Obligo, False)
            Else
                MsgBox("Abbruch der Aktion, da eine Einzeldaten importiert werden konnten.", MsgBoxStyle.Critical, "Fehler beim Import")
                Exit Sub
            End If
#End Region

#Region "3.1 ANÜ Kosten aufschlüsseln"
            dt = ConvertFremdPCosts(dt)
#End Region

#Region "4. SAP Daten lesen und Datentabelle um Bedarfsnummer(n) erweitern)"
            Dim listBestBedarfNr As tableEket_Po()
            Dim listBanfBedarfNr As tableEban_Kn()

            If SAP_Login(pbl_dtSAPCon) = True Then 'Verbindung zu SAP herstellen

                Dim SearchBestPos As Integer
                '-- Liste mit allen PSP Elementen und GJ erzeugen
                Dim PspGj As tablePspGj()
                Dim IndexPspGj As Long
                Dim SearchPspGj As Long

                ToolStripStatusOperation.Text = "Liste mit PSP-Elemten und Geschäftsjahren erstellen, zu welchen alle Bestellungen und Bestellanforderungen geladen werden..."
                StatusStrip.Refresh()

#Region "SAP Suchkriterien definieren"
                For I = 0 To dt.Rows.Count - 1
                    If Not IsNothing(dt.Rows(I).Item("PSP")) Then

                        If IsNothing(PspGj) Then
                            ReDim PspGj(0)
                            IndexPspGj = 0
                            SearchPspGj = -1
                        Else
                            SearchPspGj = Array.FindIndex(PspGj, Function(f) f.Key = dt.Rows(I).Item("qryGeschäftsjahr") & "-" & dt.Rows(I).Item("PSP"))
                            If SearchPspGj < 0 Then 'Neuen eintrag erzeugen
                                ReDim Preserve PspGj(UBound(PspGj) + 1)
                                IndexPspGj = UBound(PspGj)
                            End If
                        End If
                        If SearchPspGj < 0 Then
                            PspGj(IndexPspGj).PSP = dt.Rows(I).Item("PSP")
                            PspGj(IndexPspGj).GJ = dt.Rows(I).Item("qryGeschäftsjahr")
                            PspGj(IndexPspGj).Key = dt.Rows(I).Item("qryGeschäftsjahr") & "-" & dt.Rows(I).Item("PSP")
                        End If
                    End If
                Next
#End Region

                Try
                    '5.
#Region "BEST"
                    ToolStripStatusOperation.Text = "Bestellungen werden um Bedarfsnummern erweitert. Dieser Vorgang kann einige Minuten dauern..."
                    StatusStrip.Refresh()

                    listBestBedarfNr = Get_EketPo(PspGj)

                    progress = 50
                    For I = 0 To UBound(listBestBedarfNr)
                        If progress = I Then
                            ToolStripStatusOperation.Text = "Bestellungen um Bedarfsnummern erweitern: " & I & "/" & UBound(listBestBedarfNr)
                            StatusStrip.Refresh()
                            progress = progress + 50
                        End If

                        Dim foundRows() As Data.DataRow
                        foundRows = dt.Select("Bestellung = '" & listBestBedarfNr(I).Bestellung & "' and BestellPosition = '" & listBestBedarfNr(I).BestellPos & "'")

                        If Not IsNothing(foundRows) Then 'Es gibt mindestens einen Treffer.
                            For J = 0 To foundRows.Count - 1
                                foundRows(J).Item("txtBedarfsnummer") = listBestBedarfNr(I).Bedarfsnummer
                            Next
                        End If
                    Next
#End Region

#Region "BANF"
                    ToolStripStatusOperation.Text = "Bestellanforderungen werden um Bedarfsnummern erweitert. Dieser Vorgang kann einige Minuten dauern..."
                    StatusStrip.Refresh()

                    listBanfBedarfNr = Get_EbanKn(PspGj)

                    progress = 50
                    For I = 0 To UBound(listBanfBedarfNr)
                        If progress = I Then
                            ToolStripStatusOperation.Text = "Bestellanforderungen um Bedarfsnummern erweitern: " & I & "/" & UBound(listBanfBedarfNr)
                            StatusStrip.Refresh()
                            progress = progress + 50
                        End If

                        Dim foundRows() As Data.DataRow
                        foundRows = dt.Select("Bestellung = '" & listBanfBedarfNr(I).BANFN & "' and BestellPosition = '" & listBanfBedarfNr(I).BNFPO & "'")

                        If Not IsNothing(foundRows) Then 'Es gibt mindestens einen Treffer.
                            For J = 0 To foundRows.Count - 1
                                foundRows(J).Item("txtBedarfsnummer") = listBanfBedarfNr(I).BEDNR
                            Next
                        End If
                    Next
#End Region
                Catch 'Keine Bedarfsnummern --> Gruppierung wird grob
                End Try

                SAP_Logout() 'Verbindung mit SAP beenden
            End If
#End Region

#Region "6. Daten in Excel ausgeben"
            ToolStripStatusOperation.Text = "Daten werden in Excel ausgegeben..."
            StatusStrip.Refresh()

#Region "DataTable in String Format convertieren"
            Dim ArrList(dt.Rows.Count + 1, dt.Columns.Count) As String
            'Überschriften
            For J = 0 To dt.Columns.Count - 1
                ArrList(0, J) = dt.Columns(J).ColumnName
            Next

            For I = 0 To dt.Rows.Count - 1
                For J = 0 To dt.Columns.Count - 1
                    ArrList(I + 1, J) = dt.Rows(I)(J).ToString
                Next
            Next
#End Region

#Region "Daten in Excel schreiben"
            exApp = New Excel.Application
            exWB = exApp.Workbooks.Add()
            Sheet = CType(exWB.Worksheets.Item(1), Excel.Worksheet)
            exApp.ScreenUpdating = False
            exApp.Calculation = XlCalculation.xlCalculationManual

            Sheet.Range("A1").Resize(dt.Rows.Count + 1, dt.Columns.Count).Value = ArrList

            exApp.ScreenUpdating = True
            exApp.Calculation = XlCalculation.xlCalculationAutomatic
            exApp.Visible = True
            exApp.WindowState = Excel.XlWindowState.xlMaximized
#End Region
#End Region

#Region "8. Alte Daten löschen"
            ToolStripStatusOperation.Text = "Alte Daten auf der Datenbank werden gelöscht. Dieser Vorgang kann einige Minuten dauern..."
            StatusStrip.Refresh()
            progress = 100

            'Zu löschende Arbeitspakete herausfinden
            Dim dt_deleteArbeitspaket As System.Data.DataTable = dt.DefaultView.ToTable("DistinctTable", True, "qryArbeitspaket")
            Dim dt_deleteGJ As System.Data.DataTable = dt.DefaultView.ToTable("DistinctTable", True, "qryGeschäftsjahr")

            Try
                If Not IsNothing(dt_deleteArbeitspaket) Then
                    For I = 0 To dt_deleteArbeitspaket.Rows.Count - 1
                        If progress = I Then
                            ToolStripStatusOperation.Text = "Alte Daten auf der Datenbank werden gelöscht: " & I & "/" & dt_deleteArbeitspaket.Rows.Count - 1
                            StatusStrip.Refresh()
                            progress = progress + 100
                        End If
                        If Not IsNothing(dt_deleteGJ) Then
                            For J = 0 To dt_deleteGJ.Rows.Count - 1
                                Delete_Ist_Data(dt_deleteArbeitspaket.Rows(I).Item("qryArbeitspaket"), dt_deleteGJ.Rows(J).Item("qryGeschäftsjahr"))
                            Next J
                        End If
                    Next I
                End If
            Catch
            End Try
#End Region

#Region "9. Neue Daten auf die Datenbank schreiben"
            ToolStripStatusOperation.Text = "Neue Daten werden auf der Datenbank gespeichert. Dieser Vorgang kann einige Minuten dauern..."
            StatusStrip.Refresh()

            progress = 50 'In 50er Schritte ein Update senden
            If Not IsNothing(dt) Then
                For J = 0 To dt.Rows.Count - 1
                    If Not String.IsNullOrEmpty(dt.Rows(J).Item("qryGeschäftsjahr")) And Not String.IsNullOrEmpty(dt.Rows(J).Item("curKosten_pro_Einheit")) _
                        And Not String.IsNullOrEmpty(dt.Rows(J).Item("txtBeschreibung")) Then

                        If J = progress Then
                            ToolStripStatusOperation.Text = "Neue Daten werden auf der Datenbank gespeichert: " & J & "/" & dt.Rows.Count - 1
                            StatusStrip.Refresh()
                            progress = progress + 50
                        End If
                        Add_IstData(dt.Rows(J).Item("qryEntwurfstyp"),
                                    dt.Rows(J).Item("qryArbeitspaket"),
                                    dt.Rows(J).Item("qryVersion"),
                                    dt.Rows(J).Item("qryGeschäftsjahr"),
                                    dt.Rows(J).Item("qryKostenart"),
                                    dt.Rows(J).Item("txtBeschreibung"),
                                    dt.Rows(J).Item("txtBemerkung"),
                                    dt.Rows(J).Item("txtZusatzfeld"),
                                    dt.Rows(J).Item("curKosten_pro_Einheit"),
                                    False,
                                    Now,
                                    dt.Rows(J).Item("txtBedarfsnummer"),
                                    dt.Rows(J).Item("dblJanuar"),
                                    dt.Rows(J).Item("dblFebruar"),
                                    dt.Rows(J).Item("dblMärz"),
                                    dt.Rows(J).Item("dblApril"),
                                    dt.Rows(J).Item("dblMai"),
                                    dt.Rows(J).Item("dblJuni"),
                                    dt.Rows(J).Item("dblJuli"),
                                    dt.Rows(J).Item("dblAugust"),
                                    dt.Rows(J).Item("dblSeptember"),
                                    dt.Rows(J).Item("dblOktober"),
                                    dt.Rows(J).Item("dblNovember"),
                                    dt.Rows(J).Item("dblDezember"),
                                    dt.Rows(J).Item("qryKlassifizierung"),
                                    dt.Rows(J).Item("qryEntwicklungspaket"),
                                    dt.Rows(J).Item("qryPaket"),
                                    dt.Rows(J).Item("boolObligo"),
                                    dt.Rows(J).Item("txtKst_Lst"),
                                    dt.Rows(J).Item("lngSort"),
                                    dt.Rows(J).Item("IdSap"))
                    End If
                Next
            End If
#End Region

            'MenuShowSapData.Enabled = True
            ToolStripStatusOperation.Text = "Daten erfolgreich geladen und gespeichert"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("SapImportTomyBudget: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub LiveSAPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LiveSAPToolStripMenuItem.Click
        Console.WriteLine("CLICK: Private Sub LiveSAPToolStripMenuItem_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            intArt_der_Editierung = 4 'Live SAP Daten

            Form_Psb.Show()
        Catch ex As Exception
            MsgBox("Private Sub LiveSAPToolStripMenuItem_Click: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuSapSystem_Click(sender As Object, e As EventArgs) Handles MenuSapSystem.Click
        If pbl_dtSAPCon.Rows(0).Item("system").ToString = "P01" Then
            pbl_dtSAPCon = Set_SAP_Connection("K01")
        ElseIf pbl_dtSAPCon.Rows(0).Item("system").ToString = "K01" OrElse pbl_dtSAPCon.Rows.Count = 0 Then
            pbl_dtSAPCon = Set_SAP_Connection("P01")
        End If
    End Sub
#End Region

#Region "-- Hilfe - OK"
    Private Sub ÜbersichtSachkontenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MenuSachkonten.Click
        Console.WriteLine(">> Private Sub ÜbersichtSachkontenToolStripMenuItem_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If
            AddHandler Form_Sakto_Ekant.Activated, AddressOf Form_Sakto_Ekant.Sakto_EkantForm_Activated
            intForm_Sakto_Ekant = 0
            Form_Sakto_Ekant.Show()
        Catch ex As Exception
            MsgBox("ÜbersichtSachkontenToolStripMenuItem_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuEkant_Click(sender As Object, e As EventArgs) Handles MenuEkant.Click
        Console.WriteLine(">> Private Sub MenuEkant_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If
            AddHandler Form_Sakto_Ekant.Activated, AddressOf Form_Sakto_Ekant.Sakto_EkantForm_Activated
            intForm_Sakto_Ekant = 1
            Form_Sakto_Ekant.Show()
        Catch ex As Exception
            MsgBox("MenuEkant_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuHandbuch_Click(sender As Object, e As EventArgs) Handles MenuHandbuch.Click
        Console.WriteLine(">> Private Sub MenuHandbuch_Click")
        Try
            Const path = "Z:\E\EM\Alle\myBudget\Anwender Dokumentation myBudget.pdf"
            System.Diagnostics.Process.Start(path)
        Catch
            MsgBox("Leider ist aktuell kein Handbuch verfügbar", vbInformation, "Fehler")
        End Try
    End Sub
#End Region

#Region "-- Verwaltung --"
    Private Sub MenuPundAp_Click(sender As Object, e As EventArgs) Handles MenuPundAp.Click
        Dim Form As New UserAuthForm
        Form.ShowDialog()
    End Sub

    Private Sub MenuPasswordÄndern_Click(sender As Object, e As EventArgs) Handles MenuPasswordÄndern.Click
        Console.WriteLine("CLICK: MenuPasswordÄndern_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            Form_UserPassword.Show()
        Catch ex As Exception
            MsgBox("Private Sub MenuPasswordÄndern_Click: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub MenuBenachrichtigungsEmail_Click(sender As Object, e As EventArgs) Handles MenuBenachrichtigungsEmail.Click
        Console.WriteLine("CLICK: MenuBenachrichtigungsEmail_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            Form_UserEmail.txtEmailAddr.Text = DBStringToString(pbl_dtUser(intCurrentUser).Item("txtEmail"))
            Form_UserEmail.Show()

            'Wenn Email gelöscht wurde den Subscriptionstatus rücksetzen !ab
            If Not SubscriptionsPossible() Then
                subscriptionState = False
                ShowSubscriptionDisabled(EmailNotificationState)
            End If
        Catch ex As Exception
            MsgBox("Private Sub MenuBenachrichtigungsEmail_Click: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

#End Region

#Region "Compare Modus - OK"
    Private Sub OvalTrue_Click(sender As Object, e As EventArgs) Handles OvalTrue.Click
        Console.WriteLine(">> Private Sub OvalTrue_Click")

        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

            'Daten laden
            Dim dt As System.Data.DataTable = Get_tblData(True, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)

            UndoCompare(dt)
        Catch ex As Exception
            MsgBox("OvalTrue_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub OvalFalse_Click(sender As Object, e As EventArgs) Handles OvalFalse.Click
        Console.WriteLine(">> OvalFalse_Click")
        Try
            'Prüfen ob eine aktive Verbindung zum Server existiert
            If con.State = ConnectionState.Closed Or boolConnected = False Then
                MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
                Exit Sub
            End If

#Region "Daten"
            ToolStripStatusOperation.Text = "Plan und Ist Daten herunterladen..."
            StatusStrip.Refresh()

            Dim Ist As System.Data.DataTable
            Dim Plan As System.Data.DataTable
            Try
                Ist = Get_tblData(False, pbl_IdAp, 1, "Ist", False, pbl_LstSelGj, pbl_Sql)
                Plan = Get_tblData(True, pbl_IdAp, pbl_lngVersion, pbl_Entwurf(pbl_IndexEntwurf).txtEntwurfstyp, True, pbl_LstSelGj, pbl_Sql)
            Catch
            End Try
#End Region

            DoCompare(Plan, Ist)
        Catch ex As Exception
            MsgBox("OvalFalse_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub DoCompare(Plan As System.Data.DataTable, Ist As System.Data.DataTable)
        Console.WriteLine("< Public Sub DoCompare")
        Try
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    Exit Sub
                Else
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
            iGrid_ClearFilterAndGroups()
            AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

#Region "Aktivieren/Deaktivieren von Schaltflächen"
            'Compare Mode
            boolCompareMode = True
            OvalFalse.Visible = False
            OvalTrue.Visible = True
            'Menüaktionen deaktivieren
            MenuAktionen.Enabled = False
            ShowMultiData.Visible = False
            SplitMultiData.Visible = False
            'Aktivitäten deaktivieren
            BorderData.BorderColor = Color.SteelBlue
            'Auswahlboxen deaktivieren
            ComboProjekt.Enabled = False
            ComboVersion.Enabled = False
            ComboArbeitspaket.Enabled = False
            CheckedListGJ.Enabled = False
            ComboEntwurf.Enabled = False

            lblCompare.Text = "Compare Mode"
            lblCompare.ForeColor = Color.SteelBlue
#End Region

            Me.Refresh()

#Region "Daten anzeigen"
            If IsNothing(Plan) OrElse IsNothing(Ist) Then
                MsgBox("Keine Plan- oder Ist Daten vorhanden.", vbCritical, "Fehler: keine Daten vorhanden")
                Exit Sub
            End If

            If Plan.Rows.Count = 0 OrElse Ist.Rows.Count = 0 Then
                MsgBox("Keine Plan- oder Ist Daten vorhanden.", vbCritical, "Fehler: keine Daten vorhanden")
                Exit Sub
            End If

            pbl_dt = VergleichPlanZuIst(Plan, Ist)
            Reload_iGrid(pbl_dt)
#End Region

            'Ende
            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("DoCompare - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub UndoCompare(dt As System.Data.DataTable)
        Console.WriteLine("< Public Sub UndoCompare")
        Try
            If boolDataChanged = True Then
                If MsgBox("Alle nicht gespeicherten Informationen gehen verloren. Wollen Sie die Aktion durchführen?", MsgBoxStyle.YesNo, "Information") = vbNo Then
                    Exit Sub
                Else
                    boolDataChanged = False 'Daten gehen verloren
                End If
            End If

            RemoveHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped
            iGrid_ClearFilterAndGroups()
            AddHandler iGrid.BeforeContentsGrouped, AddressOf fGrid_BeforeContentsGrouped

#Region "Aktivieren/Deaktivieren von Schaltflächen"
            'Compare Mode
            boolCompareMode = False
            OvalFalse.Visible = True
            OvalTrue.Visible = False
            'Menüaktionen deaktivieren
            MenuAktionen.Enabled = True
            ShowMultiData.Visible = True
            SplitMultiData.Visible = True
            'Aktivitäten aktivieren
            BorderData.BorderColor = Color.DarkRed
            'Auswahlboxen aktivieren
            ComboProjekt.Enabled = True
            ComboVersion.Enabled = True
            ComboArbeitspaket.Enabled = True
            CheckedListGJ.Enabled = True
            ComboEntwurf.Enabled = True

            lblCompare.Text = "Edit Mode"
            lblCompare.ForeColor = Color.DarkRed
#End Region

            Me.Refresh()

            Reload_iGrid(dt)

            'Ende
            ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            StatusStrip.Refresh()
            TimerEditForm.Interval = 5000 '1000 = 1 sec
            TimerEditForm.Start()
        Catch ex As Exception
            MsgBox("UndoCompare - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub
#End Region

#Region "Filtermenü ein/ausklappen"
    Public Sub cmdHideFilter_Click(sender As Object, e As EventArgs) Handles cmdHideFilter.Click
        Console.WriteLine(">> Private Sub cmdHideFilter_Click")
        Try
            HideFilter()
        Catch ex As Exception
            MsgBox("cmdHideFilter_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Private Sub cmdShowFilter_Click(sender As Object, e As EventArgs) Handles cmdShowFilter.Click
        Console.WriteLine(">> Private Sub cmdShowFilter_Click")
        Try
            ShowFilter()
        Catch ex As Exception
            MsgBox("cmdShowFilter_Click - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Sub

    Public Sub HideFilter()
        Console.WriteLine("< Public Sub HideFilter")

        Me.SuspendLayout()
        Line1.Visible = False
        Line2.Visible = False
        lblProjekt.Visible = False
        ComboProjekt.Visible = False
        lblEntwurfstyp.Visible = False
        ComboEntwurf.Visible = False
        lblArbeitspaket.Visible = False
        ComboArbeitspaket.Visible = False
        cmdArbeitspaket.Visible = False
        lblGJ.Visible = False
        CheckedListGJ.Visible = False
        lblVers.Visible = False
        ComboVersion.Visible = False
        cmdVersion.Visible = False
        VersionDescription.Visible = False
        'TreeBudget.Visible = False

        SaveData.Location = New Point(6, 91)
        SplitAddRow.Location = New Point(47, 91)
        AddRow.Location = New Point(55, 91)
        ExcelExport.Location = New Point(ExcelExport.Location.X, 91)
        FooterCalcMethod.Location = New Point(FooterCalcMethod.Location.X, 91)
        SplitExcel.Location = New Point(SplitExcel.Location.X, 91)
        SplitGetSap.Location = New Point(SplitGetSap.Location.X, 91)
        cmdGetSap.Location = New Point(cmdGetSap.Location.X, 91)
        ShowMultiData.Location = New Point(ShowMultiData.Location.X, 91)
        SplitMultiData.Location = New Point(SplitMultiData.Location.X, 91)
        SplitEmail.Location = New Point(SplitEmail.Location.X, 91)
        EmailNotificationState.Location = New Point(EmailNotificationState.Location.X, 91)
        lblCompare.Location = New Point(140, 95)
        RectangleCompare.Location = New Point(280, 86)
        OvalFalse.Location = New Point(287, 91)
        OvalTrue.Location = New Point(324, 91)
        lblBearbeitung.Location = New Point(450, 95)
        BorderData.Location = New Point(6, 135)
        BorderData.Size = New Size(ClientSize.Width - 16, ClientSize.Height - StatusStrip.Height - BorderData.Location.Y - 6)
        iGrid.Location = New Point(12, 140)
        iGrid.Size = New Size(ClientSize.Width - 28, ClientSize.Height - StatusStrip.Height - iGrid.Location.Y - 12)
        lblCurrentArbeitspaket.Visible = True
        lblCurrentArbeitspaket.Location = New Point(59, 42)
        Try
            Dim foundRows() As Data.DataRow = pbl_dtArbeitspaket.Select("ID_AP = " & pbl_IdAp & "")
            Dim RowIndex As Integer = -1
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                RowIndex = pbl_dtArbeitspaket.Rows.IndexOf(foundRows(0))
            End If
            If RowIndex >= 0 Then
                lblCurrentArbeitspaket.Text = "Arbeitspaket: " & pbl_dtArbeitspaket.Rows(RowIndex).Item("txtArbeitspaket") & " - " & pbl_dtArbeitspaket.Rows(RowIndex).Item("txtPSP_Element")
            Else
                lblCurrentArbeitspaket.Text = "Arbeitspaket: -"
            End If
        Catch
            lblCurrentArbeitspaket.Visible = False
        End Try

        cmdHideFilter.Visible = False
        cmdShowFilter.Visible = True
        LineHideShow.Location = New Point(0, 72)
        Me.ResumeLayout()
    End Sub

    Public Sub ShowFilter()
        Console.WriteLine("< Public Sub ShowFilter()")

        Me.SuspendLayout()
        Line1.Visible = True
        Line2.Visible = True
        lblProjekt.Visible = True
        ComboProjekt.Visible = True
        lblEntwurfstyp.Visible = True
        ComboEntwurf.Visible = True
        lblArbeitspaket.Visible = True
        ComboArbeitspaket.Visible = True
        cmdArbeitspaket.Visible = True
        lblGJ.Visible = True
        CheckedListGJ.Visible = True
        lblVers.Visible = True
        ComboVersion.Visible = True
        cmdVersion.Visible = True
        VersionDescription.Visible = True
        'TreeBudget.Visible = True
        lblCurrentArbeitspaket.Visible = False

        SaveData.Location = New Point(6, 209)
        SplitAddRow.Location = New Point(47, 209)
        AddRow.Location = New Point(55, 209)
        ExcelExport.Location = New Point(ExcelExport.Location.X, 209)
        FooterCalcMethod.Location = New Point(FooterCalcMethod.Location.X, 209)
        SplitExcel.Location = New Point(SplitExcel.Location.X, 209)
        SplitGetSap.Location = New Point(SplitGetSap.Location.X, 209)
        cmdGetSap.Location = New Point(cmdGetSap.Location.X, 209)
        ShowMultiData.Location = New Point(ShowMultiData.Location.X, 209)
        SplitMultiData.Location = New Point(SplitMultiData.Location.X, 209)
        SplitEmail.Location = New Point(SplitEmail.Location.X, 209)
        EmailNotificationState.Location = New Point(EmailNotificationState.Location.X, 209)

        lblCompare.Location = New Point(140, 214)
        RectangleCompare.Location = New Point(280, 205)
        OvalFalse.Location = New Point(287, 210)
        OvalTrue.Location = New Point(324, 210)
        lblBearbeitung.Location = New Point(450, 217)
        BorderData.Location = New Point(6, 253)
        BorderData.Size = New Size(ClientSize.Width - 16, ClientSize.Height - StatusStrip.Height - BorderData.Location.Y - 6)
        iGrid.Location = New Point(12, 260)
        iGrid.Size = New Size(ClientSize.Width - 28, ClientSize.Height - StatusStrip.Height - iGrid.Location.Y - 12)

        cmdHideFilter.Visible = True
        cmdShowFilter.Visible = False

        LineHideShow.Location = New Point(0, 200)
        Me.ResumeLayout()
    End Sub
#End Region

#Region "Terminverfolgung"
    Private Sub TerminverfolgungToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TerminverfolgungToolStripMenuItem.Click
        Console.WriteLine(">> Private Sub TerminverfolgungToolStripMenuItem_Click")
        Form_Termine.Show()
    End Sub
#End Region

#Region "Email Notification"    '!ab added
    Private Sub EmailNotificationState_Click(sender As Object, e As EventArgs) Handles EmailNotificationState.Click
        Console.WriteLine(">> Private Sub EmailNotifationState_Click")
        ToggleSubscriptionState(sender)
        Dim msg As String
        If subscriptionState = True Then
            msg = "Email-Benachrichtigung aktiviert"
        Else
            msg = "Email-Benachrichtigung deaktiviert"
        End If

        ToolStripStatusOperation.Text = msg
        StatusStrip.Refresh()
        TimerEditForm.Interval = 5000 '1000 = 1 sec
        TimerEditForm.Start()

    End Sub

#End Region



End Class
