﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatenabfrageForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.aGrid = New TenTec.Windows.iGridLib.iGrid()
        Me.IGrid1DefaultCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGrid1DefaultColHdrStyle = New TenTec.Windows.iGridLib.iGColHdrStyle(True)
        Me.IGrid1RowTextColCellStyle = New TenTec.Windows.iGridLib.iGCellStyle(True)
        Me.IGAutoFilterManager1 = New TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager()
        Me.DropDowntblDataStruktur = New TenTec.Windows.iGridLib.iGDropDownList()
        Me.DropDownData = New TenTec.Windows.iGridLib.iGDropDownList()
        CType(Me.aGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'aGrid
        '
        Me.aGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.aGrid.DefaultCol.CellStyle = Me.IGrid1DefaultCellStyle
        Me.aGrid.DefaultCol.ColHdrStyle = Me.IGrid1DefaultColHdrStyle
        Me.aGrid.Location = New System.Drawing.Point(12, 67)
        Me.aGrid.Name = "aGrid"
        Me.aGrid.Size = New System.Drawing.Size(776, 371)
        Me.aGrid.TabIndex = 0
        '
        'IGAutoFilterManager1
        '
        Me.IGAutoFilterManager1.Grid = Me.aGrid
        '
        'DatenabfrageForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.aGrid)
        Me.Name = "DatenabfrageForm"
        Me.Text = "Daten von Datenbank abfragen"
        CType(Me.aGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents aGrid As TenTec.Windows.iGridLib.iGrid
    Friend WithEvents IGrid1DefaultCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGrid1DefaultColHdrStyle As TenTec.Windows.iGridLib.iGColHdrStyle
    Friend WithEvents IGrid1RowTextColCellStyle As TenTec.Windows.iGridLib.iGCellStyle
    Friend WithEvents IGAutoFilterManager1 As TenTec.Windows.iGridLib.Filtering.iGAutoFilterManager
    Friend WithEvents DropDowntblDataStruktur As TenTec.Windows.iGridLib.iGDropDownList
    Friend WithEvents DropDownData As TenTec.Windows.iGridLib.iGDropDownList
End Class
