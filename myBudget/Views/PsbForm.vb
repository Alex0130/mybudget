﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports myBudget.UI
Imports Microsoft.Office.Interop


Public Class PsbForm
    Public SelectedValue As String

#Region "Aktionen"
    Private Sub cmdAll_Click(sender As Object, e As EventArgs) Handles cmdAll.Click
        Console.WriteLine("AKTION: Private Sub cmdAll_Click")

        Dim I As Integer
        For I = 0 To iGridR.Rows.Count - 1
            If iGridR.Rows(I).Visible = True Then
                iGridR.Rows(I).Cells("Choose").Value = True
            End If
        Next
    End Sub

    Private Sub cmdNothing_Click(sender As Object, e As EventArgs) Handles cmdNothing.Click
        Console.WriteLine("AKTION: Private Sub cmdNothing_Click")

        Dim I As Integer
        For I = 0 To iGridR.Rows.Count - 1
            If iGridR.Rows(I).Visible = True Then
                iGridR.Rows(I).Cells("Choose").Value = False
            End If
        Next
    End Sub

    Private Sub cmdPsb_Click(sender As Object, e As EventArgs) Handles cmdPsb.Click
        Console.WriteLine("AKTION: Private Sub cmdPsb_Click")

        'Erzeugt eine Datentabelle mit den ausgewählten Daten
        Dim dt1 As System.Data.DataTable
        Dim dt2 As System.Data.DataTable
        Dim dt_Get1 As System.Data.DataTable
        Dim dt_Get2 As System.Data.DataTable
        Dim i As Integer, j As Integer

        'Prüfen ob eine aktive Verbindung zum Server existiert
        If con.State = ConnectionState.Closed Or boolConnected = False Then
            MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Exit Sub
        End If

        If iGridR.Rows.Count = 0 Then Exit Sub

        PsbStatusText.Text = "Daten werden heruntergeladen..."
        Me.Refresh()

        Try
#Region "Zentrale Daten laden: GJ, Arbeitspaket(e) und Entwurfstyp(en)"
#Region "GJ laden - als Integer List"
            Dim listGj() As Integer

            For Each item As Integer In chxGj.CheckedIndices
                If IsNothing(listGj) Then
                    ReDim listGj(0)
                Else
                    ReDim Preserve listGj(UBound(listGj) + 1)
                End If
                listGj(UBound(listGj)) = CInt(chxGj.Items(item).ToString)
            Next

            If IsNothing(listGj) Then
                MsgBox("Bitte wählen Sie mindestens ein Geschäftsjahr!", vbCritical, "Fehler")
                Exit Sub
            End If
#End Region

#Region "Arbeitspakete & PSP-Elemente laden - als Integer List"
            Dim listArbeitspaket() As Integer
            Dim listPsp() As String
            Dim listIK() As String

            If Not iGridR.Rows.Count = 0 Then
                For i = 0 To iGridR.Rows.Count - 1
                    If iGridR.Rows(i).Cells("Choose").Value = True Then
                        If IsNothing(listArbeitspaket) Then
                            ReDim listArbeitspaket(0)
                        Else
                            ReDim Preserve listArbeitspaket(UBound(listArbeitspaket) + 1)
                        End If
                        listArbeitspaket(UBound(listArbeitspaket)) = CInt(iGridR.Rows(i).Cells("ID_AP").Value)

                        If InStr(1, iGridR.Rows(i).Cells("txtPSP_Element").Value.ToString, "IK") > 0 Then
                            If IsNothing(listIK) Then
                                ReDim listIK(0)
                            Else
                                ReDim Preserve listIK(UBound(listIK) + 1)
                            End If
                            listIK(UBound(listIK)) = iGridR.Rows(i).Cells("txtPSP_Element").Value.ToString
                        Else
                                If IsNothing(listPsp) Then
                                ReDim listPsp(0)
                            Else
                                ReDim Preserve listPsp(UBound(listPsp) + 1)
                            End If
                            listPsp(UBound(listPsp)) = iGridR.Rows(i).Cells("txtPSP_Element").Value.ToString
                        End If
                    End If
                Next
            End If

            If IsNothing(listArbeitspaket) Then
                MsgBox("Bitte wählen Sie mindestens ein Arbeitspaket!", vbCritical, "Fehler")
                Exit Sub
            End If
#End Region

#Region "Entwurfstypen laden - als String List"
            Dim listEntwurf() As String

            For Each item As Integer In checkedListEntwurf.CheckedIndices
                If IsNothing(listEntwurf) Then
                    ReDim listEntwurf(0)
                Else
                    ReDim Preserve listEntwurf(UBound(listEntwurf) + 1)
                End If
                listEntwurf(UBound(listEntwurf)) = checkedListEntwurf.Items(item).ToString
            Next

            If IsNothing(listEntwurf) AndAlso Not intArt_der_Editierung = 4 Then
                MsgBox("Bitte wählen Sie mindestens einen Entwurfstyp!", vbCritical, "Fehler")
                Exit Sub
            End If
#End Region

#Region "Abfrage ob Compare Mode gestartet werden soll"
            If intArt_der_Editierung = 2 Then 'Es darf nur ein Entwurfstyp gewählt werden, welcher nicht "IST" ist
                If UBound(listEntwurf) > 0 Then
                    MsgBox("Bitte nur einen Entwurfstyp wählen!", vbCritical, "Fehler")
                    Exit Sub
                ElseIf UBound(listEntwurf) = 0 Then
                    If listEntwurf(0) = "Ist" Then
                        MsgBox("Bitte nur einen Plan Entwurfstyp wählen!", vbCritical, "Fehler")
                        Exit Sub
                    End If
                End If
            End If
#End Region

#Region "Daten herunterladen"
            If Not iGridR.Rows.Count = 0 Then
                For i = 0 To iGridR.Rows.Count - 1

                    PsbStatusText.Text = "Daten werden heruntergeladen: " & i & "/" & iGridR.Rows.Count - 1

                    If iGridR.Rows(i).Cells("Choose").Value = True Then 'Arbeitspaket wurde ausgewählt
                        j = 0
                        For Each item In checkedListEntwurf.CheckedIndices
                            If Not listEntwurf(j) = "Ist + Obligo" Then
                                If boolGetMuliDataVersions = True Then
                                    dt_Get1 = Get_tblData(False, iGridR.Rows(i).Cells("ID_AP").Value, iGridR.Rows(i).Cells(pbl_Entwurf(item).txtEntwurfstyp).Value, pbl_Entwurf(item).txtEntwurfstyp, True, listGj, pbl_Sql)
                                Else
                                    Dim intVersion = Get_LastestApVersion(iGridR.Rows(i).Cells("ID_AP").Value, pbl_Entwurf(item).txtEntwurfstyp)
                                    dt_Get1 = Get_tblData(False, iGridR.Rows(i).Cells("ID_AP").Value, intVersion, pbl_Entwurf(item).txtEntwurfstyp, True, listGj, pbl_Sql)
                                End If
                            Else
                                dt_Get1 = Get_tblData(False, iGridR.Rows(i).Cells("ID_AP").Value, 1, "Ist", False, listGj, pbl_Sql)
                            End If
                            If IsNothing(dt1) Then
                                dt1 = dt_Get1.Clone
                            End If
                            dt1.Merge(dt_Get1, True)
                            j = j + 1
                        Next
                    End If
                Next
            Else
                MsgBox("Keine Arbeitspakete vorhanden!", vbCritical, "Fehler")
                Exit Sub
            End If
#End Region

#End Region


#Region "Check which Modus is choosen"

#Region "Excel Export"
            If intArt_der_Editierung = 0 Then
                'Daten entpivotisieren
                If cbxEntpivot.Checked = True Then
                    dt1 = DataTable_Entpivotisieren(dt1, False)
                End If

#Region "DataTable in String Format convertieren"
                Dim ArrList(dt1.Rows.Count, dt1.Columns.Count - 1) As String
                'Überschriften
                For i = 0 To dt1.Columns.Count - 1
                    ArrList(0, i) = dt1.Columns(i).ColumnName
                Next

                For i = 0 To dt1.Rows.Count - 1
                    For j = 0 To dt1.Columns.Count - 1
                        ArrList(i + 1, j) = dt1.Rows(i)(j)
                    Next
                Next
#End Region

#Region "Daten in Excel schreiben"
                exApp = New Excel.Application
                exWB = exApp.Workbooks.Add()
                Sheet = CType(exWB.Worksheets.Item(1), Excel.Worksheet)
                exApp.ScreenUpdating = False
                exApp.Calculation = XlCalculation.xlCalculationManual

                Sheet.Range("A1").Resize(dt1.Rows.Count, dt1.Columns.Count - 1).Value = ArrList

                exApp.ScreenUpdating = True
                exApp.Calculation = XlCalculation.xlCalculationAutomatic
                exApp.Visible = True
                exApp.WindowState = Excel.XlWindowState.xlMaximized
#End Region

#End Region

#Region "n- Arbeitspakete bearbeiten"
            ElseIf intArt_der_Editierung = 3 Then 'n- Arbeitspakete 
                'Daten sortieren nach: Projekt, Arbeitspaket, Jahr, Monat, Entwurfstyp, Kosten
                dt1.DefaultView.Sort = "PROJEKT_ID asc, qryArbeitspaket asc, qryGeschäftsjahr asc, lngsort asc, qryEntwurfstyp asc, curKosten_pro_Einheit desc"
                dt1 = dt1.DefaultView.ToTable

                Form_Edit.iGrid_ClearFilterAndGroups()
                If boolCompareMode = True Then
                    Form_Edit.UndoCompare(dt1)
                Else
                    Form_Edit.Reload_iGrid(dt1)
                End If

                ReadOnlyMode = False
                If Not InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 AndAlso Not LCase(GotRights("PROJEKTMANAGER", 0, pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)) = "write" Then 'Admin und Projektmanager dürfen Daten verändern
                    Form_Edit.iGrid_EnableEditFunctions(False, False, "Nur Leserechte")
                Else
                    Form_Edit.iGrid_EnableEditFunctions(True, True, "Bearbeitung möglich")
                End If

                Form_Edit.iGrid_Visibility(True)
                Form_Edit.HideFilter()
#End Region

#Region "n- Arbeitspakete im Compare Modus"
            ElseIf intArt_der_Editierung = 2 Then

                'Check which Entwurfstpy is selected
                Dim CountEntwurf As Integer
                For Each item In checkedListEntwurf.CheckedIndices
                    If Not pbl_Entwurf(item).txtEntwurfstyp = "Ist" Then
                        CountEntwurf = CountEntwurf + 1
                    End If
                Next
                If CountEntwurf > 1 Then
                    MsgBox("Bitte nur einen Entwurfstyp wählen!", vbCritical, "Fehler")
                    Exit Sub
                End If

                For i = 0 To iGridR.Rows.Count - 1
                    If iGridR.Rows(i).Cells("Choose").Value = True Then 'Arbeitspaket wurde ausgewählt
                        dt_Get2 = Get_tblData(False, iGridR.Rows(i).Cells("ID_AP").Value, 1, "Ist", False, listGj, pbl_Sql)
                        If IsNothing(dt2) Then
                            dt2 = dt_Get2.Clone
                        End If
                        dt2.Merge(dt_Get2, True)
                    End If
                Next

                Form_Edit.iGrid_ClearFilterAndGroups()
                Form_Edit.DoCompare(dt1, dt2)

                ReadOnlyMode = False
                If Not InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then
                    Form_Edit.iGrid_EnableEditFunctions(False, False, "Nur Leserechte")
                End If

                Form_Edit.iGrid_Visibility(True)
                Form_Edit.HideFilter()
#End Region

#Region "Live SAP Daten herunterladen"
            ElseIf intArt_der_Editierung = 4 Then
                Dim DoExport As Boolean = False
                If cbxExcelExport.Checked = True Then
                    DoExport = True
                End If

                GetSapData(listGj, listPsp, listIK, DoExport)
#End Region
            End If

            Timer.Interval = 5000 '1000 = 1 sec
            Timer.Start()

            Form_Edit.ToolStripStatusOperation.Text = "Daten erfolgreich geladen"
            Form_Edit.StatusStrip.Refresh()
            Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
            Form_Edit.TimerEditForm.Start()

            Me.Hide() 'PsbForm verstecken
        Catch ex As Exception
            MsgBox("cmdPsb_Click: " & ex.Message.ToString)
        End Try
#End Region
    End Sub

    Private Sub UpdateData_Click(sender As Object, e As EventArgs) Handles UpdateData.Click
        Try
            Me.Hide()
            RemoveHandler MyBase.Activated, AddressOf PsbForm_Activated

            SuspendLayout()

            Layout_DgvPsb()

            ResumeLayout()

            AddHandler MyBase.Activated, AddressOf PsbForm_Activated
            Me.Show()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Methoden"
    Public Sub Layout_DgvPsb()
        Console.WriteLine("FUNC: Sub Layout_DgvPsb")

        Try
            PsbStatusText.Text = ""

            Dim K As Long, L As Long
#Region "Checkbox Listen"
            Try
                'GJ für alle Projekte
                Form_Edit.ToolStripStatusOperation.Text = "Cockpit öffnen: Projekte werden geladen..."
                Form_Edit.StatusStrip.Refresh()

                Dim where As String
                For K = 0 To pbl_dtProjekt.Rows.Count - 1
                    where = where & "qryProjekt = " & pbl_dtProjekt.Rows(K).Item("ID_PROJEKT") & " or "
                Next
                If (Len(where) > 0) Then 'Letztes "OR" abschneiden
                    where = Strings.Left(where, Len(where) - 3)
                End If
                Dim qry As String = "select * from tblGJ where " & where & "order by intGJ asc"
                Dim tblAllGJ As System.Data.DataTable = Get_GJforProjekt(qry)
                tblAllGJ = tblAllGJ.DefaultView.ToTable("DistinctTable", True, "intGJ")


                chxGj.Items.Clear()
                For K = 0 To tblAllGJ.Rows.Count - 1
                    chxGj.Items.Add(tblAllGJ.Rows(K).Item("intGJ"))
                    Try
                        For L = LBound(pbl_LstSelGj) To UBound(pbl_LstSelGj)
                            If tblAllGJ.Rows(K).Item("intGJ") = pbl_LstSelGj(L) Then
                                chxGj.SetItemChecked(K, True)
                            End If
                        Next L
                    Catch
                        If tblAllGJ.Rows(K).Item("intGJ") = Year(Now) Then
                            chxGj.SetItemChecked(K, True)
                        End If
                    End Try
                Next K

                'Entwurfstypen
                Form_Edit.ToolStripStatusOperation.Text = "Cockpit öffnen: Entwurfstypen werden geladen..."
                Form_Edit.StatusStrip.Refresh()

                checkedListEntwurf.Items.Clear()
                For K = 0 To UBound(pbl_Entwurf)
                    checkedListEntwurf.Items.Add(pbl_Entwurf(K).ReportBeschreibung)
                    If K = pbl_IndexEntwurf Then
                        checkedListEntwurf.SetItemChecked(K, True)
                    End If
                Next
            Catch ex As Exception
                MsgBox(ex.Message.ToString, vbCritical, "Fehler")
            End Try
#End Region

#Region "Button / Labels entsprechend der Report Art anzeigen"
            checkedListEntwurf.Visible = True
            Label1.Visible = True
            If intArt_der_Editierung = 0 Or intArt_der_Editierung = 1 Then 'Standard Report
                cbxEntpivot.Visible = True
                cbxExcelExport.Visible = False
                cbxGetAllVersions.Visible = True
            ElseIf intArt_der_Editierung = 2 Then 'Budget Statusbericht
                cbxEntpivot.Visible = False
                cbxExcelExport.Visible = False
                cbxGetAllVersions.Visible = True
            ElseIf intArt_der_Editierung = 3 Then 'Manager Cockpit
                cbxEntpivot.Visible = False
                cbxExcelExport.Visible = False
                cbxGetAllVersions.Visible = True
            ElseIf intArt_der_Editierung = 4 Then 'Live SAP Daten
                checkedListEntwurf.Visible = False
                Label1.Visible = False
                cbxEntpivot.Visible = False
                cbxExcelExport.Visible = True
                cbxGetAllVersions.Visible = False
            End If
#End Region

#Region "iGrid"
            Dim i As Integer, j As Integer
            '1.
            Dim dt_Ap As System.Data.DataTable
            Dim dt_All_Ap As System.Data.DataTable

            For i = 0 To pbl_dtProjekt.Rows.Count - 1 'Alle Arbeitspakete zu den Projekten laden
                Form_Edit.ToolStripStatusOperation.Text = "Cockpit öffnen: Arbeitspakete werden geladen " & i & "/" & pbl_dtProjekt.Rows.Count - 1
                Form_Edit.StatusStrip.Refresh()

                If cbxInactiveAp.Checked = True Then
                    dt_Ap = Get_tblArbeitspaket(False, pbl_dtProjekt.Rows(i).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True)
                Else
                    dt_Ap = Get_tblArbeitspaket(False, pbl_dtProjekt.Rows(i).Item("ID_PROJEKT"), pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, False)
                End If
                If i = 0 Then
                    dt_All_Ap = dt_Ap.Clone
                End If
                dt_All_Ap.Merge(dt_Ap, False)
            Next

            '2.
            iGridR.Cols.Clear()
            iGridR.Rows.Clear()

            dt_All_Ap.DefaultView.Sort = "qryProjekt asc, txtArbeitspaket asc"
            dt_All_Ap = dt_All_Ap.DefaultView.ToTable
            dt_All_Ap.Columns.Add(New DataColumn("Projekt", GetType(String)))

#Region "Projekte Name nachtragen"
            Dim curProject As Integer, prevProject As Integer = -1
            Dim IndexProjekt As Integer
            For i = 0 To dt_All_Ap.Rows.Count - 1
                curProject = dt_All_Ap.Rows(i)("qryProjekt")
                If Not curProject = prevProject Then

                    Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & dt_All_Ap.Rows(i)("qryProjekt") & "")
                    If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                        IndexProjekt = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                        prevProject = curProject
                    End If

                    dt_All_Ap.Rows(i)("Projekt") = pbl_dtProjekt.Rows(IndexProjekt).Item("txtProjekt")
                Else
                    dt_All_Ap.Rows(i)("Projekt") = pbl_dtProjekt.Rows(IndexProjekt).Item("txtProjekt")
                End If
            Next
#End Region

            iGridR.FillWithData(dt_All_Ap, False)

#Region "Additional Columns"
            iGridR.Cols.Add("Choose")
            iGridR.Cols(iGridR.Cols.Count - 1).Key = "Choose"
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.Type = TenTec.Windows.iGridLib.iGCellType.Check
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.ImageAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopCenter

            iGridR.Cols.Add("Plan")
            iGridR.Cols(iGridR.Cols.Count - 1).Key = "Plan"
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)

            iGridR.Cols.Add("Ausblick")
            iGridR.Cols(iGridR.Cols.Count - 1).Key = "Ausblick"
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)

            iGridR.Cols.Add("Ist")
            iGridR.Cols(iGridR.Cols.Count - 1).Key = "Ist"
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)

            iGridR.Cols.Add("Soll")
            iGridR.Cols(iGridR.Cols.Count - 1).Key = "Soll"
            iGridR.Cols(iGridR.Cols.Count - 1).CellStyle.ValueType = GetType(System.String)
#End Region

#Region "Columns"
            iGridR.Cols("Choose").Order = 0

            iGridR.Cols("qryProjekt").Text = "Projekt ID"
            iGridR.Cols("qryProjekt").Order = 1
            If InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then
                iGridR.Cols("qryProjekt").Visible = True
            Else
                iGridR.Cols("qryProjekt").Visible = False
            End If

            iGridR.Cols("Projekt").Text = "Projekt"
            iGridR.Cols("Projekt").Order = 2

            iGridR.Cols("ID_AP").Text = "ID_AP"
            iGridR.Cols("ID_AP").Order = 3
            If InStr(UCase(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer")), "ADMIN") > 0 Then
                iGridR.Cols("ID_AP").Visible = True
            Else
                iGridR.Cols("ID_AP").Visible = False
            End If

            iGridR.Cols("txtArbeitspaket").Text = "Arbeitspaket"
            iGridR.Cols("txtArbeitspaket").Order = 4

            iGridR.Cols("lngVorgänger_ID").Text = "Vorgänger"
            iGridR.Cols("lngVorgänger_ID").Order = 5
            iGridR.Cols("lngVorgänger_ID").Visible = False

            iGridR.Cols("txtPrämissen").Text = "Prämissen"
            iGridR.Cols("txtPrämissen").Order = 6
            iGridR.Cols("txtPrämissen").Visible = False

            iGridR.Cols("datErstellungsdatum").Text = "Erstellungsdatum"
            iGridR.Cols("datErstellungsdatum").Order = 7
            iGridR.Cols("datErstellungsdatum").Visible = False

            iGridR.Cols("txtPSP_Element").Text = "PSP Element"
            iGridR.Cols("txtPSP_Element").Order = 8

            iGridR.Cols("txtVerantwortlich").Text = "Verantwortlich"
            iGridR.Cols("txtVerantwortlich").Order = 9

            iGridR.Cols("qryTeam").Text = "Team"
            iGridR.Cols("qryTeam").Order = 10

            iGridR.Cols("Plan").Order = 11
            iGridR.Cols("Plan").CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter
            iGridR.Cols("Plan").Visible = False

            iGridR.Cols("Ausblick").Order = 12
            iGridR.Cols("Ausblick").CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter
            iGridR.Cols("Ausblick").Visible = False

            iGridR.Cols("Ist").Order = 13
            iGridR.Cols("Ist").CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter
            iGridR.Cols("Ist").Visible = False

            iGridR.Cols("Soll").Order = 14
            iGridR.Cols("Soll").CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter
            iGridR.Cols("Soll").Visible = False
#End Region

#Region "Col design"
            'Header Row
            iGridR.Header.Font = New Font(iGridR.Font, FontStyle.Bold)
            iGridR.Header.ForeColor = Color.Black

            iGridR.Cols("txtArbeitspaket").CellStyle.ForeColor = Color.DarkRed
            iGridR.Cols("txtArbeitspaket").CellStyle.Font = New Font(iGridR.Font, FontStyle.Bold)
#End Region

#Region "Versionen laden"
            If boolGetMuliDataVersions = True Then
                Get_AllVersions()
            End If
#End Region

            iGridR.Cols.AutoWidth()
            iGridR.Rows.AutoHeight()

#End Region
        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Fehler")
            Me.Close()
        End Try
    End Sub

    Public Sub Get_AllVersions()
        Try


#Region "Version pro Arbeitspaket"
            Dim lstVersion() As tableVersionArbeitspaket

            Dim dtVersion As New System.Data.DataTable
            dtVersion = Get_tblAlleVersionen()

            Dim progress As Integer = 50
            For i = 0 To iGridR.Rows.Count - 1
                If i = progress Then
                    PsbStatusText.Text = "Versionen werden geladen " & i & "/" & iGridR.Rows.Count - 1
                    StatusStrip.Refresh()
                    progress = progress + 50
                End If

#Region "Combobox Planversion"
                Dim DD_Plan As New TenTec.Windows.iGridLib.iGDropDownList
                Dim foundPlan() As Data.DataRow = dtVersion.Select("qryID_AP = " & iGridR.Rows(i).Cells("ID_AP").Value & " and qryEntwurfstyp = 'Plan'")
                If Not IsNothing(foundPlan) AndAlso foundPlan.Count > 0 Then 'Es gibt mindestens einen Treffer
                    For j = 0 To foundPlan.Count - 1
                        DD_Plan.Items.Add(foundPlan(j).Item("lngVersion"))
                    Next
                    iGridR.Rows(i).Cells("Plan").DropDownControl = DD_Plan
                    iGridR.Rows(i).Cells("Plan").Value = foundPlan(0).Item("lngVersion") 'Höchste Version vorbelegen
                End If
#End Region
#Region "Combobox Ausblick"
                Dim DD_Ausblick As New TenTec.Windows.iGridLib.iGDropDownList
                Dim foundAusblick() As Data.DataRow = dtVersion.Select("qryID_AP = " & iGridR.Rows(i).Cells("ID_AP").Value & " and qryEntwurfstyp = 'Ausblick'")
                If Not IsNothing(foundAusblick) AndAlso foundAusblick.Count > 0 Then 'Es gibt mindestens einen Treffer
                    For j = 0 To foundAusblick.Count - 1
                        DD_Ausblick.Items.Add(foundAusblick(j).Item("lngVersion"))
                    Next
                    iGridR.Rows(i).Cells("Ausblick").DropDownControl = DD_Ausblick
                    iGridR.Rows(i).Cells("Ausblick").Value = foundAusblick(0).Item("lngVersion") 'Höchste Version vorbelegen
                End If
#End Region
#Region "Combobox Soll"
                Dim DD_Soll As New TenTec.Windows.iGridLib.iGDropDownList
                Dim foundSoll() As Data.DataRow = dtVersion.Select("qryID_AP = " & iGridR.Rows(i).Cells("ID_AP").Value & " and qryEntwurfstyp = 'Soll'")
                If Not IsNothing(foundSoll) AndAlso foundSoll.Count > 0 Then 'Es gibt mindestens einen Treffer
                    For j = 0 To foundSoll.Count - 1
                        DD_Soll.Items.Add(foundSoll(j).Item("lngVersion"))
                    Next
                    iGridR.Rows(i).Cells("Soll").DropDownControl = DD_Soll
                    iGridR.Rows(i).Cells("Soll").Value = foundSoll(0).Item("lngVersion") 'Höchste Version vorbelegen
                End If
#End Region
#Region "Combobox Ist"
                Dim DD_Ist As New TenTec.Windows.iGridLib.iGDropDownList
                Dim foundIst() As Data.DataRow = dtVersion.Select("qryID_AP = " & iGridR.Rows(i).Cells("ID_AP").Value & " and qryEntwurfstyp = 'Ist'")
                If Not IsNothing(foundIst) AndAlso foundIst.Count > 0 Then 'Es gibt mindestens einen Treffer
                    For j = 0 To foundIst.Count - 1
                        DD_Ist.Items.Add(foundIst(j).Item("lngVersion"))
                    Next
                    iGridR.Rows(i).Cells("Ist").DropDownControl = DD_Ist
                    iGridR.Rows(i).Cells("Ist").Value = foundIst(0).Item("lngVersion") 'Höchste Version vorbelegen
                End If
#End Region
            Next
#End Region

            iGridR.Cols.AutoWidth()
            iGridR.Rows.AutoHeight()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Events"
    Private Sub PsbForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Console.WriteLine("EVENT: Private Sub PsbForm_Load")

        RemoveHandler MyBase.Activated, AddressOf PsbForm_Activated
        RemoveHandler MyBase.Load, AddressOf PsbForm_Load
        Me.Hide()

        SuspendLayout()

        Me.WindowState = FormWindowState.Maximized

        Layout_DgvPsb()

        ResumeLayout()
        Me.Show()
        AddHandler MyBase.Load, AddressOf PsbForm_Load
        AddHandler MyBase.Activated, AddressOf PsbForm_Activated
    End Sub

    Public Sub PsbForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Console.WriteLine("EVENT: Private Sub PsbForm_FormClosing")

        If ApplicationExit = False Then
            e.Cancel = True
            Me.Hide()
        End If
    End Sub

    Private Sub PsbForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Console.WriteLine(">Event: Private Sub PsbForm_Activated")

#Region "Button / Labels entsprechend der Report Art anzeigen"
        checkedListEntwurf.Visible = True
        Label1.Visible = True

        If intArt_der_Editierung = 0 Or intArt_der_Editierung = 1 Then 'Standard Report
            cbxEntpivot.Visible = True
            cbxExcelExport.Visible = False
            cbxGetAllVersions.Visible = True
            cmdPsb.Text = "Anzeigen"
        ElseIf intArt_der_Editierung = 2 Then 'Budget Statusbericht
            cbxEntpivot.Visible = False
            cbxExcelExport.Visible = False
            cbxGetAllVersions.Visible = True
            cmdPsb.Text = "Anzeigen"
        ElseIf intArt_der_Editierung = 3 Then 'Manager Cockpit
            cbxEntpivot.Visible = False
            cbxExcelExport.Visible = False
            cbxGetAllVersions.Visible = True
            cmdPsb.Text = "Anzeigen"
        ElseIf intArt_der_Editierung = 4 Then 'Live SAP Daten
            checkedListEntwurf.Visible = False
            cbxGetAllVersions.Visible = False
            Label1.Visible = False
            cbxEntpivot.Visible = False
            cbxExcelExport.Visible = True
            cmdPsb.Text = "Download"
        End If
#End Region
    End Sub

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick
        Console.WriteLine("< Private Sub Timer_Tick")

        PsbStatusText.Text = ""
        Timer.Stop()
    End Sub

    Private Sub cbxGetAllVersions_CheckedChanged(sender As Object, e As EventArgs) Handles cbxGetAllVersions.CheckedChanged
        Try
            If cbxGetAllVersions.Checked = True Then

                PsbStatusText.Text = "Cockpit öffnen: Alle Versionen zu allen Arbeitspaketen und Projekten werden geladen..."
                StatusStrip.Refresh()

                boolGetMuliDataVersions = True

                Get_AllVersions()

                iGridR.Cols("Plan").Visible = True
                iGridR.Cols("Ist").Visible = True
                iGridR.Cols("Soll").Visible = True
                iGridR.Cols("Ausblick").Visible = True

                PsbStatusText.Text = "Versionen erfolgreich geladen"
                StatusStrip.Refresh()
                Timer.Interval = 5000 '1000 = 1 sec
                Timer.Start()
            Else
                boolGetMuliDataVersions = False
                iGridR.Cols("Plan").Visible = False
                iGridR.Cols("Ist").Visible = False
                iGridR.Cols("Soll").Visible = False
                iGridR.Cols("Ausblick").Visible = False
            End If
        Catch ex As Exception
            MsgBox("cbxGetAllVersions_CheckedChanged: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub cbxInactiveAp_CheckedChanged(sender As Object, e As EventArgs) Handles cbxInactiveAp.CheckedChanged
        MsgBox("Bitte Daten jetzt aktualisieren", vbInformation, "Information")
        PsbStatusText.Text = "Bitte Daten jetzt aktualisieren"
        StatusStrip.Refresh()
        Timer.Interval = 5000 '1000 = 1 sec
        Timer.Start()
    End Sub

    Private Sub ExcelExport_Click(sender As Object, e As EventArgs) Handles ExcelExport.Click
        Try
            PsbStatusText.Text = "Daten werden in Excel exportiert..."
            StatusStrip.Refresh()
            Timer.Interval = 5000 '1000 = 1 sec
            Timer.Start()

            iGridToExcel(Form_Psb.iGridR)
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub
#End Region
End Class