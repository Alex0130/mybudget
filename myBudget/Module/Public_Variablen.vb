﻿Imports myBudget.UI
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Outlook

Public Module Public_Variablen

#Region "Excel Tabelle"
    Public App As Excel.Application 'XXX
    Public ThisWorkbook As Excel.Workbook 'XXX

    Public exWB As Excel.Workbook
    Public Sheet As Excel.Worksheet
    Public exApp As Excel.Application
#End Region

#Region "Outlook Und Email Notification"    '!ab added
    Public outApp As Outlook.Application
    Public subscriptionState As Boolean = False 'angezeigter aktueller Subscriptionstatus
#End Region

    Public con As OleDb.OleDbConnection
    Public strPath As String 'Speichert den Pfad an dem die Datenbank abliegt
    Public progress As Long 'Fortschrittszyklus
    Public boolConnected As Boolean 'Gibt an ob eine Verbindung zur Datenbank besteht
    Public listConnections() As tableSystem 'Speichert die zulässigen Datenbank Pfade
    Public boolLoggedIn As Boolean 'Gibt an ob ein User bereits eingeloggt ist oder nicht
    Public boolCompareMode As Boolean 'Gibt an ob der Vergleichsmodus aktiv ist
    Public intCurrentUser As Integer 'Speichert den Array Eintrag des angemeldten Users
    Public boolError As Boolean 'Gibt es einen Fehler?
    Public intArt_der_Editierung As Integer '0 = NEU, 1 = ÄNDERUNG, 2 = VIEW
    Public boolDataChanged As Boolean 'Wurden seit dem letzten Speichern Daten verändert?
    Public SelectedRow As Integer 'Welche Zeile gerade markiert ist
    Public SelectedColumn As Integer
    Public CopyRow As Integer 'Wo eingefügt werden soll
    Public CopyColumn As Integer
    Public SearchValue As String 'Suchtext
    Public ApplicationExit As Boolean
    Public IDCopyArbeitspaket As Integer 'Gibt an auf welche AP ID Daten kopiert werden sollen
    Public FooterCalculationMethod As Integer = 0 'Gibt an was der Footer berechnet
    Public IVP As Double
    Public dtIVP() As Data.DataRow
    Public ReadOnlyMode As Boolean = True
    Public boolGetMuliDataVersions As Boolean = False 'Gibt an ob im Multi Daten Cockpit explizit Versionen gewählt werden sollen oder ob die aktuellste Version gezogen wird
    Public BEST() As String
    Public BANF() As String

#Region "DataTables"
    Public pbl_dt As New System.Data.DataTable
    Public pbl_dtCopyData As New System.Data.DataTable
    Public pbl_dtSakto As New System.Data.DataTable
    Public pbl_dtEkant As New System.Data.DataTable
    Public pbl_dtArbeitspaket As New System.Data.DataTable
    Public pbl_dtSAPCon As New System.Data.DataTable
    Public pbl_dtUser As New System.Data.DataTable
    Public pbl_dtAuth As New System.Data.DataTable
    Public pbl_dtProjekt As New System.Data.DataTable
#End Region

#Region "Steuervariablen"
    Public CheckCellValueChanged As String
    Public CheckCellValueChanged2 As String
#End Region

#Region "iGrid"
    Public Structure tableSumRows
        Public Sum As Double
        Public Ist As Double
        Public IstUndObligo As Double
        Public Obligo As Double
    End Structure

#Region "CellStyles"
    Public cellStylePositive As New TenTec.Windows.iGridLib.iGCellStyleDesign
    Public cellStyleNegative As New TenTec.Windows.iGridLib.iGCellStyleDesign
    Public fCellStyleSubTotals As New TenTec.Windows.iGridLib.iGCellStyleDesign
#End Region

#End Region


#Region "Windows Formen"
    Public Form_Edit As New EditForm
    Public Form_Login As New LoginForm
    Public Form_Version As New VersionForm
    Public Form_Arbeitspaket As New ArbeitspaketForm
    Public Form_Entwicklungspaket As New EPForm
    Public Form_Projekt As New ProjektForm
    Public Form_Paket As New PaketForm
    Public Form_Kopieren As New KopierenForm
    Public Form_Psb As New PsbForm
    Public Form_Sakto_Ekant As New Sakto_EkantForm
    Public intForm_Sakto_Ekant As Integer
    Public Form_DatenVergleich As New DatenVergleichForm
    Public Form_SelectConnection As New SelectConnectionForm
    Public Form_Termine As New TermineForm
    Public Form_Datenabfrage As New DatenabfrageForm
    Public Form_UserPassword As New UserPasswordForm
    Public Form_DatenLinearVerteilen As New DatenLinearVerteilenForm
    Public Form_IVPAuswahl As New IVPAuswahlForm
    Public Form_UserEmail As New UserEmailForm

#End Region

    'Form_Edit ComboBox Indizies (gewählte Einträge)
    Public ViewCombo As UIChange = New UIChange()

    Public pbl_IdProjekt As Integer
    Public pbl_IdAp As Integer
    Public pbl_IndexEntwurf As Integer
    Public pbl_lngVersion As Long
    Public pbl_txtEp As String
    Public pbl_txtPaket As String
    Public pbl_LstSelGj() As Integer
    Public pbl_dblIstBudget As Double
    Public pbl_dblPlanBudget As Double

    'SAP Daten
    Public pbl_SapData As tableData()
    Public pbl_DeleteIst As tableDeleteIstData() 'Speichert die Arbeitspaket ID's die gelöscht werden können
    Public Structure tableDeleteIstData
        Public ID_AP As Long
        Public GJ As Integer
        Public Key As String
    End Structure

    Public pbl_KstArt As tableKostenart()
    Public Structure tableKostenart
        Public txtKostenart As String
        Public txtBeschreibung As String
        Public intReihenfolge As Long
    End Structure

    Public pbl_Entwurf As tableEntwurfstyp()
    Public pbl_ReportEntwurf As tableEntwurfstyp() 'Für Reporting
    Public Structure tableEntwurfstyp
        Public txtEntwurfstyp As String
        Public intReihenfolge As Long
        Public DatenHerkunft As String
        Public ReportBeschreibung As String
        Public boolChecked As Boolean 'Für die Kopierfunktion
    End Structure

    Public pbl_Klasse As tableKlassifizierung()
    Public Structure tableKlassifizierung
        Public txtKlasse As String
        Public lngWichtigkeit As Long
    End Structure

    Public pbl_Ep As tableEntwicklungspaket()
    Public Structure tableEntwicklungspaket
        Public txtBeschreibung As String
        Public txtEntwicklungspaket As String
        Public lngVorgänger As Long
        Public qryProjekt As Long
    End Structure

    Public pbl_Gj As tableGJ()
    Public Structure tableGJ
        Public intGJ As Long
        Public qryProjekt As Long
        Public boolChecked As Boolean 'Für die Kopierfunktion
    End Structure

    Public pbl_VersionAp As tableVersionArbeitspaket()
    Public pbl_ListVersionAp() As tableVersionArbeitspaket
    Public Structure tableVersionArbeitspaket
        Public qryID_AP As Long
        Public lngVersion As Long
        Public txtVersionErklärung As String
        Public datErstellungsdatum As Date
        Public cbxFreigabe As Boolean
        Public qryEntwurfstyp As String
        Public boolChecked As Boolean 'Für Kopierfunktion
    End Structure

    Public pbl_Data As tableData()
    Public pbl_CopyData As tableData()
    Public Structure tableData
        Public ID_DATA As Long
        Public txtBeschreibung As String
        Public txtBemerkung As String
        Public txtZusatzfeld As String
        Public txtBedarfsnummer As String
        Public qryGeschäftsjahr As Long
        Public curKosten_pro_Einheit As Double
        Public boolRenneinsatz As Boolean
        Public datErstellungsdatum As Date
        Public dblJanuar As Double
        Public dblFebruar As Double
        Public dblMärz As Double
        Public dblApril As Double
        Public dblMai As Double
        Public dblJuni As Double
        Public dblJuli As Double
        Public dblAugust As Double
        Public dblSeptember As Double
        Public dblOktober As Double
        Public dblNovember As Double
        Public dblDezember As Double
        Public qryArbeitspaket As Long
        Public qryVersion As Long
        Public txtKostenart As String
        Public qryEntwurfstyp As String
        Public txtKlasse As String
        Public txtEntwicklungspaket As String
        Public txtPaket As String
        Public boolObligo As Boolean
        Public IdEket As String 'Für ConvertEketToData
        Public txtKst_Lst As String
        Public txtBeschrKst_lst As String
        Public lngSort As Long
        Public Sum As Double 'Speichert die Summe Jan-Dez zwischen
        Public Anzahl As Double 'Speichert die Anzahl Jan-Dez zwischen
        Public ID_PROJEKT As Long 'Speichert das Projekt zum Arbeitspaket
    End Structure

    Public pbl_Team As tableTeam()
    Public Structure tableTeam
        Public txtTeam As String
        Public txtLeiterFachgebiet As String
    End Structure

    Public pbl_Paket As tablePaket()
    Public Structure tablePaket
        Public txtPaket As String
        Public txtBeschreibung As String
        Public qryArbeitspaket As Long
        Public datErstellungsdatum As Date
    End Structure

    Public pbl_Sakto As tableSachkonto()
    Public Structure tableSachkonto
        Public Sakto As String
        Public txtSachkonto As String
        Public FEHLER As Boolean
        Public qryKostenart As String
    End Structure

    Public pbl_Ekant() As tableEkant
    Public Structure tableEkant
        Public Std_Lst As String
        Public Abteilung As String
        Public Kostenstelle As String
        Public Kst_Schlüssel As String
        Public Bezeichnung As String
        Public Std_Satz As Double
    End Structure

    Public Structure tableBedNr 'Für Projektstatusbericht
        Public txtBedNr As String
    End Structure

    Public pbl_SapImport As tableSapData()
    Public Structure tableSapData
        Public Sachkonto As String 'Kostenart
        Public SachkontoBeschr As String 'KostenartBeschr.
        Public Kostenstelle As String 'PartnerKost.
        Public KstBeschreibung As String 'Beschreibung Partnerobjekt
        Public ObjBeschr 'Beschreibung Gegenkonto
        Public Belegdatum As Date 'Belegdatum
        Public Monat As Integer 'Per
        Public Jahr As Integer 'Jahr
        Public PSP As String 'Objekt
        Public Belegnummer As String 'Einkaufsbeleg
        Public Bedarfsnummer As String 'Bei BEST = aus SAP ziehen, sonst PartKost
        Public Belegtyp As String 'Per SOFTWARE
        Public BelegPos As String 'Pos
        Public Bestelltext As String 'Bestelltext
        Public Kosten As Double 'Wert
        Public Währung As String 'immer EUR
        Public Menge As Double 'Menge erf.
        Public MengenEh As String 'GME
        Public Obligo As Boolean 'Per SOFTWARE
        Public Key As String 'Per SOFTWARE
        Public Beleg As String 'BelegNr
        Public Material As String 'Material
        Public PNummer As String 'PersNr
        Public Beschreibung As String 'Beschreibung der Kostenposition für spätere Übergabe in pbl_Data Format
    End Structure
    Public Structure tablePspGj
        Public PSP As String
        Public GJ As Integer
        Public Key As String
    End Structure


    Public Structure tableEinzelDaten
        Public Entwurfstyp As String
        Public data As tableData()
    End Structure

    Public Structure tableBedNr_Kst
        Public Sum() As tableData 'Jede Bedarfsnummer/Kostenstelle hat Plan, Ist, Soll und Ausblick
        Public EinzelData() As tableEinzelDaten 'Einzelkosten pro Entwurfstyp
        Public BedNr_Kst As String
    End Structure
    Public Structure tableSakto
        Public BedNr_Kst() As tableBedNr_Kst 'Jedes Sachkonto hat n-Bedarfsnummern
        Public Sum() As tableData 'Summe aller Bedarfsnummern/Kostenstellen
        Public Sachkonto As String
        Public SachkontoBeschr As String
    End Structure
    Public Structure tableKstArt
        Public Sakto() As tableSakto 'Jede Kostenart hat n-Sachkonten
        Public Sum() As tableData 'Summe über alle Sachkonten
        Public Kostenart As String
    End Structure
    Public Structure tableAp
        Public KstArt() As tableKstArt 'Jedes Arbeitspaket hat n-Kostenarten
        Public sum() As tableData
        Public Arbeitspaket As String
        Public ID_AP As Long
        Public GJ As Integer
        Public boolSumUnplannedKstBednr As Boolean
    End Structure

    Public pbl_Filter() As tableFilter
    Public Structure tableFilter
        Public SearchText As String
        Public ColumnName As String
    End Structure

    Public pbl_Sql() As tableSqlStatement
    Public Structure tableSqlStatement
        Public Nur_Ohne As String
        Public TblFeldname As String
        Public Kriterium As String
        Public SqlStatement As String
    End Structure

    Public pbl_ProjStat() As tableAp 'Projektstatusbericht Liste

    Public Structure tableAnüStunden
        Public Kostenstelle As String
        Public Stunden As tableData
        Public Kosten As tableData
    End Structure
    Public Structure tableAnü
        Public Psp As String
        Public GJ As Integer
        Public TotalCosts As tableData
        Public TotalHours As tableData
        Public Stunden() As tableAnüStunden
    End Structure
    Public pbl_SelectedConnection As Integer
    Public Structure tableSystem
        Public System As String
        Public Path As String
        Public ServerType As String
    End Structure
    Public Structure tableMonat
        Public dblMonat As String
        Public Monat As String
        Public intMonat As Integer
    End Structure

    Public CurrRow As tableCurrRow
    Public Structure tableCurrRow
        Public RowCompare As Long
        Public RowData As Long
        Public ID_AP As Long
    End Structure

    Public tblEban_Kn As tableEban_Kn() 'EBAN_KN
    Public Structure tableEban_Kn
        Public BANFN As String
        Public BNFPO As Integer
        Public AFNAM As String
        Public MATNR As String
        Public TXZ01 As String
        Public BEDNR As String
        Public MENGE As Integer
        Public MEINS As String
        Public EBELN As String
        Public EBELP As Integer
        Public PREIS As Double
        Public PEINH As Double
        Public WAERS As String
        Public KNTTP As String
        Public FRGKZ As String
        Public LFDAT As Date
        Public SAKTO As String
        Public PSP As String
        Public BADAT As Date
        Public PosPreis As Double
        Public Key As String
    End Structure
    Public tblEket_Po As tableEket_Po()
    Public Structure tableEket_Po
        Public Bestellung As String
        Public BestellPos As Integer
        Public LöschKennz As String
        Public Materialnummer As String
        Public Kurztext As String
        Public Werk As String
        Public Menge As Double
        Public Mengeneinheit As String
        Public Bedarfsnummer As String
        Public EndlieferKennz As Boolean
        Public Lieferdatum As Date
        Public StatLieferdatum As Date
        Public Liefermenge As Double
        Public Preis As Double
        Public Preiseinheit As Double
        Public AEDAT As Date
        Public Währung As String
        Public LieferantenNr As String
        Public Lieferant As String
        Public PSP As String
        Public PosPreis As Double
        Public EndRechnung As String
        Public Obligo As Boolean
        Public Sachkonto As Long
        Public Kostenstelle As Long
        Public Auftragsnummer As Long
        Public Kostenart As String
        Public IdEket As String
    End Structure
End Module


