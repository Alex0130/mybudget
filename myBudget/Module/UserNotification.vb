﻿Module UserNotification
    Public Function NotifySubscribers() As Boolean
        Console.WriteLine("FUNC: Function NotifySubscribers")

        Dim ok As Boolean = False
        Dim mailList() As String = GetEMailListOfCurrentArbeitspaketAndTyp()
        Dim subject As String = "Arbeitspaket Datenänderung"
        Dim body As String = "Das Arbeitspaket '" & pbl_IdAp & "' wurde geändert."
        If Not mailList Is Nothing Then
            ok = SendMails(mailList, subject, body)
        End If
        Return ok
    End Function

    Public Function ToggleSubscriptionState(pbox As PictureBox) As Boolean
        Console.WriteLine("FUNC: Function NotifySubscribers")
        Dim ok As Boolean

        If (SubscriptionsPossible()) Then

            If subscriptionState Then
                ok = RemoveSubscriber()
                ShowSubscriptionDisabled(pbox)
            Else
                ok = AddSubscriber()
                ShowSubscriptionEnabled(pbox)
            End If
            subscriptionState = Not subscriptionState
        Else
            ShowSubscriptionDisabled(pbox)
        End If

        Return ok
    End Function

    Public Function AddSubscriber() As Boolean
        Console.WriteLine("FUNC: Function AddSubscriber")
        Return AddUserNotification()
    End Function

    Public Function RemoveSubscriber() As Boolean
        Console.WriteLine("FUNC: Function RemoveSubscriber")
        Return RemoveUserNotification()
    End Function

    Public Function RemoveAllUserSubscriptions() As Boolean
        If RemoveUserFromNotifications() Then
            subscriptionState = False
            'ShowSubscriptionDisabled(EditForm.EmailNotificationState)
            'EditForm.EmailNotificationState.Refresh()
            Return True
        Else

            Return False
        End If
    End Function
    Public Sub RefreshSubscriptionState(pbox As PictureBox)
        Console.WriteLine("SUB: RefreshSubscriptionState")
        If IsSubscribed() Then
            subscriptionState = True
            ShowSubscriptionEnabled(pbox)
        Else
            subscriptionState = False
            ShowSubscriptionDisabled(pbox)
        End If

    End Sub

    Public Function IsSubscribed() As Boolean
        Return FoundInNotifications()
    End Function

    Public Function SubscriptionsPossible() As Boolean
        Return Not String.IsNullOrWhiteSpace(DBStringToString(pbl_dtUser(intCurrentUser).Item("txtEmail")))
    End Function

    Public Sub ShowSubscriptionEnabled(pbox As PictureBox)
        If Not outApp Is Nothing Then
            pbox.Image = myBudget.My.Resources.EmailEnabled100
        Else
            pbox.Image = myBudget.My.Resources.EmailNotReady100
        End If
    End Sub

    Public Sub ShowSubscriptionDisabled(pBox As PictureBox)
        pBox.Image = myBudget.My.Resources.EmailDisabled100
        '!!!ab to remove pBox.Image = Nothing
        'pBox.ImageLocation = "EmailDisabled100.png"
    End Sub

End Module

