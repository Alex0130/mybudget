﻿Imports Microsoft.Office.Interop

Module SAP_Data
    Function Get_EketPo(listPspGj As tablePspGj()) As tableEket_Po() 'Bestellungen per PSP und Modelljahr
        Console.WriteLine("FUNC: Function Get_EketPo")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object, MAXROWS As Object
        Dim CDatum As String, CDouble As String

        Dim listEket_Po As tableEket_Po()
        boolGotData = False

        '--- Verbindungsdaten setzen ---
        funccontrol.Connection = conn
        If funccontrol.Count > 0 Then funccontrol.RemoveAll
        '--- Einstellungen für das Lesen der Tabellen
        RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
        QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
        DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
        OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
        SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
        SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

        '--- Tabelle aus welcher die Daten gelesen werden sollen -----
        QUERY_TAB.Value = "YCAA4019_EKET_PO" 'Tabellenname
        DELIMITER.Value = "|"
        '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(1, "FIELDNAME") = "EBELN"
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(2, "FIELDNAME") = "EBELP"
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(3, "FIELDNAME") = "LOEKZ" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(4, "FIELDNAME") = "MATNR" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(5, "FIELDNAME") = "MENGE" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(6, "FIELDNAME") = "MEINS" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(7, "FIELDNAME") = "BEDNR" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(8, "FIELDNAME") = "ELIKZ" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(9, "FIELDNAME") = "EINDT" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(10, "FIELDNAME") = "SLFDT" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(11, "FIELDNAME") = "LIF_MENGE" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(12, "FIELDNAME") = "NETPR" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(13, "FIELDNAME") = "PEINH" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(14, "FIELDNAME") = "WAERS" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(15, "FIELDNAME") = "PSP" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(16, "FIELDNAME") = "TXZ01" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(17, "FIELDNAME") = "AEDAT" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(18, "FIELDNAME") = "NAME1" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(19, "FIELDNAME") = "EREKZ" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(20, "FIELDNAME") = "SAKTO" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(21, "FIELDNAME") = "KOSTL" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(22, "FIELDNAME") = "AUFNR" '

        '--- Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen
        intRow = 1

        For K = 0 To UBound(listPspGj) 'Geht die Länge des Arrays durch
            If Not IsNothing(listPspGj(K).PSP) And Not IsNothing(listPspGj(K).GJ) Then
                OPTIONS.AppendRow
                OPTIONS(intRow, "TEXT") = "PSP = '" & listPspGj(K).PSP & "' AND EINDT >= '" & listPspGj(K).GJ & "0101' AND EINDT <= '" & listPspGj(K).GJ & "1231' OR" 'LOEKZ noch nicht ausgegrenzt
                intRow = intRow + 1
            End If
        Next K

        rowCount = OPTIONS.rowCount
        lastString = OPTIONS(rowCount, "TEXT")
        length = Len(lastString)

        If (length > 0) Then
            OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
        End If

        '--- Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen

        If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
            If SAP_DATA_TABLE.rowCount > 0 Then
                boolGotData = True

                K = 0 'K gibt den höchsten Index von listEket_Po an
                ReDim listEket_Po(K)

                Dim strSapData As String
                Dim progress As Integer = 50
                For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch
                    If progress = I Then
                        Form_Edit.ToolStripStatusOperation.Text = "Bestellung(en) herunterladen: " & I & "/" & SAP_DATA_TABLE.rowCount
                        Form_Psb.PsbStatusText.Text = "Bestellung(en) herunterladen: " & I & "/" & SAP_DATA_TABLE.rowCount
                        Form_Edit.StatusStrip.Refresh()
                        Form_Psb.StatusStrip.Refresh()
                        progress = progress + 50
                    End If

                    strSapData = SAP_DATA_TABLE(I, 1).ToString
                    Dim arHelper() As String = strSapData.Split("|")

                    ReDim Preserve listEket_Po(K)
                    For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                        If J = 0 Then
                            listEket_Po(K).Bestellung = Trim$(arHelper(J))
                        ElseIf J = 1 Then
                            listEket_Po(K).BestellPos = CInt(Trim$(arHelper(J)))
                        ElseIf J = 2 Then
                            listEket_Po(K).LöschKennz = Trim$(arHelper(J))
                        ElseIf J = 3 Then
                            listEket_Po(K).Materialnummer = Trim$(arHelper(J))
                        ElseIf J = 4 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEket_Po(K).Menge = CDbl(CDouble)
                        ElseIf J = 5 Then
                            listEket_Po(K).Mengeneinheit = Trim$(arHelper(J))
                        ElseIf J = 6 Then
                            listEket_Po(K).Bedarfsnummer = Trim$(arHelper(J))
                        ElseIf J = 7 Then
                            If Trim$(arHelper(J)) = "X" Then
                                listEket_Po(K).EndlieferKennz = True 'Lieferung abgeschlossen
                            Else
                                listEket_Po(K).EndlieferKennz = False
                            End If
                        ElseIf J = 8 Then
                            CDatum = Trim$(arHelper(J))
                            listEket_Po(K).Lieferdatum = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4))
                        ElseIf J = 10 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEket_Po(K).Liefermenge = CDbl(CDouble) 'Gelieferte Menge pro Beleg
                            If listEket_Po(K).Liefermenge = listEket_Po(K).Menge Then 'Lieferung abgeschlossen
                                listEket_Po(K).EndlieferKennz = True
                            End If
                        ElseIf J = 11 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEket_Po(K).Preis = CDbl(CDouble)
                        ElseIf J = 12 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEket_Po(K).Preiseinheit = CDbl(CDouble)
                        ElseIf J = 13 Then
                            listEket_Po(K).Währung = Trim$(arHelper(J))
                        ElseIf J = 14 Then
                            listEket_Po(K).PSP = Trim$(arHelper(J))
                        ElseIf J = 15 Then
                            listEket_Po(K).Kurztext = Trim$(arHelper(J))
                        ElseIf J = 16 Then
                            CDatum = Trim$(arHelper(J))
                            listEket_Po(K).AEDAT = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4))
                        ElseIf J = 17 Then
                            listEket_Po(K).Lieferant = Trim$(arHelper(J))
                        ElseIf J = 18 Then
                            listEket_Po(K).EndRechnung = Trim$(arHelper(J))
                            If listEket_Po(K).EndRechnung = "X" Then
                                listEket_Po(K).Obligo = False
                            Else
                                listEket_Po(K).Obligo = True
                            End If
                        ElseIf J = 19 Then
                            Try
                                listEket_Po(K).Sachkonto = CLng(Trim$(arHelper(J)))
                            Catch
                                listEket_Po(K).Sachkonto = 0
                            End Try
                        ElseIf J = 20 Then
                            Try
                                listEket_Po(K).Kostenstelle = CLng(Trim$(arHelper(J)))
                            Catch
                                listEket_Po(K).Kostenstelle = 0
                            End Try
                        ElseIf J = 21 Then
                            Try
                                listEket_Po(K).Auftragsnummer = CLng(Trim$(arHelper(J)))
                            Catch
                                listEket_Po(K).Auftragsnummer = 0
                            End Try
                        End If
                    Next J
                    'EKET ID
                    listEket_Po(K).IdEket = Trim$(arHelper(0)) & "-" & CInt(Trim$(arHelper(1))) 'Bestellung & Bestellpos

                    'Preis pro Position
                    listEket_Po(K).PosPreis = listEket_Po(K).Preis / listEket_Po(K).Preiseinheit * listEket_Po(K).Menge

                    K = K + 1
                Next I

                Return listEket_Po 'Liste übergeben

            Else
                boolGotData = False
            End If
        End If

    End Function 'PSP und GJ

    Function Get_EbanKn(listPspGj As tablePspGj()) As tableEban_Kn() 'PSP und GJ
        Console.WriteLine("FUNC: Function Get_EbanKn")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object
        Dim CDatum As String, CDouble As String

        Dim listEban_Kn As tableEban_Kn()
        boolGotData = False

        '--- Verbindungsdaten setzen ---
        funccontrol.Connection = conn
        If funccontrol.Count > 0 Then funccontrol.RemoveAll

        '--- Einstellungen für das Lesen der Tabellen
        RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
        QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
        DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
        OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
        SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
        SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

        '--- Tabelle aus welcher die Daten gelesen werden sollen -----
        QUERY_TAB.Value = "YCAA4019_EBAN_KN" 'Tabellenname
        DELIMITER.Value = "|"

        '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(1, "FIELDNAME") = "BANFN"
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(2, "FIELDNAME") = "BNFPO"
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(3, "FIELDNAME") = "AFNAM" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(4, "FIELDNAME") = "MATNR" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(5, "FIELDNAME") = "BEDNR" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(6, "FIELDNAME") = "MENGE" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(7, "FIELDNAME") = "MEINS" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(8, "FIELDNAME") = "BADAT" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(9, "FIELDNAME") = "EBELN" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(10, "FIELDNAME") = "EBELP" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(11, "FIELDNAME") = "PREIS" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(12, "FIELDNAME") = "PEINH" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(13, "FIELDNAME") = "WAERS" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(14, "FIELDNAME") = "SAKTO" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(15, "FIELDNAME") = "PSP" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(16, "FIELDNAME") = "LFDAT" '
        SAP_FIELD_TABLE.AppendRow
        SAP_FIELD_TABLE(17, "FIELDNAME") = "TXZ01" '

        '--- Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen
        intRow = 1

        For K = 0 To UBound(listPspGj) 'Geht die Länge des Arrays durch
            If Not IsNothing(listPspGj(K).PSP) And Not IsNothing(listPspGj(K).GJ) Then
                OPTIONS.AppendRow
                OPTIONS(intRow, "TEXT") = "PSP = '" & listPspGj(K).PSP & "' AND LFDAT >= '" & listPspGj(K).GJ & "0102' AND LFDAT <= '" & listPspGj(K).GJ & "1231' OR"
                intRow = intRow + 1
            End If
        Next K

        rowCount = OPTIONS.rowCount
        lastString = OPTIONS(rowCount, "TEXT")
        length = Len(lastString)

        If (length > 0) Then
            OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
        End If

        '--- Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen
        If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
            If SAP_DATA_TABLE.rowCount > 0 Then
                boolGotData = True

                K = 0 'K gibt den höchsten Index von listEban_Kn an
                ReDim listEban_Kn(K)

                Dim strSapData As String
                Dim progress As Integer = 50
                For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch
                    If progress = I Then
                        Form_Edit.ToolStripStatusOperation.Text = "Bedarfsanforderung(en) herunterladen: " & I & "/" & SAP_DATA_TABLE.rowCount
                        Form_Psb.PsbStatusText.Text = "Bedarfsanforderung(en) herunterladen: " & I & "/" & SAP_DATA_TABLE.rowCount
                        Form_Edit.StatusStrip.Refresh()
                        Form_Psb.StatusStrip.Refresh()
                        progress = progress + 50
                    End If

                    strSapData = SAP_DATA_TABLE(I, 1).ToString
                    Dim arHelper() As String = strSapData.Split("|")

                    ReDim Preserve listEban_Kn(K)
                    For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                        If J = 0 Then
                            listEban_Kn(K).BANFN = Trim$(arHelper(J))
                        ElseIf J = 1 Then
                            listEban_Kn(K).BNFPO = CInt(Trim$(arHelper(J)))
                        ElseIf J = 2 Then
                            listEban_Kn(K).AFNAM = Trim$(arHelper(J))
                        ElseIf J = 3 Then
                            listEban_Kn(K).MATNR = Trim$(arHelper(J))
                        ElseIf J = 4 Then
                            listEban_Kn(K).BEDNR = Trim$(arHelper(J))
                        ElseIf J = 5 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEban_Kn(K).MENGE = CDbl(CDouble)
                        ElseIf J = 6 Then
                            listEban_Kn(K).MEINS = Trim$(arHelper(J))
                        ElseIf J = 7 Then
                            CDatum = Trim$(arHelper(J))
                            listEban_Kn(K).BADAT = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4))
                        ElseIf J = 8 Then
                            listEban_Kn(K).EBELN = Trim$(arHelper(J))
                        ElseIf J = 9 Then
                            listEban_Kn(K).EBELP = CInt(Trim$(arHelper(J)))
                        ElseIf J = 10 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEban_Kn(K).PREIS = CDbl(CDouble)
                        ElseIf J = 11 Then
                            CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                            listEban_Kn(K).PEINH = CDbl(CDouble)
                        ElseIf J = 12 Then
                            listEban_Kn(K).WAERS = Trim$(arHelper(J))
                        ElseIf J = 13 Then
                            listEban_Kn(K).SAKTO = Trim$(arHelper(J))
                        ElseIf J = 14 Then
                            listEban_Kn(K).PSP = Trim$(arHelper(J))
                        ElseIf J = 15 Then
                            CDatum = Trim$(arHelper(J))
                            listEban_Kn(K).LFDAT = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4))
                        ElseIf J = 16 Then
                            listEban_Kn(K).TXZ01 = Trim$(arHelper(J))
                        End If
                    Next J
                    'BANF ID
                    listEban_Kn(K).Key = Trim$(arHelper(0)) & "-" & CInt(Trim$(arHelper(1))) 'Banf & Banfpos

                    'Preis pro Position
                    listEban_Kn(K).PosPreis = listEban_Kn(K).PREIS / listEban_Kn(K).PEINH * listEban_Kn(K).MENGE

                    K = K + 1
                Next I

                Return listEban_Kn 'Array übergeben

            Else
                boolGotData = False
            End If
        End If

    End Function 'PSP und GJ: In Nutzung

#Region "Terminverfolgung"
    Function Get_DatesAndComments(FromDate As String, ToDate As String, listPsp() As String) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_DatesAndComments")

        Dim i As Integer

        Try
#Region "Create Datatable"
            Form_Termine.LabelStatus.Text = "Datentabelle erzeugen"
            Form_Termine.StatusStrip1.Refresh()
            Form_Psb.StatusStrip.Refresh()

            Dim dt As New System.Data.DataTable
#Region "SAP"
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Nr", .DataType = GetType(Integer), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Bestellung", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "BestellPosition", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Materialnummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Materialkurztext", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Menge", .DataType = GetType(Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Mengeneinheit", .DataType = GetType(String), .DefaultValue = "ST"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Bedarfsnummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Lieferkennzeichen", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Lieferdatum", .DataType = GetType(Date), .DefaultValue = Now})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Stat_Lieferdatum", .DataType = GetType(Date), .DefaultValue = Now})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Liefermenge", .DataType = GetType(Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Preis", .DataType = GetType(Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Preiseinheit", .DataType = GetType(Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Währung", .DataType = GetType(String), .DefaultValue = "EUR"})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "LieferantenNummer", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Lieferant", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "PSP", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Auftrag", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Kostenstelle", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Sachkonto", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Änderungsdatum", .DataType = GetType(Date), .DefaultValue = Now})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Endrechnung", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "PreisGesamt", .DataType = GetType(Double), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "WE_Haken", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "RE_Haken", .DataType = GetType(String), .DefaultValue = ""})
#End Region

#Region "myBudget"
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Kommentar1", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Kommentar2", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Kommentar3", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "ID", .DataType = GetType(Integer), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Action", .DataType = GetType(String), .DefaultValue = ""})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "SapId", .DataType = GetType(String), .DefaultValue = ""})
#End Region
#Region "Berechnungen"
            dt.Columns.Add(New DataColumn() With {.ColumnName = "Lieferzeit", .DataType = GetType(Int32), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "LieferterminAbweichung", .DataType = GetType(Int32), .DefaultValue = 0})
            dt.Columns.Add(New DataColumn() With {.ColumnName = "ZeitBisLieferung", .DataType = GetType(Int32), .DefaultValue = 0})
#End Region
#End Region

#Region "Get myBudget Table Data: Terminverfolgung"
            Form_Termine.LabelStatus.Text = "Kommentare lesen"
            Form_Termine.StatusStrip1.Refresh()

            Dim dt_Com As System.Data.DataTable
            Dim dt_Cache As System.Data.DataTable
            If Not IsNothing(listPsp) Then
                For i = 0 To UBound(listPsp)
                    dt_Cache = Get_tblSapComments(listPsp(i))
                    'Gesamt Tabelle erzeugen/erweitern
                    If IsNothing(dt_Com) Then
                        dt_Com = dt_Cache.Clone
                    End If
                    If Not dt_Cache.Rows.Count = 0 Then
                        dt_Com.Merge(dt_Cache, True)
                    End If
                Next
            End If
#End Region

            If SAP_Login(pbl_dtSAPCon) = False Then Exit Function

#Region "Get SAP data (orders) and combine it with myBudget data"
            Dim J As Long, K As Long
            Dim intRow As Integer
            Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
            Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object, MAXROWS As Object
            Dim CDatum As String, CDouble As String

            Dim listEket_Po As tableEket_Po()
            boolGotData = False

#Region "Verbindungsdaten setzen"
            Form_Termine.LabelStatus.Text = "SAP Daten herunterladen"
            Form_Termine.StatusStrip1.Refresh()

            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll
            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_EKET_PO" 'Tabellenname
            DELIMITER.Value = "|"
            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "EBELN"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "EBELP"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "LOEKZ" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "MATNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "MENGE" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(6, "FIELDNAME") = "MEINS" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(7, "FIELDNAME") = "BEDNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(8, "FIELDNAME") = "ELIKZ" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(9, "FIELDNAME") = "EINDT" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(10, "FIELDNAME") = "SLFDT" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(11, "FIELDNAME") = "LIF_MENGE" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(12, "FIELDNAME") = "NETPR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(13, "FIELDNAME") = "PEINH" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(14, "FIELDNAME") = "WAERS" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(15, "FIELDNAME") = "PSP" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(16, "FIELDNAME") = "TXZ01" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(17, "FIELDNAME") = "AEDAT" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(18, "FIELDNAME") = "NAME1" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(19, "FIELDNAME") = "EREKZ" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(20, "FIELDNAME") = "SAKTO" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(21, "FIELDNAME") = "KOSTL" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(22, "FIELDNAME") = "AUFNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(23, "FIELDNAME") = "LIFNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(24, "FIELDNAME") = "WEPOS"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(25, "FIELDNAME") = "REPOS"

#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1

            For K = 0 To UBound(listPsp) 'Geht die Länge des Arrays durch
                If Not IsNothing(listPsp(K)) Then
                    OPTIONS.AppendRow
                    OPTIONS(intRow, "TEXT") = "PSP EQ '" & listPsp(K) & "' AND "
                    intRow = intRow + 1

                    OPTIONS.AppendRow
                    OPTIONS(intRow, "TEXT") = "LOEKZ NE 'L' AND "
                    intRow = intRow + 1

                    OPTIONS.AppendRow
                    OPTIONS(intRow, "TEXT") = "ELIKZ NE 'X' AND "
                    intRow = intRow + 1

                    OPTIONS.AppendRow
                    OPTIONS(intRow, "TEXT") = "EINDT BETWEEN '" & FromDate & "' AND '" & ToDate & "' OR"
                    intRow = intRow + 1
                End If
            Next K

            rowCount = OPTIONS.rowCount
            lastString = OPTIONS(rowCount, "TEXT")
            length = Len(lastString)

            If (length > 0) Then
                OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
            End If
#End Region

            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEket_Po an
                    ReDim listEket_Po(K)

                    Dim strSapData As String
                    Dim progress As Integer = 50
                    For i = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch
                        Dim row As Integer = i - 1
                        dt.Rows.Add()

                        If progress = i Then
                            Form_Termine.LabelStatus.Text = "Bestellung(en) herunterladen: " & i & "/" & SAP_DATA_TABLE.rowCount
                            Form_Termine.StatusStrip1.Refresh()
                            progress = progress + 50
                        End If

                        strSapData = SAP_DATA_TABLE(i, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        ReDim Preserve listEket_Po(K)
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(row).Item("Bestellung") = Trim$(arHelper(J))
                                Do Until dt.Rows(row).Item("Bestellung").ToString.Length = 10
                                    dt.Rows(row).Item("Bestellung") = "0" & dt.Rows(row).Item("Bestellung")
                                Loop
                            ElseIf J = 1 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(row).Item("BestellPosition") = Trim$(arHelper(J))
                                Do Until dt.Rows(row).Item("BestellPosition").ToString.Length = 5
                                    dt.Rows(row).Item("BestellPosition") = "0" & dt.Rows(row).Item("BestellPosition")
                                Loop
                            ElseIf J = 2 Then
                                'listEket_Po(K).LöschKennz = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(row).Item("Materialnummer") = Trim$(arHelper(J))
                            ElseIf J = 4 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(row).Item("Menge") = CDbl(CDouble)
                            ElseIf J = 5 Then
                                dt.Rows(row).Item("Mengeneinheit") = Trim$(arHelper(J))
                            ElseIf J = 6 Then
                                dt.Rows(row).Item("Bedarfsnummer") = Trim$(arHelper(J))
                            ElseIf J = 7 Then
                                dt.Rows(row).Item("Lieferkennzeichen") = Trim$(arHelper(J))
                            ElseIf J = 8 Then
                                CDatum = Trim$(arHelper(J))
                                dt.Rows(row).Item("Lieferdatum") = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4)).ToShortDateString
                            ElseIf J = 9 Then
                                CDatum = Trim$(arHelper(J))
                                dt.Rows(row).Item("Stat_Lieferdatum") = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4)).ToShortDateString
                            ElseIf J = 10 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(row).Item("Liefermenge") = CDbl(CDouble) 'Gelieferte Menge pro Beleg
                                If dt.Rows(row).Item("Liefermenge") = dt.Rows(row).Item("Menge") Then 'Lieferung abgeschlossen
                                    dt.Rows(row).Item("Lieferkennzeichen") = "X"
                                End If
                            ElseIf J = 11 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(row).Item("Preis") = CDbl(CDouble)
                            ElseIf J = 12 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(row).Item("Preiseinheit") = CDbl(CDouble)
                            ElseIf J = 13 Then
                                dt.Rows(row).Item("Währung") = Trim$(arHelper(J))
                            ElseIf J = 14 Then
                                dt.Rows(row).Item("PSP") = Trim$(arHelper(J))
                            ElseIf J = 15 Then
                                dt.Rows(row).Item("Materialkurztext") = Trim$(arHelper(J))
                            ElseIf J = 16 Then
                                CDatum = Trim$(arHelper(J))
                                dt.Rows(row).Item("Änderungsdatum") = CDate(Right$(CDatum, 2) & "." & Mid$(CDatum, 5, 2) & "." & Left$(CDatum, 4)).ToShortDateString
                            ElseIf J = 17 Then
                                dt.Rows(row).Item("Lieferant") = Trim$(arHelper(J))
                            ElseIf J = 18 Then
                                dt.Rows(row).Item("Endrechnung") = Trim$(arHelper(J))
                            ElseIf J = 19 Then
                                dt.Rows(row).Item("Sachkonto") = Trim$(arHelper(J))
                            ElseIf J = 20 Then
                                dt.Rows(row).Item("Kostenstelle") = Trim$(arHelper(J))
                            ElseIf J = 21 Then
                                dt.Rows(row).Item("Auftrag") = Trim$(arHelper(J))
                            ElseIf J = 22 Then
                                dt.Rows(row).Item("LieferantenNummer") = Trim$(arHelper(J))
                            ElseIf J = 23 Then
                                dt.Rows(row).Item("WE_Haken") = Trim$(arHelper(J))
                            ElseIf J = 24 Then
                                dt.Rows(row).Item("RE_Haken") = Trim$(arHelper(J))
                            End If
                        Next J
                        'Nr
                        dt.Rows(row).Item("Nr") = i
                        'SapId
                        dt.Rows(row).Item("SapId") = dt.Rows(row).Item("Bestellung") & "-" & dt.Rows(row).Item("BestellPosition")
                        'Preis pro Position
                        dt.Rows(row).Item("PreisGesamt") = dt.Rows(row).Item("Preis") / dt.Rows(row).Item("Preiseinheit") * dt.Rows(row).Item("Menge")
                        'Kommentare dazu lesen
                        Dim foundRows() As Data.DataRow
                        foundRows = dt_Com.Select("SapId = '" & dt.Rows(row).Item("SapId") & "'")
                        If Not foundRows.Count = 0 Then 'Es gibt Kommentare zur Bestellung
                            dt.Rows(row).Item("Kommentar1") = foundRows(0).Item("Kommentar1")
                            dt.Rows(row).Item("Kommentar2") = foundRows(0).Item("Kommentar2")
                            dt.Rows(row).Item("Kommentar3") = foundRows(0).Item("Kommentar3")
                            dt.Rows(row).Item("ID") = foundRows(0).Item("ID")
                        End If
                        'Berechnungen
                        Dim LfDat As Date = dt.Rows(row).Item("Lieferdatum")
                        Dim Stat_LfDat As Date = dt.Rows(row).Item("Stat_Lieferdatum")
                        Dim AeDat As Date = dt.Rows(row).Item("Änderungsdatum")

                        Dim Lieferzeit As TimeSpan
                        Lieferzeit = LfDat - AeDat
                        dt.Rows(row).Item("Lieferzeit") = Convert.ToInt32(Lieferzeit.TotalDays)

                        Dim LieferterminAbweichung As TimeSpan
                        LieferterminAbweichung = LfDat - Stat_LfDat
                        dt.Rows(row).Item("LieferterminAbweichung") = Convert.ToInt32(LieferterminAbweichung.TotalDays)

                        Dim ZeitBisLieferung As TimeSpan
                        ZeitBisLieferung = LfDat - Now
                        dt.Rows(row).Item("ZeitBisLieferung") = Convert.ToInt32(ZeitBisLieferung.TotalDays)
                        K = K + 1
                    Next i
                Else
                    boolGotData = False
                End If
            End If
#End Region

            SAP_Logout()

            Form_Termine.LabelStatus.Text = ""
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Function
#End Region



#Region "Get live SAP data"
    Public Function GetSapData(GJ() As Integer, PSP() As String, IK() As String, DoExcelExport As Boolean) As Boolean
        Dim progress As Integer, i As Integer, j As Integer, k As Integer
        Dim dtIst As New System.Data.DataTable
        Dim dtObligo As New System.Data.DataTable
        Dim dt As New System.Data.DataTable
        Dim dtBest As New System.Data.DataTable, dtBestCache As New System.Data.DataTable
        Dim dtBanf As New System.Data.DataTable, dtBanfCache As New System.Data.DataTable
        Dim dtObject As New System.Data.DataTable, dtObjektIK As New System.Data.DataTable
        Dim dtAuftrag As New System.Data.DataTable, dtAuftragCache As New System.Data.DataTable

        Erase BANF
        Erase BEST

#Region "Methodenprüfungen"
        If con.State = ConnectionState.Closed Or boolConnected = False Then
            MsgBox("Die Verbindung zum Server wurde unterbrochen." & vbCrLf & "Bitte verbinden Sie sich neu mit dem Server.", Title:="Fehler bei der Verbindung zum Server", Buttons:=MsgBoxStyle.Critical)
            Return False
        End If

        If UBound(GJ) < 0 Then
            MsgBox("Es wurden keine Geschäftsjahre gewählt.", Title:="Fehler", Buttons:=MsgBoxStyle.Critical)
            Return False
        End If

        If IsNothing(PSP) AndAlso IsNothing(IK) Then
            MsgBox("Es wurden keine PSP Elemente oder IK-Nummern gewählt.", Title:="Fehler", Buttons:=MsgBoxStyle.Critical)
            Return False
        End If
#End Region

#Region "DataTable Structure"
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "PSP", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "Object", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "ID_AP", .DataType = GetType(Integer), .DefaultValue = 0})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "Name", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "PNummer", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "Abteilung", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "Arbeitspaket", .DataType = GetType(String), .DefaultValue = ""})
        dtObject.Columns.Add(New DataColumn() With {.ColumnName = "Auftrag", .DataType = GetType(String), .DefaultValue = ""})

        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "Auftrag", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "Text", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "IK", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "Object", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "PSPEL", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "Name", .DataType = GetType(String), .DefaultValue = ""})
        dtAuftrag.Columns.Add(New DataColumn() With {.ColumnName = "Abteilung", .DataType = GetType(String), .DefaultValue = ""})

        dt.Columns.Add(New DataColumn() With {.ColumnName = "txtKst_Lst", .DataType = GetType(String), .DefaultValue = ""}) 'Kostenart/Sachkonto
        dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBedarfsnummer", .DataType = GetType(String), .DefaultValue = ""}) 'Kostenstelle1
        dt.Columns.Add(New DataColumn() With {.ColumnName = "datErstellungsdatum", .DataType = GetType(Date), .DefaultValue = Now})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "lngSort", .DataType = GetType(Integer), .DefaultValue = 0}) 'Monat
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Vorgang", .DataType = GetType(String), .DefaultValue = ""}) 'Vorgangstyp
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryGeschäftsjahr", .DataType = GetType(Integer), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Beschreibung_Sachkonto", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Leistungsart", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Kostenstelle2", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Object", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "PSP", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Auftrag", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Auftragstitel", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Bestellung", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "BestellPosition", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Bestelltext", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Firma", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Materialnummer", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Gesamtwert", .DataType = GetType(Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Menge", .DataType = GetType(Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "MengenEinheit", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "PNummer", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Belegnummer", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Buchungsdatum", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "IdSap", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "Bestellart", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBeschreibung", .DataType = GetType(String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "boolObligo", .DataType = GetType(System.Boolean), .DefaultValue = False})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "curKosten_pro_Einheit", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJanuar", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblFebruar", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMärz", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblApril", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblMai", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuni", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblJuli", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblAugust", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblSeptember", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblOktober", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblNovember", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "dblDezember", .DataType = GetType(System.Double), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryArbeitspaket", .DataType = GetType(System.Int16), .DefaultValue = 0})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKostenart", .DataType = GetType(System.String), .DefaultValue = "sonstige Kosten"})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryVersion", .DataType = GetType(System.Int16), .DefaultValue = 1})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryKlassifizierung", .DataType = GetType(System.String), .DefaultValue = "Muss"})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwurfstyp", .DataType = GetType(System.String), .DefaultValue = "Ist"})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryPaket", .DataType = GetType(System.String), .DefaultValue = "none"})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "qryEntwicklungspaket", .DataType = GetType(System.String), .DefaultValue = "none"})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "txtBemerkung", .DataType = GetType(System.String), .DefaultValue = ""})
        dt.Columns.Add(New DataColumn() With {.ColumnName = "txtZusatzfeld", .DataType = GetType(System.String), .DefaultValue = ""})

        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "Bestellung", .DataType = GetType(System.String), .DefaultValue = ""})
        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "BestellPosition", .DataType = GetType(System.String), .DefaultValue = ""})
        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "Bedarfsnummer", .DataType = GetType(System.String), .DefaultValue = ""})
        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "Text", .DataType = GetType(System.String), .DefaultValue = ""})
        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "Materialnummer", .DataType = GetType(System.String), .DefaultValue = ""})
        dtBest.Columns.Add(New DataColumn() With {.ColumnName = "Lieferant", .DataType = GetType(System.String), .DefaultValue = ""})

        'DataTable Strukturen übergeben
        dtIst = dt.Clone
        dtObligo = dt.Clone
        dtBanf = dtBest.Clone
        dtBanfCache = dtBest.Clone
        dtBestCache = dtBest.Clone
        dtAuftragCache = dtAuftrag.Clone
        dtObjektIK = dtObject.Clone
#End Region

        If SAP_Login(pbl_dtSAPCon) = False Then Exit Function
#Region "SAP Budget Data"
        '1. Objekte zu den PSP Elementen laden
        '2. Ist und Obligo Tabellen auslesen

#Region "E-Budget und Invest Objektnummern lesen"

#Region "E-Budget Objekte"
        If Not IsNothing(PSP) Then
            dtObject = Get_Object(PSP, dtObject)
            If dtObject.Rows.Count = 0 Then
                MsgBox("E-Budget Objekt Strukturen konnten nicht geladen werden." & vbCrLf & "Bitte überprüfen Sie die ausgewählten PSP Elemente." & vbCrLf & vbCrLf & "Gegebenenfalls fehlt eine SAP Berechtigung (RFC) - Bitte an den IT Administartor wenden!", vbCritical, "Fehler")
                Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
                Form_Edit.TimerEditForm.Start()
                Form_Psb.Timer.Interval = 5000 '1000 = 1 sec
                Form_Psb.Timer.Start()
                Exit Function
            End If

            'Arbeitspaket Daten aktualisieren: SAP überschreibt myBudget!
            For i = 0 To dtObject.Rows.Count - 1

                Form_Edit.ToolStripStatusOperation.Text = "Arbeitpaket Daten werden aktualisiert - (" & 1 & "/" & dtObject.Rows.Count & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Psb.PsbStatusText.Text = "Arbeitpaket Daten werden aktualisiert - (" & 1 & "/" & dtObject.Rows.Count & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Edit.StatusStrip.Refresh()
                Form_Psb.StatusStrip.Refresh()

                Update_ArbeitspaketVerantw(dtObject.Rows(i).Item("ID_AP"), dtObject.Rows(i).Item("Abteilung"), dtObject.Rows(i).Item("Name"), dtObject.Rows(i).Item("Arbeitspaket"))
            Next

        End If
#End Region

#Region "Invest Budget Objekte"
        If Not IsNothing(IK) Then
            Dim IKNew(UBound(IK), 1) As String
            For i = 0 To UBound(IK)
                IKNew(i, 0) = IK(i)
                IKNew(i, 1) = IK(i)
                IKNew(i, 1) = Replace(IKNew(i, 1), ".", "")
                IKNew(i, 1) = Replace(IKNew(i, 1), "-", "")
                IK(i) = Replace(IK(i), ".", "")
                IK(i) = Replace(IK(i), "-", "")
            Next

            dtObjektIK = Get_Object(IK, dtObject) 'Objekt zu den IK Nummern lesen

            For i = 0 To UBound(IKNew) 'korrekte IK Nummer in der Objekt Tabelle nachtragen
                Dim foundIK() As Data.DataRow
                foundIK = dtObjektIK.Select("PSP = '" & IKNew(i, 1) & "'")
                If foundIK.Count > 0 Then 'Es gibt mindestens einen Treffer.
                    foundIK(j).Item("PSP") = IKNew(i, 0)
                End If
            Next

            If Not IsNothing(dtObjektIK) AndAlso Not dtObjektIK.Rows.Count = 0 Then 'Auftragsnummern zu den IK Nummern/Objekten lesen
                For i = 0 To dtObjektIK.Rows.Count - 1
                    dtAuftragCache = Get_Auftrag(i, dtObjektIK, dtAuftragCache)
                    dtAuftrag.Merge(dtAuftragCache, False)
                Next
            End If

            If dtAuftrag.Rows.Count = 0 Then
                MsgBox("Invest Objekt Strukturen konnten nicht geladen werden." & vbCrLf & "Bitte überprüfen Sie die ausgewählten IK Elemente." & vbCrLf & vbCrLf & "Gegebenenfalls fehlt eine SAP Berechtigung (RFC) - Bitte an den IT Administartor wenden!", vbCritical, "Fehler")
                Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
                Form_Edit.TimerEditForm.Start()
                Form_Psb.Timer.Interval = 5000 '1000 = 1 sec
                Form_Psb.Timer.Start()
                Exit Function
            End If

            'Objekte der Auftragsnummer in die Gesamt Objekttabelle aufnehmen
            For i = 0 To dtAuftrag.Rows.Count - 1
                dtObject.Rows.Add()
                dtObject.Rows(dtObject.Rows.Count - 1).Item("PSP") = dtAuftrag.Rows(i).Item("IK")
                dtObject.Rows(dtObject.Rows.Count - 1).Item("Object") = dtAuftrag.Rows(i).Item("Object")
                dtObject.Rows(dtObject.Rows.Count - 1).Item("Arbeitspaket") = dtAuftrag.Rows(i).Item("Text")
                dtObject.Rows(dtObject.Rows.Count - 1).Item("Name") = dtAuftrag.Rows(i).Item("Name")
                dtObject.Rows(dtObject.Rows.Count - 1).Item("Abteilung") = dtAuftrag.Rows(i).Item("Abteilung")
                dtObject.Rows(dtObject.Rows.Count - 1).Item("Auftrag") = dtAuftrag.Rows(i).Item("Auftrag")
            Next
        End If
#End Region

#End Region

#Region "Ist + Obligo Daten aus SAP laden"
        For i = 0 To dtObject.Rows.Count - 1 'SAP Daten zu allen PSP Elementen (Objekten) und Geschäftsjahren
            For j = 0 To UBound(GJ)

                Form_Edit.ToolStripStatusOperation.Text = "SAP Ist + Obligo zu Objekt " & i + 1 & "/" & dtObject.Rows.Count & " - Geschäftsjahr " & GJ(j) & " wird heruntergeladen. Dieser Vorgang kann einige Minuten dauern..."
                Form_Psb.PsbStatusText.Text = "SAP Ist + Obligo zu Objekt " & i + 1 & "/" & dtObject.Rows.Count & " - Geschäftsjahr " & GJ(j) & " wird heruntergeladen. Dieser Vorgang kann einige Minuten dauern..."
                Form_Edit.StatusStrip.Refresh()
                Form_Psb.StatusStrip.Refresh()

                dtObligo = GetSapObligo(GJ(j), dtObject.Rows(i).Item("Object"), dtObject, dt)
                dtIst = GetSapIst(GJ(j), dtObject.Rows(i).Item("Object"), dtObject, dtIst)
                If Not IsNothing(dtIst) Then
                    dt.Merge(dtIst, False)
                End If
                If Not IsNothing(dtObligo) Then
                    dt.Merge(dtObligo, False)
                End If
            Next j
        Next i
#End Region

#Region "Ergebnisrechnungen löschen"
        Form_Edit.ToolStripStatusOperation.Text = "Ergebnisrechnungen werden gelöscht."
        Form_Psb.PsbStatusText.Text = "Ergebnisrechnungen werden gelöscht."
        Form_Edit.StatusStrip.Refresh()
        Form_Psb.StatusStrip.Refresh()

        Dim delRows() As Data.DataRow
        delRows = dt.Select("txtKst_Lst like '937*' or txtKst_Lst like '981*'") 'E-Budget & Invest
        If Not delRows.Count = 0 Then
            For i = delRows.Count - 1 To 0 Step -1
                delRows(i).Delete()
            Next
        End If
#End Region

#Region "ANÜ Kosten aufschlüsseln: muss vor 0€ Zeilen löschen sein!"
        dt = ConvertFremdPCosts(dt)
#End Region

#Region "0€ Zeilen löschen"
        Form_Edit.ToolStripStatusOperation.Text = "0€ Kostenbelege werden gelöscht."
        Form_Psb.PsbStatusText.Text = "0€ Kostenbelege werden gelöscht."
        Form_Edit.StatusStrip.Refresh()
        Form_Psb.StatusStrip.Refresh()

        Dim delRows2() As Data.DataRow
        delRows2 = dt.Select("Gesamtwert = 0")
        If Not delRows2.Count = 0 Then
            For i = delRows2.Count - 1 To 0 Step -1
                delRows2(i).Delete()
            Next
        End If
#End Region

        If dt.Rows.Count = 0 Then
            MsgBox("Abbruch der Aktion, da keine Einzeldaten importiert werden konnten.", MsgBoxStyle.Critical, "Fehler beim Import")
            Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
            Form_Edit.TimerEditForm.Start()
            Form_Psb.Timer.Interval = 5000 '1000 = 1 sec
            Form_Psb.Timer.Start()
            Return False
        End If
#End Region

#Region "Budget Daten um Bedarfsnummern erweitern"
        Dim start As Integer = 0, ende As Integer = 0
        If Not IsNothing(BANF) OrElse Not IsNothing(BEST) Then
#Region "SAP BANFEN"
            If Not IsNothing(BANF) Then
                Do Until ende >= UBound(BANF)
                    If ende + 1500 >= UBound(BANF) Then
                        ende = UBound(BANF)
                    Else
                        ende = ende + 1500
                    End If

                    Form_Edit.ToolStripStatusOperation.Text = "SAP BANF Daten werden heruntergeladen - (" & start & "/" & UBound(BANF) & ") - Dieser Vorgang kann einige Minuten dauern..."
                    Form_Psb.PsbStatusText.Text = "SAP BANF Daten werden heruntergeladen - (" & start & "/" & UBound(BANF) & ") - Dieser Vorgang kann einige Minuten dauern..."
                    Form_Edit.StatusStrip.Refresh()
                    Form_Psb.StatusStrip.Refresh()

                    dtBanfCache = Get_BanfBednr_V2(dtBanfCache, start, ende, BANF)
                    dtBanf.Merge(dtBanfCache, False)
                    start = ende + 1
                Loop
            End If
#End Region

#Region "SAP Bestellungen"
            If Not IsNothing(BEST) Then
                start = 0
                ende = 0
                Do Until ende >= UBound(BEST)
                    If ende + 1500 >= UBound(BEST) Then
                        ende = UBound(BEST)
                    Else
                        ende = ende + 1500
                    End If

                    Form_Edit.ToolStripStatusOperation.Text = "SAP BEST Daten werden heruntergeladen - (" & start & "/" & UBound(BEST) & ") - Dieser Vorgang kann einige Minuten dauern..."
                    Form_Psb.PsbStatusText.Text = "SAP BEST Daten werden heruntergeladen - (" & start & "/" & UBound(BEST) & ") - Dieser Vorgang kann einige Minuten dauern..."
                    Form_Edit.StatusStrip.Refresh()
                    Form_Psb.StatusStrip.Refresh()

                    dtBestCache = Get_BestBednr_V2(dtBestCache, start, ende, BEST)
                    dtBest.Merge(dtBestCache, False)
                    start = ende + 1
                Loop
            End If
#End Region

            dtBanf.Merge(dtBest, False) 'Bestellungen und Bestellanforderungen zusammen führen
            'WICHTIG: Banfen zuerst, damit die Bestellungen zum Schluss kommen und ggf. aktuelle Bedarfsnummern aus der Bestellung die der BANF überschreiben
        End If
#End Region
        SAP_Logout()

#Region "Budget Daten um Bedarfsnummern der Bestelldaten erweitern"
        If Not dtBanf.Rows.Count = 0 AndAlso Not dt.Rows.Count = 0 Then
            For i = 0 To dtBanf.Rows.Count - 1

                Form_Edit.ToolStripStatusOperation.Text = "Daten um BANF/BEST Daten erweitern - (" & i & "/" & dtBanf.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Psb.PsbStatusText.Text = "Daten um BANF/BEST Daten erweitern - (" & i & "/" & dtBanf.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Edit.StatusStrip.Refresh()
                Form_Psb.StatusStrip.Refresh()

                If Not String.IsNullOrEmpty(dtBanf(i).Item("Bestellung")) Then 'Kostposition ist eine BANF oder Bestellung
                    Dim foundBedNr() As Data.DataRow
                    foundBedNr = dt.Select("Bestellung = '" & dtBanf(i).Item("Bestellung") & "' and BestellPosition = '" & dtBanf(i).Item("BestellPosition") & "'")
                    If foundBedNr.Count > 0 Then 'Es gibt mindestens einen Treffer.
                        For j = 0 To foundBedNr.Count - 1
                            foundBedNr(j).Item("txtBedarfsnummer") = dtBanf(i).Item("Bedarfsnummer")
                            foundBedNr(j).Item("Materialnummer") = dtBanf(i).Item("Materialnummer")
                            foundBedNr(j).Item("Bestelltext") = dtBanf(i).Item("Text")
                            If foundBedNr(j).Item("Bestellart") = "BEST" Then
                                foundBedNr(j).Item("Firma") = dtBanf(i).Item("Lieferant")
                            End If
                        Next
                    End If
                End If
            Next

        End If
#End Region

#Region "Budget Daten um Auftragsnummern erweitern"
        If Not IsNothing(dtAuftrag) Then
            For i = 0 To dtAuftrag.Rows.Count - 1

                Form_Edit.ToolStripStatusOperation.Text = "Daten um AUF Daten erweitern - (" & i & "/" & dtAuftrag.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Psb.PsbStatusText.Text = "Daten um AUF Daten erweitern - (" & i & "/" & dtAuftrag.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
                Form_Edit.StatusStrip.Refresh()
                Form_Psb.StatusStrip.Refresh()

                Dim foundAuf() As Data.DataRow
                foundAuf = dt.Select("Object = '" & dtAuftrag.Rows(i).Item("Object") & "'")
                If foundAuf.Count > 0 Then 'Es gibt mindestens einen Treffer.
                    For j = 0 To foundAuf.Count - 1
                        foundAuf(j).Item("Auftrag") = dtAuftrag.Rows(i).Item("Auftrag")
                        foundAuf(j).Item("txtBedarfsnummer") = dtAuftrag.Rows(i).Item("Auftrag") 'Nutzung des Feldes "Bedarfsnummer" um nach Auftragsnummer gruppieren zu können
                        foundAuf(j).Item("Auftragstitel") = dtAuftrag.Rows(i).Item("Text")
                        foundAuf(j).Item("Bestellart") = "AUF"
                    Next j
                End If
            Next
        End If
#End Region

#Region "Beschreibungstext erstellen"
        Dim dtPersonal As New System.Data.DataTable
        dtPersonal = Get_tblEM_Personal()

        For i = 0 To dt.Rows.Count - 1

            Form_Edit.ToolStripStatusOperation.Text = "Beschreibungstexte erstellen - (" & i & "/" & dt.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
            Form_Psb.PsbStatusText.Text = "Beschreibungstexte erstellen - (" & i & "/" & dt.Rows.Count - 1 & ") - Dieser Vorgang kann einige Minuten dauern..."
            Form_Edit.StatusStrip.Refresh()
            Form_Psb.StatusStrip.Refresh()

            dt.Rows(i).Item("txtBeschreibung") = CreateCostDescripition_V2(dt, i, dtPersonal)
        Next
#End Region

        If DoExcelExport = True Then
#Region "Daten in Excel ausgeben"
            Form_Edit.ToolStripStatusOperation.Text = "Daten werden in Excel ausgegeben..."
            Form_Psb.PsbStatusText.Text = "Daten werden in Excel ausgegeben..."
            Form_Edit.StatusStrip.Refresh()
            Form_Psb.StatusStrip.Refresh()

#Region "DataTable in String Format convertieren"
            Dim ArrList(dt.Rows.Count + 1, dt.Columns.Count) As String
            ''Überschriften
            For j = 0 To dt.Columns.Count - 1
                ArrList(0, j) = dt.Columns(j).ColumnName
            Next

            For i = 0 To dt.Rows.Count - 1
                For j = 0 To dt.Columns.Count - 1
                    ArrList(i + 1, j) = dt.Rows(i)(j).ToString
                Next
            Next
#End Region

#Region "Daten in Excel schreiben"
            exApp = New Excel.Application
            exWB = exApp.Workbooks.Add()
            Sheet = CType(exWB.Worksheets.Item(1), Excel.Worksheet)
            exApp.ScreenUpdating = False
            exApp.Calculation = XlCalculation.xlCalculationManual

            Sheet.Range("A1").Resize(dt.Rows.Count + 1, dt.Columns.Count).Value = ArrList

            exApp.ScreenUpdating = True
            exApp.Calculation = XlCalculation.xlCalculationAutomatic
            exApp.Visible = True
            exApp.WindowState = Excel.XlWindowState.xlMaximized
#End Region
#End Region
        End If

#Region "Alte Daten löschen"
        Form_Edit.ToolStripStatusOperation.Text = "Alte Daten auf der Datenbank werden gelöscht. Dieser Vorgang kann einige Minuten dauern..."
        Form_Psb.PsbStatusText.Text = "Alte Daten auf der Datenbank werden gelöscht. Dieser Vorgang kann einige Minuten dauern..."
        Form_Edit.StatusStrip.Refresh()
        Form_Psb.StatusStrip.Refresh()
        progress = 100

        'Zu löschende Arbeitspakete herausfinden
        Dim dt_deleteArbeitspaket As System.Data.DataTable = dt.DefaultView.ToTable("DistinctTable", True, "qryArbeitspaket")
        Dim dt_deleteGJ As System.Data.DataTable = dt.DefaultView.ToTable("DistinctTable", True, "qryGeschäftsjahr")

        Try
            If Not IsNothing(dt_deleteArbeitspaket) Then

                For i = 0 To dt_deleteArbeitspaket.Rows.Count - 1
                    If progress = i Then
                        Form_Edit.ToolStripStatusOperation.Text = "Alte Daten auf der Datenbank werden gelöscht: " & i & "/" & dt_deleteArbeitspaket.Rows.Count - 1
                        Form_Psb.PsbStatusText.Text = "Alte Daten auf der Datenbank werden gelöscht: " & i & "/" & dt_deleteArbeitspaket.Rows.Count - 1
                        Form_Edit.StatusStrip.Refresh()
                        Form_Psb.StatusStrip.Refresh()
                        progress = progress + 100
                    End If
                    If Not IsNothing(dt_deleteGJ) Then
                        For j = 0 To dt_deleteGJ.Rows.Count - 1
                            Delete_Ist_Data(dt_deleteArbeitspaket.Rows(i).Item("qryArbeitspaket"), dt_deleteGJ.Rows(j).Item("qryGeschäftsjahr"))
                        Next j
                    End If
                Next i
            End If
        Catch
        End Try
#End Region

#Region "Neue Daten auf die Datenbank schreiben"
        Form_Edit.ToolStripStatusOperation.Text = "Neue Daten werden auf der Datenbank gespeichert. Dieser Vorgang kann einige Minuten dauern..."
        Form_Psb.PsbStatusText.Text = "Neue Daten werden auf der Datenbank gespeichert. Dieser Vorgang kann einige Minuten dauern..."
        Form_Edit.StatusStrip.Refresh()
        Form_Psb.StatusStrip.Refresh()

        progress = 50 'In 50er Schritte ein Update senden
        If Not IsNothing(dt) Then
            For j = 0 To dt.Rows.Count - 1
                If Not String.IsNullOrEmpty(dt.Rows(j).Item("qryGeschäftsjahr")) And Not String.IsNullOrEmpty(dt.Rows(j).Item("curKosten_pro_Einheit")) _
                        And Not String.IsNullOrEmpty(dt.Rows(j).Item("txtBeschreibung")) Then

                    If j = progress Then
                        Form_Edit.ToolStripStatusOperation.Text = "Neue Ist + Obligo Daten werden auf der Datenbank gespeichert: " & j & "/" & dt.Rows.Count - 1
                        Form_Psb.PsbStatusText.Text = "Neue Ist + Obligo Daten werden auf der Datenbank gespeichert: " & j & "/" & dt.Rows.Count - 1
                        Form_Edit.StatusStrip.Refresh()
                        Form_Psb.StatusStrip.Refresh()
                        progress = progress + 50
                    End If
                    Add_IstData(dt.Rows(j).Item("qryEntwurfstyp"),
                                    dt.Rows(j).Item("qryArbeitspaket"),
                                    dt.Rows(j).Item("qryVersion"),
                                    dt.Rows(j).Item("qryGeschäftsjahr"),
                                    dt.Rows(j).Item("qryKostenart"),
                                    dt.Rows(j).Item("txtBeschreibung"),
                                    dt.Rows(j).Item("txtBemerkung"),
                                    dt.Rows(j).Item("txtZusatzfeld"),
                                    dt.Rows(j).Item("curKosten_pro_Einheit"),
                                    False,
                                    Now,
                                    dt.Rows(j).Item("txtBedarfsnummer"),
                                    dt.Rows(j).Item("dblJanuar"),
                                    dt.Rows(j).Item("dblFebruar"),
                                    dt.Rows(j).Item("dblMärz"),
                                    dt.Rows(j).Item("dblApril"),
                                    dt.Rows(j).Item("dblMai"),
                                    dt.Rows(j).Item("dblJuni"),
                                    dt.Rows(j).Item("dblJuli"),
                                    dt.Rows(j).Item("dblAugust"),
                                    dt.Rows(j).Item("dblSeptember"),
                                    dt.Rows(j).Item("dblOktober"),
                                    dt.Rows(j).Item("dblNovember"),
                                    dt.Rows(j).Item("dblDezember"),
                                    dt.Rows(j).Item("qryKlassifizierung"),
                                    dt.Rows(j).Item("qryEntwicklungspaket"),
                                    dt.Rows(j).Item("qryPaket"),
                                    dt.Rows(j).Item("boolObligo"),
                                    dt.Rows(j).Item("txtKst_Lst"),
                                    dt.Rows(j).Item("lngSort"),
                                    dt.Rows(j).Item("IdSap"))
                End If
            Next
        End If
#End Region

        'MenuShowSapData.Enabled = True
        Form_Edit.ToolStripStatusOperation.Text = "Daten erfolgreich geladen und gespeichert"
        Form_Psb.PsbStatusText.Text = "Daten erfolgreich geladen und gespeichert"
        Form_Edit.StatusStrip.Refresh()
        Form_Psb.StatusStrip.Refresh()
        Form_Edit.TimerEditForm.Interval = 5000 '1000 = 1 sec
        Form_Edit.TimerEditForm.Start()
        Form_Psb.Timer.Interval = 5000 '1000 = 1 sec
        Form_Psb.Timer.Start()

        Return True
    End Function

    Function GetSapObligo(GJ As Integer, Objekt As String, dtObject As System.Data.DataTable, dtStructure As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: GetSapObligo")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object
        Dim CDatum As String, CDouble As String
        Dim curPsp As String, curAp As String, curObject As String
        Dim dt As New System.Data.DataTable

        Try
            dt = dtStructure.Clone

            boolGotData = False

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll

            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_COOI" 'Tabellenname
            DELIMITER.Value = "|"

            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "REFBN"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "RFPOS"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "LIFNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "OBJNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "GJAHR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(6, "FIELDNAME") = "SAKTO" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(7, "FIELDNAME") = "LTXT" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(8, "FIELDNAME") = "PERIO" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(9, "FIELDNAME") = "MATNR" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(10, "FIELDNAME") = "SGTXT" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(11, "FIELDNAME") = "MEGBTR" 'offene Menge
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(12, "FIELDNAME") = "MEINH" '
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(13, "FIELDNAME") = "WKGBTR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(14, "FIELDNAME") = "REFBT"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1
            OPTIONS.AppendRow
            OPTIONS(intRow, "TEXT") = "OBJNR = '" & Objekt & "' AND GJAHR = '" & GJ & "'"
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEban_Kn an

                    Dim strSapData As String
                    Dim progress As Integer = 1
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("Bestellung") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("Bestellung").ToString.Length = 10
                                    dt.Rows(K).Item("Bestellung") = "0" & dt.Rows(K).Item("Bestellung")
                                Loop
                            ElseIf J = 1 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("BestellPosition") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("BestellPosition").ToString.Length = 5
                                    dt.Rows(K).Item("BestellPosition") = "0" & dt.Rows(K).Item("BestellPosition")
                                Loop
                            ElseIf J = 2 Then
                                'dt.Rows(K).Item("Firma") = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(K).Item("Object") = Trim$(arHelper(J))
                            ElseIf J = 4 Then
                                dt.Rows(K).Item("qryGeschäftsjahr") = CInt(Trim$(arHelper(J)))
                            ElseIf J = 5 Then
                                Dim Sakto As String = Trim$(arHelper(J))
                                dt.Rows(K).Item("txtKst_Lst") = Right(Sakto, 6)
                            ElseIf J = 6 Then
                                dt.Rows(K).Item("Leistungsart") = Trim$(arHelper(J))
                            ElseIf J = 7 Then
                                dt.Rows(K).Item("lngSort") = CInt(Trim$(arHelper(J)))
                            ElseIf J = 8 Then
                                dt.Rows(K).Item("Materialnummer") = Trim$(arHelper(J))
                            ElseIf J = 9 Then
                                dt.Rows(K).Item("Bestelltext") = Trim$(arHelper(J))
                            ElseIf J = 10 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(K).Item("Menge") = CDbl(CDouble)
                            ElseIf J = 11 Then
                                dt.Rows(K).Item("MengenEinheit") = Trim$(arHelper(J))
                            ElseIf J = 12 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(K).Item("Gesamtwert") = CDbl(CDouble)
                            ElseIf J = 13 Then
                                dt.Rows(K).Item("Bestellart") = Trim$(arHelper(J))
                                If dt.Rows(K).Item("Bestellart") = "010" Then
                                    dt.Rows(K).Item("Bestellart") = "BANF"
                                ElseIf dt.Rows(K).Item("Bestellart") = "020" Then
                                    dt.Rows(K).Item("Bestellart") = "BEST"
                                Else
                                    dt.Rows(K).Item("Bestellart") = ""
                                End If
                            End If
                        Next J
                        dt.Rows(K).Item("boolObligo") = True

#Region "BANF und BEST Liste"
                        If dt.Rows(K).Item("Bestellart") = "BANF" Then
                            If IsNothing(BANF) Then
                                ReDim BANF(0)
                                BANF(0) = dt.Rows(K).Item("Bestellung")
                            Else
                                ReDim Preserve BANF(UBound(BANF) + 1)
                                BANF(UBound(BANF)) = dt.Rows(K).Item("Bestellung")
                            End If
                        ElseIf dt.Rows(K).Item("Bestellart") = "BEST" Then
                            If IsNothing(BEST) Then
                                ReDim BEST(0)
                                BEST(0) = dt.Rows(K).Item("Bestellung")
                            Else
                                ReDim Preserve BEST(UBound(BEST) + 1)
                                BEST(UBound(BEST)) = dt.Rows(K).Item("Bestellung")
                            End If
                        End If
#End Region

#Region "PSP"
                        If Not dt.Rows(K).Item("Object") = curObject Then
                            Dim foundObject() As Data.DataRow
                            foundObject = dtObject.Select("Object = '" & dt.Rows(K).Item("Object") & "'")
                            If Not IsNothing(foundObject) Then 'Es gibt mindestens einen Treffer.
                                For J = 0 To foundObject.Count - 1
                                    dt.Rows(K).Item("PSP") = foundObject(J).Item("PSP")
                                    dt.Rows(K).Item("qryArbeitspaket") = foundObject(J).Item("ID_AP")
                                    curObject = foundObject(J).Item("Object")
                                    curPsp = foundObject(J).Item("PSP")
                                    curAp = foundObject(J).Item("ID_AP")
                                    Exit For
                                Next
                            End If
                        Else
                            dt.Rows(K).Item("PSP") = curPsp
                            dt.Rows(K).Item("qryArbeitspaket") = curAp
                        End If
#End Region

                        K = K + 1
                    Next I

                    dt = ConvertTotblData(dt)

                    Return dt
                Else
                    boolGotData = False
                    Return dt
                End If
            End If
#End Region

        Catch ex As Exception
            MsgBox("GetSapObligo: " & I & " " & J & " " & ex.Message.ToString)
        End Try
    End Function

    Function GetSapIst(GJ As Integer, Objekt As String, dtObject As System.Data.DataTable, dtStructure As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: GetSapIst")

        Dim I As Long, J As Long, K As Long, L As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object
        Dim CDatum As String, CDouble As String
        Dim curPsp As String, curAp As String, curObject As String
        Dim dt As New System.Data.DataTable

        Try
            dt = dtStructure.Clone

            boolGotData = False

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll

            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_COEP" 'Tabellenname
            DELIMITER.Value = "|"

            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "BELNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "PERIO"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "WKGBTR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "MBGBTR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "OBJNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(6, "FIELDNAME") = "GJAHR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(7, "FIELDNAME") = "KSTAR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(8, "FIELDNAME") = "LTEXT"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(9, "FIELDNAME") = "VRGNG"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(10, "FIELDNAME") = "PAROB"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(11, "FIELDNAME") = "USPOB"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(12, "FIELDNAME") = "BEKNZ"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(13, "FIELDNAME") = "MEINH"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(14, "FIELDNAME") = "SGTXT"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(15, "FIELDNAME") = "MATNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(16, "FIELDNAME") = "EBELN"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(17, "FIELDNAME") = "EBELP"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(18, "FIELDNAME") = "PERNR"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1
            OPTIONS.AppendRow
            OPTIONS(intRow, "TEXT") = "OBJNR = '" & Objekt & "' AND GJAHR = '" & GJ & "'"
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEban_Kn an

                    Dim strSapData As String
                    Dim progress As Integer = 1
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 Then
                                dt.Rows(K).Item("Belegnummer") = Trim$(arHelper(J))
                            ElseIf J = 1 Then
                                dt.Rows(K).Item("lngSort") = CInt(Trim$(arHelper(J)))
                            ElseIf J = 2 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(K).Item("Gesamtwert") = CDbl(CDouble)
                            ElseIf J = 3 Then
                                CDouble = Replace(Trim$(arHelper(J)), ".", ",")
                                dt.Rows(K).Item("Menge") = CDbl(CDouble)
                            ElseIf J = 4 Then
                                dt.Rows(K).Item("Object") = Trim$(arHelper(J))
                            ElseIf J = 5 Then
                                dt.Rows(K).Item("qryGeschäftsjahr") = CInt(Trim$(arHelper(J)))
                            ElseIf J = 6 Then
                                Dim Sakto As String = Trim$(arHelper(J))
                                dt.Rows(K).Item("txtKst_Lst") = Right(Sakto, 6)
                            ElseIf J = 7 Then
                                dt.Rows(K).Item("Leistungsart") = Trim$(arHelper(J))
                            ElseIf J = 8 Then
                                dt.Rows(K).Item("Vorgang") = Trim$(arHelper(J))
                            ElseIf J = 9 Then
                                dt.Rows(K).Item("txtBedarfsnummer") = Trim$(arHelper(J))
                            ElseIf J = 10 Then
                                dt.Rows(K).Item("Kostenstelle2") = Trim$(arHelper(J)) '
                            ElseIf J = 11 Then
                                'dt.Rows(K).Item("lngSort") = CInt(Trim$(arHelper(J)))
                            ElseIf J = 12 Then
                                dt.Rows(K).Item("MengenEinheit") = Trim$(arHelper(J))
                            ElseIf J = 13 Then
                                dt.Rows(K).Item("Bestelltext") = Trim$(arHelper(J))
                                'dt.Rows(K).Item("Firma") = Trim$(arHelper(J))
                            ElseIf J = 14 Then
                                dt.Rows(K).Item("Materialnummer") = Trim$(arHelper(J))
                            ElseIf J = 15 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("Bestellung") = Trim$(arHelper(J))
                                dt.Rows(K).Item("Bestellart") = "BEST"
                                Do Until dt.Rows(K).Item("Bestellung").ToString.Length = 10
                                    dt.Rows(K).Item("Bestellung") = "0" & dt.Rows(K).Item("Bestellung")
                                Loop
                            ElseIf J = 16 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("BestellPosition") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("BestellPosition").ToString.Length = 5
                                    dt.Rows(K).Item("BestellPosition") = "0" & dt.Rows(K).Item("BestellPosition")
                                Loop
                            ElseIf J = 17 Then
                                dt.Rows(K).Item("PNummer") = Trim$(arHelper(J))
                            End If
                        Next J
                        dt.Rows(K).Item("boolObligo") = False

#Region "BANF und BEST Liste"
                        If dt.Rows(K).Item("Bestellart") = "BANF" Then
                            If IsNothing(BANF) Then
                                ReDim BANF(0)
                                BANF(0) = dt.Rows(K).Item("Bestellung")
                            Else
                                ReDim Preserve BANF(UBound(BANF) + 1)
                                BANF(UBound(BANF)) = dt.Rows(K).Item("Bestellung")
                            End If
                        ElseIf dt.Rows(K).Item("Bestellart") = "BEST" Then
                            If IsNothing(BEST) Then
                                ReDim BEST(0)
                                BEST(0) = dt.Rows(K).Item("Bestellung")
                            Else
                                ReDim Preserve BEST(UBound(BEST) + 1)
                                BEST(UBound(BEST)) = dt.Rows(K).Item("Bestellung")
                            End If
                        End If
#End Region

#Region "PSP"
                        If Not dt.Rows(K).Item("Object") = curObject Then
                            Dim foundObject() As Data.DataRow
                            foundObject = dtObject.Select("Object = '" & dt.Rows(K).Item("Object") & "'")
                            If Not IsNothing(foundObject) Then 'Es gibt mindestens einen Treffer.
                                For J = 0 To foundObject.Count - 1
                                    dt.Rows(K).Item("PSP") = foundObject(J).Item("PSP")
                                    dt.Rows(K).Item("qryArbeitspaket") = foundObject(J).Item("ID_AP")
                                    curObject = foundObject(J).Item("Object")
                                    curPsp = foundObject(J).Item("PSP")
                                    curAp = foundObject(J).Item("ID_AP")
                                    Exit For
                                Next
                            End If
                        Else
                            dt.Rows(K).Item("PSP") = curPsp
                            dt.Rows(K).Item("qryArbeitspaket") = curAp
                        End If
#End Region

                        If Not String.IsNullOrEmpty(dt.Rows(K).Item("Bestellung")) Then
                            dt.Rows(K).Item("Bestellart") = "BEST"
                        End If

#Region "Kostenstelle"
                        Dim Kst As String
                        If Not String.IsNullOrEmpty(dt.Rows(K).Item("Kostenstelle2")) Then
                            dt.Rows(K).Item("Kostenstelle2") = Get_KstOfObjekt(dt.Rows(K).Item("Kostenstelle2").ToString)
                            dt.Rows(K).Item("txtBedarfsnummer") = dt.Rows(K).Item("Kostenstelle2").ToString
                        ElseIf Not String.IsNullOrEmpty(dt.Rows(K).Item("txtBedarfsnummer")) Then
                            dt.Rows(K).Item("txtBedarfsnummer") = Get_KstOfObjekt(dt.Rows(K).Item("txtBedarfsnummer").ToString)
                        End If
#End Region

                        K = K + 1
                    Next I

                    dt = ConvertTotblData(dt)

                    Return dt
                Else
                    boolGotData = False
                    Return dt
                End If
            End If
#End Region

        Catch ex As Exception
            MsgBox("GetSapIst: " & ex.Message.ToString)
        End Try
    End Function

    Private Function Get_BestBednr_V2(dtStructure As System.Data.DataTable, start As Integer, ende As Integer, BEST() As String) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_BestBednr")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object, MAXROWS As Object

        Dim dt As New System.Data.DataTable
        dt = dtStructure.Clone

        Try
            boolGotData = False

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll
            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_EKET_PO" 'Tabellenname
            DELIMITER.Value = "|"
            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "EBELN"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "EBELP"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "MATNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "BEDNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "TXZ01"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(6, "FIELDNAME") = "NAME1"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1

            For K = 0 To UBound(BEST) 'Geht die Länge des Arrays durch
                OPTIONS.AppendRow
                OPTIONS(intRow, "TEXT") = "EBELN = '" & BEST(K) & "' OR"
                intRow = intRow + 1
            Next K

            rowCount = OPTIONS.rowCount
            lastString = OPTIONS(rowCount, "TEXT")
            length = Len(lastString)

            If (length > 0) Then
                OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
            End If
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEket_Po an

                    Dim strSapData As String
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("Bestellung") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("Bestellung").ToString.Length = 10
                                    dt.Rows(K).Item("Bestellung") = "0" & dt.Rows(K).Item("Bestellung")
                                Loop
                            ElseIf J = 1 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("BestellPosition") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("BestellPosition").ToString.Length = 5
                                    dt.Rows(K).Item("BestellPosition") = "0" & dt.Rows(K).Item("BestellPosition")
                                Loop
                            ElseIf J = 2 Then
                                dt.Rows(K).Item("Materialnummer") = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(K).Item("Bedarfsnummer") = Trim$(arHelper(J))
                            ElseIf J = 4 Then
                                dt.Rows(K).Item("Text") = Trim$(arHelper(J))
                            ElseIf J = 5 Then
                                dt.Rows(K).Item("Lieferant") = Trim$(arHelper(J))
                            End If
                        Next J
                        K = K + 1
                    Next I
                Else
                    boolGotData = False
                End If
            End If
#End Region
            Return dt
        Catch ex As Exception
            MsgBox("Get_BestBednr: " & ex.Message.ToString)
            Return dt
        End Try

    End Function

    Private Function Get_BanfBednr_V2(dtStructure As System.Data.DataTable, start As Integer, ende As Integer, BANF() As String) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_BestBednr")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object, MAXROWS As Object

        Dim dt As New System.Data.DataTable
        dt = dtStructure.Clone

        Try
            boolGotData = False

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll
            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_EBAN_KN" 'Tabellenname
            DELIMITER.Value = "|"
            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "BANFN"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "BNFPO"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "MATNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "BEDNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "TXZ01"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1

            For K = 0 To UBound(BANF) 'Geht die Länge des Arrays durch
                OPTIONS.AppendRow
                OPTIONS(intRow, "TEXT") = "BANFN = '" & BANF(K) & "' OR" 'alle PSP Daten ziehen
                intRow = intRow + 1
            Next K

            rowCount = OPTIONS.rowCount
            lastString = OPTIONS(rowCount, "TEXT")
            length = Len(lastString)

            If (length > 0) Then
                OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
            End If
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEket_Po an

                    Dim strSapData As String
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("Bestellung") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("Bestellung").ToString.Length = 10
                                    dt.Rows(K).Item("Bestellung") = "0" & dt.Rows(K).Item("Bestellung")
                                Loop
                            ElseIf J = 1 AndAlso Not String.IsNullOrEmpty(Trim$(arHelper(J))) Then
                                dt.Rows(K).Item("BestellPosition") = Trim$(arHelper(J))
                                Do Until dt.Rows(K).Item("BestellPosition").ToString.Length = 5
                                    dt.Rows(K).Item("BestellPosition") = "0" & dt.Rows(K).Item("BestellPosition")
                                Loop
                            ElseIf J = 2 Then
                                dt.Rows(K).Item("Materialnummer") = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(K).Item("Bedarfsnummer") = Trim$(arHelper(J))
                            ElseIf J = 4 Then
                                dt.Rows(K).Item("Text") = Trim$(arHelper(J))
                            End If
                        Next J
                        K = K + 1
                    Next I
                Else
                    boolGotData = False
                End If
            End If
#End Region
            Return dt
        Catch ex As Exception
            MsgBox("Get_BanfBednr: " & ex.Message.ToString)
            Return dt
        End Try

    End Function

    Private Function Get_Object(PSP() As String, dtStructure As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_Object")

        Dim I As Long, J As Long, K As Long
        Dim intRow As Long
        Dim rowCount As Long, lastString As String, length As Integer, boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object, MAXROWS As Object

        Dim dt As New System.Data.DataTable
        dt = dtStructure.Clone

        Try
            boolGotData = False

            Form_Edit.ToolStripStatusOperation.Text = "SAP Objektstrukturen werden heruntergeladen. Dieser Vorgang kann einige Minuten dauern..."
            Form_Psb.PsbStatusText.Text = "SAP Objektstrukturen werden heruntergeladen. Dieser Vorgang kann einige Minuten dauern..."
            Form_Edit.StatusStrip.Refresh()
            Form_Psb.StatusStrip.Refresh()

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll
            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_PRPS" 'Tabellenname
            DELIMITER.Value = "|"
            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "POSID"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "OBJNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "POST1"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "VERNA"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(5, "FIELDNAME") = "VERNR"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1

            For K = 0 To UBound(PSP) 'Geht die Länge des Arrays durch
                OPTIONS.AppendRow
                OPTIONS(intRow, "TEXT") = "POSID = '" & PSP(K) & "' OR"
                intRow = intRow + 1
            Next K

            rowCount = OPTIONS.rowCount
            lastString = OPTIONS(rowCount, "TEXT")
            length = Len(lastString)

            If (length > 0) Then
                OPTIONS(rowCount, "TEXT") = Left(lastString, length - 3)
            End If
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEket_Po an

                    Dim strSapData As String
                    Dim progress As Integer = 1
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch
                        If progress = I Then
                            Form_Edit.ToolStripStatusOperation.Text = "Objektstrukturen aufbauen: " & I & "/" & SAP_DATA_TABLE.rowCount
                            Form_Psb.PsbStatusText.Text = "Objektstrukturen aufbauen: " & I & "/" & SAP_DATA_TABLE.rowCount
                            Form_Edit.StatusStrip.Refresh()
                            Form_Psb.StatusStrip.Refresh()
                            progress = progress + 50
                        End If

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 Then
                                dt.Rows(K).Item("PSP") = Trim$(arHelper(J))
                            ElseIf J = 1 Then
                                dt.Rows(K).Item("Object") = Trim$(arHelper(J))
                            ElseIf J = 2 Then
                                dt.Rows(K).Item("Arbeitspaket") = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(K).Item("Name") = Trim$(arHelper(J))
                            ElseIf J = 4 Then
                                dt.Rows(K).Item("PNummer") = Trim$(arHelper(J))
                            End If
                        Next J

                        Dim Abteilung As String = Mid(dt.Rows(K).Item("Name").ToString, InStr(1, dt.Rows(K).Item("Name").ToString, "/") + 1, Len(dt.Rows(K).Item("Name").ToString))
                        Abteilung = Replace(Abteilung, " ", "") 'Leerzeichen entfernen
                        dt.Rows(K).Item("Abteilung") = Abteilung
                        Dim Name As String = Mid(dt.Rows(K).Item("Name").ToString, 1, InStr(1, dt.Rows(K).Item("Name").ToString, "/") - 1)
                        Name = Replace(Name, " ", "") 'Leerzeichen entfernen
                        dt.Rows(K).Item("Name") = Name

                        K = K + 1
                    Next I
#Region "ID_AP ergänzen"
                    Dim curAp As String, curPsp As String
                    Dim dtAp As New System.Data.DataTable
                    dtAp = Get_tblAlleArbeitspakete(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True)
                    If Not dtAp.Rows.Count = 0 AndAlso Not dt.Rows.Count = 0 Then
                        For K = 0 To dt.Rows.Count - 1
                            If Not curPsp = dt.Rows(K).Item("PSP") Then

                                Dim foundRows() As Data.DataRow = dtAp.Select("txtPSP_Element = '" & dt.Rows(K).Item("PSP") & "'")
                                Dim RowIndex As Integer = -1
                                If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                                    RowIndex = dtAp.Rows.IndexOf(foundRows(0))
                                    curPsp = dt.Rows(K).Item("PSP")
                                    curAp = dtAp.Rows(RowIndex).Item("ID_AP")
                                    dt.Rows(K).Item("ID_AP") = curAp
                                End If
                            Else
                                dt.Rows(K).Item("ID_AP") = curAp
                            End If
                        Next K
                    End If
#End Region
                Else
                    boolGotData = False
                End If
            End If
#End Region
            Return dt
        Catch ex As Exception
            MsgBox("Get_Object: " & ex.Message.ToString)
            Return dt
        End Try

    End Function

    Function Get_Auftrag(row As Integer, dtObject As System.Data.DataTable, dtStructure As System.Data.DataTable) As System.Data.DataTable
        Console.WriteLine("FUNC: GetSapIst")

        Dim I As Long, J As Long, K As Long, L As Long
        Dim intRow As Long
        Dim boolGotData As Boolean
        Dim RFC_READ_TABLE As Object, QUERY_TAB As Object, DELIMITER As Object, OPTIONS As Object, SAP_DATA_TABLE As Object, SAP_FIELD_TABLE As Object
        Dim dt As New System.Data.DataTable
        Dim curPSPEL As String, curIK As String, curName As String, curAbteilung As String

        Try
            dt = dtStructure.Clone

            boolGotData = False

#Region "Verbindungsdaten setzen"
            funccontrol.Connection = conn
            If funccontrol.Count > 0 Then funccontrol.RemoveAll

            '--- Einstellungen für das Lesen der Tabellen
            RFC_READ_TABLE = funccontrol.Add("Y_CAA4019_RFC_READ_TABLE")
            QUERY_TAB = RFC_READ_TABLE.exports("QUERY_TABLE")
            DELIMITER = RFC_READ_TABLE.exports("DELIMITER")
            OPTIONS = RFC_READ_TABLE.Tables("OPTIONS")
            SAP_DATA_TABLE = RFC_READ_TABLE.Tables("DATA")
            SAP_FIELD_TABLE = RFC_READ_TABLE.Tables("FIELDS")

            '--- Tabelle aus welcher die Daten gelesen werden sollen -----
            QUERY_TAB.Value = "YCAA4019_AUFK" 'Tabellenname
            DELIMITER.Value = "|"

            '--- Ausgabe: welche Tabelleninformationen werden ausgegeben
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(1, "FIELDNAME") = "AUFNR"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(2, "FIELDNAME") = "KTEXT"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(3, "FIELDNAME") = "PSPEL"
            SAP_FIELD_TABLE.AppendRow
            SAP_FIELD_TABLE(4, "FIELDNAME") = "OBJNR"
#End Region

#Region "Suchfilter: Gibt nur die Werte der Tabelle aus, welche den folgenden Kriterien entsprechen"
            intRow = 1
            OPTIONS.AppendRow
            OPTIONS(intRow, "TEXT") = "PSPEL = '" & dtObject.Rows(row).Item("Object").ToString & "'"
#End Region

#Region "Datenausgabe"
            If RFC_READ_TABLE.Call = True Then 'Es existieren Daten in der Tabelle
                If SAP_DATA_TABLE.rowCount > 0 Then
                    boolGotData = True

                    K = 0 'K gibt den höchsten Index von listEban_Kn an

                    Dim strSapData As String
                    Dim progress As Integer = 1
                    For I = 1 To SAP_DATA_TABLE.rowCount 'Geht die komplette SAP Fund-Tabelle durch

                        strSapData = SAP_DATA_TABLE(I, 1).ToString
                        Dim arHelper() As String = strSapData.Split("|")

                        dt.Rows.Add()
                        For J = 0 To UBound(arHelper) 'Schreibt alle Werte in das SAP Array
                            If J = 0 Then
                                dt.Rows(K).Item("Auftrag") = Trim$(arHelper(J))
                            ElseIf J = 1 Then
                                dt.Rows(K).Item("Text") = Trim$(arHelper(J))
                            ElseIf J = 2 Then
                                dt.Rows(K).Item("PSPEL") = Trim$(arHelper(J))
                            ElseIf J = 3 Then
                                dt.Rows(K).Item("Object") = Trim$(arHelper(J))
                            End If
                        Next J

                        K = K + 1
                    Next I

                    For I = 0 To dt.Rows.Count - 1
                        If Not dt.Rows(I).Item("PSPEL") = curPSPEL Then
                            Dim foundIK() As Data.DataRow
                            foundIK = dtObject.Select("Object like '*" & dt.Rows(I).Item("PSPEL") & "*'")
                            If foundIK.Count > 0 Then 'Es gibt mindestens einen Treffer.
                                dt.Rows(I).Item("IK") = foundIK(0).Item("PSP")
                                dt.Rows(I).Item("Name") = foundIK(0).Item("Name")
                                dt.Rows(I).Item("Abteilung") = foundIK(0).Item("Abteilung")
                                curPSPEL = dt.Rows(I).Item("PSPEL")
                                curAbteilung = dt.Rows(I).Item("Abteilung")
                                curName = dt.Rows(I).Item("Name")
                                curIK = foundIK(0).Item("PSP")
                            End If
                        Else
                            dt.Rows(I).Item("IK") = curIK
                            dt.Rows(I).Item("Abteilung") = curAbteilung
                            dt.Rows(I).Item("Name") = curName
                        End If
                    Next I

                    Return dt
                Else
                    boolGotData = False
                    Return dt
                End If
            End If
#End Region

        Catch ex As Exception
            MsgBox("GetSapIst: " & ex.Message.ToString)
        End Try
    End Function

    Function Get_KstOfObjekt(input As String) As String
        Dim i As Long
        For i = Len(input) To 0 Step -1
            If Len(input) = 0 Then
                Return ""
            ElseIf Not IsNumeric(input.Substring(Len(input) - 1, 1)) Then
                input = Left(input, Len(input) - 1)
            Else
                input = Right(input, 4)
                Return input
            End If
        Next
    End Function
#End Region



End Module
