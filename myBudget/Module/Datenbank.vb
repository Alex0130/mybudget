﻿Option Explicit On
Imports ADODB
Imports System.Data.OleDb
Imports System.Net.NetworkInformation


Module Datenbank

    'Datumformat für SQL Server language 'english' eingestellt. Hat der Zielserver eine Deutsche Spracheinstellung muss das Datumsformat angepasst werden.
#Region "Connection"
    Function Connect_DB(IndexCon As Integer) As Boolean 'Stellt die Verbindung zur Access Datenbank her - OK
        Console.WriteLine("FUNC: Function Connect_DB")

        Dim strConnect As String

        If boolConnected = True Then
            Exit Function
        End If

        strConnect = listConnections(IndexCon).Path.ToString
        '-
        Try
            con = New OleDb.OleDbConnection(strConnect)
            con.Open()

            boolConnected = True
            boolError = False

            AddHandler NetworkChange.NetworkAvailabilityChanged, AddressOf OnLanStatusChanged 'Event wenn Netzwerk- oder WLAN Verbindung verloren geht

            Return True
        Catch
            boolConnected = False
            boolError = True
            Return False
        End Try
    End Function

    Function Close_DB() 'Schließt die Verbindung zur Access Datenbank - OK
        Console.WriteLine("FUNC: Function Close_DB")

        On Error Resume Next
        con.Close()
        boolConnected = False
    End Function

    Sub OnLanStatusChanged(sender As Object, e As NetworkAvailabilityEventArgs)
        Console.WriteLine("EVENT: Sub OnLanStatusChanged")

        If e.IsAvailable = False Then
            Close_DB()
            MsgBox("Die Serververbindung wurde unterbrochen. Bitte stellen Sie eine Netzwerkverbindung her und verbinden Sie sich manuell neu mit dem Server!", vbCritical, "Lost Server Connection")
        End If

    End Sub
#End Region

#Region "User" 'Auf DataTable umgestellt
    Function Get_tblUser(boolPublish As Boolean) As System.Data.DataTable
        Console.WriteLine("Function Get_tblUser")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from tblUser order by txtName asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            If boolPublish = True Then
                pbl_dtUser = dt_data
            End If

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblUser: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Update_User(txtPNummer As String, qryProjekt As Long, qryArbeitspaket As Long, qryVersion As Long, qryEntwurfstyp As String, qryGj As String) 'OK
        Console.WriteLine("FUNC: Function Update_User")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "update tblUser set " &
        "qryProjekt = " & qryProjekt & ", " &
        "qryArbeitspaket = " & qryArbeitspaket & ", " &
        "qryVersion = " & qryVersion & ", " &
        "qryGj = '" & qryGj & "', " &
        "qryEntwurfstyp = '" & qryEntwurfstyp & "' " &
        "where txtPNummer = '" & txtPNummer & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_User - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_UserPassword(PNummer As String, Password As String) As Boolean
        Console.WriteLine("FUNC: Function Update_UserPassword")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "update tblUser set " &
        "txtPassword = '" & Password & "' " &
        "where txtPNummer = '" & PNummer & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Update_UserPassword - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function
#End Region

#Region "Authorisation" 'Auf DataTable umgestellt
    Function Get_tblAuthorization(boolPublish As Boolean, txtPNummer As String) As System.Data.DataTable
        Console.WriteLine("Function Get_tblAuthorization")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "SQL Statement"
            qry = "SELECT *" _
            & " from tblAuthorisierung" _
            & " where tblAuthorisierung.qryPNummer = '" & txtPNummer & "'" _
            & " order by txtTransaktion desc"
#End Region

#Region "Start Command"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData)
#End Region

            If boolPublish = True Then
                pbl_dtAuth = dt_data
            End If

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Function Get_tblAuthorization: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Add_Authorization(txtTransaktion As String, intID As Long, qryPNummer As String, ReadWrite As String) As Boolean
        Console.WriteLine("FUNC: Function Add_Authorization")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblAuthorisierung ([txtTransaktion], intID, [qryPNummer], [ReadWrite]) " &
            "values ('" & txtTransaktion & "', " &
        "" & intID & ", " &
        "'" & qryPNummer & "', " &
        "'" & ReadWrite & "' " &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()

            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Authorization - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Function Delete_UserTransaktion(User As String, txtTransaktion As String) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblAuthorisierung where txtTransaktion = '" & UCase(txtTransaktion) & "' and qryPNummer = '" & User & "' or " &
                "txtTransaktion = '" & LCase(txtTransaktion) & "' and qryPNummer = '" & User & "'"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_UserTransaktion - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab 
        End Try
    End Function

    Function Delete_UserEntry(User As String, txtTransaktion As String, intID As Integer) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblAuthorisierung where (txtTransaktion = '" & UCase(txtTransaktion) & "' or txtTransaktion = '" & LCase(txtTransaktion) & "') and qryPNummer = '" & User & "' and intID = " & intID

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_UserTransaktion - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab
        End Try
    End Function

    Function Delete_ArbeitspaketAuth(ID_AP As Integer) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblAuthorisierung where txtTransaktion = 'AP' and intID = " & ID_AP & " Or txtTransaktion = 'ap' and intID = " & ID_AP & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_VersionArbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False   '!ab 
        End Try
    End Function
#End Region

#Region "Projekt" 'Auf DataTable umgestellt
    Function Get_tblProjektViaSql(boolPublish As Boolean, field As String, Sql As String, OnlyActiveProjects As Boolean) As System.Data.DataTable
        Console.WriteLine("Function Get_tblProjektViaSql")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "SQL Statement"
            If OnlyActiveProjects = True Then
                Sql = Sql & " and (datProjektende >= '" & Now.ToString("yyyyMMdd") & "')"   '!ab mod.
            End If
            qry = "SELECT " & field & " from tblProjekt" & Sql & " order by txtProjekt asc"
#End Region

#Region "Start Command"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData)
#End Region

            If boolPublish = True Then
                pbl_dtProjekt = dt_data
            End If

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblProjektViaSql: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_tblAlleProjekte(qryPNummer As String, dtAuth As System.Data.DataTable, OnlyActiveProjects As Boolean) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblAlleProjekte")

        Dim qry As String, activeProjects As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            If OnlyActiveProjects = True Then
                activeProjects = " and (datProjektende >= '" & Now.ToString("yyyyMMdd") & "')"    '!ab mod.
            Else
                activeProjects = ""
            End If

            Dim whereID_PROJEKT As String = SqlWhereFieldEquals("ID_PROJEKT", pbl_dtUser(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)

            qry = "select * from tblProjekt where " & whereID_PROJEKT & activeProjects & " order by txtProjekt asc"

            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblAlleProjekte: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Add_Projekt(txtProjekt As String, datProjektstart As Date, intProjektlaufzeit As Long) As Integer
        Console.WriteLine("FUNC: Function Add_Projekt")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Dim ProjektEnde As Date
        Dim JahrProjektEnde As Integer = Year(datProjektstart) + intProjektlaufzeit - 1 'Start Geschäftsjahr gleich Jahr Nr. 1
        ProjektEnde = JahrProjektEnde & ".12.31" '31.12.EndeJahr

        Try
            statement = "insert into tblProjekt ([txtProjekt], [datProjektstart], intProjektlaufzeit, [datProjektende]) " &
            "values ('" & txtProjekt & "', " &
        "'" & datProjektstart.ToString("yyyyMMdd HH:mm:ss") & "', " & '!ab mod.
        "" & intProjektlaufzeit & ", " &
        "'" & ProjektEnde.ToString("yyyyMMdd HH:mm:ss") & "'" & '!ab mod.
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()

            'Get ID of new added row
            statement = "select max(ID_PROJEKT) from tblProjekt"
            cmd.CommandText = statement
            Dim id As Integer = cmd.ExecuteScalar()
            Return id

            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Projekt - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Get_LastestIpProjekt() As Integer
        Console.WriteLine("FUNC: Function Get_LastestIpProjekt")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            qry = "select max(ID_PROJEKT) from tblProjekt"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
            cmd.CommandText = qry
            Dim id As Integer = cmd.ExecuteScalar()
            Return id
        Catch ex As Exception
            MsgBox("Get_LastestIpProjekt: " & ex.Message.ToString)
            Return -1
        End Try
    End Function
#End Region

#Region "Kostenart"
    Function Get_Kostenart(boolPublish As Boolean) As tableKostenart() 'OK
        Console.WriteLine("FUNC: Function Get_Kostenart")

        Dim qry As String
        Dim i As Integer
        Dim listKostenart() As tableKostenart

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
            & " from tblKostenart order by intReihenfolge asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listKostenart(i) 'Arraylänge anpassen
            listKostenart(i).txtKostenart = reader("txtKostenart").ToString
            listKostenart(i).txtBeschreibung = reader("txtBeschreibung").ToString
            listKostenart(i).intReihenfolge = CInt(reader("intReihenfolge").ToString)
            i = i + 1
        Loop
        Get_Kostenart = listKostenart

        If boolPublish = True Then
            Erase pbl_KstArt
            pbl_KstArt = listKostenart
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function
#End Region

#Region "Entwurfstyp"
    Function Get_Entwurfstyp(boolPublish As Boolean) As tableEntwurfstyp() 'OK
        Console.WriteLine("FUNC: Function Get_Entwurfstyp")

        Dim qry As String
        Dim i As Integer
        Dim listEntwurfstyp As tableEntwurfstyp()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
            & " from tblEntwurfstyp order by intReihenfolge asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listEntwurfstyp(i) 'Arraylänge anpassen
            listEntwurfstyp(i).txtEntwurfstyp = reader("txtEntwurfstyp").ToString
            listEntwurfstyp(i).intReihenfolge = CInt(reader("intReihenfolge").ToString)
            listEntwurfstyp(i).DatenHerkunft = reader("DatenHerkunft").ToString
            listEntwurfstyp(i).ReportBeschreibung = reader("ReportBeschreibung").ToString
            i = i + 1
        Loop
        Get_Entwurfstyp = listEntwurfstyp

        If boolPublish = True Then
            Erase pbl_Entwurf
            pbl_Entwurf = listEntwurfstyp
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function
#End Region


#Region "Team"
    Function Get_Team(boolPublish As Boolean) As tableTeam() 'OK
        Console.WriteLine("FUNC: Function Get_Team")

        Dim qry As String
        Dim i As Integer
        Dim listTeam As tableTeam()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
            & " from tblTeam"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listTeam(i) 'Arraylänge anpassen
            listTeam(i).txtTeam = reader("txtTeam").ToString
            listTeam(i).txtLeiterFachgebiet = reader("txtLeiterFachgebiet").ToString
            i = i + 1
        Loop
        Get_Team = listTeam

        If boolPublish = True Then
            Erase pbl_Team
            pbl_Team = listTeam
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function
#End Region

#Region "Klassifizierung"
    Function Get_Klassifizierung(boolPublish As Boolean) As tableKlassifizierung() 'OK
        Console.WriteLine("FUNC: Function Get_Klassifizierung")

        Dim qry As String
        Dim i As Integer
        Dim listKlassifizierung As tableKlassifizierung()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
            & " from tblKlassifizierung" _
            & " order by lngWichtigkeit asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listKlassifizierung(i) 'Arraylänge anpassen
            listKlassifizierung(i).txtKlasse = reader("txtKlasse").ToString
            listKlassifizierung(i).lngWichtigkeit = CInt(reader("lngWichtigkeit").ToString)
            i = i + 1
        Loop
        Get_Klassifizierung = listKlassifizierung

        If boolPublish = True Then
            Erase pbl_Klasse
            pbl_Klasse = listKlassifizierung
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function
#End Region

#Region "Entwicklungspaket"
    Function Get_Entwicklungspaket(boolPublish As Boolean, ID_Projekt As Long) As tableEntwicklungspaket() 'OK
        Console.WriteLine("FUNC: Function Get_Entwicklungspaket")

        Dim qry As String
        Dim i As Integer
        Dim listEntwicklungspaket As tableEntwicklungspaket()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblEntwicklungspaket" _
        & " where qryProjekt = " & ID_Projekt & "" _
        & " order by ID asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listEntwicklungspaket(i) 'Arraylänge anpassen
            listEntwicklungspaket(i).txtEntwicklungspaket = reader("txtEntwicklungspaket").ToString
            listEntwicklungspaket(i).txtBeschreibung = reader("txtBeschreibung").ToString
            listEntwicklungspaket(i).lngVorgänger = CInt(reader("lngVorgänger").ToString)
            listEntwicklungspaket(i).qryProjekt = CInt(reader("qryProjekt").ToString)
            i = i + 1
        Loop
        Get_Entwicklungspaket = listEntwicklungspaket

        If boolPublish = True Then
            Erase pbl_Ep
            pbl_Ep = listEntwicklungspaket
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function

    Function Update_Entwicklungspaket(txtEntwicklungspaketNeu As String, txtBeschreibung As String, txtEntwicklungspaketAlt As String, qryProjekt As Long) 'OK
        Console.WriteLine("FUNC: Function Update_Entwicklungspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblEntwicklungspaket set " &
        "txtEntwicklungspaket = '" & txtEntwicklungspaketNeu & "', " &
        "txtBeschreibung = '" & txtBeschreibung & "' " &
        " where txtEntwicklungspaket = '" & txtEntwicklungspaketAlt & "' and qryProjekt = " & qryProjekt & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()

            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Entwicklungspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Add_Entwicklungspaket(txtEntwicklungspaket As String, txtBeschreibung As String, qryProjekt As Long) 'OK
        Console.WriteLine("FUNC: Function Add_Entwicklungspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblEntwicklungspaket ([txtEntwicklungspaket], [txtBeschreibung], qryProjekt) " &
            "values ('" & txtEntwicklungspaket & "', " &
        "'" & txtBeschreibung & "', " &
         qryProjekt & " " &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Entwicklungspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function
#End Region

#Region "Arbeitspaket" 'Auf DataTable umgestellt
    Function Get_tblArbeitspaket(boolPublish As Boolean, ID_Projekt As Long, qryPNummer As String, dtAuth As System.Data.DataTable, GetInactiveData As Boolean) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblArbeitspaket")

        Dim qry As String
        Dim inactive As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            If GetInactiveData = True Then
                inactive = ""
            Else
                inactive = " and (not boolInactive = 1 or boolInactive is NULL)"
            End If
            Dim whereID_AP As String = SqlWhereFieldEquals("ID_AP", pbl_dtUser(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)
            Dim whereID_Projekt As String = SqlWhereFieldEquals("qryProjekt", pbl_dtUser(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)

            qry = "select * from tblArbeitspaket where " & whereID_AP & " and (qryProjekt = " & ID_Projekt & ")" & inactive & " order by txtArbeitspaket asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

#Region "publish DataTable"
            If boolPublish = True Then
                pbl_dtArbeitspaket.Clear()
                pbl_dtArbeitspaket = dt_data
            End If
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblArbeitspaket: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_tblAlleArbeitspakete(qryPNummer As String, dtAuth As System.Data.DataTable, GetInactiveData As Boolean) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblAlleArbeitspakete")

        Dim qry As String, inactive As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            If GetInactiveData = True Then
                inactive = ""
            Else
                inactive = " and (not boolInactive = 1 or boolInactive is NULL)"
            End If
            Dim whereID_AP As String = SqlWhereFieldEquals("ID_AP", pbl_dtUser(intCurrentUser).Item("txtPNummer"), pbl_dtAuth)

            qry = "select * from tblArbeitspaket where " & whereID_AP & inactive & " order by qryProjekt asc, txtArbeitspaket asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblSachkonto: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_LastestIpArbeitspaket() As Integer
        Console.WriteLine("FUNC: Function Get_LastestIpArbeitspaket")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            qry = "select max(ID_AP) from tblArbeitspaket"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
            cmd.CommandText = qry
            Dim id As Integer = cmd.ExecuteScalar()
            Return id
        Catch ex As Exception
            MsgBox("Get_LastestIpArbeitspaket: " & ex.Message.ToString)
            Return -1
        End Try
    End Function

    Function Add_Arbeitspaket(txtArbeitspaket As String, txtPrämissen As String, txtVerantwortlich As String,
                      datErstellungsdatum As Date, ID_Projekt As Long,
                      txtPSP_Element As String, qryTeam As String) As Integer
        Console.WriteLine("FUNC: Function Add_Arbeitspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "insert into tblArbeitspaket ([txtArbeitspaket], [txtPrämissen], [txtVerantwortlich], [datErstellungsdatum], qryProjekt, [txtPSP_Element], [qryTeam], [boolInactive]) " &
            "values ('" & txtArbeitspaket & "', " &
        "'" & txtPrämissen & "', " &
        "'" & txtVerantwortlich & "', " &
        "'" & datErstellungsdatum.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        ID_Projekt & ", " &
        "'" & txtPSP_Element & "', " &
        "'" & qryTeam & "', " &
        "'" & Convert.ToInt32(False) & "'" &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()

            'Get ID of new added row
            statement = "select max(ID_AP) from tblArbeitspaket"
            cmd.CommandText = statement
            Dim id As Integer = cmd.ExecuteScalar()
            Return id

            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_Arbeitspaket(ID_AP As Long, txtArbeitspaket As String, txtPrämissen As String, txtVerantwortlich As String,
                      txtPSP_Element As String, qryTeam As String, boolInactive As Boolean)
        Console.WriteLine("FUNC: Function Update_Arbeitspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblArbeitspaket set " &
        "txtArbeitspaket = '" & txtArbeitspaket & "', " &
        "txtPrämissen = '" & txtPrämissen & "', " &
        "txtVerantwortlich = '" & txtVerantwortlich & "', " &
        "txtPSP_Element = '" & txtPSP_Element & "', " &
        "boolInactive = '" & Convert.ToInt32(boolInactive) & "', " &
        "qryTeam = '" & qryTeam & "' " &
        " where ID_AP = " &
        ID_AP & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_ArbeitspaketVerantw(ID_AP As Long, qryTeam As String, txtVerantwortlich As String, Bezeichnung As String)
        Console.WriteLine("FUNC: Function Update_Arbeitspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblArbeitspaket set " &
        "txtVerantwortlich = '" & txtVerantwortlich & "', " &
        "txtArbeitspaket = '" & Bezeichnung & "', " &
        "qryTeam = '" & qryTeam & "' " &
        " where ID_AP = " &
        ID_AP & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()


            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Delete_Arbeitspaket(ID_AP As Integer) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblArbeitspaket where ID_AP = " & ID_AP & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()

            'Arbeitspaket auch aus der Notification-List entfernen
            'Evetuellen Fehler Ignorieren 
            RemoveAPFromNotifications(ID_AP)    '!ab added

            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_Arbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab
        End Try
    End Function
#End Region

#Region "Version Arbeitspaket"
    Function Get_VersionArbeitspaket(boolPublish As Boolean, ID_AP As Long, qryEntwurfstyp As String) As tableVersionArbeitspaket() 'OK
        Console.WriteLine("FUNC: Function Get_VersionArbeitspaket")

        Dim qry As String
        Dim i As Integer
        Dim listVersion As tableVersionArbeitspaket()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblVersionArbeitspaket" _
        & " where qryID_AP = " & ID_AP & "and qryEntwurfstyp = '" & qryEntwurfstyp & "'" _
        & " order by lngVersion desc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listVersion(i) 'Arraylänge anpassen
            listVersion(i).qryID_AP = CInt(reader("qryID_AP").ToString)
            listVersion(i).lngVersion = CInt(reader("lngVersion").ToString)
            listVersion(i).txtVersionErklärung = reader("txtVersionErklärung").ToString
            listVersion(i).datErstellungsdatum = CDate(reader("datErstellungsdatum").ToString)
            listVersion(i).cbxFreigabe = CBool(reader("cbxFreigabe").ToString)
            listVersion(i).qryEntwurfstyp = reader("qryEntwurfstyp").ToString
            i = i + 1
        Loop
        Get_VersionArbeitspaket = listVersion

        If boolPublish = True Then
            Erase pbl_VersionAp
            pbl_VersionAp = listVersion
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function

    Function Get_tblAlleVersionen() As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblAlleVersionen")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * " &
        "from tblVersionArbeitspaket " &
        "order by qryID_AP asc, qryEntwurfstyp asc, lngVersion desc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblAlleVersionen: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Add_VersionArbeitspaket(ID_AP As Long, lngVersion As Long, txtVersionErklärung As String, qryEntwurfstyp As String) 'OK
        Console.WriteLine("FUNC: Function Add_VersionArbeitspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblVersionArbeitspaket (qryID_AP, lngVersion, [txtVersionErklärung], [datErstellungsdatum], cbxFreigabe, [qryEntwurfstyp]) " &
            "values (" & ID_AP & ", " &
        "" & lngVersion & ", " &
        "'" & txtVersionErklärung & "', " &
        "'" & Now.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        "" & Convert.ToInt32(False) & ", " &
        "'" & qryEntwurfstyp & "' " &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_VersionArbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_VersionArbeitspaket(ID_AP As Long, lngVersion As Long, qryEntwurfstyp As String, cbxFreigabe As Boolean, txtVErsionErklärung As String) 'OK
        Console.WriteLine("FUNC: Function Update_VersionArbeitspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblVersionArbeitspaket set " &
        "cbxFreigabe = " & Convert.ToInt32(cbxFreigabe) & ", " &
        "txtVErsionErklärung = '" & txtVErsionErklärung & "' " &
        "where qryID_AP = " & ID_AP & " and " &
        "lngVersion = " & lngVersion & " and " &
        "qryEntwurfstyp = '" & qryEntwurfstyp & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_VersionArbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Get_LastestApVersion(qryID_AP As Integer, qryEntwurfstyp As String) As Integer
        Console.WriteLine("FUNC: Function Get_LastestIpVersion")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            qry = "select max(lngVersion) from tblVersionArbeitspaket where qryID_AP = " & qryID_AP & " and qryEntwurfstyp = '" & qryEntwurfstyp & "'"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
            cmd.CommandText = qry
            Dim id As Integer = cmd.ExecuteScalar()
            Return id
        Catch ex As Exception
            MsgBox("Get_LastestIpProjekt: " & ex.Message.ToString)
            Return -1
        End Try
    End Function

    Function Delete_VersionArbeitspaket(ID_AP As Integer) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblVersionArbeitspaket where qryID_AP = " & ID_AP & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_VersionArbeitspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab 
        End Try
    End Function
#End Region

#Region "Geschäftsjahr"
    Function Get_GJ(boolPublish As Boolean, ID_Projekt As Long) As tableGJ() 'OK
        Console.WriteLine("FUNC: Function Get_GJ")

        Dim qry As String
        Dim i As Integer
        Dim listGJ As tableGJ()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblGJ" _
        & " where qryProjekt = " & ID_Projekt & "" _
        & " order by intGJ asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listGJ(i) 'Arraylänge anpassen
            listGJ(i).intGJ = CInt(reader("intGJ").ToString)
            listGJ(i).qryProjekt = CInt(reader("qryProjekt").ToString)
            i = i + 1
        Loop
        Get_GJ = listGJ

        If boolPublish = True Then
            Erase pbl_Gj
            pbl_Gj = listGJ
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function

    Function Get_GJforProjekt(whereQry As String) As System.Data.DataTable 'OK
        Console.WriteLine("FUNC: Function Get_GJ")

        Dim qry As String
        Dim i As Integer
        Dim listGJ As tableGJ()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = whereQry
        Dim cmd As New OleDb.OleDbCommand(qry, con)
#Region "Fill Datatable"
        Dim dt_data As New System.Data.DataTable
        Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
        dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

        Return dt_data
        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function

    Function Add_GJ(lngGJ As Long, qryProjekt As Long) 'OK
        Console.WriteLine("FUNC: Function Add_GJ")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblGJ (intGJ, qryProjekt) " &
            "values (" & lngGJ & ", " &
        "" & qryProjekt & " " &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_GJ - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function
#End Region

#Region "tblData"
    Function Get_tblDataStructure() As System.Data.DataTable
        Console.WriteLine("Function Get_tblDataStructure")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "SQL Statement"
            qry = "SELECT * from tblData where ID_DATA = 0"
#End Region

#Region "Start Command"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data = ReadData.GetSchemaTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblData: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_tblDataViaSql(field As String, Sql As String) As System.Data.DataTable
        Console.WriteLine("Function Get_tblDataViaSql")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "SQL Statement"
            qry = "SELECT DISTINCT " & field & " from tblData" & Sql
#End Region

#Region "Start Command"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData)
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblDataViaSql: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_tblData(boolPublish As Boolean, qryArbeitspaket As Long, qryVersion As Long, qryEntwurfstyp As String, GetPlanData As Boolean, listGj() As Integer, listSql() As tableSqlStatement) As System.Data.DataTable 'OK
        Console.WriteLine("FUNC: Function Get_tblData")

        Dim qry As String
        Dim i As Integer

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "SQL Statement"
            Dim FixString As String
            FixString = "qryArbeitspaket = " & qryArbeitspaket & " And qryVersion = " & qryVersion & " And qryEntwurfstyp = '" & qryEntwurfstyp & "'"

            Dim WhereSqlStatement = CreateSqlStatement(FixString, listSql, listGj)

            If GetPlanData = True Then 'Plan Daten lesen
                qry = "SELECT *" _
                        & " from tblData" _
                        & WhereSqlStatement _
                        & " order by lngSort asc, ID_DATA asc" 'So wie die Daten erfasst wurden, werden Sie auch wieder ausgegeben, sprich Entwicklungsumfänge bleiben zusammen
            Else 'Ist Daten lesen
                qry = "SELECT *" _
                        & " from tblIstData" _
                        & WhereSqlStatement _
                        & " order by qryArbeitspaket asc, qryGeschäftsjahr asc, lngSort asc, qryKostenart asc" 'So wie die Daten erfasst wurden, werden Sie auch wieder ausgegeben, sprich Entwicklungsumfänge bleiben zusammen

                Get_Sachkonto(True) 'Holt sich die aktuellen Sachkonto - Kostenart Verknüpfungen
            End If
#End Region

#Region "Start Command"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable

#Region "Extend DataTable"
            'DataTable um weitere Felder erweitern: Kosten pro Jahr, Anzahl pro Jahr, Projekt, Übersetzung von ID's
            dt_data.Columns.Add(New DataColumn("CostsPerYear", GetType(Double)))
            dt_data.Columns.Add(New DataColumn("AmountPerYear", GetType(Double)))

            'Projekt ID des Arbeitspakets
            Dim dtAp As New System.Data.DataTable
            dtAp = Get_tblAlleArbeitspakete(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True) 'Alle möglichen Arbeitspakete lesen

            Dim IndexAp As Integer = -1
            Dim foundRows() As Data.DataRow = dtAp.Select("ID_AP = " & qryArbeitspaket & "")
            If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                IndexAp = dtAp.Rows.IndexOf(foundRows(0))
            End If

            Dim ProjectIndex As Integer
            If Not IndexAp = -1 Then
                Dim foundProjekt() As Data.DataRow = pbl_dtProjekt.Select("ID_PROJEKT = " & dtAp.Rows(IndexAp).Item("qryProjekt") & "")
                If Not IsNothing(foundProjekt) AndAlso foundProjekt.Count > 0 Then 'Es gibt mindestens einen Treffer
                    ProjectIndex = pbl_dtProjekt.Rows.IndexOf(foundProjekt(0))
                End If
            End If

            If Not IndexAp = -1 AndAlso Not ProjectIndex = -1 Then
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "txtArbeitspaket", .DataType = GetType(String), .DefaultValue = dtAp.Rows(IndexAp).Item("txtArbeitspaket")})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PSP", .DataType = GetType(String), .DefaultValue = dtAp.Rows(IndexAp).Item("txtPSP_Element")})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PSP_Verantw", .DataType = GetType(String), .DefaultValue = dtAp.Rows(IndexAp).Item("txtVerantwortlich")})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PROJEKT_ID", .DataType = GetType(Long), .DefaultValue = pbl_dtProjekt.Rows(ProjectIndex).Item("ID_PROJEKT")})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "Projekt", .DataType = GetType(String), .DefaultValue = pbl_dtProjekt.Rows(ProjectIndex).Item("txtProjekt")})
            Else
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "txtArbeitspaket", .DataType = GetType(String), .DefaultValue = ""})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PSP", .DataType = GetType(String), .DefaultValue = ""})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PSP_Verantw", .DataType = GetType(String), .DefaultValue = ""})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "PROJEKT_ID", .DataType = GetType(Long), .DefaultValue = 0})
                dt_data.Columns.Add(New DataColumn() With {.ColumnName = "Projekt", .DataType = GetType(String), .DefaultValue = ""})
            End If

            'Kommentare zu Ist Daten lesen
            Dim dtCom As New System.Data.DataTable
            If GetPlanData = False And Not IndexAp = -1 Then 'Ist Daten um SAP Kommentare anreichern
                dtCom = Get_tblSapComments(dtAp.Rows(IndexAp).Item("txtPSP_Element"))
            End If

            'Berechnung Kosten pro Jahr und Anzahl pro Jahr
            For i = 0 To dt_data.Rows.Count - 1
                dt_data.Rows(i)("AmountPerYear") = dt_data.Rows(i)("dblJanuar") + dt_data.Rows(i)("dblFebruar") + dt_data.Rows(i)("dblMärz") + dt_data.Rows(i)("dblApril") + dt_data.Rows(i)("dblMai") + dt_data.Rows(i)("dblJuni") + dt_data.Rows(i)("dblJuli") + dt_data.Rows(i)("dblAugust") + dt_data.Rows(i)("dblSeptember") + dt_data.Rows(i)("dblOktober") + dt_data.Rows(i)("dblNovember") + dt_data.Rows(i)("dblDezember")
                dt_data.Rows(i)("CostsPerYear") = dt_data.Rows(i)("AmountPerYear") * dt_data.Rows(i)("curKosten_pro_Einheit")

                If GetPlanData = False AndAlso Not IndexAp = -1 Then 'Ist Daten um SAP Kommentare anreichern
                    If Not dtCom.Rows.Count = 0 Then 'Es gibt Kommentare zum PSP Element
                        Dim foundRows2() As Data.DataRow
                        foundRows2 = dtCom.Select("SapId = '" & dt_data.Rows(i).Item("IdSap") & "'")
                        If Not foundRows2.Count = 0 Then 'Es gibt Kommentare zur Bestellung
                            dt_data.Rows(i).Item("txtBemerkung") = foundRows2(0).Item("Kommentar1")
                            dt_data.Rows(i).Item("txtZusatzfeld") = foundRows2(0).Item("Kommentar2")
                        End If
                    End If
                End If

                'Zeile initialisieren - es darf keine NULL Felder geben
                For j = 0 To dt_data.Columns.Count - 1
                    If String.IsNullOrEmpty(dt_data.Rows(i).Item(j).ToString) Then
                        If dt_data.Columns(j).DataType = GetType(Double) OrElse dt_data.Columns(j).DataType = GetType(Integer) OrElse dt_data.Columns(j).DataType = GetType(Int64) OrElse dt_data.Columns(j).DataType = GetType(Int16) OrElse dt_data.Columns(j).DataType = GetType(Int32) Then
                            dt_data.Rows(i).Item(j) = 0
                        ElseIf dt_data.Columns(j).DataType = GetType(Boolean) Then
                            dt_data.Rows(i).Item(j) = False
                        ElseIf dt_data.Columns(j).DataType = GetType(Date) Then
                            dt_data.Rows(i).Item(j) = Now
                        Else 'String
                            dt_data.Rows(i).Item(j) = ""
                        End If
                    End If
                Next
            Next



#End Region

#Region "publish DataTable"
            If boolPublish = True Then
                pbl_dt.Clear()
                pbl_dt = dt_data
            End If
#End Region

#End Region 'Fill DataTable
            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblData: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_Data_KstBednr(qryArbeitspaket As Long, qryVersion As Long, qryEntwurfstyp As String) As tableData() 'YYY
        Console.WriteLine("FUNC: Function Get_Data_KstBednr")

        Dim qry As String
        Dim i As Integer
        Dim listData As tableData()
        Dim currentArbeitspaket As String, IndexProjekt As Long = -1

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            Dim dtAllAp As New System.Data.DataTable
            dtAllAp = Get_tblAlleArbeitspakete(pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer"), pbl_dtAuth, True) 'Alle möglichen Arbeitspakete lesen

            qry = "SELECT *" _
        & " from tblData" _
        & " where qryArbeitspaket = " & qryArbeitspaket & " And qryVersion = " & qryVersion & " And qryEntwurfstyp = '" & qryEntwurfstyp & "' and txtBedarfsnummer is not null" _
        & " order by ID_DATA asc" 'So wie die Daten erfasst wurden, werden Sie auch wieder ausgegeben, sprich Entwicklungsumfänge bleiben zusammen

            Dim cmd As New OleDb.OleDbCommand(qry, con)
            Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

            i = 0
            Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
                ReDim Preserve listData(i) 'Arraylänge anpassen
                If Not IsNothing(reader("ID_DATA")) Then
                    listData(i).ID_DATA = CInt(reader("ID_DATA").ToString)
                End If
                If Not IsNothing(reader("txtBeschreibung")) Then
                    listData(i).txtBeschreibung = reader("txtBeschreibung").ToString
                End If
                If Not IsNothing(reader("txtBemerkung")) Then
                    listData(i).txtBemerkung = reader("txtBemerkung").ToString
                End If
                If Not IsNothing(reader("txtZusatzfeld")) Then
                    listData(i).txtZusatzfeld = reader("txtZusatzfeld").ToString
                End If
                If Not IsNothing(reader("txtBedarfsnummer")) Then
                    listData(i).txtBedarfsnummer = reader("txtBedarfsnummer").ToString
                End If
                If Not IsNothing(reader("qryGeschäftsjahr")) Then
                    listData(i).qryGeschäftsjahr = CInt(reader("qryGeschäftsjahr").ToString)
                End If
                If Not IsNothing(reader("curKosten_pro_Einheit")) Then
                    listData(i).curKosten_pro_Einheit = CDbl(reader("curKosten_pro_Einheit").ToString)
                End If
                If Not IsNothing(reader("boolRenneinsatz")) Then
                    listData(i).boolRenneinsatz = CBool(reader("boolRenneinsatz").ToString)
                End If
                If Not IsNothing(reader("datErstellungsdatum")) Then
                    listData(i).datErstellungsdatum = CDate(reader("datErstellungsdatum").ToString)
                End If
                If Not IsNothing(reader("dblJanuar")) Then
                    listData(i).dblJanuar = CDbl(reader("dblJanuar").ToString)
                End If
                If Not IsNothing(reader("dblFebruar")) Then
                    listData(i).dblFebruar = CDbl(reader("dblFebruar").ToString)
                End If
                If Not IsNothing(reader("dblMärz")) Then
                    listData(i).dblMärz = CDbl(reader("dblMärz").ToString)
                End If
                If Not IsNothing(reader("dblApril")) Then
                    listData(i).dblApril = CDbl(reader("dblApril").ToString)
                End If
                If Not IsNothing(reader("dblMai")) Then
                    listData(i).dblMai = CDbl(reader("dblMai").ToString)
                End If
                If Not IsNothing(reader("dblJuni")) Then
                    listData(i).dblJuni = CDbl(reader("dblJuni").ToString)
                End If
                If Not IsNothing(reader("dblJuli")) Then
                    listData(i).dblJuli = CDbl(reader("dblJuli").ToString)
                End If
                If Not IsNothing(reader("dblAugust")) Then
                    listData(i).dblAugust = CDbl(reader("dblAugust").ToString)
                End If
                If Not IsNothing(reader("dblSeptember")) Then
                    listData(i).dblSeptember = CDbl(reader("dblSeptember").ToString)
                End If
                If Not IsNothing(reader("dblOktober")) Then
                    listData(i).dblOktober = CDbl(reader("dblOktober").ToString)
                End If
                If Not IsNothing(reader("dblNovember")) Then
                    listData(i).dblNovember = CDbl(reader("dblNovember").ToString)
                End If
                If Not IsNothing(reader("dblDezember")) Then
                    listData(i).dblDezember = CDbl(reader("dblDezember").ToString)
                End If
                If Not IsNothing(reader("qryArbeitspaket")) Then
                    listData(i).qryArbeitspaket = reader("qryArbeitspaket").ToString
                End If
                If Not IsNothing(reader("qryVersion")) Then
                    listData(i).qryVersion = CInt(reader("qryVersion").ToString)
                End If
                If Not IsNothing(reader("qryKostenart")) Then
                    listData(i).txtKostenart = reader("qryKostenart").ToString
                End If
                If Not IsNothing(reader("qryEntwurfstyp")) Then
                    listData(i).qryEntwurfstyp = reader("qryEntwurfstyp").ToString
                End If
                If Not IsNothing(reader("qryKlassifizierung")) Then
                    listData(i).txtKlasse = reader("qryKlassifizierung").ToString
                End If
                If Not IsNothing(reader("qryEntwicklungspaket")) Then
                    listData(i).txtEntwicklungspaket = reader("qryEntwicklungspaket").ToString
                End If
                If Not IsNothing(reader("qryPaket")) Then
                    listData(i).txtPaket = reader("qryPaket").ToString
                End If
                If Not IsNothing(reader("boolObligo")) Then
                    listData(i).boolObligo = CBool(reader("boolObligo").ToString)
                End If
                If Not IsNothing(reader("txtKst_Lst")) Then
                    listData(i).txtKst_Lst = reader("txtKst_Lst").ToString
                End If
                If Not IsNothing(reader("lngSort")) Then
                    listData(i).lngSort = CInt(reader("lngSort").ToString)
                End If

                'Projekt ID
                Dim Projekt_ID As Long
                Try
                    If Not currentArbeitspaket = listData(i).qryArbeitspaket Then
                        currentArbeitspaket = listData(i).qryArbeitspaket

                        Dim foundRows() As Data.DataRow = dtAllAp.Select("ID_AP = " & currentArbeitspaket & "")
                        If Not IsNothing(foundRows) AndAlso foundRows.Count > 0 Then 'Es gibt mindestens einen Treffer
                            IndexProjekt = dtAllAp.Rows.IndexOf(foundRows(0))
                        Else
                            IndexProjekt = -1
                        End If

                        If Not IndexProjekt = -1 Then
                            listData(i).ID_PROJEKT = dtAllAp.Rows(IndexProjekt).Item("qryProjekt")
                            Projekt_ID = dtAllAp.Rows(IndexProjekt).Item("qryProjekt")
                        Else
                            Projekt_ID = 0
                        End If
                    Else
                        listData(i).ID_PROJEKT = Projekt_ID
                    End If
                Catch ex As Exception
                    MsgBox("Get_Data_KstBednr - Nr: " & i & " - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
                End Try

                i = i + 1
            Loop
            Get_Data_KstBednr = listData

            boolError = False
            Exit Function
        Catch ex As Exception
            MsgBox("Get_Data_KstBednr - Fehler: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Add_Data(qryEntwurfstyp As String, ID_AP As Long,
                      qryVersion As Long, qryGeschäftsjahr As Long, txtKostenart As String,
                      txtBeschreibung As String, txtBemerkung As String, txtZusatzfeld As String, curKosten_pro_Einheit As Double,
                      boolRenneinsatz As Boolean, datErstellungsdatum As Date, txtBedarfsnummer As String,
                      dblJanuar As Double, dblFebruar As Double, dblMärz As Double, dblApril As Double, dblMai As Double, dblJuni As Double,
                      dblJuli As Double, dblAugust As Double, dblSeptember As Double, dblOktober As Double, dblNovember As Double, dblDezember As Double, qryKlassifizierung As String,
                      qryEntwicklungspaket As String, qryPaket As String, boolObligo As Boolean, txtKst_Lst As String, lngSort As Long, Benutzer As String) As Integer
        Console.WriteLine("FUNC: Function Add_Data")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblData ([txtBeschreibung], [txtBemerkung], [txtZusatzfeld], [txtBedarfsnummer], qryGeschäftsjahr, [curKosten_pro_Einheit], [boolRenneinsatz], [datErstellungsdatum], " &
            "[dblJanuar], [dblFebruar], [dblMärz], [dblApril], [dblMai], [dblJuni], [dblJuli], [dblAugust], [dblSeptember], [dblOktober], [dblNovember], [dblDezember], " &
            "qryArbeitspaket, qryVersion, [qryKostenart], [qryEntwurfstyp], [qryKlassifizierung], [qryEntwicklungspaket], [qryPaket], [boolObligo], [txtKst_Lst], lngSort, [Benutzer]) " &
            "values ('" & txtBeschreibung & "', " &
        "'" & txtBemerkung & "', " &
        "'" & txtZusatzfeld & "', " &
        "'" & txtBedarfsnummer & "', " &
        qryGeschäftsjahr & ", " &
        "'" & Replace(curKosten_pro_Einheit, ",", ".") & "', " &
        "'" & Convert.ToInt32(boolRenneinsatz) & "', " &
        "'" & datErstellungsdatum.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        "'" & Replace(dblJanuar, ",", ".") & "', " &
        "'" & Replace(dblFebruar, ",", ".") & "', " &
        "'" & Replace(dblMärz, ",", ".") & "', " &
        "'" & Replace(dblApril, ",", ".") & "', " &
        "'" & Replace(dblMai, ",", ".") & "', " &
        "'" & Replace(dblJuni, ",", ".") & "', " &
        "'" & Replace(dblJuli, ",", ".") & "', " &
        "'" & Replace(dblAugust, ",", ".") & "', " &
        "'" & Replace(dblSeptember, ",", ".") & "', " &
        "'" & Replace(dblOktober, ",", ".") & "', " &
        "'" & Replace(dblNovember, ",", ".") & "', " &
        "'" & Replace(dblDezember, ",", ".") & "', " &
        ID_AP & ", " &
        qryVersion & ", " &
        "'" & txtKostenart & "', " &
        "'" & qryEntwurfstyp & "', " &
        "'" & qryKlassifizierung & "', " &
        "'" & qryEntwicklungspaket & "', " &
        "'" & qryPaket & "', " &
        "'" & Convert.ToInt32(boolObligo) & "', " &
        "'" & txtKst_Lst & "', " &
         lngSort & ", " &
         "'" & Benutzer & "'" &
        ")"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery() 'Datensatz speichern

            'Get ID of new added row
            statement = "select max(ID_DATA) from tblData"
            cmd.CommandText = statement
            Dim id As Integer = cmd.ExecuteScalar()
            Return id


            boolError = False
        Catch ex As Exception
            Return -1
            boolError = True
            MsgBox("Add_Data - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Delete_Defined_ID_DATA(ID_DATA As Long) As Boolean
        Console.WriteLine("FUNC: Function Delete_Defined_ID_DATA")

        Dim statement As String

#Region "1. Benutzer aktualisieren"
        Dim StrUser As String = pbl_dtUser.Rows(intCurrentUser).Item("txtPNummer") & " - " & pbl_dtUser.Rows(intCurrentUser).Item("txtName")
        If Update_User(ID_DATA, StrUser) = False Then Exit Function
#End Region

#Region "2. Datensatz in Historie Tabelle kopieren"
        If Clone_Defined_IdData(ID_DATA) = False Then Exit Function
#End Region

#Region "3. Datensatz löschen"
        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "delete from tblData where ID_DATA = " & ID_DATA & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_Defined_ID_DATA - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab
        End Try
#End Region
    End Function

    Function Clone_Defined_IdData(ID_DATA As Long) As Boolean
        Console.WriteLine("FUNC: Function Clone_Defined_IdData")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "INSERT INTO His_tblData SELECT * FROM tblData " &
            "where ID_DATA = " & ID_DATA & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Clone_Defined_IdData - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Function Update_User(ID_DATA As Integer, Benutzer As String) As Boolean
        Console.WriteLine("FUNC: Function Update_User")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblData set " &
        "Benutzer = '" & Benutzer & "', " &
        "datErstellungsdatum = '" & Now.ToString("yyyyMMdd HH:mm:ss") & "'" &   '!ab mod.
        " where ID_DATA = " & ID_DATA & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Data_Paket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Function Update_Data_Entwicklungspaket(ID_PROJEKT As Long, qryEntwicklungspaketNeu As String, qryEntwicklungspaketAlt As String) 'OK
        Console.WriteLine("FUNC: Function Update_Data_Entwicklungspaket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "UPDATE tblData Set tblData.qryEntwicklungspaket = '" & qryEntwicklungspaketNeu & "' 
            FROM(tblProjekt INNER JOIN tblArbeitspaket On tblProjekt.[ID_PROJEKT] = tblArbeitspaket.[qryProjekt]) INNER JOIN tblData On tblArbeitspaket.[ID_AP] = tblData.[qryArbeitspaket]" &
            "where tblProjekt.ID_PROJEKT = " & ID_PROJEKT & " and tblData.qryEntwicklungspaket = '" & qryEntwicklungspaketAlt & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Data_Entwicklungspaket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_Data_Paket(qryArbeitspaket As Long, txtPaketAlt As String, txtPaketNeu As String) 'OK
        Console.WriteLine("FUNC: Function Update_Data_Paket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "update tblData set " &
        "qryPaket = '" & txtPaketNeu & "' " &
        " where qryArbeitspaket = " & qryArbeitspaket & " and qryPaket = '" & txtPaketAlt & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Data_Paket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_Data_IdData(ID_DATA As Long, txtBeschreibung As String, txtBemerkung As String, txtZusatzfeld As String,
                                txtBedarfsnummer As String, curKosten_pro_Einheit As Double, boolRenneinsatz As Boolean,
                                dblJanuar As Double, dblFebruar As Double, dblMärz As Double, dblApril As Double, dblMai As Double,
                                dblJuni As Double, dblJuli As Double, dblAugust As Double, dblSeptember As Double, dblOktober As Double, dblNovember As Double,
                                dblDezember As Double, boolObligo As Boolean, qryKostenart As String, qryKlassifizierung As String,
                                qryEntwicklungspaket As String, qryPaket As String, txtKst_Lst As String, lngSort As Long, qryGeschäftsjahr As Long, Benutzer As String) As Boolean
        Console.WriteLine("FUNC: Function Update_Data_IdData")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten aktualisieren
        Try
            statement = "update tblData set " &
        "txtBeschreibung = '" & txtBeschreibung & "', " &
        "txtBemerkung = '" & txtBemerkung & "', " &
            "txtZusatzfeld = '" & txtZusatzfeld & "', " &
            "txtBedarfsnummer = '" & txtBedarfsnummer & "', " &
            "curKosten_pro_Einheit = '" & Replace(curKosten_pro_Einheit, ",", ".") & "', " &
            "boolRenneinsatz = " & Convert.ToInt32(boolRenneinsatz) & ", " &
            "datErstellungsdatum = '" & Now.ToString("yyyyMMdd HH:mm:ss") & "', " & '!ab mod.
            "dblJanuar = '" & Replace(dblJanuar, ",", ".") & "', " &
            "dblFebruar = '" & Replace(dblFebruar, ",", ".") & "', " &
            "dblMärz = '" & Replace(dblMärz, ",", ".") & "', " &
            "dblApril = '" & Replace(dblApril, ",", ".") & "', " &
            "dblMai = '" & Replace(dblMai, ",", ".") & "', " &
            "dblJuni = '" & Replace(dblJuni, ",", ".") & "', " &
            "dblJuli = '" & Replace(dblJuli, ",", ".") & "', " &
            "dblAugust = '" & Replace(dblAugust, ",", ".") & "', " &
            "dblSeptember = '" & Replace(dblSeptember, ",", ".") & "', " &
            "dblOktober = '" & Replace(dblOktober, ",", ".") & "', " &
            "dblNovember = '" & Replace(dblNovember, ",", ".") & "', " &
            "dblDezember = '" & Replace(dblDezember, ",", ".") & "', " &
            "boolObligo = " & Convert.ToInt32(boolObligo) & ", " &
            "qryKostenart = '" & qryKostenart & "', " &
            "qryKlassifizierung = '" & qryKlassifizierung & "', " &
            "qryEntwicklungspaket = '" & qryEntwicklungspaket & "', " &
            "qryPaket = '" & qryPaket & "', " &
            "txtKst_Lst = '" & txtKst_Lst & "', " &
            "lngSort = " & lngSort & ", " &
            "qryGeschäftsjahr = " & qryGeschäftsjahr & ", " &
            "Benutzer = '" & Benutzer & "' " &
            "where ID_DATA = " & ID_DATA & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Data_IdData - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Function Update_Data_IdData_Double(ID_DATA As Long, FieldName As String, Value As Double, Benutzer As String) As Boolean
        Console.WriteLine("FUNC: Function Update_Data_IdData_Double")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Aktualisiert nur den Wert eines bestimmten Monats einer Daten ID
        Try
            statement = "update tblData set " &
            FieldName & " = '" & Replace(Value, ",", ".") & "', " &
            "Benutzer = '" & Benutzer & "'" &
            " where ID_DATA = " & ID_DATA & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Data_IdData - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function
#End Region

#Region "Paket"
    Function Get_Paket(boolPublish As Boolean, ID_AP As Long) As tablePaket() 'OK
        Console.WriteLine("FUNC: Function Get_Paket")

        Dim qry As String
        Dim i As Integer
        Dim listPaket As tablePaket()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblPaket" _
        & " where qryArbeitspaket = " & ID_AP & "" _
        & " order by datErstellungsdatum asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listPaket(i) 'Arraylänge anpassen
            listPaket(i).txtPaket = reader("txtPaket").ToString
            listPaket(i).txtBeschreibung = reader("txtBeschreibung").ToString
            listPaket(i).qryArbeitspaket = CInt(reader("qryArbeitspaket").ToString)
            listPaket(i).datErstellungsdatum = CDate(reader("datErstellungsdatum").ToString)
            i = i + 1
        Loop
        Get_Paket = listPaket

        If boolPublish = True Then
            Erase pbl_Paket
            pbl_Paket = listPaket
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function

    Function Add_Paket(txtPaket As String, txtBeschreibung As String, qryArbeitspaket As Long) 'OK
        Console.WriteLine("FUNC: Function Add_Paket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblPaket ([txtPaket], [txtBeschreibung], qryArbeitspaket, [datErstellungsdatum]) " &
            "values ('" & txtPaket & "', " &
        "'" & txtBeschreibung & "', " &
        "" & qryArbeitspaket & ", " &
        "'" & Now.ToString("yyyyMMdd HH:mm:ss") & "' " &    '!ab mod.
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Paket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Update_Paket(txtPaketAlt As String, txtPaketNeu As String, txtBeschreibung As String, qryArbeitspaket As Long) 'OK
        Console.WriteLine("FUNC: Function Update_Paket")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "update tblPaket set " &
        "txtPaket = '" & txtPaketNeu & "', " &
        "txtBeschreibung = '" & txtBeschreibung & "' " &
        "where qryArbeitspaket = " & qryArbeitspaket & " and txtPaket = '" & txtPaketAlt & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Update_Paket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function

    Function Delete_Paket(ID_AP As Integer) As Boolean
        If con.State = ConnectionState.Closed Then Exit Function

        Dim statement As String
        Try
            statement = "delete from tblPaket where qryArbeitspaket = " & ID_AP & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("Delete_Paket - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False    '!ab
        End Try
    End Function
#End Region

#Region "Sachkonto"
    Function Get_tblSachkonto(boolPublish As Boolean) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblSachkonto")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from tblSachkonto order by Sakto asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

#Region "publish DataTable"
            If boolPublish = True Then
                pbl_dtSakto.Clear()
                pbl_dtSakto = dt_data
            End If
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblSachkonto: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_Sachkonto(boolPublish As Boolean) As tableSachkonto() 'OK
        Console.WriteLine("FUNC: Function Get_Sachkonto")

        Dim qry As String
        Dim i As Integer
        Dim listSachkonto As tableSachkonto()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblSachkonto" _
        & " order by Sakto asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listSachkonto(i) 'Arraylänge anpassen
            listSachkonto(i).Sakto = reader("Sakto").ToString
            listSachkonto(i).txtSachkonto = reader("txtSachkonto").ToString
            listSachkonto(i).FEHLER = CBool(reader("FEHLER").ToString)
            listSachkonto(i).qryKostenart = reader("qryKostenart").ToString
            i = i + 1
        Loop
        Get_Sachkonto = listSachkonto

        If boolPublish = True Then
            Erase pbl_Sakto
            pbl_Sakto = listSachkonto
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function 'OLD

    Function Add_Sachkonto(Sakto As String, txtSachkonto As String, qryKostenart As String, FEHLER As Boolean) 'OK
        Console.WriteLine("FUNC: Function Add_Sachkonto")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblSachkonto ([Sakto], [txtSachkonto], [qryKostenart], [FEHLER]) " &
            "values ('" & Sakto & "', " &
            "'" & txtSachkonto & "', " &
        "'" & qryKostenart & "', " &
        "" & Convert.ToInt32(FEHLER) & "" &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Sachkonto - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function
#End Region

#Region "IVP"
    Function Get_tblEkant(boolPublish As Boolean) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblEkant")

        Dim qry As String
        Dim i As Integer

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from tblEkant order by Std_Lst asc, Abteilung asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

#Region "publish DataTable"
            If boolPublish = True Then
                pbl_dtEkant.Clear()
                pbl_dtEkant = dt_data
            End If
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblEkant: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function

    Function Get_Ekant(boolPublish As Boolean) As tableEkant() 'OK
        Console.WriteLine("FUNC: Function Get_Ekant")

        Dim qry As String
        Dim i As Integer
        Dim listEkant As tableEkant()

        If con.State = ConnectionState.Closed Then Exit Function

        On Error GoTo fehler
        qry = "SELECT *" _
        & " from tblEkant" _
        & " order by Std_Lst asc, Abteilung asc"
        Dim cmd As New OleDb.OleDbCommand(qry, con)
        Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader

        i = 0
        On Error Resume Next
        Do While reader.Read 'Speichert alle Daten der Datenbank in ein definiertes Array
            ReDim Preserve listEkant(i) 'Arraylänge anpassen
            listEkant(i).Std_Lst = reader("Std_Lst").ToString
            listEkant(i).Abteilung = reader("Abteilung").ToString
            listEkant(i).Kostenstelle = reader("Kostenstelle").ToString
            listEkant(i).Kst_Schlüssel = reader("Kst_Schlüssel").ToString
            listEkant(i).Bezeichnung = reader("Bezeichnung").ToString
            listEkant(i).Std_Satz = CDbl(reader("Std_Satz").ToString)
            i = i + 1
        Loop
        Get_Ekant = listEkant

        If boolPublish = True Then
            Erase pbl_Ekant
            pbl_Ekant = listEkant
        End If

        boolError = False
        Exit Function
fehler:
        boolError = True
    End Function
#End Region

#Region "Ist Data"
    Function Delete_Ist_Data(ID_AP As Long, GJ As Long) 'OK
        Console.WriteLine("FUNC: Function Delete_Ist_Data")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "delete from tblIstData where qryArbeitspaket = " & ID_AP & " and qryGeschäftsjahr = " & GJ & ""
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch
            boolError = True
        End Try
    End Function

    Function Add_IstData(qryEntwurfstyp As String, ID_AP As Long,
                      qryVersion As Long, qryGeschäftsjahr As Long, txtKostenart As String,
                      txtBeschreibung As String, txtBemerkung As String, txtZusatzfeld As String, curKosten_pro_Einheit As Double,
                      boolRenneinsatz As Boolean, datErstellungsdatum As Date, txtBedarfsnummer As String,
                      dblJanuar As Double, dblFebruar As Double, dblMärz As Double, dblApril As Double, dblMai As Double, dblJuni As Double,
                      dblJuli As Double, dblAugust As Double, dblSeptember As Double, dblOktober As Double, dblNovember As Double, dblDezember As Double, qryKlassifizierung As String,
                      qryEntwicklungspaket As String, qryPaket As String, boolObligo As Boolean, txtKst_Lst As String, lngSort As Long, IdSap As String) 'OK
        Console.WriteLine("FUNC: Function Add_IstData")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblIstData ([txtBeschreibung], [txtBemerkung], [txtZusatzfeld], [txtBedarfsnummer], qryGeschäftsjahr, [curKosten_pro_Einheit], [boolRenneinsatz], datErstellungsdatum, " &
            "[dblJanuar], [dblFebruar], [dblMärz], [dblApril], [dblMai], [dblJuni], [dblJuli], [dblAugust], [dblSeptember], [dblOktober], [dblNovember], [dblDezember], " &
            "qryArbeitspaket, qryVersion, [qryKostenart], [qryEntwurfstyp], [qryKlassifizierung], [qryEntwicklungspaket], [qryPaket], [boolObligo], [txtKst_Lst], lngSort, [IdSap]) " &
            "values ('" & txtBeschreibung & "', " &
        "'" & txtBemerkung & "', " &
        "'" & txtZusatzfeld & "', " &
        "'" & txtBedarfsnummer & "', " &
        qryGeschäftsjahr & ", " &
        "'" & Replace(curKosten_pro_Einheit, ",", ".") & "', " &
        Convert.ToInt32(boolRenneinsatz) & ", " &
        "'" & datErstellungsdatum.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        "'" & Replace(dblJanuar, ",", ".") & "', " &
        "'" & Replace(dblFebruar, ",", ".") & "', " &
        "'" & Replace(dblMärz, ",", ".") & "', " &
        "'" & Replace(dblApril, ",", ".") & "', " &
        "'" & Replace(dblMai, ",", ".") & "', " &
        "'" & Replace(dblJuni, ",", ".") & "', " &
        "'" & Replace(dblJuli, ",", ".") & "', " &
        "'" & Replace(dblAugust, ",", ".") & "', " &
        "'" & Replace(dblSeptember, ",", ".") & "', " &
        "'" & Replace(dblOktober, ",", ".") & "', " &
        "'" & Replace(dblNovember, ",", ".") & "', " &
        "'" & Replace(dblDezember, ",", ".") & "', " &
        ID_AP & ", " &
        qryVersion & ", " &
        "'" & txtKostenart & "', " &
        "'" & qryEntwurfstyp & "', " &
        "'" & qryKlassifizierung & "', " &
        "'" & qryEntwicklungspaket & "', " &
        "'" & qryPaket & "', " &
        "" & Convert.ToInt32(boolObligo) & ", " &
        "'" & txtKst_Lst & "', " &
        "" & lngSort & ", " &
        "'" & IdSap & "'" &
        ")"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_IstData - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function
#End Region

#Region "Log"
    Function Add_Log(LogDate As Date, User As String, Nachname As String, Vorname As String, LogType As String) 'OK
        Console.WriteLine("FUNC: Function Add_Log")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        'Daten schreiben
        Try
            statement = "insert into tblUserLog (LogDate, [User], [Nachname], [Vorname], [LogType]) " &
            "values ('" & LogDate.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        "'" & User & "', " &
        "'" & Nachname & "', " &
        "'" & Vorname & "', " &
        "'" & LogType & "' " &
        ")"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("Add_Log - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
        End Try
    End Function
#End Region

#Region "Terminverfolgung"
    Function Get_tblSapComments(PSP As String) As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblSapComments")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from tblSapComments where PSP = '" & PSP & "' order by ID asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblTerminverfolgung: " & ex.Message.ToString, vbCritical, "Fehler")
        End Try
    End Function

    Function Get_ID(SapId As String) As Integer
        Console.WriteLine("FUNC: Function Get_ID")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from tblSapComments where SapId = '" & SapId & "' order by ID asc"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region
            If Not dt_data.Rows.Count = 0 Then
                Return dt_data.Rows(0).Item("ID")
            End If
        Catch ex As Exception
            MsgBox("Get_tblTerminverfolgung: " & ex.Message.ToString, vbCritical, "Fehler")
            Return 0
        End Try
    End Function

    Function Add_CommentRow(Kommentar1 As String, Kommentar2 As String, Kommentar3 As String, SapId As String, Benutzer As String, PSP As String) As Boolean
        Console.WriteLine("FUNC: Function Add_CommentRow")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "insert into tblSapComments ([Kommentar1], [Kommentar2], [Kommentar3], [SapId], [Benutzer], [Änderungsdatum], [PSP]) " &
            "values ('" & Kommentar1 & "', " &
        "'" & Kommentar2 & "', " &
        "'" & Kommentar3 & "', " &
        "'" & SapId & "', " &
        "'" & Benutzer & "', " &
        "'" & Now.ToString("yyyyMMdd HH:mm:ss") & "', " &   '!ab mod.
        "'" & PSP & "')"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox("Add_RowTerminverfolgung: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Function Update_CommentRow(Kommentar1 As String, Kommentar2 As String, Kommentar3 As String, Benutzer As String, PSP As String, ID As Integer) As Boolean
        Console.WriteLine("FUNC: Function Update_CommentRow")

        Dim statement As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
            statement = "update tblSapComments set " &
        "Kommentar1 = '" & Kommentar1 & "', " &
        "Kommentar2 = '" & Kommentar2 & "', " &
        "Kommentar3 = '" & Kommentar3 & "', " &
        "Benutzer = '" & Benutzer & "', " &
        "Änderungsdatum = '" & Now.ToString & "', " &
        "PSP = '" & PSP & "' " &
        "where ID = " & ID & ""

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox("Update_Terminverfolung: " & ex.Message.ToString, vbCritical, "Fehler beim Speichern der Daten auf der Datenbank!")
            Return False
        End Try
    End Function
#End Region

#Region "myBudget Version"
    Function Get_tblmyBudetVersion() As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblmyBudetVersion")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from myBudget_Version"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblmyBudetVersion: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function
#End Region

#Region "EM Personal"
    Function Get_tblEM_Personal() As System.Data.DataTable
        Console.WriteLine("FUNC: Function Get_tblEM_Personal")

        Dim qry As String

        If con.State = ConnectionState.Closed Then Exit Function

        Try
#Region "Start Command"
            qry = "SELECT * from EM_Personal"
            Dim cmd As New OleDb.OleDbCommand(qry, con)
#End Region

#Region "Fill Datatable"
            Dim dt_data As New System.Data.DataTable
            Dim ReadData As OleDb.OleDbDataReader = cmd.ExecuteReader
            dt_data.Load(ReadData) 'Läd die Daten von der Datenbank (SQL Server) in eine DataTable
#End Region

            boolError = False
            Return dt_data
        Catch ex As Exception
            MsgBox("Get_tblEM_Personal: " & ex.Message.ToString, vbCritical, "Fehler")
            boolError = True
        End Try
    End Function
#End Region

#Region "UserNotification"
    Public Function AddUserNotification() As Boolean
        Console.WriteLine("FUNC: Function AddUserNotification")
        If con.State = ConnectionState.Closed Then Return False

        Dim pNummer As String = GetPNummerOfCurrentUser(intCurrentUser)
        Dim entwurfstyp As String = GetEntwurfstypFromIndex(pbl_IndexEntwurf)
        If String.IsNullOrEmpty(pNummer) Or String.IsNullOrWhiteSpace(entwurfstyp) Then Return False

        Dim statement As String
        Try
            statement = "insert into tblUserAP (qryPNummer, qryID_AP, qryEntwurfstyp) " &
            "values ('" & pNummer & "', " & "'" & pbl_IdAp & "', " & "'" & entwurfstyp & "')"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("AddUserNotification - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Eintragen der Daten in der Datenbank!")
            Return False
        End Try
    End Function

    Public Function SetCurrentUserEmailAddress(emailAddr As String) As Boolean
        Console.WriteLine("FUNC: Function SetCurrentUserEmailAddress")
        If con.State = ConnectionState.Closed Then Return False
        Dim pNummer As String = GetPNummerOfCurrentUser(intCurrentUser)
        Dim statement As String

        Try
            statement = "update tblUser set " &
            "txtEmail = '" & emailAddr & "' " &
            "where txtPNummer = '" & pNummer & "'"

            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            pbl_dtUser(intCurrentUser).Item("txtEmail") = emailAddr ' update current userTable
            boolError = False
            Return True

        Catch ex As Exception
            boolError = True
            MsgBox("SetCurrentUserEMailAddress - Fehler: Entwurfstyp " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten in der Tabelle!")
            Return False
        End Try

    End Function

    Public Function FoundInNotifications() As Boolean
        Console.WriteLine("FUNC: Function FoundInNotifications")
        If con.State = ConnectionState.Closed Then Return False

        Dim pNummer As String = GetPNummerOfCurrentUser(intCurrentUser)
        Dim entwurfstyp As String = GetEntwurfstypFromIndex(pbl_IndexEntwurf)
        If String.IsNullOrEmpty(pNummer) Or String.IsNullOrWhiteSpace(entwurfstyp) Then Return False

        Dim statement As String
        Try
            statement = "Select * from tblUserAP where qryPNummer = '" & pNummer & "' AND qryID_AP = '" & pbl_IdAp & "'" & " AND qryEntwurfstyp ='" & entwurfstyp & "'"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader()
            Dim found As Boolean = False
            Do While reader.Read
                found = True
            Loop
            boolError = False
            Return found
        Catch ex As Exception
            boolError = True
            MsgBox("FoundInNotifications - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Public Function GetEMailListOfCurrentArbeitspaketAndTyp() As String()
        Dim emailList() As String = Nothing
        Console.WriteLine("FUNC: Function GetEMailListOfCurrentArbeitspaketAndTyp")
        If con.State = ConnectionState.Closed Then Return Nothing

        Dim entwurfstyp As String = GetEntwurfstypFromIndex(pbl_IndexEntwurf)
        If String.IsNullOrWhiteSpace(entwurfstyp) Then Return Nothing
        Dim statement As String

        Try
            statement = "Select txtEmail from tblUser as us, tblUserAP as usAp where us.txtPNummer = usAp.qryPNummer And usAp.qryID_AP ='" & pbl_IdAp & "'" & " AND usAp.qryEntwurfstyp ='" & entwurfstyp & "'"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            Dim reader As OleDb.OleDbDataReader = cmd.ExecuteReader()
            Dim i As Integer = 0
            Do While reader.Read
                ReDim Preserve emailList(i) 'Arraylänge anpassen
                emailList(i) = reader("txtEmail").ToString
                i = i + 1
            Loop
            boolError = False
            Return emailList
        Catch ex As Exception
            boolError = True
            MsgBox("GetEMailListOfCurrentArbeitspakerAndType - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten auf der Datenbank!")
            Return Nothing
        End Try

    End Function

    Public Function RemoveUserNotification() As Boolean
        Console.WriteLine("FUNC: Function RemoveUserNotification")
        If con.State = ConnectionState.Closed Then Return False

        Dim pNummer As String = GetPNummerOfCurrentUser(intCurrentUser)
        Dim entwurfstyp As String = GetEntwurfstypFromIndex(pbl_IndexEntwurf)
        If String.IsNullOrEmpty(pNummer) Or String.IsNullOrWhiteSpace(entwurfstyp) Then Return False

        Dim statement As String
        Try
            statement = "delete from tblUserAP where qryPNummer = '" & pNummer & "' AND qryID_AP = '" & pbl_IdAp & "'" & " AND qryEntwurfstyp ='" & entwurfstyp & "'"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("RemoveUserNotification - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Löschen der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    ' Löschen aller Notifications des aktuellen users
    Public Function RemoveUserFromNotifications() As Boolean
        Console.WriteLine("FUNC: Function RemoveUserFromNotifications")
        If con.State = ConnectionState.Closed Then Return False

        Dim pNummer As String = GetPNummerOfCurrentUser(intCurrentUser)
        If String.IsNullOrEmpty(pNummer) Then Return False

        Dim statement As String
        Try
            statement = "delete from tblUserAP where qryPNummer = '" & pNummer & "'"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("RemoveUserFromNotifications - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Löschen der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Public Function RemoveAPFromNotifications(ID_AP As Integer) As Boolean
        Console.WriteLine("FUNC: Function RemoveAPFromNotifications")
        If con.State = ConnectionState.Closed Then Return False

        Dim statement As String
        Try
            statement = "delete from tblUserAP where qryId_AP = '" & ID_AP & " '"
            Dim cmd As New OleDb.OleDbCommand(statement, con)
            cmd.ExecuteNonQuery()
            boolError = False
            Return True
        Catch ex As Exception
            boolError = True
            MsgBox("RemoveAPFromNotifications - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Löschen der Daten auf der Datenbank!")
            Return False
        End Try
    End Function

    Public Function GetPNummerOfCurrentUser(index As Integer) As String
        Console.WriteLine("FUNC: Function GetPNummerOfCurrentUser")
        Dim userId As String = ""

        Try
            userId = pbl_dtUser(index).Item("txtPNummer")
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("GetPNummerOfCurrentUser - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten in der Tabelle!")
        End Try

        Return userId
    End Function

    Public Function GetEmailAddressOfCurrentUser(index As Integer) As String
        Console.WriteLine("FUNC: Function GetEmailAddressOfCurrentUser")
        Dim emailAddr As String = Nothing

        Try
            'emailAddr = pbl_dtUser(index).Item("txtEmail") !!ab todo remove
            emailAddr = If(IsDBNull(pbl_dtUser(index).Item("txtEmail")), String.Empty, pbl_dtUser(index).Item("txtEmail").ToString())
            boolError = False
        Catch ex As Exception
            boolError = True
            MsgBox("GetEmailAddressOfCurrentUser - Fehler: " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten in der Tabelle!")
        End Try

        Return emailAddr
    End Function

    Public Function GetEntwurfstypFromIndex(index As Integer) As String
        Console.WriteLine("FUNC: Function GetEntwurfstypFromIndex")
        Dim entwurfstyp As String = ""
        boolError = False

        Try
            entwurfstyp = pbl_Entwurf(index).txtEntwurfstyp
        Catch ex As Exception
            boolError = True
            MsgBox("GetEntwurfstypFromIndex - Fehler: Entwurfstyp " & ex.Message.ToString, vbCritical, "Fehler beim Suchen der Daten in der Tabelle!")
        End Try

        Return entwurfstyp
    End Function
#End Region

End Module
